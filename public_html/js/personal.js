var configtable=
    {
        "language":{
            "processing":"Procesando información...",
            "zeroRecords":"No hay información disponible",
            "paginate":{
                "first":"Primera",
                "previous":"Anterior",
                "last":"Ultima",
                "next":"Siguiente"
            },
            "info":"_START_ a _END_ de _TOTAL_ Registros",
            "previous":"Pagina anterior",
            "search":"Buscar en la tabla _INPUT_ ",
            "infoEmpty":"No hay registros",
            // "infoFiltered":"(filtrado de _MAX_ registros totales)",
            "infoFiltered":"que cumplan el filtro",
            "lengthMenu":"Mostrar _MENU_ filas",
            "buttons":{
                "copyTitle":"Copiado al portapapeles",
                "copySuccess":{
                    "_":" %d Registros",
                    "1":" 1 Registro"}
            }
        },
        // "ajax":{
        //     "type":"GET",
        //     "url":"",
        //     "data":{}
        // },
        "data":'',
        "columns":'',
        "dom":"<'top'Bf>rt<'bottom'lip><'clear'>",
        "buttons":[
            {
                text: 'Copiar',
                extend: "copy",
                className: "btn-sm",
                "exportOptions":{
                    "columns":":visible"
                }
            },
            {
                text: 'CSV',
                extend: "csv",
                className: "btn-sm",
                "exportOptions":{
                    "columns":":visible"
                }
            },
            {
                text: 'Excel',
                extend: "excel",
                className: "btn-sm",
                "exportOptions":{
                    "columns":":visible"
                }
            },
            {
                text: 'PDF',
                extend: "pdfHtml5",
                className: "btn-sm",
                "exportOptions":{
                    "columns":":visible"
                }
            },
            {
                text: 'Imprimir',
                extend: "print",
                className: "btn-sm",
                "exportOptions":{
                    "columns":":visible"
                }
            },
        ],
        'responsive': true
    };
var nowdate =moment().format("YYYY-MM-DD");
var configdatepicker ={
    format: "yyyy-mm-dd",
    language: "es",
    keyboardNavigation: false,
    todayHighlight: true
};
var table='';
var jqxhr;

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

Array.prototype.count_value = function(){
    var count = {};
    for(var i = 0; i < this.length; i++){
        if(!(this[i] in count))count[this[i]] = 0;
        count[this[i]]++;
    }
    return count;
};
Array.prototype.repetidos = function(){
    var count = {};
    for(var i = 0; i < this.length; i++){
        if(!(this[i] in count))count[this[i]] = 0;
        count[this[i]]++;
    }
    for (var key in count) {
        // check if the property/key is defined in the object itself, not in parent
        if (count.hasOwnProperty(key)) {
            if (count[key]>1){
                return false;
            }
        }
    }
    return true;

};

function ayudaicon(form){
    $('#'+form+' :input').each(function(index,obj) {
        id=$(obj).attr('id');
        type=$(obj).attr('type');
        ayu=$(obj).attr('data-ayuda');
        if(type != 'hidden'){
            if(ayu != 'off'){
                campoayuda='<b id="ayu_'+id+'" class="ic_ayuda" style="display: block;"></b>';
                $(obj).parent().parent().prepend(campoayuda);
            }
        }
        $(".ic_ayuda").hide();
        $(obj).on("focus", function (e) {
            $(".ic_ayuda").hide();
            $(this).parent().parent().find(".ic_ayuda").show();
        });
    });
    $(".ic_ayuda").hide();
}

function validarfrm(form){
    var resp= true;
    var campo;
    var campos=[];
    $('#'+form+' :input').each(function(index,obj){
        campo = $(this).attr('id');
        var temp={
            'resp':true,
            'campo':campo,
            'mensaje': ''
        };
        temp=valida(obj,temp);
        if(!temp.resp){
            resp=false;
        }
        campos.push(temp);
    });
    $.each(campos, function( index, value ) {
        if (value.campo!= undefined){
            agregaerror(value);
        }
    });
    return resp;
}
function agregaerror(objeto){
    if(objeto != undefined){
        if(objeto.resp){
            $('#'+objeto.campo).parent().parent().removeClass("has-error");
            $('#'+objeto.campo).parent().children("span").remove();
        }else{
            if ($('#'+objeto.campo).parent().parent().attr("class").indexOf('has-error')== -1) {
                $('#'+objeto.campo).parent().parent().addClass("has-error");
                $('#'+objeto.campo).parent().append('<span class="help-block">'+objeto.mensaje+'</span>');
            }else{
                $('#'+objeto.campo).parent().children(".help-block").effect( "shake", "slow" );
                $('#'+objeto.campo).parent().children(".help-block").html(objeto.mensaje);
            }
        }
    }
}

function agregaerrorchecktext(objeto){
    if(objeto != undefined){
        if(objeto.resp){
            $('#'+objeto.campo).parent().parent().parent().parent().parent().removeClass("has-error");
            $('#'+objeto.campo).parent().parent().children("span").remove();
        }else{
            if ($('#'+objeto.campo).parent().parent().parent().parent().parent().attr("class").indexOf('has-error')== -1) {
                $('#'+objeto.campo).parent().parent().parent().parent().parent().addClass("has-error");
                $('#'+objeto.campo).parent().parent().append('<span class="help-block">'+objeto.mensaje+'</span>');
            }else{
                $('#'+objeto.campo).parent().parent().children(".help-block").effect( "shake", "slow" );
                $('#'+objeto.campo).parent().parent().children(".help-block").html(objeto.mensaje);
            }
        }
    }
}

function validacampo(objeto){
    if(objeto != undefined){
        if ($('#'+objeto).parent().parent().attr("class").indexOf('has-error')== -1) {
            return true
        }else{
            return false
        }
    }
}

function valida(obj,temp){
    if(!$(obj).attr('omit')){
        var val = $(obj).val().trim();
        if($(obj).attr('type')=='text'){
            $(obj).val(val);
        }
        var patron = $(obj).attr('pattern');
        var Ncampo = $(obj).attr('ref');
        var regla = $(obj).attr('required');
        var msjpatr = $(obj).attr('title');
        var tipe = $(obj).attr('data-conekta');
        if (regla != undefined){
            if(patron!= undefined){
                if(!val.match(patron)){
                    if (val==""){
                        temp.mensaje ='Debe llenar el campo';
                    }else{
                        temp.mensaje = msjpatr+' en el campo';
                    }
                    temp.resp=false;
                }
            }else{
                if (val==""){
                    temp.mensaje ='Debe llenar el campo';
                    temp.resp=false;
                    if (tipe=='card[number]'){
                        $('#visaimg').hide('slow');
                        $('#masterimg').hide('slow');
                        $('#americanimg').hide('slow');
                    }
                    if(tipe=='card[number]'){
                        var campotemp = $('input[data-conekta="card[cvc]"]').attr('id');
                        if($('#'+campotemp).val().trim() != ''){
                            var objtemp={
                                'resp':true,
                                'campo':campotemp,
                                'mensaje': ''
                            };
                            if (cardtype != undefined){
                                if(!$.payment.validateCardCVC($('#'+campotemp).val().trim(),cardtype)){
                                    objtemp.mensaje ='Numero de seguridad invalido';
                                    objtemp.resp=false;
                                    agregaerror(objtemp);
                                }else{
                                    agregaerror(objtemp);
                                }
                            }else {
                                objtemp.mensaje ='Verifica tu numero de tarjeta';
                                objtemp.resp=false;
                                agregaerror(objtemp);
                            }

                        }
                    }
                }else{
                    switch (tipe){
                        case 'card[number]':
                            if($.payment.validateCardNumber(val)){
                                var cardtype=$.payment.cardType(val);
                                switch(cardtype) {
                                    case 'visa':
                                        $('#visaimg').show('slow');
                                        $('#masterimg').hide('slow');
                                        $('#americanimg').hide('slow');
                                        break;
                                    case 'mastercard':
                                        $('#masterimg').show('slow');
                                        $('#visaimg').hide('slow');
                                        $('#americanimg').hide('slow');
                                        break;
                                    case 'amex':
                                        $('#americanimg').show('slow');
                                        $('#visaimg').hide('slow');
                                        $('#masterimg').hide('slow');
                                        break;
                                    default:
                                        $('#visaimg').hide('slow');
                                        $('#masterimg').hide('slow');
                                        $('#americanimg').hide('slow');
                                }
                            }else{
                                temp.mensaje ='El número de tarjeta no es válido';
                                temp.resp=false;
                                $('#visaimg').hide('slow');
                                $('#masterimg').hide('slow');
                                $('#americanimg').hide('slow');
                            }
                            var campotemp = $('input[data-conekta="card[cvc]"]').attr('id');
                            if($('#'+campotemp).val().trim() != ''){
                                var objtemp={
                                    'resp':true,
                                    'campo':campotemp,
                                    'mensaje': ''
                                };
                                if (cardtype != undefined){
                                    if(!$.payment.validateCardCVC($('#'+campotemp).val().trim(),cardtype)){
                                        objtemp.mensaje ='Numero de seguridad invalido';
                                        objtemp.resp=false;
                                        agregaerror(objtemp);
                                    }else{
                                        agregaerror(objtemp);
                                    }
                                }else {
                                    objtemp.mensaje ='Verifica tu numero de tarjeta';
                                    objtemp.resp=false;
                                    agregaerror(objtemp);
                                }

                            }
                            break;
                        case 'card[expcard]':
                            expcard=$.payment.cardExpiryVal(val);
                            if(expcard!= undefined){
                                if(!$.payment.validateCardExpiry(expcard.month, expcard.year)){
                                    temp.mensaje ='Fecha de expiracion invalida';
                                    temp.resp=false;
                                };

                            }else{
                                temp.mensaje ='Fecha de expiracion invalida';
                                temp.resp=false;
                            }
                            break;
                        case 'card[cvc]':
                            campotemp = $('input[data-conekta="card[number]"]').attr('id');
                            objtemp={
                                'resp':true,
                                'campo':campotemp,
                                'mensaje': ''
                            };
                            respvalccv=valida($('#'+campotemp),objtemp);
                            agregaerror(respvalccv);
                            if(respvalccv.resp){
                                cardtype=$.payment.cardType($('#'+campotemp).val().trim());

                                if(!$.payment.validateCardCVC(val,cardtype)){
                                    temp.mensaje ='Numero de seguridad invalido';
                                    temp.resp=false;
                                };
                            }else{
                                temp.mensaje ='Verifica tu numero de tarjeta';
                                temp.resp=false;
                            }
                            break;
                        default:
                    }
                }
            }
        }else{
            if(patron!= undefined){
                if (val!=""){
                    if(!val.match(patron)){

                    }
                }
            }
        }
        return temp;
    }
}

function errorTreal (form)
{
    $('#'+form+' :input').each(function(index,obj){
        campo = $(this).attr('id');
        $( "#"+campo ).change(function() {
            obj = $(this).attr('id');
            temp={
                'resp':true,
                'campo':obj,
                'mensaje': ''
            };
            value=valida(this,temp);
            agregaerror(value);
        });
    });
}

function errorTrealcampo(campo){
    $( "#"+campo ).change(function() {
        obj = $(this).attr('id');
        temp={
            'resp':true,
            'campo':obj,
            'mensaje': ''
        };
        value=valida(this,temp);
        agregaerror(value);
    });
}

function restringirform (form,complete=true,comands=true){
    if (typeof form === 'string'){
        if(complete){
            $('#'+form+' :input').each(function(index,obj){
                $(this).attr( 'autocomplete', 'off' );
            });
        }
        if(comands){
            $('#'+form+' :input').bind("cut copy paste",function(e) {
                e.preventDefault();
            });
        }
    }else if(typeof form === 'object'){
        $.each( form ,function (key,data) {
            if (complete) {
                $('#' + data + ' :input').each(function (index, obj) {
                    $(this).attr('autocomplete', 'off');
                });
            }
            if (comands) {
                $('#' + data + ' :input').bind("cut copy paste", function (e) {
                    e.preventDefault();
                });
            }
        });
    }

}

function dialogooption(url,mensaje,titulo,datos='',reload=true,metodo='POST',callback='',valida='',inicia='',dialogo='dialogoopcion',contenedor='mensajedialogo'){
    $('#'+contenedor).html(mensaje);
    if (typeof inicia === 'function'){
        inicia();
    }
    $('#'+dialogo).attr('title',titulo);
    $("#"+dialogo).dialog({
        buttons:[
            {
                text:"si",
                click:function(){
                    data={};
                    if(!(metodo=='GET'||metodo=='get')) {
                        data._method = metodo;
                        if (typeof callback === 'function') {
                            data.form = callback();
                        }
                        data.datos = datos;
                    }
                    if (typeof valida === 'function'){
                        if(valida()){
                            ajaxreloadself(url,metodo,data,reload);
                            $(this).dialog("close");
                        }
                    }else{
                        ajaxreloadself(url,metodo,data,reload);
                        $(this).dialog("close");
                    }

                }
            },
            {
                text:"no",
                click:function(){
                    $(this).dialog("close");
                }
            }
        ]
    })
}
function ajaxreloadself (url ,metodo="POST" ,data='',reload=true )
{
    $.ajax({
        url:url,
        type:metodo,
        data:data,
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        },
        datatype:"aplication/json",
        beforeSend: function(){

        }
    }).always(function(json, textStatus,errorThrown){
        if (reload){
            location.reload();
        }
        if(json!=undefined){
            datosresp=json;
        }
    });
}

//data,url,before='',success='',error='',datatype='json',type='POST'
function peticionAJAX(peticionAJAX){
    opciones={};
    if(peticionAJAX.data!=undefined){
        opciones.data=peticionAJAX.data;
        if(!(peticionAJAX.type=='GET'||peticionAJAX.type=='get')){
            opciones.data._method=peticionAJAX.type;
            opciones.headers={
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            };
        }
    }
    if(peticionAJAX.url!=undefined){
        opciones.url=peticionAJAX.url;
    }
    if(peticionAJAX.datatype!=undefined){
        opciones.datatype=peticionAJAX.datatype;
    }else{
        opciones.datatype="aplication/json";
    }
    if(peticionAJAX.type!=undefined){
        opciones.type=peticionAJAX.type;
    }else{
        opciones.type='GET';
    }
    // before: function() {} , success: function(json) {} , error: function(json.responseJSON){}
    if(peticionAJAX.before!= undefined){
        opciones.beforeSend = peticionAJAX.before;
    }
    if(peticionAJAX.success!= undefined){
        opciones.success = peticionAJAX.success;
    }
    if(peticionAJAX.error!= undefined){
        opciones.error = peticionAJAX.error;
    }
    jqxhr=$.ajax(opciones);
    //jqxhr.always(function (json, textStatus, errorThrown) { });
    //jqxhr.done(function (json, textStatus, errorThrown) { });
    //jqxhr.fail(function (json, textStatus, errorThrown) { });
}

function llenacombo(campo,contenedor,parent,url,initfunc='') {
    id = $('#'+campo).find(":selected").val();
    var ajax={};
    if(id.length){
        ajax.url=url+id;
        peticionAJAX(ajax);
        jqxhr.always(function (json, textStatus, errorThrown) {
            if (json != undefined){
                if(json.resp != undefined && json.resp == 'success'){
                    $('#'+parent).show('slow');
                    apendcombo (contenedor,json.datosresp,true);
                    if (typeof initfunc == 'function'){
                        initfunc(json);
                    }
                }
            }else{
                $('#'+parent).hide('slow');
            }
        });
    }else{
        $('#'+parent).hide('slow');
    }
}
function llenadatoscombo(campo,parent,url,initfunc='') {
    id = $('#'+campo).find(":selected").val();
    var ajax={};
    if(id.length){
        ajax.url=url+id;
        peticionAJAX(ajax);
        jqxhr.always(function (json, textStatus, errorThrown) {
            if (json != undefined){
                if(json.resp != undefined && json.resp == 'success'){
                    $('#'+parent).show('slow');
                    if (typeof initfunc == 'function'){
                        initfunc(json);
                    }
                }
            }else{
                $('#'+parent).hide('slow');
            }
        });
    }else{
        $('#'+parent).hide('slow');
    }
}

function ajustecolumnastabla(obj,llavenueva){
    $.each(obj,function (key,value) {
        $.each(value,function (llave,valor) {
            temp={};
            temp[llavenueva]=valor;
            obj[key]=temp;
        })
    });
    return obj;
}

function tabladinamica(idtabla,init = '' ) {
    ajustecolumnastabla(configtable.columns,'title');
    if (!configtable.data.length) {
        configtable.language.infoEmpty='';
    }
    table = $('#'+idtabla)
        .on( 'init.dt',init)
        .on( 'dblclick', 'tr', function () {
            data = table.row( this ).data();
            if(data!=undefined) {
                if ($(this).hasClass('selectedrowtable')) {
                    $(this).removeClass('selectedrowtable');
                }
                else {
                    table.$('tr.selectedrowtable').removeClass('selectedrowtable');
                    $(this).addClass('selectedrowtable');
                }
            }
        })
         .DataTable(configtable);
}

function tabladinamicaajax(idtabla,headtabla,url,pietabla='',init = '',metodo="GET",data='') {
    ajustecolumnastabla(configtable.columns,'data');
    configtable.ajax=
        {
            "type":metodo,
            "url":url,
            "data":data
        };
    tabs = configtable.columns;
    $.each(tabs, function(i,item){
        if(headtabla!=undefined){
            $("#"+headtabla).append("<th>"+tabs[i].data+"</th>");
        }
        if(pietabla!=undefined || pietabla===''){
            $("#"+pietabla).append("<th>"+tabs[i].data+"</th>");
        }

    });
    if (!configtable.data.length) {
        configtable.language.infoEmpty='';
    }
    table = $('#'+idtabla)
        .on( 'init.dt',init)
        .on( 'click', 'tr', function () {
            if(data!=undefined) {
                if ($(this).hasClass('selectedrowtable')) {
                    $(this).removeClass('selectedrowtable');
                }
                else {
                    table.$('tr.selectedrowtable').removeClass('selectedrowtable');
                    $(this).addClass('selectedrowtable');
                }
            }
        })
        .DataTable(configtable);
}


function disabledchange (form,select,atrib,value,datameses){
    var myform = $("#"+form);
    var inputsoff=$("#"+form).find(':input:disabled').removeAttr('disabled');
    if($('option:selected', $('#'+select)).attr(atrib)==value){
        inputsoff.attr('disabled','disabled');
    }else{
        myform.find(':input:disabled').removeAttr('disabled');
    }
    if (typeof select === 'string'){
        $('#'+select).change(function () {
            val= $('option:selected', this).attr(atrib);
            if(val!=undefined){
                if(val==value){
                    inputsoff.attr('disabled','disabled');
                }else{
                    myform.find(':input:disabled').removeAttr('disabled');
                }
            }else{
                myform.find(':input:disabled').removeAttr('disabled');
            }
        });
    }else if(typeof select === 'object'){
        $.each( select ,function (key,data) {
            $('#'+data).change(function () {
                val= $('option:selected', $('#'+select[0])).attr(atrib);
                $('#ingresos').val('');
                $('#iva_ingresos').val('');
                $('#iva_retenido').val('');
                $('#deducciones').val('');
                $('#iva_acreditable').val('');
                $('#iva_pagado').val('');
                $('#retenciones_ISR').val('');
                $('#ISR_pagado').val('');
                if(val!=undefined){
                    if(val==value){
                        inputsoff.attr('disabled','disabled');
                        seleccionado=datameses[$('option:selected', $('#'+select[1])).val()][$('option:selected', $('#'+select[0])).val()];
                        $('#ingresos').val(seleccionado.datos[0].monto_mes);
                        $('#iva_ingresos').val(seleccionado.datos[0].ingresos_mes);
                        $('#iva_retenido').val(seleccionado.datos[0].iva_ret_mes);
                        $('#deducciones').val(seleccionado.datos[0].deducciones_mes);
                        $('#iva_acreditable').val(seleccionado.datos[0].iva_acre_mes);
                        $('#iva_pagado').val(seleccionado.datos[0].iva_pag_mes);
                        $('#retenciones_ISR').val(seleccionado.datos[0].isr_mes);
                        $('#ISR_pagado').val(seleccionado.datos[0].isr_pag_mes);
                    }else{
                        myform.find(':input:disabled').removeAttr('disabled');
                    }
                }else{
                    myform.find(':input:disabled').removeAttr('disabled');
                }
            });
        });
    }
}

function restringe(campo,patern,max=0) {
    $('#'+campo).on('input keyup ', function () {
        var regla = new RegExp(patern,'gm');
        if (max > 0 &&  patern !=''){
            this.value = this.value.replace(regla,'').slice(0, max);
        }else if (max > 0 && patern ==''){
            this.value = this.value.slice(0, max);
        }else if(max == 0 && patern !=''){
            this.value = this.value.replace(regla,'');
        }
    });
}

function restringecampo(campo) {
    var patron = $('#'+campo).attr('data-pattern');
    var tamano =$('#'+campo).attr('data-length');
    if(patron.length){
        if (tamano.length){
            restringe(campo,patron,tamano);
        }else{
            restringe(campo,patron);
        }
    }
}
function restringefrm(form){
    $('#'+form+' :input').each(function(index,obj){
        var campo = $(obj).attr('id');
        if(campo.length){
            restringecampo(campo);
        }
    });
}


function apendcombo (combo,data,selectpicker=false){
    $('#'+combo).find('option').remove();
    $.each(data, function (indice, val) { // indice, valor
        $('#'+combo).append('<option value="' + indice + '">' + val + '</option>');
    });
    if(selectpicker){
        $('#'+combo).selectpicker('refresh');
    }
}

function agrega(agrega,contenedor,html,claseselect,clasedate,tipo,selectid){
    $('.'+claseselect).selectpicker();
    $('.'+clasedate).datepicker(configdatepicker);
    $("#"+agrega).on('click', function(){
        $('#'+contenedor).append(html);
        $('.'+claseselect).selectpicker();
        $('.'+clasedate).datepicker(configdatepicker);
        layouts=$(tipo+"[id="+selectid+"]");
        $(layouts[(layouts.length-1)]).selectpicker('val','');
    });
}

function agregainput(agrega,contenedor,html){
    $('#'+contenedor).append(html);
    $("#"+agrega).on('click', function(){
        $('#'+contenedor).append(html);
    });
}


function elimina(claseelimina,claseselect){
    $(document).on("click","."+claseelimina,function(){

        $('.'+claseselect).selectpicker('destroy');

        if( $('.'+claseselect).length!=1){
            $(this).parent().parent().parent().remove();
        }
        $('.'+claseselect).selectpicker();
        //verifica que haya mas de un elemento para poder eliminar
    });
}

function apendcomboanio (combo,data, anio,selectpicker=false){
    $('#'+combo).find('option').remove();
    $.each(data[anio], function (indice, val) { // indice, valor
        if(val.estatus)
        {
            $('#'+combo).append('<option data-icon="glyphicon-ok" value="' + indice + '">' + val.mes + '</option>');
        }else{
            $('#'+combo).append('<option value="' + indice + '">' + val.mes + '</option>');
        }
    });
    if(selectpicker){
        $('#'+combo).selectpicker('refresh');
    }
}

function mascaraMoneda(num, prefix) {
    num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
    splitRight = splitRight + '00';
    splitRight = splitRight.substr(0, 3);
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
        splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
    }
    return prefix + ' ' + splitLeft + splitRight;
}

function progresbar(element,inicio, fin, intervalos, texto=''){
    var elem = document.getElementById(element);
    var width = inicio;
    var id = setInterval(frame, intervalos);
    function frame() {
        if (width >= fin) {
            clearInterval(id);
        } else {
            width++;
            elem.style.width = width + '%';
            elem.innerHTML = width * 1 + '%'+'<span class="show">'+texto+'</span>';
        }
    }
}

function formcampodinamico(obj , boton , tipo , form){

    var arrayid=[];

    $.each(obj,function (key , value) {
        if(value.datosuser != undefined && typeof value.datosuser === 'object' && value.datosuser.length > 0){
            $.each(value.datosuser ,function (llave , valor) {
                html = renderdefault(value.tmpl,value);
                $('#'+value.grupo).append(html);
                layouts=$(tipo+"[id="+value.selectid+"]");
                $(layouts[llave]).selectpicker('val',valor);
                layoutsfecha=$("input[name='"+value.picker+"']");
                $(layoutsfecha[(layoutsfecha.length-1)]).val(value.fechas[llave]);
            });
        } else{
            html = renderdefault(value.tmpl,value);
            $('#'+value.grupo).append(html);
            layouts=$(tipo+"[id="+value.selectid+"]");
            $(layouts[0]).selectpicker('val','');
        }
        arrayid.push(value.selectid);
        agrega(value.agrega, value.grupo, html, value.selectpick, value.datepick,tipo,value.selectid);
    });
    if (typeof boton === 'object' ){
        $.each( boton ,function (asoc,data) {
            $('#'+data).click(function () {
                if(validar(arrayid,tipo)){
                    $('#'+form).submit();
                }
            });
        });
    }
    else if (typeof boton === 'string' ){
        $('#'+boton).click(function () {
            if(validar(arrayid,tipo)){
                $('#'+form).submit();
            }
        });
    }

    elimina('eliminar','selectpick');
}

// Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla

// Evento que selecciona la fila y la elimina

function renderdefault(tmpl,datos){
    html = $("#"+tmpl).render(datos);
    return html;
}

function validar(valores,tipo){
    var resp = true;
    if(typeof valores==='object'){
        var datos = mapear(valores,tipo);
        Object.keys(datos).forEach(function (key) {
            if(!validaarray(datos[key],key)){
                resp=false;
            }
        });

    }else{
        resp=false;
    }
    return resp;
}
function mapear(claves,tipo='input'){
    var temp=[];
    if (typeof claves==='object') {
        $.each( claves, function( key, value ) {
            temp[value]= $(tipo+"[id='"+value+"']").map(function(){
                return $(this).val();
            }).get();
        });
    } else if(typeof claves==='string') {
        temp[claves] = $(tipo+"[id='"+claves+"']").map(function () {
            return $(this).val();
        }).get();
    } else {
        temp=false;
    }
    return temp;
}
function notificar(campos,type='',clase='',nonb=false){
    if (typeof campos==='object') {
        campos.type = switchtype(type);
        campos.addclass=clase;
        campos.styling= 'bootstrap3';
        campos.nonblock= {
            nonblock: nonb
        };
        new PNotify(campos);
    }
}
function switchtype(type){
    if (type != undefined ){
        switch (type){
            case 1:
                return 'success';
                break;
            case 2:
                return 'info';
                break;
            case 3:
                return 'error';
                break;
            default:
                return '';
        }
    }else{
        return '';
    }
}

function restrictpattern (){

}

function botoneshref (botones) {
    if(typeof botones==='object'){
        $.each( botones, function( key, boton ) {
            botonhref (boton.name , boton.url)
        });
    }
}
function botonhref (boton , url) {
    $('#'+boton).click(function () {
        if($(this).attr('disabled')==undefined){
            document.location.href=url;
        }
    });
}
function validaarray(dato,nombre){
    var resp = false;
    detalle={};
    if (dato.length > 0){
        resp = true;
        if (!dato.repetidos()){
            detalle.title='Revisar '+nombre+'es';
            detalle.text=nombre+' repetido';
            notificar(detalle);
            resp = false;
        }
    }else {
        detalle.title = 'Revisar ' + nombre;
        if (nombre != 'regimen') {
            detalle.text = 'Debe elegir al menos una ' + nombre;
        }else{
            detalle.text = 'Debe elegir al menos un ' + nombre;
        }
        notificar(detalle);
        resp = false;

    }
    return resp;
}

$.views.converters({
    number: function (valor) {

        if (!isNaN(parseFloat(valor)))
            return toCurrency(valor, 2, '.', ',');
        else
            return valor;
    },

    fechaFormatter: function (value) {
        var retorno = "";
        value = value.replace(/\//g, "");
        eval("var fechaobj = new Date(" + value + ")");
        retorno = fechaobj.getFullYear() + "-" + (fechaobj.getMonth() + 1) + "-" + fechaobj.getDate();
        //El valor es una fecha con el siguiente formato
        return retorno;
    },

    dateformat: function (value) {
        date= moment(value, 'YYYY-MM-DD HH:mm:ss').format('YYYY/MM/DD hh:mm A');
        return date;
    },


    MonedaMX: function (valor) {
        return mascaraMoneda(valor, '$'); // Funcion que se encuentra en el archivo /js/Utilerias.js
    },
    mascaraNum: function (valor) {
        var num = new Number(valor);
        var fix = num.toFixed(2);
        return fix;
    }
});

$.views.helpers({
    exist: function(val){
        if(val == undefined || val == null || val == ''){
            return false;
        }else{
            return true;
        }
    }
});


$.views.settings.delimiters("{%","%}");
$(document).ready(function () {
    $('.dropdown-submenu a.test').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
})
