<?php
/**
 * Created by PhpStorm.
 * User: UltraBook HP
 * Date: 09/10/2017
 * Time: 02:54 PM
 */

    function notificaciones (){
        $notas=Auth::user()->notificationslimit;

        return$notas;
    }

    function notificacionnum(){
        $num = Auth::user()->unreadNotifications()->groupBy('notifiable_type')->count()? Auth::user()->unreadNotifications()->groupBy('notifiable_type')->count():'';
        return $num;
    }
    function plan (){
        $plan="";
        if(!empty( \Illuminate\Support\Facades\Auth::user()->conekta_plan )){
            $plan=\Illuminate\Support\Facades\Auth::user()->plazo->plan->plan;
        }else{
            $plan='Básico';
        }

        return $plan;
    }

function pasosreg ($tipo){
    $resp="tagcheck";
    switch ($tipo){
        case 0 :
            if(empty( \Illuminate\Support\Facades\Auth::user()->conekta_plan )){
                $resp='';
            }
            break;
        case 1 :
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->rfc )){
                $resp='datospersona/0/edit';
            }else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->datospersona->id_codepostal )){
                $resp='datospersona/0/edit';
            }else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->obligaciones )){
                $resp='datosfiscales/0/edit';
            }else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->actividades )){
                $resp='datosfiscales/0/edit';
            }else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->regimenes )){
                $resp='datosfiscales/0/edit';
            }else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->regimenes )){
                $resp='datosfiscales/0/edit';
            }
            else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->regimenes )){
                $resp='datosfiscales/0/edit';
            }
            else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->regimenes )){
                $resp='datosfiscales/0/edit';
            }else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->key )){
                $resp='datosfiscales/1/edit';
            }else if(empty( \Illuminate\Support\Facades\Auth::user()->persona->cer )){
                $resp='datosfiscales/1/edit';
            }
            break;
        case 2 :
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->rfc )){
                $resp='';
            }
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->datospersona->id_codepostal )){
                $resp='';
            }
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->datosfiscales->ini_oper )){
                $resp='';
            }
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->obligaciones )){
                $resp='';
            }
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->actividades )){
                $resp='';
            }
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->regimenes )){
                $resp='';
            }
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->key )){
                $resp='';
            }
            if(empty( \Illuminate\Support\Facades\Auth::user()->persona->cer )){
                $resp='';
            }
            break;
        default:
            $resp='';

    }
    return $resp;
}

    function peticiones ($id){
        switch ($id){
            case 0 :
                $num= \App\Models\Peticion::whereNull('id_usuarioasignado')->count();
                break;
            case 1 :
                $num= \App\Models\DetalleDeclaracion::whereNull('archivofinal')->where('id_usuario','=',Auth::id())->count();
                break;
            case 2 :
                $num =  \App\Models\DetallePosicionfinan::whereNull('archivo')->where('id_usuario','=',Auth::id())->count();
//                $num= Auth::user()->detalleposicion->count();
                break;
            case 3 :
                $num = \App\Models\DetalleCalculoimp::whereNull('archivo')->where('id_usuario','=',Auth::id())->count();
//                $num= Auth::user()->detallecalculo->count();
                break;
            case 4 :
                $num = \App\Models\Peticion::where('id_usuario','=',Auth::id())->where('id_tipo_peticion','=',2)->count();
//                $num= Auth::user()->detallecalculo->count();
                break;
            case 5 :
                $num = \App\Models\Peticion::where('id_usuario','=',Auth::id())->where('id_tipo_peticion','=',1)->count();
//                $num= Auth::user()->detallecalculo->count();
                break;
            default:
                $num=0;

        }

        return $num;
    }

    function dashboard($id){
        $buscar=[];
        $buscar[]=['id_persona','=',\Illuminate\Support\Facades\Auth::user()->persona->id_persona];
        switch ($id){
            case 0 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','emitida'];
                if (\App\Models\Factura::where($buscar)->exists()){
                    $num='$ '.number_format(\App\Models\Factura::where($buscar)->sum('total'), 2, '.', ',');
                }else{
                    $num ='$ 0';
                }
                return $num;
                break;
            case 1 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','recibida'];
                if (\App\Models\Factura::where($buscar)->exists()){
                    $num='$ '.number_format(\App\Models\Factura::where($buscar)->sum('total'), 2, '.', ',');
                }else{
                    $num ='$ 0';
                }
                return $num;
                break;
            case 2 :
                $buscar[]=['pagada','=','1'];
                $buscar[]=['tipo','=','emitida'];
                if (\App\Models\Factura::where($buscar)->whereRaw('MONTH(`fechapago`)= MONTH(NOW())')->exists()){
                    $temptotal=\App\Models\Factura::where($buscar)->whereRaw('MONTH(`fechapago`)= MONTH(NOW())')->sum('total');
                    $tempsubtotal=\App\Models\Factura::where($buscar)->whereRaw('MONTH(`fechapago`)= MONTH(NOW())')->sum('subtotal');
                    $ivacob=$temptotal-$tempsubtotal;
                    $num=$ivacob;
                }else{
                    $num = 0;
                }
                $buscar2[]=['pagada','=','1'];
                $buscar2[]=['tipo','=','recibida'];
                if (\App\Models\Factura::where($buscar2)->whereRaw('MONTH(`fechapago`)= MONTH(NOW())')->exists()){
                    $temptotal2=\App\Models\Factura::where($buscar2)->whereRaw('MONTH(`fechapago`)= MONTH(NOW())')->sum('total');
                    $tempsubtotal2=\App\Models\Factura::where($buscar2)->whereRaw('MONTH(`fechapago`)= MONTH(NOW())')->sum('subtotal');
                    $ivacob2=$temptotal2-$tempsubtotal2;
                    $num2=$ivacob2;
                }else{
                    $num2 =0;
                }
                return '$ '.number_format(($num2-$num), 2, '.', ',');;
                break;
            case 3 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','recibida'];
                if (\App\Models\Factura::where($buscar)->exists()){
                    $temptotal=\App\Models\Factura::where($buscar)->sum('total');
                    $tempsubtotal=\App\Models\Factura::where($buscar)->sum('subtotal');
                    $ivacob=$temptotal-$tempsubtotal;
                    $num='$ '.number_format($ivacob, 2, '.', ',');
                }else{
                    $num ='$ 0';
                }
                return $num;
                break;
            case 4 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','emitida'];
                if (\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER)) <= 30')->exists()){
                    $num=\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER)) <= 30')->sum('total');
                }else{
                    $num =0;
                }
                return $num;
                break;
            case 5 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','emitida'];
                if (\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER)) BETWEEN  30 AND 61 ')->exists()){
                    $num=\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER)) BETWEEN  30 AND 61 ')->sum('total');
                }else{
                    $num =0;
                }
                return $num;
                break;
            case 6 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','emitida'];
                if (\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER))> 61')->exists()){
                    $num=\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER))> 61')->sum('total');
                }else{
                    $num =0;
                }
                return $num;
                break;
            case 7 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','recibida'];
                if (\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER)) <= 30')->exists()){
                    $num=\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER)) <= 30')->sum('total');
                }else{
                    $num =0;
                }
                return $num;
                break;
            case 8 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','recibida'];
                if (\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER)) BETWEEN  30 AND 61 ')->exists()){
                    $num=\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED INTEGER)) BETWEEN  30 AND 61 ')->sum('total');
                }else{
                    $num =0;
                }
                return $num;
                break;
            case 9 :
                $buscar[]=['pagada','=','0'];
                $buscar[]=['tipo','=','recibida'];
                if (\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER))> 61')->exists()){
                    $num=\App\Models\Factura::where($buscar)->whereRaw('(CONVERT( DATEDIFF(NOW(),`fecha`),UNSIGNED  INTEGER))> 61')->sum('total');
                }else{
                    $num =0;
                }
                return $num;
                break;
            default:
                $num=0;

        }
    }

    function factemp(){
        if(\App\Models\Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->exists()){
            $enabled=true;
        }else{
            $enabled=false;
        }
        return $enabled;
    }

    function factempback($id){
        if(\App\Models\Tempfac::where('id_persona','=',$id)->exists()){
            $enabled=true;
        }else{
            $enabled=false;
        }
        return $enabled;
    }


    function textlargenotifi($text,$tipo){
        if($tipo==1){
            if(strlen($text)>29){
                return substr_replace($text, '...', 21);
            }else{
                return $text;
            }
        }else{
            if(strlen($text)>47){
                return substr_replace($text, '...', 46);
            }else{
                return $text;
            }
        }

    }

?>