<?php

namespace App\Http\Controllers;

use App\Models\Declaraciones;
use App\Models\personas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Models\Meses;

class DeclaracionesAntController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $columnas = array(
            ["head" => "Mes"],
            ["head" => "Año"],
            ["head" => "Ingresos"],
            ["head" => "Iva de ingresos"],
            ["head" => "Iva retenido"],
            ["head" => "Deducciones declaradas"],
            ["head" => "IVA acreditable"],
            ["head" => "IVA pagado"],
            ["head" => "Retenciones ISR"],
            ["head" => "ISR pagado"],
            ["head" => "acciones"]
        );
        $id = Auth::user()->persona->id_persona;
        $declaraciones = Declaraciones::where('id_persona', $id)->get();

        $datos = [];
        foreach ($declaraciones as $declaracion) {
            $temp = [];
            $temp[] = $declaracion->meses->mes;
            $temp[] = $declaracion->anio;
            $temp[] = $declaracion->monto_mes;
            $temp[] = $declaracion->ingresos_mes;
            $temp[] = $declaracion->iva_ret_mes;
            $temp[] = $declaracion->deducciones_mes;
            $temp[] = $declaracion->iva_acre_mes;
            $temp[] = $declaracion->iva_pag_mes;
            $temp[] = $declaracion->isr_mes;
            $temp[] = $declaracion->isr_pag_mes;
            $temp[] = '<a class="btn btn-success" href="'.url('declaracion/'.$declaracion->id_declaraciones.'/edit').'">
                        <i class="fa fa-pencil-square-o" >
                        </i>
                       </a>
                       ';
            $datos[] = $temp;
        }
        return view('declaraciones.listado',['declaraciones'=>$declaraciones,'meses'=>'[]','datos'=>$datos,'columnas'=>$columnas]);
    }

    public function declaracionesantback ($id){
        $columnas = array(
            ["head" => "Mes"],
            ["head" => "Año"],
            ["head" => "Ingresos"],
            ["head" => "Iva de ingresos"],
            ["head" => "Iva retenido"],
            ["head" => "Deducciones declaradas"],
            ["head" => "IVA acreditable"],
            ["head" => "IVA pagado"],
            ["head" => "Retenciones ISR"],
            ["head" => "ISR pagado"],
            ["head" => "acciones"]
        );
        $declaraciones = Declaraciones::where('id_persona', $id)->get();
        $user = personas::find($id)->usuario;
        $datos = [];
        foreach ($declaraciones as $declaracion) {
            $temp = [];
            $temp[] = $declaracion->meses->mes;
            $temp[] = $declaracion->anio;
            $temp[] = $declaracion->monto_mes;
            $temp[] = $declaracion->ingresos_mes;
            $temp[] = $declaracion->iva_ret_mes;
            $temp[] = $declaracion->deducciones_mes;
            $temp[] = $declaracion->iva_acre_mes;
            $temp[] = $declaracion->iva_pag_mes;
            $temp[] = $declaracion->isr_mes;
            $temp[] = $declaracion->isr_pag_mes;
            $temp[] = '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Editar" href="'.url('back/declaraciones/'.$declaracion->id_declaraciones.'/edit').'">
                        <i class="fa fa-pencil-square-o" >
                        </i>
                       </a>
                       <a class="btn btn-danger eliminar" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Eliminar" data-url="'.url('back/declaraciones/'.$declaracion->id_declaraciones).'" type="button">
                                <i class="fa fa-trash">
                                </i>
                            </a>
                       ';
            $datos[] = $temp;
        }

        $userdate =Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->year;
//        $userdate =Carbon::createFromFormat('Y-m-d H:i:s','2015-10-12 21:19:09')->year;
        $date= Carbon::now()->year;
        $anio=$this->aniosanteriores($userdate,$date);
        $datameses=[];
        foreach ($anio as $key=>$val){
            if($val==$date)
            {
                $datameses[$val]=$this->mesesrestantesback ($val,$id);
            } else{
                $datameses[$val]=$this->mesesrestantesback ($val,$id,false);
            }

        }
        $declaraciones= new Declaraciones();

        $url='back/declaraciones/'.$id;

        $metodo='POST';

        return view('backoffice.declaraciones.create',['datos'=>$datos,'columnas'=>$columnas, 'declaraciones'=>$declaraciones,'anio'=>$anio,'meses'=>collect($datameses),'url'=>$url,'metodo'=>$metodo]);

    }

    public function borradeclaracion($id){
        $declaracion = Declaraciones::find($id);
        $idpersona=$declaracion->id_persona;
        $declaracion->delete();
        return redirect('back/declaraciones/'.$idpersona);
    }


    public function agregadeclacionantback(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'mes' => 'required',
            'anio' => 'required',
            'ingresos' => 'required|numeric',
            'iva_ingresos' => 'required|numeric',
            'iva_retenido' => 'required|numeric',
            'deducciones' => 'required|numeric',
            'iva_acreditable' => 'required|numeric',
            'iva_pagado' => 'required|numeric',
            'retenciones_ISR' => 'required|numeric',
            'ISR_pagado' => 'required|numeric',
        ]);

        $mes=Meses::where('number','=',$request['mes'])->first();
        $existe = Declaraciones::whereRaw('id_persona = '.$id.' AND mes = "'.$request['mes'].'" AND anio = "'.$request['anio'].'"')->get();
        if($existe->count())
        {
            Session::flash('message', '!Ya declaraste el mes '.$mes->mes.' del '.$request['anio'].' !');
            return back()->withErrors($validator)->withInput();
        }

        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $declaraciones= new Declaraciones();
        $declaraciones->id_persona=$id;
        $declaraciones->mes=$request['mes'];
        $declaraciones->anio=$request['anio'];
        $declaraciones->monto_mes=$request['ingresos'];
        $declaraciones->ingresos_mes=$request['iva_ingresos'];
        $declaraciones->iva_ret_mes=$request['iva_retenido'];
        $declaraciones->deducciones_mes=$request['deducciones'];
        $declaraciones->iva_acre_mes=$request['iva_acreditable'];
        $declaraciones->iva_pag_mes=$request['iva_pagado'];
        $declaraciones->isr_mes=$request['retenciones_ISR'];
        $declaraciones->isr_pag_mes=$request['ISR_pagado'];
        $declaraciones->save();

        Session::flash('message', '!Tu declaracion del mes '.$mes->mes.' del '.$request['anio'].' ha sido registrada !');

    return redirect('back/declaraciones/'.$id);
    }

    public function editadeclaracion($id){
        $declaracion=Declaraciones::find($id);
        $url='back/declaraciones/'.$id;
        $metodo='PUT';
        return view('backoffice.declaraciones.edit',['declaracion'=>$declaracion,'url'=>$url,'metodo'=>$metodo]);
    }
    public function updatedeclaracion(Request $request ,$id){
        $validator = Validator::make($request->all(), [
            'mes' => 'required',
            'anio' => 'required',
            'ingresos' => 'required|numeric',
            'iva_ingresos' => 'required|numeric',
            'iva_retenido' => 'required|numeric',
            'deducciones' => 'required|numeric',
            'iva_acreditable' => 'required|numeric',
            'iva_pagado' => 'required|numeric',
            'retenciones_ISR' => 'required|numeric',
            'ISR_pagado' => 'required|numeric',
        ]);

        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }
        $declaraciones= Declaraciones::find($id);
        $declaraciones->monto_mes=$request['ingresos'];
        $declaraciones->ingresos_mes=$request['iva_ingresos'];
        $declaraciones->iva_ret_mes=$request['iva_retenido'];
        $declaraciones->deducciones_mes=$request['deducciones'];
        $declaraciones->iva_acre_mes=$request['iva_acreditable'];
        $declaraciones->iva_pag_mes=$request['iva_pagado'];
        $declaraciones->isr_mes=$request['retenciones_ISR'];
        $declaraciones->isr_pag_mes=$request['ISR_pagado'];
        $declaraciones->save();

        return redirect('back/declaraciones/'.$declaraciones->id_persona);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userdate =Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->created_at)->year;
//        $userdate =Carbon::createFromFormat('Y-m-d H:i:s','2015-10-12 21:19:09')->year;
        $date= Carbon::now()->year;
        $anio=$this->aniosanteriores($userdate,$date);
        $datameses=[];
            foreach ($anio as $key=>$val){
                if($val==$date)
                {
                    $datameses[$val]=$this->mesesrestantes($val);
                } else{
                    $datameses[$val]=$this->mesesrestantes($val,false);
                }

            }
        $declaraciones= new Declaraciones();

        return view('declaraciones.create',['declaraciones'=>$declaraciones,'anio'=>$anio,'meses'=>collect($datameses),'url'=>'/declaracion','metodo'=>'post']);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mes' => 'required',
            'anio' => 'required',
            'ingresos' => 'required|numeric',
            'iva_ingresos' => 'required|numeric',
            'iva_retenido' => 'required|numeric',
            'deducciones' => 'required|numeric',
            'iva_acreditable' => 'required|numeric',
            'iva_pagado' => 'required|numeric',
            'retenciones_ISR' => 'required|numeric',
            'ISR_pagado' => 'required|numeric',
        ]);
        $mes=Meses::where('number','=',$request['mes'])->first();
        $existe = Declaraciones::whereRaw('id_persona = '.Auth::user()->persona->id_persona.' AND mes = "'.$request['mes'].'" AND anio = "'.$request['anio'].'"')->get();
        if($existe->count())
        {
            Session::flash('message', '!Ya declaraste el mes '.$mes->mes.' del '.$request['anio'].' !');
//            return $existe;
            return back()->withErrors($validator)->withInput();
        }
        if($validator->fails())
        {
                return back()->withErrors($validator)->withInput();
        }


        $declaraciones= new Declaraciones();
        $declaraciones->id_persona=Auth::user()->persona->id_persona;
        $declaraciones->mes=$request['mes'];
        $declaraciones->anio=$request['anio'];
        $declaraciones->monto_mes=$request['ingresos'];
        $declaraciones->ingresos_mes=$request['iva_ingresos'];
        $declaraciones->iva_ret_mes=$request['iva_retenido'];
        $declaraciones->deducciones_mes=$request['deducciones'];
        $declaraciones->iva_acre_mes=$request['iva_acreditable'];
        $declaraciones->iva_pag_mes=$request['iva_pagado'];
        $declaraciones->isr_mes=$request['retenciones_ISR'];
        $declaraciones->isr_pag_mes=$request['ISR_pagado'];
        $declaraciones->save();


        Session::flash('message', '!Tu declaracion del mes '.$mes->mes.' del '.$request['anio'].' ha sido registrada !');

        return view('declaraciones.aftercreate');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Declaraciones  $declaraciones
     * @return \Illuminate\Http\Response
     */
    public function show(Declaraciones $declaraciones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Declaraciones  $declaraciones
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $declaracion=Declaraciones::find($id);
        return view('declaraciones.edit',['declaracion'=>$declaracion,'url'=>'declaracion/'.$id,'metodo'=>'PUT']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Declaraciones  $declaraciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'mes' => 'required',
            'anio' => 'required',
            'ingresos' => 'required|numeric',
            'iva_ingresos' => 'required|numeric',
            'iva_retenido' => 'required|numeric',
            'deducciones' => 'required|numeric',
            'iva_acreditable' => 'required|numeric',
            'iva_pagado' => 'required|numeric',
            'retenciones_ISR' => 'required|numeric',
            'ISR_pagado' => 'required|numeric',
        ]);

        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }
        $declaraciones= Declaraciones::find($id);
        $declaraciones->monto_mes=$request['ingresos'];
        $declaraciones->ingresos_mes=$request['iva_ingresos'];
        $declaraciones->iva_ret_mes=$request['iva_retenido'];
        $declaraciones->deducciones_mes=$request['deducciones'];
        $declaraciones->iva_acre_mes=$request['iva_acreditable'];
        $declaraciones->iva_pag_mes=$request['iva_pagado'];
        $declaraciones->isr_mes=$request['retenciones_ISR'];
        $declaraciones->isr_pag_mes=$request['ISR_pagado'];
        $declaraciones->save();

        return redirect('declaracion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Declaraciones  $declaraciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Declaraciones $declaraciones)
    {

    }

    public function aniosanteriores($anioini,$aniofin){
        $diff = intval($aniofin)-intval($anioini);
        if(!$diff){
            $resp=[$aniofin=>$aniofin];
        }else{
            $anio=[];
            $year= intval($anioini);
            for ($i=0;$diff>=$i;$i++){
                $yeartemp=$year+$i;
                $temp=['year'=>$yeartemp];
                $anio[]=$temp;
            }
            $resp=collect($anio)->mapWithKeys(function ($item){
                return [$item['year'] => $item['year']];
            });
        }
        return $resp;
    }

    private function mesesrestantes($anio,$filter=true,$mesesonly=true){
        $meses=Meses::all();
        if($mesesonly){
            if($filter){
                $restantes = collect($meses)->mapWithKeys(function ($item) use ($anio) {
                    $date= Carbon::now();
                    $mes=$date->month;

                    if ($item['number'] < $mes) {
                        $existe = Declaraciones::whereRaw('id_persona = '.Auth::user()->persona->id_persona.' AND mes = "'.$item->number.'" AND anio = "'.$anio.'"')->get();
                        if($existe->count()) {
                            return [$item->number => array('mes'=>$item->mes,'estatus'=>1,'anio'=>$anio,'datos'=>$existe)];
                        }else{
                            return [$item->number => array('mes'=>$item->mes,'estatus'=>0,'anio'=>$anio)];
                        }
                    } else {
                        return [];
                    }

                });
            }else{
                $restantes = collect($meses)->mapWithKeys(function ($item) use ($anio) {
                    $existe = Declaraciones::whereRaw('id_persona = '.Auth::user()->persona->id_persona.' AND mes = "'.$item['number'].'" AND anio = "'.$anio.'"')->get();
                    if($existe->count()) {
                        return [$item->number => array('mes'=>$item->mes,'estatus'=>1,'anio'=>$anio,'datos'=>$existe)];
                    }else{
                        return [$item->number => array('mes'=>$item->mes,'estatus'=>0,'anio'=>$anio)];
                    }
                });
            }
            return $restantes;
        }else{
            return $meses;
        }

    }

    private function mesesrestantesback ($anio,$id,$filter=true,$mesesonly=true){
        $meses=Meses::all();
        if($mesesonly){
            if($filter){
                $restantes = collect($meses)->mapWithKeys(function ($item) use ($anio, $id) {
                    $date= Carbon::now();
                    $mes=$date->month;

                    if ($item['number'] < $mes) {
                        $existe = Declaraciones::whereRaw('id_persona = '.$id.' AND mes = "'.$item->number.'" AND anio = "'.$anio.'"')->get();
                        if($existe->count()) {
                            return [$item->number => array('mes'=>$item->mes,'estatus'=>1,'anio'=>$anio,'datos'=>$existe)];
                        }else{
                            return [$item->number => array('mes'=>$item->mes,'estatus'=>0,'anio'=>$anio)];
                        }
                    } else {
                        return [];
                    }

                });
            }else{
                $restantes = collect($meses)->mapWithKeys(function ($item) use ($anio, $id) {
                    $existe = Declaraciones::whereRaw('id_persona = '.$id.' AND mes = "'.$item['number'].'" AND anio = "'.$anio.'"')->get();
                    if($existe->count()) {
                        return [$item->number => array('mes'=>$item->mes,'estatus'=>1,'anio'=>$anio,'datos'=>$existe)];
                    }else{
                        return [$item->number => array('mes'=>$item->mes,'estatus'=>0,'anio'=>$anio)];
                    }
                });
            }
            return $restantes;
        }else{
            return $meses;
        }

    }
}
