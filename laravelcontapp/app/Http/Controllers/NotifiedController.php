<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\personas;
use App\Notifications\personal;

class NotifiedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notas=Auth::user()->notifications;
        return view('notificaciones.listado',['notes'=>$notas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usersdata=User::role('user')->get();

        $users=$usersdata->mapWithKeys(function ($item){
            $name=$item->persona->nombre.' '.$item->persona->paterno.' '.$item->persona->materno.' '.$item->persona->rfc.' '.$item->email;
            return[$item['id_usuario']=>$name];
        });

        $url='notified';
        $metodo='POST';

        return view('backoffice.notificacion.createmensaje',['users'=>$users,'url'=>$url,'metodo'=>$metodo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tipo'=>'required',
            'usuario'=>'required_if:tipo,==,2',
            'titulo' => 'required',
            'mensaje' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        if($request['tipo'] == 2) {
            $users = collect($request['usuario']);
            foreach ($users as $usr) {
                $user = User::find($usr);
                $user->notify(new personal(['user' => $user->name, 'id' => $user->id_usuario, 'title' => $request['titulo'], 'data' => $request['mensaje'], 'url' => url('notified')]));
            }
            Session::flash('send', '¡Se enviaron correctamente las notificaciones!');
        }else if ($request['tipo'] == 1){
            $users = User::role('user')->get();
            foreach ($users as $user) {
                $user->notify(new personal(['user' => $user->name, 'id' => $user->id_usuario, 'title' => $request['titulo'], 'data' => $request['mensaje'], 'url' => url('notified')]));
            }
            Session::flash('send', '¡Se enviaron correctamente las notificaciones!');
        }
        return redirect('notified/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function readednote(Request $request)
    {
        $notes=Auth::user()->unreadNotifications()->where('id',$request['nota'])->get();
        $notes->markAsRead();
        return response()->json([
            'resp' => 'success',
            'xml' => $notes
        ]);

    }
}
