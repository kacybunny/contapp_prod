<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Peticion;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('backuser')){
            $peticiones=Peticion::whereNull('id_usuarioasignado')->get();
            $temp=[];
            $datos=[];
            foreach ($peticiones as $peticion)
            {
                $temp=[];
                $temp[]=$peticion->ticket;
                $temp[]=$peticion->usuario->persona->rfc;
                $temp[]=$peticion->meses->mes;
                $temp[]=$peticion->ano;
                $temp[]=$peticion->tipo->tipo;
                $temp[]='';
                $datos[]=$temp;
            }

            $columnas = array(
                ["head"=> "Número de solicitud"],
                ["head"=> "RFC"],
                ["head"=> "Mes"],
                ["head"=> "Año"],
                ["head"=> "Tipo"],
                ["head"=> "Acciones"]
            );

            return view('backoffice.peticion.listado',['datos'=>$datos,'columnas'=>$columnas]);
        }
        if(Auth::user()->hasRole('user')) {
            $planselect = Auth::user()->planselect;
            if (collect($planselect)->count()){
              if ($planselect->watch ==0){

                  $planselect->watch=1;
                  $planselect->save();
                  if ($planselect->plan->plan != 'Básico'){
                      Session::flash('pago',$planselect->plan->plan);
                      return redirect('pagos/create');
                  }else{
                      return view('home');
                  }
              }else{
                  return view('home');
              }
            }else{
                return view('home');
            }

        }
    }
}
