<?php

namespace App\Http\Controllers;

use App\Models\personas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class PersonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\personas  $personas
     * @return \Illuminate\Http\Response
     */
    public function show($personas)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\personas  $personas
     * @return \Illuminate\Http\Response
     */
    public function edit(personas $personas)
    {
        $persona=Auth::user()->persona;
       return view('Persona.edit',['persona'=>$persona]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\personas  $personas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'a_paterno'=> 'required',
            'a_materno'=>'required',
        ]);

        if ($validator->fails()){
            return back()->withErrors($validator->errors())->withInput();
        }

        $persona=Auth::user()->persona;
        $persona->nombre = $request['nombre'] ;
        $persona->paterno = $request['a_paterno'] ;
        $persona->materno = $request['a_materno'] ;

        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $extension = $request->file('foto')->getClientOriginalExtension();
            $url ='Perfil/'.Auth::user()->email. '.' . $extension;
            Storage::disk('custom')->put($url, File::get($file));
            $persona->foto = Auth::user()->email. '.' . $extension;
        }

        $persona->save();
return redirect('persona/0/edit');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\personas  $personas
     * @return \Illuminate\Http\Response
     */
    public function destroy(personas $personas)
    {
        //
    }

    public function personajson ()
    {
        if(personas::where('id_usuario', Auth::id() )->exists()){
            $persona=personas::where('id_usuario', Auth::id())->first();
        }else{
            $persona=[];
        }
        return response()->json($persona);
    }

    public function personajson2 ($id)
    {
        if(personas::where('id_usuario',$id)->exists()){
            $persona=personas::where('id_usuario',$id)->get()[0];
        }else{
            $persona=[];
        }
        return response()->json($persona);
    }
    public function rfconly (Request $request)
    {

        $validacion= Validator::make($request->all(),[
            'rfc'=> array ('required','max:13','regex:/^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$/')
        ]);


        if($validacion->fails())
        {
            $errores=collect($validacion->errors()->toArray());
            $resp=[];
            foreach ($errores as $key => $valor){
                $temp=[];
                $temp['campo']=$key;
                $temp['resp']=false;
                $temp['mensaje']=$valor[0];
                $resp[]=$temp;
            }

            return response()->json(['error'=>$resp],403);
        }

        $persona=personas::where('id_usuario', Auth::id())->first();
        $persona->rfc=$request['rfc'];
        $persona->save();
        return response()->json($persona);
    }

}
