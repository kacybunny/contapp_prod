<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailActive;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/
	
	use AuthenticatesUsers;
	
	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'logout']);
	}

    protected function validateLogin(Request $request)
    {

        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function login(Request $request)
    {

        session()->forget('pasosreg');

        $search=[];

        if (!empty($request['email'])){
            $search[]=['email', '=',$request['email']];
            $search[]=['verified', '=',0];
            if(User::where($search)->exists()){
                Session::flash('reenvio',base64_encode($request['email']));
            }
        }
        if (User::where('email', '=', $request['email'])->exists()) {
            $this->validateLogin($request);
        }else{
            $errors = [$this->username() => trans('auth.exist')];
            if ($request->expectsJson()) {
                return response()->json($errors, 422);
            }

            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors($errors);
        }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.

        if ($this->hasTooManyLoginAttempts($request)) {

            if (User::where('email', '=', $request['email'])->exists()){
                $data=User::where('email',$request['email'])->first();

                if($data->active && $data->verified){
                DB::beginTransaction();
                try
                {
                    $data=User::where('email',$request['email'])->firstOrFail();
                    $data->active=0;
                    $data->verified=0;
                    $data->email_token=str_random(10);
                    session('data',$request);
                    // After creating the user send an email with the random token generated in the create method above
                    $email = new EmailActive(new User(['email_token' => $data->email_token, 'name' => $data->name]));
                    Mail::to($data->email)->send($email);
                    $data->save();
                    DB::commit();
//                    $this->clearLoginAttempts($request);
                }
                catch(Exception $e)
                {
                    DB::rollback();
                    return back();
                }
                    return $this->sendLockoutResponse($request);
                }elseif(!$data->active && $data->verified){
                    return $this->sendLockoutResponse2($request);
                }else{
                    return $this->sendLockoutResponse($request);
                }
            }else{
                return $this->sendFailedLoginResponse($request);
            }
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendLockoutResponse(Request $request)
    {

        $message = Lang::get('auth.bruteforce');

        $errors = [$this->username() => $message];

        if ($request->expectsJson()) {
            return response()->json($errors, 423);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    protected function sendLockoutResponse2(Request $request)
    {

        $message = Lang::get('auth.dimiss');

        $errors = [$this->username() => $message];

        if ($request->expectsJson()) {
            return response()->json($errors, 423);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function activeusr ($token ){
        $data=User::whereRaw('email_token ="'.$token.'"AND  verified = 0 AND  active = 0')->firstOrFail();
        session(['token' => $data->email_token ,'mail' => $data->email]);
        return view('auth.confirmacionact',['user'=>$data]);
    }

    public function changepass (Request $request){
        if(session()->has('token')){

            $validacion=Validator::make($request->all(), [
                'password' => 'required|min:6|confirmed',
            ]);
            if($validacion->fails())
            {
                return back()->withErrors($validacion);

            }else{
                $user=User::where('email_token',session('token'))->firstOrFail();
                $user->password = bcrypt($request['password']);
                $user->unlocked();
                $user->save();
                session()->flush();
                return view('auth.exito');
            }
        }
        $this->redirectTo('/register');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        if (User::where('email', '=', $request['email'])->exists()) {
            $data=User::where('email',$request['email'])->first();

            if (!$data->active && !$data->verified) {
                $errors = [$this->username() => trans('auth.bruteforce')];
            }
            elseif(!$data->active && $data->verified)
            {
                $errors = [$this->username() => trans('auth.dimiss')];
            } else{
                $errors = [$this->username() => trans('auth.failed')];
            }
        }else{
            $errors = [$this->username() => trans('auth.exist')];
        }


        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }
}


