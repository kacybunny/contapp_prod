<?php

namespace App\Http\Controllers\Auth;

use App\Mail\EmailRegistroDatosCampana;
use App\Models\datospersona;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\personas;
use Illuminate\Http\Request;
use App\Mail\EmailVerification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Datosfiscales;
use App\Models\Planselect;
use App\Models\Planes;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/
	
	use RegistersUsers;
	
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $registerView = 'auth.register';
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

    public function showRegistrationFormplan($plan)
    {
        return view('auth.registerplan',['plan'=>$plan]);
    }
	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'nombre' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
            'terminos' =>'required',
            'paterno' => 'required|max:255',
            'materno' => 'required|max:255',
            'telefono'=> 'min:10'
//            'password' => 'required|min:6|confirmed',
		],['email.unique'=>'El Email ya esta registrado',
            'terminos.required'=>'Debes Aceptar los Términos y Condiciones primero'
        ]);
	}
	
	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
        return User::create([
            'name' => $data['nombre'],
            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
            'email_token' => str_random(10),
            'active'=>1,
        ]);
	}
    protected function adicionales($user,$request)
    {

        $datos = new datospersona;
        $datos->telefono=$request['telefono'];
        $datos->save();

        $datosfisc = new Datosfiscales();
        $datosfisc->save();

        $persona = new personas();
        $persona->nombre=$request['nombre'];
        $persona->paterno=$request['paterno'];
        $persona->materno=$request['materno'];
        $persona->id_usuario = $user;
        $persona->id_datos_persona = $datos->id_datos_persona;
        $persona->id_datos_fiscales=$datosfisc->id_datos_fiscales;
        $persona->save();

        if(!empty($request['plan'])){

            if(Planes::where('id_plan','=',$request['plan'])->exists()){
                $planselect= new Planselect();
                $planselect->id_usuario=$user;
                $planselect->id_plan=$request['plan'];
                $planselect->save();
            }

        }
    }

    public function reenvio($dato){
        $search[]=['email', '=',base64_decode($dato)];
        $search[]=['verified', '=',0];
        if(User::where($search)->exists()){
            $user=User::where($search)->first();
            $email = new EmailVerification(new User(['email_token' => $user->email_token, 'name' => $user->name]));
            Mail::to($user->email)->send($email);
            Session::flash('message', '!Te hemos enviado un mail de verificación!');
            return redirect('login');
        }
        return back()->withInput();
    }

    public function register(Request $request)
    {
        $search=[];

        if (!empty($request['email'])){
            $search[]=['email', '=',$request['email']];
            $search[]=['verified', '=',0];
            if(User::where($search)->exists()){
                Session::flash('reenvio',base64_encode($request['email']));
            }
        }

        // Laravel validation
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
            $this->throwValidationException($request, $validator);
        }
        // Using database transactions is useful here because stuff happening is actually a transaction
        // I don't know what I said in the last line! Weird!
        DB::beginTransaction();
        try
        {
            $user = $this->create($request->all());
            $this->adicionales($user->id_usuario,$request->all());
            // After creating the user send an email with the random token generated in the create method above
//            $persona=$user->persona;
//            $datospersona=$persona->datospersona;
//            $maildatos= new EmailRegistroDatosCampana($user,$persona,$datospersona);
            $maildatos= new EmailRegistroDatosCampana($user);
            Mail::to('soporte@contapp.mx')->send($maildatos);
            $email = new EmailVerification(new User(['email_token' => $user->email_token, 'name' => $user->name]));
            Mail::to($user->email)->send($email);
            $user->assignRole('user');
            DB::commit();
//            Session::flash('message', '!Te hemos enviado un mail de verificacion!');
            return view('auth.mailEnviado');
        }
        catch(Exception $e)
        {
            DB::rollback();
            return back();
        }
    }
    public function verify($token)
    {
        // The verified method has been added to the user model and chained here
        // for better readability
//        User::where('email_token',$token)->firstOrFail()->verified();
        $user=User::whereRaw('email_token ="'.$token.'"AND  verified = 0')->firstOrFail();

        session(['token' => $user->email_token ,'mail' => $user->email]);

        return view('auth.confirmacion');
    }

    public function terminateRegister(Request $request)
    {
        if(session()->has('token')){

            $validacion=Validator::make($request->all(), [
            'password' => 'required|between:6,8|confirmed',
            ],['password.between'=>'El campo de contraseña debe contener entre 6 y 8 caracteres',
            ]);


            if($validacion->fails())
            {
                return back()->withErrors($validacion);

            }else{
                $user=User::where('email_token',session('token'))->firstOrFail();
                $user->password = bcrypt($request['password']);
                $user->verified();
                $user->save();
                session()->flush();
                return view('auth.bienvenida');
            }
        }
        $this->redirectTo('/register');
    }

}
