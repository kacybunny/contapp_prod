<?php

namespace App\Http\Controllers;

use App\Models\Factura;
use App\Models\PagosFact;
use App\Models\personas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use XSLTProcessor;
use App\Models\User;
use Artisaninweb\SoapWrapper\SoapWrapper;
use SoapClient;
use Illuminate\Support\Facades\DB;
use App\Models\Tempfac;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Watson\BootstrapForm\Facades\BootstrapForm;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columnas = array(
            ["head" => "Tipo de factura"],
            ["head" => "Razón Social"],
            ["head" => "Fecha"],
            ["head" => "Monto total"],
//            ["head" => "Acciones"]
        );

        $facturastemp=Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->get();

    $datos = [];
        foreach ($facturastemp as $facturatemp) {
            $temp = [];
            $temp[] = $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp[] = $facturatemp->fecha;
            $temp[] = '$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
//            $temp[] = '';
            $datos[] = $temp;
        }

        return view('factura.listadotemp',['facturastemp'=>$facturastemp,'datos' => $datos, 'columnas' => $columnas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $columnas = array(
            ["head" => "Tipo de factura"],
            ["head" => "Razón Social"],
            ["head" => "Fecha"],
            ["head" => "Monto total"],
            ["head" => "Acciones"]
        );

        $facturastemp=Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->get();

        $datos = [];
        foreach ($facturastemp as $facturatemp) {
            $temp = [];
            $temp[] = $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp[] = $facturatemp->fecha;
            $temp[] = '$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
//            $temp[] = '';
            $datos[] = $temp;
        }

        return view('factura.create',['datos' => $datos, 'columnas' => $columnas]);
    }
    public function create2($id)
    {
        $columnas = array(
            ["head" => "Tipo de factura"],
            ["head" => "Razón Social"],
            ["head" => "Fecha"],
            ["head" => "Monto total"],
            ["head" => "Acciones"]
        );

        $usuario = personas::find($id)->usuario;
        $facturastemp=Tempfac::where('id_persona','=',$id)->get();

        $datos = [];
        foreach ($facturastemp as $facturatemp) {
            $temp = [];
            $temp[] = $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp[] = $facturatemp->fecha;
            $temp[] = '$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
//            $temp[] = '';
            $datos[] = $temp;
        }

        $url='';

        return view('backoffice.usuarios.createfactura',['url'=>$url,'datos' => $datos,'id_usr'=>$usuario->id_usuario,'id'=>$id, 'columnas' => $columnas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
//            'file' => 'required',
            'file' => 'required|max:5024',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'resp' => 'error',
                'error' => $validator->errors()
            ],403);
        }
        try {
            $file = $request->file('file');
            $extensio = $request->file->getClientOriginalExtension();
            if (strtolower($extensio) != 'xml') {
                return response()->json([
                    'resp' => 'error',
                    'error' => ['file' => array('El archivo no es un archivo xml ', 'estatus' => 'mime')],
                ], 403);
            }
//            $fileName = $file->getClientOriginalName();
            $fileName = Auth::user()->persona->rfc.'-'.uniqid().'.'.$extensio;
            Storage::disk('public')->put('/temp/'.$fileName, \File::get($file));
        }catch(\Exception $e){
            return response()->json([
                'resp' => 'error',
                'error' => ['file'=>array('El archivo no es un archivo valido','estatus'=>$e->getMessage())],
            ],403);
        }

//        $request->file->move(storage_path('app/public/assets'), $fileName);
//        $xml->xpath('//cfdi:Comprobante');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Emisor');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:RegimenFiscal');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Complemento//tfd:TimbreFiscalDigital');
//            //ruta al archivo XML del CFDI
//
//            $xmlFile=Storage::disk('public')->get('/assets/'.$fileName);
//            $xmlFile = mb_convert_encoding( $xmlFile, 'UTF-8' );
//
//            // Ruta al archivo XSLT
//
//            $xslFile=Storage::disk('public')->get('/assets/xslt/cadenaoriginal_3_2.xslt');
//            $xslFile = mb_convert_encoding( $xslFile, 'UTF-8' );
//
//            // Crear un objeto DOMDocument para cargar el CFDI
//            $xmldoc = new DOMDocument("1.0","UTF-8");
//            // Cargar el CFDI
//            $xmldoc->loadXML($xmlFile);
//
//            // Crear un objeto DOMDocument para cargar el archivo de transformación XSLT
//            $xsldoc = new DOMDocument();
//            $xsldoc->load('C:/xampp/htdocs/contap/laravelcontapp/storage/app/public/assets/xslt/cadenaoriginal_3_2.xslt');
//
//            // Crear el procesador XSLT que nos generará la cadena original con base en las reglas descritas en el XSLT
//            $proc = new XSLTProcessor;
//            $proc->registerPHPFunctions();
//            // Cargar las reglas de transformación desde el archivo XSLT.
//            $proc->importStyleSheet($xsldoc);
//            // Generar la cadena original y asignarla a una variable
//            $cadenaOriginal = $proc->transformToXML($xmldoc);
//
//            echo $cadenaOriginal;
//            $soapWrapper=new SoapWrapper();
//
//            $soapWrapper->add('ConsultaCFDIService',function ($service) {
//                $service
//                    ->wsdl('https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl')
//                    ->trace(true)                                                   // Optional: (parameter: true/false)
//                    ->options( ['stream_context'=>stream_context_create( ['http' => ['timeout'=>1] ] ) ]);   // Optional: Set some extra options
//            });
//
//            $data="?re=".$xml['Receptor']['@attributes']['rfc']."&rr=".$xml['Emisor']['@attributes']['rfc']."&tt=".str_pad(number_format($xml['@attributes']['total'],6,".",""),17,0,STR_PAD_LEFT)."&id=".strtoupper($xml['Complemento']['TimbreFiscalDigital']['@attributes']['UUID']);
//            $soapWrapper->call('ConsultaCFDIService', ['expresionImpresa'=>$data]);

        DB::beginTransaction();
        try{

            $xml =collect( Parser::xmlname('/temp/'.$fileName));
            try {
                $options = array('trace' => true, 'stream_context' => stream_context_create(['http' => ['timeout' => 1]]));

                $client = new SoapClient("https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl", $options);
                //uso: asumimos que tenemos  el RFC del emisor, el RFC del receptor
                //     el total y el uuid (folio fiscal).
                $data = "?re=" . $xml['emisor']['@attributes']['rfc'] . "&rr=" . $xml['receptor']['@attributes']['rfc'] . "&tt=" . number_format($xml['@attributes']['total'], 6, ".", "") . "&id=" . strtoupper($xml['complemento']['timbrefiscaldigital']['@attributes']['uuid']);
                $resultado = $client->Consulta(['expresionImpresa' => $data]);
                $xml['validate'] = ['codigo' => $resultado->ConsultaResult->CodigoEstatus, 'estado' => $resultado->ConsultaResult->Estado];
            }catch (\Exception $e){
                $xml['validate'] = ['codigo' => 'invalido', 'estado' => 'error webservice'];
                Storage::disk('public')->delete('/temp/'.$fileName);
                return response()->json([
                    'resp' => 'error',
                    'error' => ['file'=>array('El servicio de validacion no esta disponible intenta mas tarde'),'estatus'=>$xml['validate']['estado']],
                ],403);
            }
//            $xml['validate'] = ['codigo' => 'Vigente', 'estado' => 'Vigente'];
            if ($xml['validate']['estado'] != 'Vigente'){
                Storage::disk('public')->delete('/temp/'.$fileName);
                return response()->json([
                    'resp' => 'error',
                    'error' => ['file'=>array('Esta factura no esta timbrada en el sat'),'estatus'=>$xml['validate']['estado']],
                ],403);
            }else {
                $tipo="";
                if ($xml['emisor']['@attributes']['rfc'] == strtoupper(Auth::user()->persona->rfc)){
                    $tipo = 'Emitida';
                }elseif ($xml['receptor']['@attributes']['rfc'] == strtoupper(Auth::user()->persona->rfc)) {
                    $tipo = 'Recibida';
                }else{
                    Storage::disk('public')->delete('/temp/'.$fileName);
                    return response()->json([
                        'resp' => 'error',
                        'error' => ['file'=>array('El archivo no conincide con su rfc revise su rfc o su archivo'),'estatus'=>$xml['validate']['estado']],
                    ],403);
                }
                $buscar=[];
                $buscar[]=['uuid','=',$xml['complemento']['timbrefiscaldigital']['@attributes']['uuid']];
                if(!Tempfac::where($buscar)->exists() && !Factura::where($buscar)->exists()){
                $tempfact = new Tempfac();
                $fecha=Carbon::createFromFormat('Y-m-d H:i:s',str_replace('T',' ',$xml['@attributes']['fecha']))->toDateString();
                $tempfact->uuid=$xml['complemento']['timbrefiscaldigital']['@attributes']['uuid'];
                $tempfact->file=$fileName;
                $tempfact->fecha=$fecha;
                $tempfact->tipo=$tipo;
                $tempfact->factura = $xml;
                $tempfact->id_persona = Auth::user()->persona->id_persona;
                $tempfact->save();
                }else{
                    if(Factura::where($buscar)->exists()){
                        Storage::disk('public')->delete('/temp/'.$fileName);
                    }
                    return response()->json([
                        'resp' => 'error',
                        'error' => ['file'=>array('Esta Factura ya está cargada en el sistema, no se puede duplicar','estatus'=>'cargada')],
                    ],403);
                }
                DB::commit();
            }

        }catch(\Exception $e){
            DB::rollback();
            Storage::disk('public')->delete('/temp/'.$fileName);
            return response()->json([
                'resp' => 'error',
                'error' => ['file'=>array('Este archivo no es un archivo valido','estatus'=>$e->getMessage())],
            ],403);

        }
//        if(count($xml['cfdi:Conceptos']['cfdi:Concepto'])>1){
//            $conceptos =$xml = collect($xml['cfdi:Conceptos']['cfdi:Concepto'])->flatten(1)->unique(function ($item) {
//                return $item['cantidad'].$item['unidad'].$item['descripcion'].$item['valorUnitario'].$item['importe'];
//            });
//
//            $xml['cfdi:Conceptos']['cfdi:Concepto']=$conceptos;
//        }
//        return back()
//            ->with('success','File Uploaded successfully.')
//            ->with('path',$fileName);
        return response()->json([
            'resp' => 'success',
            'xml' => $xml
        ]);
    }

    public function store2(Request $request , $id)
    {
        $validator = Validator::make($request->all(), [
//            'file' => 'required',
            'file' => 'required|max:5024',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'resp' => 'error',
                'error' => $validator->errors()
            ],403);
        }
        $persona = personas::find($id);
        try {
            $file = $request->file('file');
            $extensio = $request->file->getClientOriginalExtension();
            if (strtolower($extensio) != 'xml') {
                return response()->json([
                    'resp' => 'error',
                    'error' => ['file' => array('El archivo no es un archivo xml ', 'estatus' => 'mime')],
                ], 403);
            }
//            $fileName = $file->getClientOriginalName();
            $fileName = $persona->rfc.'-'.uniqid().'.'.$extensio;
            Storage::disk('public')->put('/temp/'.$fileName, \File::get($file));
        }catch(\Exception $e){
            return response()->json([
                'resp' => 'error',
                'error' => ['file'=>array('El archivo no es un archivo valido','estatus'=>$e->getMessage())],
            ],403);
        }

//        $request->file->move(storage_path('app/public/assets'), $fileName);
//        $xml->xpath('//cfdi:Comprobante');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Emisor');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:RegimenFiscal');
//        $xml->xpath('//cfdi:Comprobante//cfdi:Complemento//tfd:TimbreFiscalDigital');
//            //ruta al archivo XML del CFDI
//
//            $xmlFile=Storage::disk('public')->get('/assets/'.$fileName);
//            $xmlFile = mb_convert_encoding( $xmlFile, 'UTF-8' );
//
//            // Ruta al archivo XSLT
//
//            $xslFile=Storage::disk('public')->get('/assets/xslt/cadenaoriginal_3_2.xslt');
//            $xslFile = mb_convert_encoding( $xslFile, 'UTF-8' );
//
//            // Crear un objeto DOMDocument para cargar el CFDI
//            $xmldoc = new DOMDocument("1.0","UTF-8");
//            // Cargar el CFDI
//            $xmldoc->loadXML($xmlFile);
//
//            // Crear un objeto DOMDocument para cargar el archivo de transformación XSLT
//            $xsldoc = new DOMDocument();
//            $xsldoc->load('C:/xampp/htdocs/contap/laravelcontapp/storage/app/public/assets/xslt/cadenaoriginal_3_2.xslt');
//
//            // Crear el procesador XSLT que nos generará la cadena original con base en las reglas descritas en el XSLT
//            $proc = new XSLTProcessor;
//            $proc->registerPHPFunctions();
//            // Cargar las reglas de transformación desde el archivo XSLT.
//            $proc->importStyleSheet($xsldoc);
//            // Generar la cadena original y asignarla a una variable
//            $cadenaOriginal = $proc->transformToXML($xmldoc);
//
//            echo $cadenaOriginal;
//            $soapWrapper=new SoapWrapper();
//
//            $soapWrapper->add('ConsultaCFDIService',function ($service) {
//                $service
//                    ->wsdl('https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl')
//                    ->trace(true)                                                   // Optional: (parameter: true/false)
//                    ->options( ['stream_context'=>stream_context_create( ['http' => ['timeout'=>1] ] ) ]);   // Optional: Set some extra options
//            });
//
//            $data="?re=".$xml['Receptor']['@attributes']['rfc']."&rr=".$xml['Emisor']['@attributes']['rfc']."&tt=".str_pad(number_format($xml['@attributes']['total'],6,".",""),17,0,STR_PAD_LEFT)."&id=".strtoupper($xml['Complemento']['TimbreFiscalDigital']['@attributes']['UUID']);
//            $soapWrapper->call('ConsultaCFDIService', ['expresionImpresa'=>$data]);

        DB::beginTransaction();
        try{

            $xml =collect( Parser::xmlname('/temp/'.$fileName));
            try {
                $options = array('trace' => true, 'stream_context' => stream_context_create(['http' => ['timeout' => 1]]));

                $client = new SoapClient("https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl", $options);
                //uso: asumimos que tenemos  el RFC del emisor, el RFC del receptor
                //     el total y el uuid (folio fiscal).
                $data = "?re=" . $xml['emisor']['@attributes']['rfc'] . "&rr=" . $xml['receptor']['@attributes']['rfc'] . "&tt=" . number_format($xml['@attributes']['total'], 6, ".", "") . "&id=" . strtoupper($xml['complemento']['timbrefiscaldigital']['@attributes']['uuid']);
                $resultado = $client->Consulta(['expresionImpresa' => $data]);
                $xml['validate'] = ['codigo' => $resultado->ConsultaResult->CodigoEstatus, 'estado' => $resultado->ConsultaResult->Estado];
            }catch (\Exception $e){
                $xml['validate'] = ['codigo' => 'invalido', 'estado' => 'error webservice'];
                Storage::disk('public')->delete('/temp/'.$fileName);
                return response()->json([
                    'resp' => 'error',
                    'error' => ['file'=>array('El servicio de validacion no esta disponible intenta mas tarde'),'estatus'=>$xml['validate']['estado']],
                ],403);
            }
//            $xml['validate'] = ['codigo' => 'Vigente', 'estado' => 'Vigente'];
            if ($xml['validate']['estado'] != 'Vigente'){
                Storage::disk('public')->delete('/temp/'.$fileName);
                return response()->json([
                    'resp' => 'error',
                    'error' => ['file'=>array('Esta factura no esta timbrada en el sat'),'estatus'=>$xml['validate']['estado']],
                ],403);
            }else {
                $tipo="";
                if ($xml['emisor']['@attributes']['rfc'] == strtoupper($persona->rfc)){
                    $tipo = 'Emitida';
                }elseif ($xml['receptor']['@attributes']['rfc'] == strtoupper($persona->rfc)) {
                    $tipo = 'Recibida';
                }else{
                    Storage::disk('public')->delete('/temp/'.$fileName);
                    return response()->json([
                        'resp' => 'error',
                        'error' => ['file'=>array('El archivo no conincide con su rfc revise su rfc o su archivo'),'estatus'=>$xml['validate']['estado']],
                    ],403);
                }
                $buscar=[];
                $buscar[]=['uuid','=',$xml['complemento']['timbrefiscaldigital']['@attributes']['uuid']];
                if(!Tempfac::where($buscar)->exists() && !Factura::where($buscar)->exists()){
                    $tempfact = new Tempfac();
                    $fecha=Carbon::createFromFormat('Y-m-d H:i:s',str_replace('T',' ',$xml['@attributes']['fecha']))->toDateString();
                    $tempfact->uuid=$xml['complemento']['timbrefiscaldigital']['@attributes']['uuid'];
                    $tempfact->file=$fileName;
                    $tempfact->fecha=$fecha;
                    $tempfact->tipo=$tipo;
                    $tempfact->factura = $xml;
                    $tempfact->id_persona = $persona->id_persona;
                    $tempfact->save();
                }else{
                    if(Factura::where($buscar)->exists()){
                        Storage::disk('public')->delete('/temp/'.$fileName);
                    }
                    return response()->json([
                        'resp' => 'error',
                        'error' => ['file'=>array('Esta Factura ya está cargada en el sistema, no se puede duplicar','estatus'=>'cargada')],
                    ],403);
                }
                DB::commit();
            }

        }catch(\Exception $e){
            DB::rollback();
            Storage::disk('public')->delete('/temp/'.$fileName);
            return response()->json([
                'resp' => 'error',
                'error' => ['file'=>array('Este archivo no es un archivo valido','estatus'=>$e->getMessage())],
            ],403);

        }
//        if(count($xml['cfdi:Conceptos']['cfdi:Concepto'])>1){
//            $conceptos =$xml = collect($xml['cfdi:Conceptos']['cfdi:Concepto'])->flatten(1)->unique(function ($item) {
//                return $item['cantidad'].$item['unidad'].$item['descripcion'].$item['valorUnitario'].$item['importe'];
//            });
//
//            $xml['cfdi:Conceptos']['cfdi:Concepto']=$conceptos;
//        }
//        return back()
//            ->with('success','File Uploaded successfully.')
//            ->with('path',$fileName);
        return response()->json([
            'resp' => 'success',
            'xml' => $xml
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show($factura)
    {
        $tipo='';
        if($factura==1)
        {
            $tipo='Recibida';
        } else{
            $tipo='Emitida';
        }
        $buscar=[];
        $buscar[]=['id_persona','=',Auth::user()->persona->id_persona];
        $buscar[]=['tipo','=',$tipo];
        $plan="";
        if(!empty(Auth::user()->conekta_plan )){
            $plan=Auth::user()->plazo->plan->plan;
        }else{
            $plan='Básico';
        }
        $facturastemp=Factura::where($buscar)->get();

        $datos = [];
        foreach ($facturastemp as $facturatemp) {
            $temp = [];
            $temp[] = $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp[] = $facturatemp->fecha;
            $temp[] = '$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
            if ($facturatemp->pagada){
                $temp[] = 'Pagada '.$facturatemp->fechapago;
                //                '<a class="btn btn-success editpayfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar no pagada" data-mensaje="<p>Marcar como no pagada la factura con monto total de '.'$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',').'</p>" data-url="'.url('nopagada/'.$facturatemp->id_factura).'" type="button">
//                <i class="fa fa-edit">
//                                </i>
//                            </a>'
                $temp[] = '<a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturatemp->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';

            }else{
                $temp[] = 'Pendiente';
                $temp[] = '<a class="btn btn-success payfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar pagada" data-mensaje=
"<p>Marcar como pagada la factura con monto total de '.'$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',').'
</p>'."
<div class='input-group date bootdatepick'>
  <input id='fechapago' type='text'  placeholder='YYY-MM-DD' class='form-control'>
  <span class='input-group-addon'>
  <i class='glyphicon glyphicon-th'>
  </i>
  </span>
</div>
<p>Cantidad</p>
<input id='pago' type='text' placeholder='0.00' class='form-control'>".'" data-url="'.url('pagada/'.$facturatemp->id_factura).'"
 type="button">
                                <i class="fa fa-money">
                                </i>
                            </a>
                            <a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturatemp->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';

            }
            $temp[]=$facturatemp->factura;
            $temp[]=$facturatemp->pagado;
            $temp[]=number_format($facturatemp->total,2);
            $datos[] = $temp;
        }

        $columnas = array(
            ["head" => "Tipo de factura"],
            ["head" => "Razón social"],
            ["head" => "Fecha"],
            ["head" => "Monto total"],
            ["head" => "Estatus"],
            ["head" => "Acciones"]
        );
        return view('factura.listado',['facturastemp'=>$facturastemp,'datos' => $datos, 'columnas' => $columnas]);
    }

    function busquedapagos(){
        $metodo='GET';
        $url=url('pagosfactsearch');
        return view('factura.searchpagos',['metodo'=>$metodo,'url'=>$url]);
    }

    function pagosfact(Request $request){
        $buscar=[];
        $buscarentre=[];
        $contador=1;

        $buscar[] = ['id_usuario', '=', Auth::id()];

        if(!empty($request['tipo'])) {
            if($request['tipo']==3)
            {
                $buscar[] = ['tipo', 'like', '%%'];
            }else{
                $buscar[] = ['tipo', '=', $request['tipo']];
            }
            $contador++;
        }
        if(!empty($request['fecha_inicio']) && !empty($request['fecha_final'])) {
            $buscarentre[] = ['fecha',[$request['fecha_inicio'],$request['fecha_final']]];
            $contador++;
        }
        if(!empty($request['monto_minimo']) && !empty($request['monto_maximo'])) {
            $buscarentre[] = ['pago', [$request['monto_minimo'] , $request['monto_maximo']]];
            $contador++;
        }
        switch ($contador) {
            case 0:
                Session::flash('message', 'No se encontraron resultados');
                Session::flash('tipo', 'error');
                return back()->withInput();
                break;
            case 2:
            case 1:
               if (collect($buscarentre)->count() && collect($buscar)->count()) {
                    $facturas = PagosFact::where($buscar)->whereBetween($buscarentre[0][0], $buscarentre[0][1])->get();
               } else if (collect($buscarentre)->count()) {
                    if (collect($buscarentre)->count() == 2) {
                        $facturas = PagosFact::whereBetween($buscarentre[0][0], $buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0], $buscarentre[1][1])->get();
                    } else {
                        $facturas = PagosFact::whereBetween($buscarentre[0][0], $buscarentre[0][1])->get();
                    }
                } else if (collect($buscar)->count()) {
                    $facturas = PagosFact::where($buscar)->get();
                }
                break;
            case 4:
            case 3:
                if (collect($buscarentre)->count() && collect($buscar)->count()) {
                    if (collect($buscarentre)->count() == 2) {
                        $facturas = PagosFact::where($buscar)
                            ->whereBetween($buscarentre[0][0], $buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0], $buscarentre[1][1])->get();
                    } else {
                        $facturas = PagosFact::where($buscar)
                            ->whereBetween($buscarentre[0][0], $buscarentre[0][1])->get();
                    }
                }
                break;
        }

        if(!collect($facturas)->count()){
            Session::flash('message','No se encontraron resultados');
            Session::flash('tipo', 'error');
            return back()->withInput();
        }

        $datos = [];
        foreach ($facturas as $facturatemp) {
            $temp = [];
            if($facturatemp->tipo == 1){
                $temp[] = 'Pago';
            }else{
                $temp[] = 'Cobro';
            }
            $datosfactura = $facturatemp->factura->factura;
            if ($facturatemp->factura->tipo=='Emitida'){
                $temp[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->factura->tipo=='Recibida'){
                $temp[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp[] = $facturatemp->fecha;
            $temp[] = '$ '.number_format($facturatemp->pago, 2, '.', ',');
            $temp[] = '<a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a> ';
            $temp[] = $datosfactura;
            $temp[] = Carbon::createFromFormat('Y-m-d H:i:s',str_replace('T',' ',$datosfactura['@attributes']['fecha']))->toDateString();
            $temp[] = $facturatemp->factura->tipo;
            $datos[] = $temp;
        }

        $columnas = array(
            ["head" => "Tipo"],
            ["head" => "Razón social"],
            ["head" => "Fecha pago"],
            ["head" => "Monto pago"],
            ["head" => "Acciones"]
        );
        return view('factura.listadopagos',['datos' => $datos, 'columnas' => $columnas]);
    }

    public function buscarfactura(Request $request,$id){

        $buscar=[];
        $buscarrfc=[];
        $buscarrazon=[];
        $buscarentre=[];
        $contador=1;
        $tipo='';

        $buscar[] = ['id_persona', '=', Auth::user()->persona->id_persona];

        if($request['tipo']==1)
        {
            $tipo = 'Emitida';
        } else if ($request['tipo']==2) {
            $tipo='Recibida';
        }
        if(!empty($request['tipo'])) {
            if($request['tipo']==3)
            {
                $buscar[] = ['tipo', 'like', '%%'];
            }else{
                $buscar[] = ['tipo', '=', $tipo];
            }
            $contador++;
        }

        if(!empty($request['fecha_inicio']) && !empty($request['fecha_final'])) {
            $buscarentre[] = ['fecha',[$request['fecha_inicio'],$request['fecha_final']]];
            $contador++;
        }
        if(!empty($request['monto_minimo']) && !empty($request['monto_maximo'])) {
            $buscarentre[] = ['total', [$request['monto_minimo'] , $request['monto_maximo']]];
            $contador++;
        }
        if(!empty($request['rfc'])) {
            $buscarrfc[] = ['rfcEmisor', 'like', '%' . $request['rfc'] . '%'];
            $buscarrfc[] = ['rfcReceptor', 'like', '%' . $request['rfc'] . '%'];
            $contador++;
            $contador++;
        }
        if(!empty($request['razon'])) {
            $buscarrazon[] = ['nombreEmisor', 'like', '%' . $request['razon'] . '%'];
            $buscarrazon[] = ['nombreReceptor', 'like', '%' . $request['razon'] . '%'];
            $contador++;
            $contador++;
        }
        switch ($contador) {
            case 0:
                Session::flash('message','No se encontraron resultados');
                Session::flash('tipo', 'error');
                return back()->withInput();
                break;
            case 2:
            case 1:
                if(collect($buscarrazon)->count()){
                    $facturas= Factura::Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]])->get();
                }
                else if(collect($buscarrfc)->count()){
                    $facturas= Factura::Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]])->get();
                }
                else if(collect($buscarentre)->count() && collect($buscar)->count()){
                    $facturas= Factura::where($buscar)->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                }
                else if(collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $facturas= Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $facturas= Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }
                else if(collect($buscar)->count()){
                    $facturas= Factura::where($buscar)->get();
                }
                break;
            case 4:
            case 3:
                if(collect($buscarrazon)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $facturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $facturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }

                } else if(collect($buscarrfc)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $facturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else {
                        $facturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                } else if(collect($buscarrazon)->count() && collect($buscar)->count()){
                    $facturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                        $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                    })->get();
                } else if(collect($buscarrfc)->count() && collect($buscar)->count()){
                    $facturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                        $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                    })->get();
                }else if(collect($buscarentre)->count() && collect($buscar)->count()){
                    if(collect($buscarentre)->count()==2){
                    $facturas = Factura::where($buscar)
                        ->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                        ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $facturas = Factura::where($buscar)
                            ->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }else if(collect($buscarrazon)->count() && collect($buscarrfc)->count()){
                    $facturas= Factura::Where(function ($query) use ($buscarrfc) {
                        $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                    })->Where(function ($query) use ($buscarrazon) {
                        $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                    })->get();
                }else if(collect($buscarrazon)->count()&& collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2) {
                        $facturas = Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->get();
                    }else{
                        $facturas = Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])->Where(function ($query) use ($buscarrazon) {
                                $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                            })->get();
                    }

                }else if(collect($buscarentre)->count()&& collect($buscarrfc)->count()){
                    if(collect($buscarentre)->count()==2){
                        $facturas= Factura::Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $facturas= Factura::Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }

                break;
            case 5:
            case 6:
            case 7:
            case 8:
                if(collect($buscarrazon)->count() && collect($buscarrfc)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $facturas= Factura::where($buscar)->
                        Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $facturas= Factura::where($buscar)->
                        Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }

                }else if(collect($buscarrazon)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $facturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $facturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }

                } else if(collect($buscarrfc)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $facturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else {
                        $facturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }
                break;
        }
        if(!collect($facturas)->count()){
            Session::flash('message','No se encontraron resultados');
            Session::flash('tipo', 'error');
            return back()->withInput();
        }

        $datos = [];
        foreach ($facturas as $facturatemp) {
            $temp = [];
            $temp[] = $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp[] = $facturatemp->fecha;
            $temp[] = '$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
            if ($facturatemp->pagada){
                $temp[] = 'Pagada '.$facturatemp->fechapago;
//                '<a class="btn btn-success editpayfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar no pagada" data-mensaje="<p>Marcar como no pagada la factura con monto total de '.'$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',').'</p>" data-url="'.url('nopagada/'.$facturatemp->id_factura).'" type="button">
//                <i class="fa fa-edit">
//                                </i>
//                            </a>'
                $temp[] = '<a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturatemp->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
            }else{
                $temp[] = 'Pendiente';
                $temp[] = '<a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturatemp->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>
                        <a class="btn btn-danger eliminar" data-url="'.url('/'.$facturatemp->id_factura).'" type="button">
                                <i class="fa fa-trash">
                                </i>
                            </a>';
            }
            $temp[]=$facturatemp->factura;
            $temp[]=$facturatemp->pagado;
            $temp[]=number_format($facturatemp->total,2);
            $datos[] = $temp;
        }

        $columnas = array(
            ["head" => "Tipo de factura"],
            ["head" => "Razón social"],
            ["head" => "Fecha"],
            ["head" => "Monto total"],
            ["head" => "Estatus"],
            ["head" => "Acciones"]
        );
        return view('factura.listado',['datos' => $datos, 'columnas' => $columnas]);
//        return redirect('resultados');
    }

    public function buscarform($id)
    {
        $metodo='GET';
        $url=url('facturasresult/'.$id);
        return view('factura.search',['metodo'=>$metodo,'url'=>$url]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(Factura $factura)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Factura $factura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $factura= Factura::find($id);
        Storage::disk('public')->delete('/'.$factura->persona->rfc.'/'.'xml/'.$factura->tipo.'/'.$factura->file);
        $factura->delete();
        return response()->json(['success'=>'Exito']);
    }

    public function guardafactura(){
        if(Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->exists()){
            $facturastemp=Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->get();
            foreach ($facturastemp as $facturatemp){
                $arraycreate=[];
                $arraycreate['id_persona']= $facturatemp->id_persona;
                $arraycreate['tipo']=$facturatemp->tipo;
                $arraycreate['fecha']=$facturatemp->fecha;
                $arraycreate['uuid']=$facturatemp->uuid;
                $arraycreate['file']=$facturatemp->file;
                $arraycreate['version']=$facturatemp->factura['@attributes']['version'];
                $arraycreate['serie']=$facturatemp->factura['complemento']['timbrefiscaldigital']['@attributes']['nocertificadosat'];
                $arraycreate['folio']=$facturatemp->factura['@attributes']['nocertificado'];
                if($facturatemp->factura['@attributes']['version']=='3.2'){
                    $arraycreate['formaPago']=$facturatemp->factura['@attributes']['formadepago'];
                    $arraycreate['metodoPago']=$facturatemp->factura['@attributes']['metododepago'];
                }else if ($facturatemp->factura['@attributes']['version']=='3.3'){
                    $arraycreate['formaPago']=$facturatemp->factura['@attributes']['formapago'];
                    $arraycreate['metodoPago']=$facturatemp->factura['@attributes']['metodopago'];
                }
                $arraycreate['subtotal']=$facturatemp->factura['@attributes']['subtotal'];
                $arraycreate['total']=$facturatemp->factura['@attributes']['total'];
                $arraycreate['TipoComprobante']=$facturatemp->factura['@attributes']['tipodecomprobante'];
                $arraycreate['rfcEmisor']=$facturatemp->factura['emisor']['@attributes']['rfc'];
                $arraycreate['nombreEmisor']=$facturatemp->factura['emisor']['@attributes']['nombre'];
                $arraycreate['rfcReceptor']=$facturatemp->factura['receptor']['@attributes']['rfc'];
                $arraycreate['nombreReceptor']=$facturatemp->factura['receptor']['@attributes']['nombre'];
                $arraycreate['factura']=$facturatemp->factura;
                Factura::create($arraycreate);
                Storage::disk('public')->move('/temp/'.$facturatemp->file, '/'.Auth::user()->persona->rfc.'/'.'xml/'.$facturatemp->tipo.'/'.$facturatemp->file);
                $facturatemp->delete();
            }
            Session::flash('message', '! Se cargaron correctamente las facturas !');
            Session::flash('tipo', 'success');
            return redirect('factura/create');
        }else{
            Session::flash('message', '! No hay facturas para cargar !');
            Session::flash('tipo', 'error');
            return redirect('factura/create');
        }

    }

    public function guardafactura2($id){
        if(Tempfac::where('id_persona','=',$id)->exists()){
            $facturastemp=Tempfac::where('id_persona','=',$id)->get();
            foreach ($facturastemp as $facturatemp){
                $arraycreate=[];
                $arraycreate['id_persona']= $facturatemp->id_persona;
                $arraycreate['tipo']=$facturatemp->tipo;
                $arraycreate['fecha']=$facturatemp->fecha;
                $arraycreate['uuid']=$facturatemp->uuid;
                $arraycreate['file']=$facturatemp->file;
                $arraycreate['version']=$facturatemp->factura['@attributes']['version'];
                $arraycreate['serie']=$facturatemp->factura['complemento']['timbrefiscaldigital']['@attributes']['nocertificadosat'];
                $arraycreate['folio']=$facturatemp->factura['@attributes']['nocertificado'];
                if($facturatemp->factura['@attributes']['version']=='3.2'){
                    $arraycreate['formaPago']=$facturatemp->factura['@attributes']['formadepago'];
                    $arraycreate['metodoPago']=$facturatemp->factura['@attributes']['metododepago'];
                }else if ($facturatemp->factura['@attributes']['version']=='3.3'){
                    $arraycreate['formaPago']=$facturatemp->factura['@attributes']['formapago'];
                    $arraycreate['metodoPago']=$facturatemp->factura['@attributes']['metodopago'];
                }
                $arraycreate['subtotal']=$facturatemp->factura['@attributes']['subtotal'];
                $arraycreate['total']=$facturatemp->factura['@attributes']['total'];
                $arraycreate['TipoComprobante']=$facturatemp->factura['@attributes']['tipodecomprobante'];
                $arraycreate['rfcEmisor']=$facturatemp->factura['emisor']['@attributes']['rfc'];
                $arraycreate['nombreEmisor']=$facturatemp->factura['emisor']['@attributes']['nombre'];
                $arraycreate['rfcReceptor']=$facturatemp->factura['receptor']['@attributes']['rfc'];
                $arraycreate['nombreReceptor']=$facturatemp->factura['receptor']['@attributes']['nombre'];
                $arraycreate['factura']=$facturatemp->factura;
                Factura::create($arraycreate);
                Storage::disk('public')->move('/temp/'.$facturatemp->file, '/'.Auth::user()->persona->rfc.'/'.'xml/'.$facturatemp->tipo.'/'.$facturatemp->file);
                $facturatemp->delete();
            }
            Session::flash('message', '! Se cargaron correctamente las facturas !');
            Session::flash('tipo', 'success');
            return redirect('factura/create');
        }else{
            Session::flash('message', '! No hay facturas para cargar !');
            Session::flash('tipo', 'error');
            return redirect('factura/create');
        }

    }
    public function limpiarlista2 ($id){
        if(Tempfac::where('id_persona','=',$id)->exists()){
            $facturastemp=Tempfac::where('id_persona','=',$id)->get();
            foreach ($facturastemp as $facturatemp){
                Storage::disk('public')->delete('/temp/'.$facturatemp->file);
                $facturatemp->delete();
            }
            Session::flash('message', '! Se limpió correctamente la lista de facturas !');
            Session::flash('tipo', 'success');
            return redirect('factura/create');
        }else{
            Session::flash('message', '! No hay facturas cargadas !');
            Session::flash('tipo', 'error');
            return redirect('factura/create');
        }
    }
    public function limpiarlista (){
        if(Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->exists()){
            $facturastemp=Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->get();
            foreach ($facturastemp as $facturatemp){
                Storage::disk('public')->delete('/temp/'.$facturatemp->file);
                $facturatemp->delete();
            }
            Session::flash('message', '! Se limpió correctamente la lista de facturas !');
            Session::flash('tipo', 'success');
            return redirect('factura/create');
        }else{
            Session::flash('message', '! No hay facturas cargadas !');
            Session::flash('tipo', 'error');
            return redirect('factura/create');
        }
    }

    public function facturapagada(Request $request ,$id){

        $factura = Factura::find($id);

        $factura->fechapago=$request['form']['fecha'];
        $pago = new PagosFact();
        $pago->id_factura = $factura->id_factura;
        $pago->id_usuario= Auth::id();
        if($factura->tipo=='Recibida'){
            $pago->tipo=1;
        }else{
            $pago->tipo=2;
        }
        $pago->pago = $request['form']['pago'];
        $pago->fecha=$request['form']['fecha'];
        $pago->save();

        if(PagosFact::where('id_factura','=',$id)->exists()){
            $totalpagado=PagosFact::where('id_factura','=',$id)->sum('pago');
        }else{
            $totalpagado = $request['form']['pago'];
        }
        $factura->pagado= $totalpagado;
        $factura->save();
        $total=str_replace(',','',number_format($factura->total,2));
        $compara =((float)$total - (float)$totalpagado);
        if($compara == 0){
            $factura->pagada=1;
            $factura->save();
        }
        return response()->json(['resp'=>'success']);
    }
    public function facturanopagada($id){
        $factura = Factura::find($id);
        $factura->pagada=0;
        $factura->fechapago=null;
        $factura->save();
        return response()->json(['resp'=>'success']);
    }

    public function facturastemporales(){
        $facturastemp=Tempfac::where('id_persona','=',Auth::user()->persona->id_persona)->get();
        $datos=[];
        foreach ($facturastemp as $facturatemp)
        {
            $temp=[];
            $temp['Tipo de factura']= $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp['Razón Social'] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp['Razón Social'] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp['Fecha']=$facturatemp->fecha;
            $temp['Monto total']='$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
            $temp['Acciones']='<a class="btn btn-danger eliminar" data-url="'.url('api/eliminatempfact/'.$facturatemp->id_tempfact).'" type="button">
                                <i class="fa fa-trash">
                                </i>
                            </a>
                            <a class="btn btn-dark detallefact" type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a>';
            $temp['datosfact']=$facturatemp->factura;
            $datos[]=$temp;
        }
        return response()->json(['data'=>$datos]);
    }

    public function facturastemporales2($id){
        $facturastemp=Tempfac::where('id_persona','=',$id)->get();
        $datos=[];
        foreach ($facturastemp as $facturatemp)
        {
            $temp=[];
            $temp['Tipo de factura']= $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp['Razón Social'] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp['Razón Social'] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp['Fecha']=$facturatemp->fecha;
            $temp['Monto total']='$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
            $temp['Acciones']='<a class="btn btn-danger eliminar" data-url="'.url('api/eliminatempfact/'.$facturatemp->id_tempfact).'" type="button">
                                <i class="fa fa-trash">
                                </i>
                            </a>
                            <a class="btn btn-dark detallefact" type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a>';
            $temp['datosfact']=$facturatemp->factura;
            $datos[]=$temp;
        }
        return response()->json(['data'=>$datos]);
    }

    public function eliminafactemp($id){

       $tempfact= Tempfac::find($id);
        Storage::disk('public')->delete('/temp/'.$tempfact->file);
        $tempfact->delete();
        return response()->json(['success'=>'Exito']);
    }

    public function descarga($id){

        if(Factura::where('id_factura','=',$id)->exists()){
            $tempfact= Factura::find($id);
        }else{
            return back();
        }

        return response()->download(storage_path('app/public/'). '/'.Auth::user()->persona->rfc.'/'.'xml/'.$tempfact->tipo.'/'.$tempfact->file);
//        $file = Storage::disk('local')->get($detalle->archivo);

//        return Response::make($file, 200, [
//            'Content-Type' => 'application/pdf',
//            'Content-Disposition' => 'inline; filename="'.$peticion->ticket.'"'
//        ]);

//        return response($file, 200)
//            ->header('Content-Type','application/pdf' );
    }

}
