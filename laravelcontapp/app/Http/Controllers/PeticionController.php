<?php

namespace App\Http\Controllers;

use App\Models\DetalleCalculoimp;
use App\Models\DetallePosicionfinan;
use App\Models\DetalleDeclaracion;
use App\Models\TipoPeticion;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Peticion;
use App\Models\Meses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Response;
use App\Notifications\PostNewNotification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class PeticionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $columnas = array(
            ["head" => "Número de solicitud"],
            ["head" => "RFC"],
            ["head" => "Mes"],
            ["head" => "Año"],
            ["head" => "Tipo"],
            ["head" => "Usuario asignado"],
            ["head" => "Acciones"]
        );
        $peticiones = Peticion::all();
        $datos=[];
        foreach ($peticiones as $peticion) {
            $temp = [];
            $temp[] = $peticion->ticket;
            $temp[] = $peticion->usuario->persona->rfc;
            $temp[] = $peticion->meses->mes;
            $temp[] = $peticion->ano;
            $temp[] = $peticion->tipo->tipo;
            if(!empty($peticion->usuarioasignado)){
                $temp[] = $peticion->usuarioasignado->name;
                $text='Reasignar';
            }else{
                $temp[] = 'Ninguno';
                $text='Asignar';
            }
            $temp[] ='<a class="btn btn-primary " data-toggle= "tooltip" data-placement="top" title="" data-original-title="'.$text.'"  href="' . url('back/peticiones/' . $peticion->ticket.'/reasigna') . '" type="button">
                        <i class="fa fa-user">
                        </i>
                        </a>';
            $datos[] = $temp;
        }
        return view('backoffice.peticion.listadogestiona',['datos' => $datos, 'columnas' => $columnas]);
    }

    public function reasigna($ticket)
    {
        $usersdata=User::role('backuser')->get();

        $users=$usersdata->mapWithKeys(function ($item){
            return[$item['id_usuario']=>$item['name']];
        });

        if (Peticion::where('ticket', '=', $ticket)->exists()){
            $peticion = Peticion::where('ticket', '=', $ticket)->get()[0];
        }else{
            back();
        }
        if(!empty($peticion->usuarioasignado)){
            $temp[] = $peticion->usuarioasignado->name;
            $text='Reasignar';
        }else{
            $temp[] = 'Ninguno';
            $text='Asignar';
        }

        $url = 'back/peticiones/'.$ticket.'/reasigna' ;
        $metodo = 'POST' ;
        return view('backoffice.peticion.asigna',['text'=>$text,'users'=>$users,'peticion'=>$peticion,'url'=>$url, 'metodo'=>$metodo]);
    }

    public function asigna(Request $request, $ticket)
    {
        $validator = Validator::make($request->all(), [
            'usuario'=>'required|exists:users,id_usuario',
        ]);
        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }
        if (Peticion::where('ticket', '=', $ticket)->exists()){
            $peticion = Peticion::where('ticket', '=', $ticket)->get()[0];
        }else{
            back();
        }
        $peticion->id_usuarioasignado=$request['usuario'];
        $peticion->save();
        return redirect('back/peticiones');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buscar = [];
        if ($id) {
            $url = 'peticiones/' . $id;
            $metodo = 'PUT';
            $tipos = TipoPeticion::all();
            foreach ($tipos as $tipo) {
                if (strpos($tipo->tipo, $id) !== false) {
                    $buscar[] = ['id_tipo_peticion', '=', $tipo->id_tipo_peticion];
                    $titulo = $tipo->tipo;
                    $idtipo = $tipo->id_tipo_peticion;
                };
            }
        }

        $columnas = array(
            ["head" => "Número de solicitud"],
            ["head" => "RFC"],
            ["head" => "Mes"],
            ["head" => "Año"],
            ["head" => "Tipo"],
            ["head" => "Documentos"],
            ["head" => "Acciones pendientes"]
        );
        if (Auth::user()->hasRole('backuser')) {

            $buscar[] = ['id_usuarioasignado', '=', Auth::id()];
            $peticiones = Peticion::where($buscar)->get();
            $datos = [];
            foreach ($peticiones as $peticion) {
                $boton = '';
                $temp = [];
                switch ($peticion->id_tipo_peticion) {
                    case 1 :
                        $detalle = $peticion->detallecalculo;
                        break;
                    case 2 :
                        $detalle = $peticion->detalleposicion;
                        break;
                    case 3 :
                        $detalle = $peticion->detalledeclaracion;
                        break;
                    default:

                }
                if ($peticion->id_tipo_peticion != 3) {
                    if ($detalle->archivo != null) {
                        $temp[] = $peticion->ticket;
                        $temp[] = '<a class="" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Editar" href="'.url('back/usuarios/'.$peticion->usuario->id_usuario.'/edit').'">
                                '.$peticion->usuario->persona->rfc.'
                            </a>';
                        $temp[] = $peticion->meses->mes;
                        $temp[] = $peticion->ano;
                        $temp[] = $peticion->tipo->tipo;
                        $temp[] = '<a class="btn btn-primary dowload" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descargar '.$titulo.'" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                        $temp[] ='<a class="btn btn-primary filepdf" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Cargar '.$titulo.'" data-mensaje="" data-url="' . url('peticiones/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-upload">
                        </i>
                        </a>';
                        $datos[] = $temp;
                    }
                } else {
                    if ($detalle->archivofinal != null) {
                        $temp[] = $peticion->ticket;
                        $temp[] = '<a class="" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Editar" href="'.url('back/usuarios/'.$peticion->usuario->id_usuario.'/edit').'">
                                '.$peticion->usuario->persona->rfc.'
                            </a>';
                        $temp[] = $peticion->meses->mes;
                        $temp[] = $peticion->ano;
                        $temp[] = $peticion->tipo->tipo;
                        $botones='';
                        $botones2='';
                        if ($detalle->paso == 4 ||$detalle->paso < 4  ) {
                            $botones2 .= '<a class="btn btn-primary filepdf" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Carga declaracion previa" data-mensaje="" data-url="' . url('peticiones/' . $peticion->ticket) . '" type="button">
                                <i class="fa fa-file-pdf-o">
                                </i>
                            </a>';
                        }
                        if ($detalle->paso == 3 ||$detalle->paso < 3) {
                            $botones .= '<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                        }
                        if ($detalle->paso <= 2 ||$detalle->paso < 2) {
                            $botones2 .= '<a class="btn btn-success filepdf" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Carga declaracion final" data-mensaje="" data-url="' . url('back/cargadeclaracion/' . $peticion->ticket) . '" type="button">
                                <i class="fa fa-file-pdf-o">
                                </i>
                            </a>';
                        }
                        if ($detalle->paso <= 1 ||$detalle->paso < 1) {
                            $botones .= '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion final"href="' . url('api/descargadeclaracion/' . $peticion->ticket) . '" type="button">
                            <i class="fa fa-cloud-download">
                            </i>
                            </a>';
                        }
                        $temp[] = $botones;
                        if($detalle->paso != 1){
                            $temp[] = $botones2;
                        }else{
                            $temp[] = '';
                        }
                        $datos[] = $temp;
                    }
                }

            }
            return view('backoffice.peticion.listafiltro', ['titulo' => $titulo, 'url' => $url, 'metodo' => $metodo, 'datos' => $datos, 'columnas' => $columnas]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buscar = [];
        if ($id) {
            $url = 'peticiones/' . $id;
            $metodo = 'PUT';
            $tipos = TipoPeticion::all();
            foreach ($tipos as $tipo) {
                if (strpos($tipo->tipo, $id) !== false) {
                    $buscar[] = ['id_tipo_peticion', '=', $tipo->id_tipo_peticion];
                    $titulo = $tipo->tipo;
                    $idtipo = $tipo->id_tipo_peticion;
                };
            }
        }

        $columnas = array(
            ["head" => "Número de solicitud"],
            ["head" => "RFC"],
            ["head" => "Mes"],
            ["head" => "Año"],
            ["head" => "Tipo"],
            ["head" => "Documentos"],
            ["head" => "Acciones pendientes"]
        );
        if (Auth::user()->hasRole('user')) {

            if (!empty(Auth::user()->plazo)) {
                if ($idtipo == 3) {
                    if (Auth::user()->plazo->plan->plan != 'Completo') {
                        Session::flash('upgrade', 1);
                        return redirect('/');
                    }
                }
            }else{
                if ($idtipo == 3) {
                    Session::flash('upgrade', 1);
                    return redirect('/');
                }
            }
            $buscar[] = ['id_usuario', '=', Auth::id()];
            $userdate = Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->created_at)->year;
            $date = Carbon::now()->year;
            if ($date != $userdate) {
                $anio = $this->aniosanteriores($userdate, $date);
            } else {
                $anio = $this->aniosanteriores($userdate, $date);
            }

            $datameses = [];
            foreach ($anio as $key => $val) {
                if ($val == $date) {
                    if($idtipo != 3){
                        $datameses[$val] = $this->mesesrestantes($val,true,false);
                    }else{
                        $datameses[$val] = $this->mesesrestantes($val);
                    }

                } else {
                    if ($date) {

                    }
                    $datameses[$val] = $this->mesesrestantes($val, false);
                }

            }

            $peticiones = Peticion::where($buscar)->orderBy('created_at','desc')->get();
            $datos = [];
            foreach ($peticiones as $peticion) {
                $temp = [];
                $temp[] = $peticion->ticket;
                $temp[] = $peticion->usuario->persona->rfc;
                $temp[] = $peticion->meses->mes;
                $temp[] = $peticion->ano;
                $temp[] = $peticion->tipo->tipo;
                switch ($peticion->id_tipo_peticion) {
                    case 1 :
                        $detalle = $peticion->detallecalculo;
                        break;
                    case 2 :
                        $detalle = $peticion->detalleposicion;
                        break;
                    case 3 :
                        $detalle = $peticion->detalledeclaracion;
                        break;
                    default:

                }
                if ($peticion->id_tipo_peticion != 3) {
                    if ($detalle->archivo != null) {
                        $temp[] = '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descargar '.$titulo.'" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                        $temp[] = 'Terminado';
                    } else {
                        $temp[] = '';
                        $temp[] = 'En espera';
                    }
                } else {
                    $botones='';
                    $botones2='';
                    if ($detalle->paso == 3 || $detalle->paso < 3) {
                        $botones .= '<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                    }
                    if ($detalle->paso == 3 ) {
                        $botones2 .= '<a class="btn btn-success autoriza" data-href-no="' . url('api/acepta/2/' . $peticion->ticket) . '" data-href-yes="' . url('api/acepta/1/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-check-square-o">
                        Autorizar
                        </i>
                        </a>';
//                        '<a class="btn btn-warning" href="' . url('api/acepta/2/' . $peticion->ticket) . '" type="button">
//                        <i class="fa fa-close">
//                        </i>
//                        </a>';
                    }
                    if ($detalle->paso == 1 || $detalle->paso < 2 ) {
                        $botones .= '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion final" href="' . url('api/descargadeclaracion/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                    }
                    if ($detalle->paso == 1 ) {
                        $botones2 .= '<a class="btn btn-success payfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar pagada" data-mensaje="<p>Marcar como pagada la declaracion '.$peticion->ticket.' del mes de '.$peticion->meses->mes.'</p>'."<div class='input-group date bootdatepick'>
<input id='fechapago' type='text'  placeholder='YYY-MM-DD' class='form-control'><span class='input-group-addon'><i class='glyphicon glyphicon-th'></i></span>
</div>".'" data-url="'.url('declaracionpagada/'.$peticion->ticket).'" type="button">
                                <i class="fa fa-money">
                                </i>
                            </a>';
                    }
                    $temp[] = $botones;
                    if($botones2 != ''){
                        $temp[] = $botones2;
                    }else if ($detalle->paso > 0) {
                        $temp[] = 'En espera';
                    }else{
                        $temp[] = 'Terminado';
                    }

                }
                $datos[] = $temp;
            }

            return view('peticiones.edit', ['titulo' => $titulo, 'url' => $url, 'metodo' => $metodo, 'anio' => $anio, 'meses' => collect($datameses), 'datos' => $datos, 'columnas' => $columnas]);
        }
        if (Auth::user()->hasRole('backuser')) {

            $buscar[] = ['id_usuarioasignado', '=', Auth::id()];
            $peticiones = Peticion::where($buscar)->get();
            $datos = [];
            foreach ($peticiones as $peticion) {
                $boton = '';
                $temp = [];
                switch ($peticion->id_tipo_peticion) {
                    case 1 :
                        $detalle = $peticion->detallecalculo;
                        break;
                    case 2 :
                        $detalle = $peticion->detalleposicion;
                        break;
                    case 3 :
                        $detalle = $peticion->detalledeclaracion;
                        break;
                    default:

                }
                if ($peticion->id_tipo_peticion != 3) {
                    if ($detalle->archivo == null) {
                        $temp[] = $peticion->ticket;
                        $temp[] = '<a class="" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Editar" href="'.url('back/usuarios/'.$peticion->usuario->id_usuario.'/edit').'">
                                '.$peticion->usuario->persona->rfc.'
                            </a>';
                        $temp[] = $peticion->meses->mes;
                        $temp[] = $peticion->ano;
                        $temp[] = $peticion->tipo->tipo;
                        $temp[] ='';
                        $temp[] = '<a class="btn btn-primary filepdf" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Cargar '.$titulo.'" data-mensaje="" data-url="' . url('peticiones/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-file-pdf-o">
                        </i>
                        </a>';
                        $datos[] = $temp;
                    }
                } else {
                    if ($detalle->archivofinal == null) {
                        $temp[] = $peticion->ticket;
                        $temp[] = '<a class="" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Editar" href="'.url('back/usuarios/'.$peticion->usuario->id_usuario.'/edit').'">
                                '.$peticion->usuario->persona->rfc.'
                            </a>';
                        $temp[] = $peticion->meses->mes;
                        $temp[] = $peticion->ano;
                        $temp[] = $peticion->tipo->tipo;
                        $botones='';
                        $botones2='';
                        if ($detalle->paso == 4 ||$detalle->paso < 4  ) {
                            $botones2 .= '<a class="btn btn-primary filepdf" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Carga declaracion previa" data-mensaje="" data-url="' . url('peticiones/' . $peticion->ticket) . '" type="button">
                                <i class="fa fa-file-pdf-o">
                                </i>
                            </a>';
                        }
                        if ($detalle->paso == 3 ||$detalle->paso < 3) {
                            $botones .= '<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                        }
                        if ($detalle->paso == 2 ||$detalle->paso < 2) {
                            $botones2 .= '<a class="btn btn-success filepdf" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Carga declaracion final" data-mensaje="" data-url="' . url('back/cargadeclaracion/' . $peticion->ticket) . '" type="button">
                                <i class="fa fa-file-pdf-o">
                                </i>
                            </a>';
                        }
                        if ($detalle->paso == 1 ||$detalle->paso < 1) {
                            $botones .= '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descargadeclaracion/' . $peticion->ticket) . '" type="button">
                            <i class="fa fa-cloud-download">
                            </i>
                            </a>';
                        }
                        $temp[] = $botones;
                        if($botones2 != ''){
                            $temp[] = $botones2;
                        }else if ($detalle->paso > 0) {
                            $temp[] = 'En espera';
                        }
                        $datos[] = $temp;
                    }
                }

            }
            return view('backoffice.peticion.listafiltro', ['titulo' => $titulo, 'url' => $url, 'metodo' => $metodo, 'datos' => $datos, 'columnas' => $columnas]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id) {
            if (Auth::user()->hasRole('user')) {
                DB::beginTransaction();
                try {
                    $tipos = TipoPeticion::all();
                    foreach ($tipos as $tipo) {
                        if (strpos($tipo->tipo, $id) !== false) {
                            $buscar[] = ['id_tipo_peticion', '=', $tipo->id_tipo_peticion];
                            $titulo = $tipo->tipo;
                        };
                    }
                    $tipo = TipoPeticion::where($buscar)->first();
                    $peticion = new Peticion();
                    $peticion->id_tipo_peticion = $tipo->id_tipo_peticion;
                    $peticion->id_usuario = Auth::id();
                    $peticion->mes = $request['mes'];
                    $peticion->ano = $request['anio'];
                    $peticion->save();
                    $fecha = str_replace('-', '', Carbon::now()->toDateString());
                    $detalle = '';
                    $id_detalle = '';
                    switch ($tipo->id_tipo_peticion) {
                        case 1 :
                            $detalle = new DetalleCalculoimp();
                            $detalle->save();
                            $id_detalle = $detalle->id_detalle_calculo;
                            break;
                        case 2 :
                            $detalle = new DetallePosicionfinan();
                            $detalle->save();
                            $id_detalle = $detalle->id_detalle_posicion;
                            break;
                        case 3 :
                            $detalle = new DetalleDeclaracion();
                            $detalle->save();
                            $id_detalle = $detalle->id_detalle_decalarcion;
                            break;
                        default:

                    }
                    $detalle->ticket = $fecha . '-' . $peticion->id_peticion . $id_detalle . $tipo->tipo[0];
                    $detalle->save();
                    $peticion->ticket = $fecha . '-' . $peticion->id_peticion . $id_detalle . $tipo->tipo[0];
                    $peticion->id_detalle = $id_detalle;
                    $peticion->save();
                    DB::commit();
                    $url = 'peticiones/' . $id . '/edit';
                    return view('peticiones.aftercreate', ['titulo' => $titulo, 'si' => $url]);
                } catch (\Exception $e) {
                    DB::rollback();
                    return back();
                }
            }
            if (Auth::user()->hasRole('backuser')) {
                $peticion = Peticion::where('ticket', '=', $id)->first();
                switch ($peticion->id_tipo_peticion) {
                    case 1 :
                        $detalle = $peticion->detallecalculo;
                        break;
                    case 2 :
                        $detalle = $peticion->detalleposicion;
                        break;
                    case 3 :
                        $detalle = $peticion->detalledeclaracion;
                        break;
                    default:

                }
                $rfc = $peticion->usuario->persona->rfc;
                try {
                    if (!$request->hasFile('archivo')) {
                        Session::flash('file', '!Debes cargar un archivo!');
                        return back();
                    }
                    $file = $request->file('archivo');
                    $extensio = $request->file('archivo')->getClientOriginalExtension();
//                    if (strtolower($extensio) != 'pdf') {
//                        Session::flash('file', '!El archivo que intentas subir no es un pdf!');
//                        return back();
//                    }
//                    $nombre = $file->getClientOriginalName();
                    $url = $rfc . '/pdf/' . $peticion->tipo->tipo . '/' . $peticion->ticket . '.' . $extensio;

                    Storage::disk('public')->put($url, \File::get($file));
                } catch (\Exception $e) {
                    Session::flash('file', '!No se ha podido cargar el archivo!');
                    return back();
                }
                if ($peticion->id_tipo_peticion != 3) {
                    $detalle->archivo = $url;
                    $detalle->save();
                    $user = $peticion->usuario;
                    $ruta = explode(' ', $peticion->tipo->tipo)[0];
                    $user->notify(new PostNewNotification(['user' => $user->name, 'id' => $user->id_usuario, 'title' => $peticion->tipo->tipo, 'data' => 'se ha procesado su solicitud', 'url' => url('peticiones/' . $ruta . '/edit')]));

                } else {
                    $detalle->paso=3;
                    $detalle->archivofinal = null;
                    $detalle->archivorevision = $url;
                    $detalle->save();
                    $user = $peticion->usuario;
                    $ruta = explode(' ', $peticion->tipo->tipo)[0];
                    $user->notify(new PostNewNotification(['user' => $user->name, 'id' => $user->id_usuario, 'title' => $peticion->tipo->tipo, 'data' => 'se ha procesado su solicitud', 'url' => url('peticiones/' . $ruta . '/edit')]));
                }
                return redirect('peticiones/' . $ruta . '/edit');
            }
        }
    }

    public function aceptadeclaracion ($act , $ticket){
        if (Peticion::where('ticket', '=', $ticket)->exists()){
            $peticion = Peticion::where('ticket', '=', $ticket)->get()[0];
            switch ($peticion->id_tipo_peticion) {
                case 3 :
                    $detalle = $peticion->detalledeclaracion;
                    break;
                default:
            }
            if ($act==1){
                $detalle->paso=2;
            }else{
                $detalle->paso=4;
            }
            $detalle->save();
            $ruta = explode(' ', $peticion->tipo->tipo)[0];
//            return redirect(URL::previous());
//            return redirect('peticiones/' . $ruta . '/edit');
            return redirect('peticiones/' . $ruta .'/edit');
        }else{
            back();
        }
    }

    public function updatedeclaracion(Request $request, $id)
    {
        $peticion = Peticion::where('ticket', '=', $id)->first();
        switch ($peticion->id_tipo_peticion) {
            case 3 :
                $detalle = $peticion->detalledeclaracion;
                break;
            default:
        }
        $rfc = $peticion->usuario->persona->rfc;
        try {
            if (!$request->hasFile('archivo')) {
                Session::flash('file', '!Debes cargar un archivo!');
                return back();
            }
            $file = $request->file('archivo');
            $extensio = $request->file('archivo')->getClientOriginalExtension();
//                    if (strtolower($extensio) != 'pdf') {
//                        Session::flash('file', '!El archivo que intentas subir no es un pdf!');
//                        return back();
//                    }
//                    $nombre = $file->getClientOriginalName();
            $url = $rfc . '/pdf/' . $peticion->tipo->tipo . '/' . $peticion->ticket .'_final'. '.' . $extensio;

            Storage::disk('public')->put($url, \File::get($file));
        } catch (\Exception $e) {
            Session::flash('file', '!No se ha podido cargar el archivo!');
            return back();
        }
        if ($peticion->id_tipo_peticion == 3) {
            $detalle->paso=1;
            $detalle->archivofinal = $url;
            $detalle->save();
            $user = $peticion->usuario;
            $ruta = explode(' ', $peticion->tipo->tipo)[0];
            $user->notify(new PostNewNotification(['user' => $user->name, 'id' => $user->id_usuario, 'title' => $peticion->tipo->tipo, 'data' => 'se ha procesado su solicitud', 'url' => url('peticiones/' . $ruta . '/edit')]));
        }
        return redirect('peticiones/' . $ruta );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function declaracionpagada(Request $request ,$id){

        if (DetalleDeclaracion::where('ticket','=',$id)->exists()){
            $peticion = DetalleDeclaracion::where('ticket','=',$id)->get();
            $peticion[0]->paso=0;
            $peticion[0]->pago=$request['form']['fecha'];
            $peticion[0]->save();
            return response()->json(['resp'=>'success',]);
        }else{
            return response()->json(['resp'=>'error',]);
        }

    }

    public function aniosanteriores($anioini, $aniofin)
    {
        $diff = intval($aniofin) - intval($anioini);
        if (!$diff) {
            $resp = [$aniofin => $aniofin];
        } else {
            $anio = [];
            $year = intval($anioini);
            for ($i = 0; $diff >= $i; $i++) {
                $yeartemp = $year + $i;
                $temp = ['year' => $yeartemp];
                $anio[] = $temp;
            }
            $resp = collect($anio)->mapWithKeys(function ($item) {
                return [$item['year'] => $item['year']];
            });
        }
        return $resp;
    }

    private function mesesrestantes($anio, $filter = true, $filter2 = true, $mesesonly = true, $mesesbackyear = [])
    {
        $meses = Meses::all();
        if ($mesesonly) {
            if ($filter) {
                $restantes = collect($meses)->mapWithKeys(function ($item) use ($anio,$filter2) {
                    $date = Carbon::now();
                    $mes = $date->month;
                    if($filter2){
                        if ($item['number'] < $mes) {
                            return [$item->number => array('mes' => $item->mes, 'anio' => $anio)];
                        } else {
                            return [];
                        }
                    }else{
                        if ($item['number'] <= $mes) {
                            return [$item->number => array('mes' => $item->mes, 'anio' => $anio)];
                        } else {
                            return [];
                        }
                    }
                });
            } else {
                $restantes = collect($meses)->mapWithKeys(function ($item) use ($anio) {
                    return [$item->number => array('mes' => $item->mes, 'anio' => $anio)];
                });
            }
            return $restantes;
        } else {
            if ($mesesbackyear == []) {
                return $meses;
            } else {
                $resp = collect($meses)->mapWithKeys(function ($item) use ($anio, $mesesbackyear) {
                    $acept = collect($mesesbackyear)->search($item->number);
                    if ($acept >= 0) {
                        return [$item->number => array('mes' => $item->mes, 'anio' => $anio)];
                    } else {
                        return [];
                    }
                });
                return $resp;
            }
        }
    }

    public function descarga($ticket)
    {
        if (Peticion::where('ticket', '=', $ticket)->exists()) {
            $peticion = Peticion::where('ticket', '=', $ticket)->first();
            switch ($peticion->id_tipo_peticion) {
                case 1 :
                    $detalle = DetalleCalculoimp::where('ticket', '=', $ticket)->first();
                    break;
                case 2 :
                    $detalle = DetallePosicionfinan::where('ticket', '=', $ticket)->first();
                    break;
                case 3 :
                    $detalle = DetalleDeclaracion::where('ticket', '=', $ticket)->first();
                    break;
                default:
                    return back();
            }

        } else {
            return back();
        }

        if ($peticion->id_tipo_peticion!=3){
            return response()->download(storage_path('app/public/') . $detalle->archivo);
        }else{
            return response()->download(storage_path('app/public/') . $detalle->archivorevision);
        }

//        $file = Storage::disk('local')->get($detalle->archivo);

//        return Response::make($file, 200, [
//            'Content-Type' => 'application/pdf',
//            'Content-Disposition' => 'inline; filename="'.$peticion->ticket.'"'
//        ]);

//        return response($file, 200)
//            ->header('Content-Type','application/pdf' );
    }

    public function descargadeclaracion($ticket)
    {
        $buscar=[];
        $buscar[]=['id_tipo_peticion','=',3];
        $buscar[]=['ticket', '=', $ticket];
        if (Peticion::where($buscar)->exists()) {
            $peticion = Peticion::where($buscar)->first();
            switch ($peticion->id_tipo_peticion) {
                case 3 :
                    $detalle = DetalleDeclaracion::where('ticket', '=', $ticket)->first();
                    break;
                default:
                    return back();
            }
        } else {
            return back();
        }
            return response()->download(storage_path('app/public/') . $detalle->archivofinal);
    }

    public function asignarpeticion(Request $request, $id)
    {
        if (Peticion::where('ticket', '=', $id)->exists()) {
            $peticion = Peticion::where('ticket', '=', $id)->first();
            switch ($peticion->id_tipo_peticion) {
                case 1 :
                    $detalle = DetalleCalculoimp::where('ticket', '=', $id)->first();
                    $detalle->id_usuario = Auth::id();
                    $detalle->save();
                    $id_detalle = $detalle->id_detalle_calculo;
                    break;
                case 2 :
                    $detalle = DetallePosicionfinan::where('ticket', '=', $id)->first();
                    $detalle->id_usuario = Auth::id();
                    $detalle->save();
                    $id_detalle = $detalle->id_detalle_posicion;
                    break;
                case 3 :
                    $detalle = DetalleDeclaracion::where('ticket', '=', $id)->first();
                    $detalle->id_usuario = Auth::id();
                    $detalle->save();
                    $id_detalle = $detalle->id_detalle_decalarcion;
                    break;
                default:
            }
            $peticion->id_usuarioasignado = Auth::id();
            $peticion->save();
            return response()->json(['resp' => 'success'], 200);
        } else {
            return response()->json(['resp' => 'error'], 500);
        }
    }

    public function buscarform($id)
    {
        $metodo = 'GET';
        $titulo = '';
        $tipos = TipoPeticion::all();
        foreach ($tipos as $tipo) {
            if (strpos($tipo->tipo, $id) !== false) {
//                $buscar[] = ['id_tipo_peticion', '=', $tipo->id_tipo_peticion];
                $titulo = $tipo->tipo;
            };
        }
        $meses= collect(Meses::all()->toArray());
        $meses=$meses->mapWithKeys(function ($item){
            return[$item['number']=>$item['mes']];
        });
        $url = url('peticionesresult/' . $id);
        return view('peticiones.search', ['metodo' => $metodo, 'url' => $url, 'titulo' => $titulo,'meses'=>$meses]);
    }

    public function buscarpeticion(Request $request, $id)
    {
        $buscar = [];
        $buscarentre=[];
        $columnas = array(
            ["head" => "Número de solicitud"],
            ["head" => "RFC"],
            ["head" => "Mes"],
            ["head" => "Año"],
            ["head" => "Tipo"],
            ["head" => "Documentos"],
            ["head" => "Acciones pendientes"]
        );

        if ($id) {
            $tipos = TipoPeticion::all();
            foreach ($tipos as $tipo) {
                if (strpos($tipo->tipo, $id) !== false) {
                    $buscar[] = ['id_tipo_peticion', '=', $tipo->id_tipo_peticion];
                    $titulo = $tipo->tipo;
                    $idtipo = $tipo->id_tipo_peticion;
                };
            }
        }
        $buscar[] = ['id_usuario', '=', Auth::id()];
        if(empty($request['fecha_inicio']) && empty($request['fecha_final']) && empty($request['tipo']) && empty($request['mes'])) {
            Session::flash('message','No se encontraron resultados');
            Session::flash('tipo', 'error');
            return back()->withInput();
        }
        if(!empty($request['mes'])) {
            $buscar[] = ['mes', '=', $request['mes']];
        }
        if(!empty($request['fecha_inicio']) && !empty($request['fecha_final'])) {
            $buscarentre[] = ['created_at',[$request['fecha_inicio'],$request['fecha_final']]];
        }

        if (collect($buscarentre)->count()){
            $peticiones = Peticion::where($buscar)->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
        }else{
            $peticiones = Peticion::where($buscar)->get();
        }
        $datos = [];
        foreach ($peticiones as $peticion) {
            $temp = [];
            switch ($peticion->id_tipo_peticion) {
                case 1 :
                    $detalle = $peticion->detallecalculo;
                    break;
                case 2 :
                    $detalle = $peticion->detalleposicion;
                    break;
                case 3 :
                    $detalle = $peticion->detalledeclaracion;
                    break;
                default:
                    return back()->withInput();
            }

                if($request['tipo']==1)
                {
                    if ($peticion->id_tipo_peticion != 3) {
                        if ($detalle->archivo != null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga '.$titulo.'" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                            $temp[] = 'Terminado';
                        }
                    }else{
                        if ($detalle->archivofinal != null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>
                        <a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                            if ($detalle->paso == 1 ) {
                                $temp[] = '<a class="btn btn-success payfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar pago" data-mensaje="<p>Marcar como pagada la declaracion '.$peticion->ticket.' del mes de '.$peticion->meses->mes.'</p>'."<div class='input-group date bootdatepick'>
<input id='fechapago' type='text'  placeholder='YYY-MM-DD' class='form-control'><span class='input-group-addon'><i class='glyphicon glyphicon-th'></i></span>
</div>".'" data-url="'.url('declaracionpagada/'.$peticion->ticket).'" type="button">
                                <i class="fa fa-money">
                                </i>
                            </a>';
                            }else{
                                $temp[] = 'En espera';
                            }
                        }
                    }
                } else if ($request['tipo']==2) {
                    if ($peticion->id_tipo_peticion != 3) {
                        if ($detalle->archivo == null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '';
                            $temp[] = 'En espera';
                        }
                    }else{
                        if ($detalle->archivofinal == null && $detalle->archivorevision == null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '';
                            $temp[] = 'En espera';
                        }else if ($detalle->archivofinal == null && $detalle->archivorevision != null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $botones='';
                            $botones2='';
                            if ($detalle->paso == 3 || $detalle->paso < 3) {
                                $botones .= '<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                            }
                            if ($detalle->paso == 3 ) {
                                $botones2 .= '<a class="btn btn-success autoriza" data-href-no="' . url('api/acepta/2/' . $peticion->ticket) . '" data-href-yes="' . url('api/acepta/1/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-check-square-o">
                        Autorizar
                        </i>
                        </a>';
//                        '<a class="btn btn-warning" href="' . url('api/acepta/2/' . $peticion->ticket) . '" type="button">
//                        <i class="fa fa-close">
//                        </i>
//                        </a>';
                            }
                            if ($detalle->paso == 1 || $detalle->paso < 2 ) {
                                $botones .= '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion final" href="' . url('api/descargadeclaracion/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                            }
                            if ($detalle->paso == 1 ) {
                                $botones2 .= '<a class="btn btn-success payfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar pagada" data-mensaje="<p>Marcar como pagada la declaracion '.$peticion->ticket.' del mes de '.$peticion->meses->mes.'</p>'."<div class='input-group date bootdatepick'>
<input id='fechapago' type='text'  placeholder='YYY-MM-DD' class='form-control'><span class='input-group-addon'><i class='glyphicon glyphicon-th'></i></span>
</div>".'" data-url="'.url('declaracionpagada/'.$peticion->ticket).'" type="button">
                                <i class="fa fa-money">
                                </i>
                            </a>';
                            }
                            $temp[] = $botones;
                            if($botones2 != ''){
                                $temp[] = $botones2;
                            }else if ($detalle->paso > 0) {
                                $temp[] = 'En espera';
                            }else{
                                $temp[] = 'Terminado';
                            }
                        }
                    }
                } else if ($request['tipo']==3 || empty($request['tipo'])) {
                    if ($peticion->id_tipo_peticion != 3) {
                        if ($detalle->archivo != null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '<a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga '.$titulo.'" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                            $temp[] = 'Terminado';
                        }else if ($detalle->archivo == null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '';
                            $temp[] = 'En espera';
                        }
                    }else{
                        if ($detalle->archivofinal != null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>
                        <a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion final" href="' . url('api/descargadeclaracion/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                            if ($detalle->paso == 1 ) {
                                $temp[] = '<a class="btn btn-success payfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar pagada" data-mensaje="<p>Marcar como pagada la declaracion '.$peticion->ticket.' del mes de '.$peticion->meses->mes.'</p>'."<div class='input-group date bootdatepick'>
<input id='fechapago' type='text'  placeholder='YYY-MM-DD' class='form-control'><span class='input-group-addon'><i class='glyphicon glyphicon-th'></i></span>
</div>".'" data-url="'.url('declaracionpagada/'.$peticion->ticket).'" type="button">
                                <i class="fa fa-money">
                                </i>
                            </a>';
                            }else{
                                $temp[] = 'Terminado';
                            }
                        }else if ($detalle->archivofinal == null && $detalle->archivorevision == null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '';
                            $temp[] = 'En espera';
                        }else if ($detalle->archivofinal == null && $detalle->archivorevision != null) {
                            $temp[] = $peticion->ticket;
                            $temp[] = $peticion->usuario->persona->rfc;
                            $temp[] = $peticion->meses->mes;
                            $temp[] = $peticion->ano;
                            $temp[] = $peticion->tipo->tipo;
                            $temp[] = '<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga declaracion previa" href="' . url('api/descarga/' . $peticion->ticket) . '" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
                            if ($detalle->paso == 3 ) {
                                $temp[] = '<a class="btn btn-success autoriza" data-href-no="' . url('api/acepta/2/' . $peticion->ticket) . '" data-href-yes="' . url('api/acepta/1/' . $peticion->ticket) . '" type="button">
                                <i class="fa fa-check-square-o">
                                Autorizar
                                </i>
                                </a>';
                            }else{
                                $temp[] = 'En espera';
                            }
                        }
                    }
                }
            if(collect($temp)->count()){
                $datos[] = $temp;
            }
        }
        if(!collect( $datos)->count()){
            Session::flash('message','No se encontraron resultados');
            Session::flash('tipo', 'error');
            return back()->withInput();
        }
        return view('peticiones.listadoresult', ['titulo' => $titulo, 'datos' => $datos, 'columnas' => $columnas]);
    }
}

