<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\obligaciones;
use App\Models\Actividades;
use App\Models\regimen;
use App\Models\Datosfiscales;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Detalleactividad;
use App\Models\Detalleobligacion;
use App\Models\Detalleregimen;
use App\Models\personas;

class DatosfiscalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id==0){
            $obligaciones = obligaciones::all();
            $actividades = Actividades::all();
            $regimen = regimen::all();
            $persona = Auth::user()->persona;
            $obligacionespersona=$persona->obligaciones;
            if(empty($obligacionespersona)){
                $obligacionespersona = new obligaciones();
            }
            $actividadespersona=$persona->actividades;
            if(empty($actividadespersona)){
                $actividadespersona= new Actividades();
            }
            $regimenpersona=$persona->regimenes;
            if(empty($regimenpersona)){
                $regimenpersona = new regimen();
            }
            $fiscales = Datosfiscales::find($persona->id_datos_fiscales);
            return view('datosfiscales.edit',['regimenpersona'=>$regimenpersona,'actividadespersona'=>$actividadespersona,'obligacionespersona'=>$obligacionespersona,'obligaciones'=>$obligaciones,'actividades'=>$actividades,'regimen'=>$regimen,'fiscales'=>$fiscales,]);
        } else if ($id == 1){
            return view('datosfiscales.editfirmas');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id==0) {
            $validator = Validator::make($request->all(), [
                'tasaiva' => 'required',
            ]);
            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            $persona = Auth::user()->persona;

            $fiscales = Datosfiscales::find($persona->id_datos_fiscales);
            $fiscales->iva = $request['tasaiva'];
            $fiscales->save();

            if (!empty($persona->obligaciones)) {
                $obligaciones = $persona->obligaciones;
                $obligaciones->obligaciones = $request['obligaciones'];
                $obligaciones->fechas=$request['fechainiobl'];
                $obligaciones->save();
            } else {
                $obligaciones = new Detalleobligacion();
                $obligaciones->id_persona = $persona->id_persona;
                $obligaciones->obligaciones = $request['obligaciones'];
                $obligaciones->fechas=$request['fechainiobl'];
                $obligaciones->save();
            }
            if (!empty($persona->actividades)) {
                $actividades = $persona->actividades;
                $actividades->actividades = $request['actividades'];
                $actividades->fechas=$request['fechainiact'];
                $actividades->save();
            } else {
                $actividades = new Detalleactividad();
                $actividades->id_persona = $persona->id_persona;
                $actividades->actividades = $request['actividades'];
                $actividades->fechas=$request['fechainiact'];
                $actividades->save();
            }
            if (!empty($persona->regimenes)) {
                $regimenes = $persona->regimenes;
                $regimenes->regimenes = $request['regimen'];
                $regimenes->fechas=$request['fechainireg'];
                $regimenes->save();
            } else {
                $regimenes = new Detalleregimen();
                $regimenes->id_persona = $persona->id_persona;
                $regimenes->regimenes = $request['regimen'];
                $regimenes->fechas=$request['fechainireg'];
                $regimenes->save();
            }
            return redirect('datosfiscales/1/edit');
        }
        if($id==1){
            $validator = Validator::make($request->all(), [
                'contrasena_SAT' => 'required|same:confirmar_contrasena_SAT',
                'contrasena_llave_privada' => 'required|same:confirma_contrasena_llave_privada',
                'certificado' => 'File',
                'llave_privada' => 'File',
            ]);
            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $fiscales = Datosfiscales::find($persona->id_datos_fiscales);
            $fiscales->pass_sat = $request['contrasena_SAT'];
            $fiscales->cer = $request['certificado'];
            $fiscales->key = $request['llave_privada'];
            $fiscales->pass_privado = $request['contrasena_llave_privada'];
            $fiscales->save();
            return redirect('datosfiscales/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
