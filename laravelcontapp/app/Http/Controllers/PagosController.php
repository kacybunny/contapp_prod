<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Webhooks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Planes;
use App\Models\Plazos;
use App\Models\Pagos;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Precios;


class PagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $columnas = array(
            ["head" => "Tipo"],
            ["head" => "Estatus"],
            ["head" => "Fecha"],
            ["head" => "Acciones"]
        );
        $datos = [];
        Auth::user()->webhook;
        if(Auth::user()->webhook){
            $hooks=Auth::user()->webhook()->orderBy('created_at', 'desc')->get();

            foreach ($hooks as $hook) {
                $temp = [];
                if($hook->tipo == 'subscription.created'){
                    $temp[] = 'Suscripcion';
                    $temp[] = 'Creada';
                    $temp[] = $hook->created_at->format('d M Y - H:i:s');
                    $temp[] = '';
                    $temp[] = $hook->objeto;
                    $datos[] = $temp;
                }
                if($hook->tipo == 'subscription.paid'){
                    $temp[] = 'Suscripcion';
                    $temp[] = 'Pagada';
                    $temp[] = $hook->created_at->format('d M Y - H:i:s');
                    $temp[] = '';
                    $temp[] = $hook->objeto;
                    $datos[] = $temp;
                }if($hook->tipo == 'subscription.canceled'){
                    $temp[] = 'Suscripcion';
                    $temp[] = 'Cancelada';
                    $temp[] = $hook->created_at->format('d M Y - H:i:s');
                    $temp[] = '';
                    $temp[] = $hook->objeto;
                    $datos[] = $temp;
                }
                if($hook->tipo == 'charge.created'){
                    $temp[] = 'Cargo';
                    $temp[] = 'Pendiente';
                    $temp[] = $hook->created_at->format('d M Y - H:i:s');
                    $temp[] = '';
                    $temp[] = $hook->objeto;
                    $datos[] = $temp;
                }
                if($hook->tipo == 'charge.paid'){
                    $temp[] = 'Cargo';
                    $temp[] = 'Cobrado';
                    $temp[] = $hook->created_at->format('d M Y - H:i:s');
                    $temp[] = '<a class="btn btn-dark detalle" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a>';
                    $temp[] = $hook->objeto;
                    $datos[] = $temp;
                }

            }
        }


        return view('pagos.listado',[ 'datos' => $datos, 'columnas' => $columnas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $planes=[];

        $planesmodel = Planes::all();

        foreach ($planesmodel as $llave=>$valor) {
            $planarray = [];
            $planarray['plan'] = $valor->plan;
            $precio = 0;
            $dt = Carbon::now();
            if (Precios::where('id_plan', '=', $valor->id_plan)->where('fechaactivo', '>=', $dt)->exists()) {
                $precios = Precios::where('id_plan', '=', $valor->id_plan)->where('fechaactivo', '>=', $dt)->get();
                $precio = $precios[0]->precio;
            } else if (Precios::where('id_plan', '=', $valor->id_plan)->where('fechaactivo', '=', '0000-00-00')->exists()) {
                $precios = Precios::where('id_plan', '=', $valor->id_plan)->where('fechaactivo', '=', '0000-00-00')->get();
                $precio = $precios[0]->precio;
            }
            $planarray['precio']=$precio;
            $planarray['titulo']=$valor->titulo;
            $planarray['mensaje']=$valor->mensaje;
            $planarray['url']=$valor->url;
            $planarray['plazo']=[];
            if($precio != 0) {
                $plazosmodel = $valor->plazo;
                foreach ($plazosmodel as $key => $value) {
                    if($value->visible == 1){
                        $plazoarray = [];
                        if ($value->porcentaje == 0) {
                            $descuentotemp = 1;
                        } else {
                            $descuentotemp = $value->porcentaje;
                        }
                        $plazoarray['num'] = $value->id_plazo;
                        $plazoarray['titulo'] = $value->titulo;
                        if (!empty($precio)) {
                            if ($value->intervalo=='month') {
                                $plazoarray['mensaje'] = str_replace('{%MonedaMX%}', '$ ' . number_format($descuentotemp * $precio * 1, 2, '.', ','), $value->mesnaje);
                                $plazoarray['modalidad']='Pago mensual';
                                $plazoarray['precio'] = $descuentotemp * $precio;
                                $plazoarray['subtotal'] = $descuentotemp * $precio * 1;
                                $plazoarray['total'] = ($descuentotemp * $precio * 1) * 1.16;
                                $plazoarray['precioiva'] = (($descuentotemp * $precio * 1) * 1.16) - ($descuentotemp * $precio * 1);
                            }else if($value->intervalo=='year'){
                                $plazoarray['mensaje'] = str_replace('{%MonedaMX%}', '$ ' . number_format($descuentotemp * $precio * 12, 2, '.', ','), $value->mesnaje);
                                $plazoarray['modalidad']='Pago anual';
                                $plazoarray['precio'] = $descuentotemp * $precio;
                                $plazoarray['subtotal'] = $descuentotemp * $precio * 12;
                                $plazoarray['total'] = ($descuentotemp * $precio * 12) * 1.16;
                                $plazoarray['precioiva'] = (($descuentotemp * $precio * 12) * 1.16) - ($descuentotemp * $precio * 12);
                            }
                        }
                        $plazoarray['iva'] = '16 %';
                        $planarray['plazo'][] = $plazoarray;
                    }

                }
            }
            $planes[]=$planarray;
        }

        return view('pagos.checkout',['planes'=>$planes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Conekta\Conekta::setApiKey("key_YD1knCmKwZCT1F75DceYFw");
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');
        DB::beginTransaction();
        try {
            $user=Auth::user();
            $buscar=[];
            $buscar[]=['id_usuario','=',Auth::id()];
            if (empty($user->conekta_id)){
                $persona=$user->persona;
                $customerarray=[];

                $customerarray["name"]= $persona->nombre;
                $customerarray["email"]=$user->email;
//                if(!empty($persona->datospersona->telefono)){
////                    $customerarray["phone"]=$persona->datospersona->telefono;
//                }
                $customerarray["payment_sources"]=array(array(
                    "type" => "card",
                    "token_id" => $request['conektaTokenId']
//                    "token_id" => 'tok_test_visa_4242'
                ));
                $customer=\Conekta\Customer::create(
                    $customerarray
                );

                $user->conekta_id = $customer['id'];
                $datoscustomer=collect(json_decode(json_encode($customer),true))->toArray();
                $user->last_four = $datoscustomer['payment_sources']['data'][0]['last4'];
                $user->card_type = $datoscustomer['payment_sources']['data'][0]['brand'];
                $user->save();

            }else{
                try{
                    $customer = \Conekta\Customer::find($user->conekta_id);
                    if (!empty($user->conekta_plan)) {
                        $customer->subscription->cancel();
                    }
                }catch (\Exception $ex){
                    $persona=$user->persona;
                    $customerarray=[];

                    $customerarray["name"]= $persona->nombre;
                    $customerarray["email"]=$user->email;
//                if(!empty($persona->datospersona->telefono)){
////                    $customerarray["phone"]=$persona->datospersona->telefono;
//                }
                    $customerarray["payment_sources"]=array(array(
                        "type" => "card",
                        "token_id" => $request['conektaTokenId']
//                    "token_id" => 'tok_test_visa_4242'
                    ));
                    $customer=\Conekta\Customer::create(
                        $customerarray
                    );

                    $user->conekta_id = $customer['id'];
                    $datoscustomer=collect(json_decode(json_encode($customer),true))->toArray();
                    $user->last_four = $datoscustomer['payment_sources']['data'][0]['last4'];
                    $user->card_type = $datoscustomer['payment_sources']['data'][0]['brand'];
                    $user->save();
                }
            }
            if (User::where($buscar)->whereNull('conekta_subscription')) {
                $subscription = $customer->createSubscription(
                    array(
                        'plan' => (string)$request['planid']
                    )
                );

                $user->conekta_subscription = $subscription['id'];
                $user->conekta_plan = $subscription['plan_id'];
                $datossuscripcion = collect(json_decode(json_encode($subscription), true))->toArray();
                $user->subscription_ends_at = $datossuscripcion['billing_cycle_end'];
                $user->save();
            }else{

            }
            DB::commit();
        } catch (\Conekta\ProccessingError $error){
            DB::rollback();

            return response()->json(['error'=>$error],403);
//            echo $error->getMesage();
        } catch (\Conekta\ParameterValidationError $error){
            DB::rollback();
            return response()->json(['error'=>$error],403);
//            echo $error->getMessage();
        } catch (\Conekta\Handler $error){
            DB::rollback();
            return response()->json(['error'=>$error],403);
//            echo $error->getMessage();
        } catch (\Exception $e){
            DB::rollback();

            return response()->json(['error'=>$e],403);
//            echo $e->getMessage();
        }
        $dt = Carbon::now();
        return response()->json(['resp'=>'ok','suscripcion'=>$user->conekta_subscription]);

//        $customer = Conekta_Customer::find($customer);
//        $customer->delete();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
