<?php

namespace App\Http\Controllers;

use App\Models\Precios;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Planes;
use App\Models\Plazos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PlanesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function indexplan()
    {
        $planes=Planes::all();
        $temp=[];
        $datos=[];
        foreach ($planes as $plan)
        {
            $temp=[];
            $temp[]=$plan->plan;
            $temp[]=$plan->titulo;
            $temp[]=$plan->mensaje;
            $temp[]=$plan->url;
            $temp[] = '<a class="btn btn-success" href="'.url('back/updateplan/'.$plan->id_plan).'" type="button">
                        <i class="fa fa-pencil-square-o">
                        </i>
                        </a>';
            $datos[]=$temp;
        }
        $columnas = array(
            ["head"=> "Plan"],
            ["head"=> "Titulo"],
            ["head"=> "Mensaje"],
            ["head"=> "Url"],
            ["head"=> "Acciones"]
        );
        return view('backoffice.planes.listado',['columnas'=>$columnas, 'datos'=>$datos]);
    }
    public function indexplazo()
    {
        $plazos=Plazos::all();
        $temp=[];
        $datos=[];
        foreach ($plazos as $plazo)
        {
            $temp=[];
            $temp[]=$plazo->plan->plan;
            $temp[]=$plazo->titulo;
            $temp[]=$plazo->mesnaje;
            if($plazo->intervalo=='month'){
                $temp[]='Mensual';
            }else if ($plazo->intervalo=='year'){
                $temp[]='Anual';
            }
            $temp[]=$plazo->prueba;
            $temp[]=$plazo->porcentaje;
            if($plazo->visible==1){
                $temp[]='Si';
            }else {
                $temp[]='No';
            }
            $temp[] = '<a class="btn btn-success" href="'.url('back/updateplazo/'.$plazo->id_plazo).'" type="button">
                        <i class="fa fa-pencil-square-o">
                        </i>
                        </a>';
            $datos[]=$temp;
        }
        $columnas = array(
            ["head"=> "Plan"],
            ["head"=> "Titulo"],
            ["head"=> "Mensaje"],
            ["head"=> "Modalidad"],
            ["head"=> "Prueba"],
            ["head"=> "Precio"],
            ["head"=> "Visible"],
            ["head"=> "Acciones"]
        );
        return view('backoffice.planes.listado',['columnas'=>$columnas, 'datos'=>$datos]);
    }
    public function indexprecios()
    {
        $plazos=Precios::all();
        $temp=[];
        $datos=[];
        foreach ($plazos as $plazo)
        {
            $temp=[];
            $temp[]=$plazo->plan->plan;
            $temp[]=$plazo->precio;
            $temp[]=$plazo->fechaactivo;
            $temp[] = '<a class="btn btn-danger" href="'.url('/').'" type="button">
                        <i class="fa fa-trash-o">
                        </i>
                        </a>';
            $datos[]=$temp;
        }
        $columnas = array(
            ["head"=> "Plan"],
            ["head"=> "Precio"],
            ["head"=> "fecha"],
            ["head"=> "Acciones"]
        );
        return view('backoffice.planes.listado',['columnas'=>$columnas, 'datos'=>$datos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{

            \Conekta\Conekta::setApiKey("key_YD1knCmKwZCT1F75DceYFw");
            \Conekta\Conekta::setApiVersion("2.0.0");
            \Conekta\Conekta::setLocale('es');
            $resp=[];
            $plazos=Plazos::all();
            $dt = Carbon::now();
            foreach ($plazos as $plazo){
                $precio = null;
                if (Precios::where('id_plan', '=', $plazo->plan->id_plan)->where('fechaactivo', '>=', $dt)->exists()) {
                    $precios = Precios::where('id_plan', '=', $plazo->id_plan)->where('fechaactivo', '>=', $dt)->get();
                    $precio = $precios[0]->precio;
                } else if (Precios::where('id_plan', '=', $plazo->plan->id_plan)->where('fechaactivo', '=', '0000-00-00')->exists()) {
                    $precios = Precios::where('id_plan', '=', $plazo->plan->id_plan)->where('fechaactivo', '=', '0000-00-00')->get();
                    $precio = $precios[0]->precio;
                }

                if(!empty($precio)){
                    if($precio>=5 && $precio<=1000 ){
                        $datos = [] ;
                        if ($plazo->porcentaje == 0) {
                            $descuentotemp = 1;
                        } else {
                            $descuentotemp = $plazo->porcentaje;
                        }
                        $datos['id'] = (string)$plazo->id_plazo ;
                        $datos['name'] = $plazo->plan->plan ;
                        $datos['currency'] = "MXN" ;
                        $datos['interval'] = $plazo->intervalo ;
                        $datos['frequency'] = 1 ;
                        $datos['trial_period_days'] = 0 ;
                        if ($plazo->intervalo=='month') {
                            $datos['expiry_count'] = 12 ;
                            $datos['amount'] =(int) str_replace(".","",(string)number_format((($descuentotemp * $precio * 1) * 1.16), 2, '.', '')) ;
                        }else if($plazo->intervalo=='year'){
                            $datos['expiry_count'] = 6 ;
                            $datos['amount'] =(int) str_replace(".","",(string)number_format((($descuentotemp * $precio * 12) * 1.16), 2, '.', '')) ;
                        }
                        try {
                            $conektaplan = \Conekta\Plan::find($datos['id']);
                        } catch (\Conekta\Handler $error) {
                            $conektaplan = [];
                        }
                        if($datos['amount']>=500 && $datos['amount']<=100000 ) {
                            if (empty($conektaplan)) {
                                $plan = \Conekta\Plan::create($datos);
                            } else {
                                if ($datos['amount'] != $conektaplan->amount) {
                                    $plan = $conektaplan->delete();
                                    $plan = \Conekta\Plan::create($datos);
                                } else {
                                    $plan = [$datos['id'] => $datos['name']];
                                }
                            }
                            $resp[] = $plan;
                        }else{
                            if (!empty($conektaplan)) {
                                $plan = $conektaplan->delete();
                            }
                            $plazo->visible=0;
                            $plazo->save();
                            $plan=$plazo->id_plazo.' monto exedido';
                            $resp[] = $plan;
                        }
                    }
                }
            }
            return response()->json(['resp'=>$resp],200);

        } catch (\Conekta\ProccessingError $error){
            echo $error->getMesage() .'process';
        } catch (\Conekta\ParameterValidationError $error){
            echo $error->getMessage() .'parameter';
        } catch (\Conekta\Handler $error){
            echo $error->getMessage().'handler';
        } catch (\Exception $e){
            return $e->getMessage().'exeption';
        }

    }
    public function modificaplan($id)
    {
        $plan=Planes::find($id);
        $url=url('back/updateplan/'.$id);
        $metodo='PUT';
        $titulo='Edita Plan '.$plan->plan;
        $form='backoffice.planes.formplan';

        return view('backoffice.planes.edit',['form' => $form ,'titulo' => $titulo ,  'plan' => $plan ,'url'=>$url ,'metodo'=>$metodo ]);

    }
    public function modificaplazo($id)
    {
        $plazo=Plazos::find($id);
        $url=url('back/updateplazo/'.$id);
        $metodo='PUT';
        if($plazo->intervalo == 'month'){
            $extra='mensual del plan '.$plazo->plan->plan;
        }else if ($plazo->intervalo == 'year'){
            $extra='anual del plan '.$plazo->plan->plan;
        }
        $titulo='Edita Plazo '.$extra;
        $form='backoffice.planes.formplazos';
        return view('backoffice.planes.edit',['form' => $form ,'titulo' => $titulo , 'plazo' => $plazo ,'url'=>$url ,'metodo'=>$metodo ]);
    }
    public function creaprecio()
    {
        $planesfull=collect(Planes::all()->toArray());

        $planes=$planesfull->mapWithKeys(function ($item){
            return[$item['id_plan']=>$item['plan']];
        });
        $url=url('back/craprecio');
        $metodo='POST';

        $titulo='Agregar precio';
        $form='backoffice.planes.formprecios';

        return view('backoffice.planes.edit',[ 'form' => $form ,'titulo' => $titulo , 'planes' => $planes  ,'url'=>$url ,'metodo'=>$metodo ]);
    }

    public function updateplan (Request $request , $id){

        $validator = Validator::make($request->all(), [
            'titulo' => 'required',
            'mensaje'=> 'required',
            'url'=>'required',
        ]);

        if ($validator->fails()){
            return back()->withErrors($validator->errors())->withInput();
        }

        $plan=Planes::find($id);
        $plan->titulo=$request['titulo'];
        $plan->mensaje=$request['mensaje'];
        $plan->url=$request['url'];
        $plan->save();
        return redirect('back/planeslist');
    }

    public function updateplazo (Request $request , $id){

        $validator = Validator::make($request->all(), [
            'titulo' => 'required',
            'mensaje'=> 'required',
            'prueba'=>'required',
            'porcentaje'=>'required',
            'visible'=>'required',
        ]);

        if ($validator->fails()){
            return back()->withErrors($validator->errors())->withInput();
        }

        $plazo=Plazos::find($id);
        $plazo->titulo=$request['titulo'];
        $plazo->mesnaje=$request['mensaje'];
        $plazo->prueba=$request['prueba'];
        $plazo->porcentaje=$request['porcentaje'];
        $plazo->visible=$request['visible'];
        $plazo->save();
        return redirect('back/plazoslist');
    }

    public function storeprecio (Request $request )
    {
        $validator = Validator::make($request->all(), [
            'plan' => 'required',
            'precio' => 'required',
            'fecha_inicio' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }
        $precio = new Precios();
        $precio->id_plan = $request['plan'];
        $precio->precio = $request['precio'];
        $precio->fechaactivo = $request['fecha_inicio'];
        $precio->save();
        return redirect('back/precioslist');
    }

    public function datosplanes($id){
        if (!empty($id) && Planes::where('id_plan','=',$id)->exists()){
            $plan=Planes::find($id);
            $resp=['resp'=>'success','datosresp'=>$plan];
        }else{
            $resp=['resp'=>'error','datosresp'=>'vacio'];
        }
        return response()->json($resp);
    }
    public function datosplazos($id){
        if (!empty($id) && Planes::where('id_plan','=',$id)->exists()){
            $plan=Planes::find($id)->plazo;
            $resp=['resp'=>'success','datosresp'=>$plan];
        }else{
            $resp=['resp'=>'error','datosresp'=>'vacio'];
        }
        return response()->json($resp);
    }
    public function datosprecios($id){
        if (!empty($id) && Planes::where('id_plan','=',$id)->exists()){
            $plan=Planes::find($id)->precio;
            $resp=['resp'=>'success','datosresp'=>$plan];
        }else{
            $resp=['resp'=>'error','datosresp'=>'vacio'];
        }
        return response()->json($resp);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            DB::beginTransaction();
            try{

                DB::commit();
            }catch (\Exception $ex){
                DB::rollback();
                return back();
            }

            \Conekta\Conekta::setApiKey("key_YD1knCmKwZCT1F75DceYFw");
            \Conekta\Conekta::setApiVersion("2.0.0");
            \Conekta\Conekta::setLocale('es');

//                $datos = [] ;
//                $datos['id'] =  ;
//                $datos['name'] =  ;
//                $datos['amount'] = str_replace(".","",(string)number_format(('' * ('' == 0 ? 1 : '')), 2, '.', '')) ;
//                $datos['currency'] = "MXN" ;
//                $datos['interval'] = "month" ;
//                $datos['frequency'] = 1 ;
//                $datos['trial_period_days'] = 0 ;
//                $datos['expiry_count'] = 12 ;

        } catch (\Conekta\ProccessingError $error){
            echo $error->getMesage();
        } catch (\Conekta\ParameterValidationError $error){
            echo $error->getMessage();
        } catch (\Conekta\Handler $error){
            echo $error->getMessage();
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }


    public function cambiaplan(){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Plazos::where('id_plazo','','')){
            $plazo=Plazos::find(3);
            $conectaplan=\Conekta\Plan::find($id);
            $datos=[];
            $datos['id']=$plazo->id_plazo;
            $datos['name']=$plazo->plan->plan;
            $datos['amount']=str_replace(".","",(string)number_format(($plazo->plan->precio * ($plazo->descuento == 0 ? 1 : $plazo->descuento)), 2, '.', ''));
            $datos['currency']="MXN";
            $datos['interval']="month";
            $datos['frequency']=1;
            $datos['trial_period_days'] = 0;
            $datos['expiry_count']=0;

            $conectaplan->update($datos);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $conectaplan=\Conekta\Plan::find($id);
        $conectaplan->delete();

    }

    public function planeslist(){
        $plazos=Plazos::all();
        $temp=[];
        $datos=[];
        foreach ($plazos as $plazo)
        {
            $temp=[];
            $temp['Plan']=$plazo->plan->plan;
            $temp['Precio']=$plazo->plan->precio * $plazo->ssdescuento;
            $temp['Titulo']=$plazo->plan->titulo;
            $temp['Mensaje']=$plazo->plan->mensaje;
            $temp['Plazo']=$plazo->titulo;
            $temp['Acciones']='';
            $datos[]=$temp;
        }
        return response()->json(['data'=>$datos]);
    }

    public function miplan(){
        if(User::where('id_usuario','=',Auth::id())->whereNotNull('conekta_plan')->exists()){
                $plan=Auth::user()->plazo->plan;
                return response()->json(['plan'=>$plan->plan]);
        }
        else {
            return response()->json(['plan'=>'Básico']);
        }
    }
}
