<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Plazos;
use App\Models\Planes;
use App\Models\Pagos;
use App\Models\Webhooks;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WebhooksController extends Controller
{

    public function recibewebhoook (Request $request){

        $webhook= new Webhooks();
        $webhook->objeto=(array)json_decode($request->getContent(), true);
        $campo='';
        $webhook->save();
        if(collect($webhook->objeto)->has('data')){
            $data=$webhook->objeto['data'];

            if(collect($webhook->objeto)->has('type')) {
                $type = $webhook->objeto['type'];
                $webhook->tipo = $type;
                if (strpos($type, 'customer')!== false) {
                    $campo = 'id';
                } else {
                    $campo = 'customer_id';
                }
            }
            if($campo!='') {
                if (collect($data)->has('object')) {
                    $object = $data['object'];
                    if (collect($object)->has($campo)) {
                        if (User::where('conekta_id', '=', $object[$campo])->exists()) {
                            $usuario = User::where('conekta_id', '=', $object[$campo])->get();
                            $webhook->id_usuario = $usuario[0]->id_usuario;
                        }
                    }
                }
            }
        }
        $webhook->save();

        if(!empty($type)){
            $user=$usuario[0];
            if($type=='subscription.payment_failed'){
                if(!empty($user)){
                    DB::beginTransaction();
                    try{
                        if (!empty($user->conekta_id)&&!empty($user->conekta_plan)) {
                            \Conekta\Conekta::setApiKey("key_YD1knCmKwZCT1F75DceYFw");
                            \Conekta\Conekta::setApiVersion("2.0.0");
                            \Conekta\Conekta::setLocale('es');
                            $user->conekta_plan=null;
                            $user->save();
                            $customer = \Conekta\Customer::find($user->conekta_id);
                            $subscription = $customer->subscription->cancel();
                            DB::commit();
                        }else{
                            DB::commit();
                        }
                    }catch (\Exception $ex){
                        DB::rollback();
                    }
                }
            }else if ($type=='subscription.paid'){

            }
        }

        return response($webhook->tipo, 200)->header('Content-Type', 'text/plain');
    }
}