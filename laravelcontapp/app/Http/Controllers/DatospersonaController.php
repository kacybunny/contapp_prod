<?php

namespace App\Http\Controllers;

use App\Models\datospersona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Codepostals;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class DatospersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\datospersona  $datospersona
     * @return \Illuminate\Http\Response
     */
    public function show(datospersona $datospersona)
    {
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\datospersona  $datospersona
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id=Auth::user()->persona->id_datos_persona;
        $datospersona=datospersona::find($id);
        $cp=$datospersona->cp;
        $persona=Auth::user()->persona;
//        return $cp;
        if (!empty($cp))
        {
            $idcp=$cp->id_codepostal;
            $cp=$cp->d_codigo;
        }else{
            $cp='';
            $idcp='';
        }
        return view('datospersona.edit',['datospersona'=>$datospersona,'cp'=>$cp,'idcp'=>$idcp,'persona'=>$persona]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\datospersona  $datospersona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'rfc'=>'required',
            'cp'=>'required',
            'colonia' => 'required',
            'vialidad' => 'required',
            'num_ext' => 'required',
        ]);

        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $id = Auth::user()->persona->id_datos_persona;
        $persona=Auth::user()->persona;
        $persona->rfc=$request['rfc'];
        $persona->save();
        $datospersona = datospersona::find($id);
        $datospersona->id_codepostal=$request['colonia'];
        $datospersona->localidad=$request['localidad'];
        $datospersona->vialidad=$request['vialidad'];
        $datospersona->num_int=$request['num_int'];
        $datospersona->num_ext=$request['num_ext'];
        $datospersona->save();

        Session::flash('message', '!Se guardaron los datos con exito!');

        return redirect('/datosfiscales/0/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\datospersona  $datospersona
     * @return \Illuminate\Http\Response
     */
    public function destroy(datospersona $datospersona)
    {
        //
    }


    public function codpostal($code){
        $codigo=Codepostals::where('d_codigo','=',$code)->get()->toArray();
        $colonias=[];
        foreach ($codigo as $key=>$value){
            $colonias[$value['id_codepostal']]=$value['d_asenta'];
        }
        $codigo[0]['d_asenta']=$colonias;
        return response()->json([
            'resp' => 'success',
            'colonia'=>$codigo[0],
        ]);

    }

    public function pasosReg(){
        session(['pasosreg' => '1']);
    }
}
