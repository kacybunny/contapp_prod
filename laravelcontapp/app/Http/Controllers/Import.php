<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Codepostals;

class Import extends Controller
{
    public function importcsv (Request $request){
        Excel::load($request->file, function($reader) {

            $excel = $reader->get();

            // iteracción
            $reader->each(function ($row) {

                $cp = new Codepostals();
                $cp->d_codigo = $row->d_codigo;
                $cp->d_asenta = $row->d_asenta;
                $cp->D_mnpio = $row->d_mnpio;
                $cp->d_estado = $row->d_estado;
                $cp->save();

            });
        });
        return back();
    }
}
