<?php

namespace App\Http\Controllers;

use App\Mail\EmailRegistroDatosCampana;
use App\Mail\EmailVerification;
use App\Models\Actividades;
use App\Models\Datosfiscales;
use App\Models\datospersona;
use App\Models\Factura;
use App\Models\obligaciones;
use App\Models\personas;
use App\Models\regimen;
use App\Models\User;
use App\Notifications\personal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url='back/usuarios';
        $metodo='POST';
        return view('backoffice.usuarios.create',['metodo'=>$metodo,'url'=>$url]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Laravel validation
        $validator =  Validator::make($request->all(), [
        'nombre' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'paterno' => 'required|max:255',
        'materno' => 'required|max:255',
//            'password' => 'required|min:6|confirmed',
        ],['email.unique'=>'El Email ya esta registrado',
        ]);
        if ($validator->fails())
        {
            $this->throwValidationException($request, $validator);
        }
        // Using database transactions is useful here because stuff happening is actually a transaction
        // I don't know what I said in the last line! Weird!
        DB::beginTransaction();
        try
        {
            $user = User::create([
                'name' => $request['nombre'],
                'email' => $request['email'],
//            'password' => bcrypt($data['password']),
                'email_token' => str_random(10),
                'active'=>1,
            ]);

            $datos = new datospersona;
            $datos->telefono=$request['telefono'];
            $datos->save();

            $datosfisc = new Datosfiscales();
            $datosfisc->save();

            $persona = new personas();
            $persona->nombre=$request['nombre'];
            $persona->paterno=$request['paterno'];
            $persona->materno=$request['materno'];
            $persona->id_usuario = $user->id_usuario;
            $persona->id_datos_persona = $datos->id_datos_persona;
            $persona->id_datos_fiscales=$datosfisc->id_datos_fiscales;
            $persona->save();

            // After creating the user send an email with the random token generated in the create method above
//            $persona=$user->persona;
//            $datospersona=$persona->datospersona;
//            $maildatos= new EmailRegistroDatosCampana($user,$persona,$datospersona);
            $user->assignRole('backuser');
            if ($request['tipo_usuario']==1){
                $user->assignRole('Admin');
            }
            $maildatos= new EmailRegistroDatosCampana($user);
            Mail::to('soporte@contapp.mx')->send($maildatos);
            $email = new EmailVerification(new User(['email_token' => $user->email_token, 'name' => $user->name]));
            Mail::to($user->email)->send($email);

            DB::commit();
            Session::flash('message', '¡hemos enviado un mail de verificacion!');
            return redirect('back/usuarios/create');
        }
        catch(Exception $e)
        {
            DB::rollback();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id == 1){
            $usuarios = User::role('user')->get();
        }else{
            if(!Auth::user('Admin')){
                return redirect('/');
            }
            $usuarios = User::role('backuser')->get();
        }
       $columnas = array(
            ["head" => "Nombre"],
            ["head" => "Apellidos"],
            ["head" => "Correo"],
            ["head" => "Telefono"],
            ["head" => "RFC"],
            ["head" => "Tipo de usuario"],
            ["head" => "Verificado"],
            ["head" => "Fecha de registro"],
            ["head" => "Acciones"],
        );

        $datos = [];
        foreach ($usuarios as $usuario) {
            $temp = [];
            $temp[] = $usuario->persona->nombre;
            $temp[] = $usuario->persona->paterno.' '.$usuario->persona->materno;
            $temp[] = $usuario->email;
            $temp[] = $usuario->persona->datospersona->telefono;
            $temp[] = $usuario->persona->rfc;
            if($usuario->hasRole('user')){
                $columnas[5]['head']='Plan';
                if(!empty( $usuario->conekta_plan )){
                    $temp[] = $usuario->plazo->plan->plan;
                }else{
                    $temp[] = 'Básico';
                }
            }else{
                if($usuario->hasRole('Admin')){
                    $temp[] = 'Administrador';
                }else{
                    $temp[] = 'Operador';
                }
            }

              if ($usuario->verified == 1)
            {
              $temp[] = 'Si';
            }else if($usuario->verified == 0)
            {
              $temp[] = 'No';
            }
            
//            $temp[] = $usuario->created_at->format('d/m/Y h:i:s A') ;
            $temp[] = $usuario->created_at->format('d/m/Y') ;
            $botones='';

            $botones.='<a class="btn btn-primary" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Notificacion" href="'.url('back/usuarios/'.$usuario->id_usuario.'/mens').'" type="button">
                                <i class="fa fa-comment">
                                </i>
                            </a>';
            $botones.='<a class="btn btn-success edita" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Editar" href="'.url('back/usuarios/'.$usuario->id_usuario.'/edit').'" type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a>';


            if($usuario->hasRole('user')){
                $botones.='<a class="btn btn-aqua" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Exportar Facturas" href="'.url('back/export/'.$usuario->persona->id_persona).'" type="button">
                                <i class="fa fa-cloud-download">
                                </i>
                            </a>';
                $botones.='<a class="btn btn-orange" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Cargar facturas" href="'.url('subefact/'.$usuario->persona->id_persona).'" type="button">
                                <i class="fa fa-file">
                                </i>
                            </a>';
                $botones.='<a class="btn btn-purple" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Cargar declaración anterior" href="'.url('back/declaraciones/'.$usuario->persona->id_persona).'" type="button">
                                <i class="fa fa-file">
                                </i>
                            </a>';
            }else{
                $botones.='<a class="btn btn-orange" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Cambiar permiso" href="'.url('back/permisouser/'.$usuario->id_usuario).'" type="button">
                                <i class="fa fa-bolt">
                                </i>
                            </a>';
            }
            if ($usuario->active == 1){
                $botones.='<a class="btn btn-warning" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Desactivar" href="'.url('back/usuarios/'.$usuario->id_usuario.'/desact').'" type="button">
                                <i class="fa fa-eye-slash">
                                </i>
                            </a>';
            }else{
                $botones.='<a class="btn btn-info" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Activar" href="'.url('back/usuarios/'.$usuario->id_usuario.'/act').'" type="button">
                                <i class="fa fa-eye">
                                </i>
                            </a>';
            }
            if(Auth::user()->hasRole('Admin')){
                $botones.='<a class="btn btn-danger eliminar" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Eliminar" data-url="'.url('back/usuarios/'.$usuario->id_usuario).'" type="button">
                                <i class="fa fa-trash">
                                </i>
                            </a>';
            }

            $temp[] = $botones;
            $datos[] = $temp;
        }

        return view('backoffice.usuarios.listado', ['datos' => $datos, 'columnas' => $columnas]);
    }

    public function formpermisos ($id){
        $user=User::find($id);
        $url='back/permisouser/'.$id;
        $metodo='PUT';
        return view('backoffice.usuarios.asigna',['user'=>$user,'url'=>$url,'metodo'=>$metodo]);
    }
    public function permisos (Request $request,$id){

        $validator = Validator::make($request->all(), [
            'tipo_usuario' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }
        $user=User::find($id);
        if($user->hasRole('Admin') && $user->hasRole('backuser')){
            if($request['tipo_usuario']!=1){
                $user->removeRole('Admin');
            }
        }else if( $user->hasRole('backuser') && !$user->hasRole('Admin')){
            if($request['tipo_usuario']==1){
                $user->assignRole('Admin');
            }
        }
        return redirect('back/usuarios/0');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obligaciones = obligaciones::all();
        $actividades = Actividades::all();
        $regimen = regimen::all();
        $usuario= User::find($id);

        $persona = $usuario->persona;
        $obligacionespersona=$persona->obligaciones;
        if(empty($obligacionespersona)){
            $obligacionespersona = new obligaciones();
        }
        $actividadespersona=$persona->actividades;
        if(empty($actividadespersona)){
            $actividadespersona= new Actividades();
        }
        $regimenpersona=$persona->regimenes;
        if(empty($regimenpersona)){
            $regimenpersona = new regimen();
        }
        $fiscales = Datosfiscales::find($persona->id_datos_fiscales);

        $iddatospersona=$persona->id_datos_persona;
        $datospersona=datospersona::find($iddatospersona);
        $cp=$datospersona->cp;
//        return $cp;
        if (!empty($cp))
        {
            $idcp=$cp->id_codepostal;
            $cp=$cp->d_codigo;
        }else{
            $cp='';
            $idcp='';
        }

      return view('backoffice.usuarios.gestion',['id'=>$id , 'usuario'=>$usuario ,'metodo'=>'PUT','datospersona'=>$datospersona,'cp'=>$cp,'idcp'=>$idcp,'persona'=>$persona,'regimenpersona'=>$regimenpersona,'actividadespersona'=>$actividadespersona,'obligacionespersona'=>$obligacionespersona,'obligaciones'=>$obligaciones,'actividades'=>$actividades,'regimen'=>$regimen,'fiscales'=>$fiscales]);
    }

    public function facturas($id){
        $buscar=[];
        $buscar[]=['id_persona','=',$id];
        $facturastemp=Factura::where($buscar)->get();

        $datos = [];
        foreach ($facturastemp as $facturatemp) {
            $temp = [];
            $temp[] = $facturatemp->tipo;
            $datosfactura = $facturatemp->factura;
            if ($facturatemp->tipo=='Emitida'){
                $temp[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturatemp->tipo=='Recibida'){
                $temp[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temp[] = $facturatemp->fecha;
            $temp[] = '$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',');
            if ($facturatemp->pagada){
                $temp[] = 'Pagada '.$facturatemp->fechapago;
                //                '<a class="btn btn-success editpayfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar no pagada" data-mensaje="<p>Marcar como no pagada la factura con monto total de '.'$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',').'</p>" data-url="'.url('nopagada/'.$facturatemp->id_factura).'" type="button">
//                <i class="fa fa-edit">
//                                </i>
//                            </a>'
                $temp[] = '<a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturatemp->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';

            }else{
                $temp[] = 'Pendiente';
                $temp[] = '<a class="btn btn-success payfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar pagada" data-mensaje=
"<p>Marcar como pagada la factura con monto total de '.'$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',').'
</p>'."
<div class='input-group date bootdatepick'>
  <input id='fechapago' type='text'  placeholder='YYY-MM-DD' class='form-control'>
  <span class='input-group-addon'>
  <i class='glyphicon glyphicon-th'>
  </i>
  </span>
</div>
<p>Cantidad</p>
<input id='pago' type='text' placeholder='0.00' class='form-control'>".'" data-url="'.url('pagada/'.$facturatemp->id_factura).'"
 type="button">
                                <i class="fa fa-money">
                                </i>
                            </a>
                            <a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturatemp->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
            }
            $temp[]=$facturatemp->factura;
            $temp[]=$facturatemp->pagado;
            $temp[]=number_format($facturatemp->total,2);
            $datos[] = $temp;
        }

        $columnas = array(
            ["head" => "Tipo de factura"],
            ["head" => "Razón social"],
            ["head" => "Fecha"],
            ["head" => "Monto total"],
            ["head" => "Estatus"],
            ["head" => "Acciones"]
        );
        return view('factura.listado',['facturastemp'=>$facturastemp,'datos' => $datos, 'columnas' => $columnas]);
    }

    public function cancelsuscription($id){
        $user=User::find($id);
        DB::beginTransaction();
        try{
            if (!empty($user->conekta_id)&&!empty($user->conekta_plan)) {
                \Conekta\Conekta::setApiKey("key_YD1knCmKwZCT1F75DceYFw");
                \Conekta\Conekta::setApiVersion("2.0.0");
                \Conekta\Conekta::setLocale('es');
                $user->conekta_plan=null;
                $user->save();

                $customer = \Conekta\Customer::find($user->conekta_id);
                $subscription = $customer->subscription->cancel();

                Session::flash('reset', '¡Se cancelo la suscripcion de este usuario!');
                DB::commit();
                return back();
            }else{
                Session::flash('reset', '¡Este usuario tiene El plan Básico!');
                DB::commit();
                return back();
            }
        }catch (\Exception $ex){
            DB::rollback();
            Session::flash('reset', '¡No se pudo cancelar intenta mas tarde!');
            return back();
        }


    }

    public function desactivar($id){
        $user=User::find($id);
        $user->active=0;
        $user->save();
        if ($user->hasRole('user')){
            $dir=1;
        }else{
            $dir=2;
        }
        return redirect('back/usuarios/'.$dir);
    }
    public function activar($id){
        $user=User::find($id);
        $user->active=1;
        $user->save();
        if ($user->hasRole('user')){
            $dir=1;
        }else{
            $dir=2;
        }
        return redirect('back/usuarios/'.$dir);
    }

    public function mensaje($id){
        $user=User::find($id);
        $url='back/usuarios/'.$id.'/mens';
        $metodo='POST';
        return view('backoffice.usuarios.createmensaje',['user'=>$user,'url'=>$url,'metodo'=>$metodo]);
    }

    public function notificar(Request $request ,$id){
        $user=User::find($id);

        $validator = Validator::make($request->all(), [
            'titulo'=>'required',
            'mensaje'=>'required',
        ]);

        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        if ($user->hasRole('user')){
            $dir=1;
        }else{
            $dir=0;
        }

        $user->notify(new personal(['user' => $user->name, 'id' => $user->id_usuario, 'title' => $request['titulo'], 'data' => $request['mensaje'], 'url' => url('notified')]));

        return redirect('back/usuarios/'.$dir);
    }

    public function linkpassword($id){
        $user=User::find($id);
        $response= Password::sendResetLink(['email' => $user->email]);
        switch ($response) {
            case Password::RESET_LINK_SENT:
                Session::flash('reset', '¡El email para restablecer la contraseña se envio correctamente!');
                return back();
            case Password::INVALID_USER:
                Session::flash('reset', '¡El email no se envio!');
                return back();
            default:
                Session::flash('reset', '¡El email no se envio!');
                return back();
        }
    }

    public function formactoblreg (Request $request, $id){
        $validator = Validator::make($request->all(), [
            'tasaiva' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }
        $persona = User::find($id)->persona;

        $fiscales = Datosfiscales::find($persona->id_datos_fiscales);
        $fiscales->iva = $request['tasaiva'];
        $fiscales->save();

        if (!empty($persona->obligaciones)) {
            $obligaciones = $persona->obligaciones;
            $obligaciones->obligaciones = $request['obligaciones'];
            $obligaciones->fechas=$request['fechainiobl'];
            $obligaciones->save();
        } else {
            $obligaciones = new Detalleobligacion();
            $obligaciones->id_persona = $persona->id_persona;
            $obligaciones->obligaciones = $request['obligaciones'];
            $obligaciones->fechas=$request['fechainiobl'];
            $obligaciones->save();
        }
        if (!empty($persona->actividades)) {
            $actividades = $persona->actividades;
            $actividades->actividades = $request['actividades'];
            $actividades->fechas=$request['fechainiact'];
            $actividades->save();
        } else {
            $actividades = new Detalleactividad();
            $actividades->id_persona = $persona->id_persona;
            $actividades->actividades = $request['actividades'];
            $actividades->fechas=$request['fechainiact'];
            $actividades->save();
        }
        if (!empty($persona->regimenes)) {
            $regimenes = $persona->regimenes;
            $regimenes->regimenes = $request['regimen'];
            $regimenes->fechas=$request['fechainireg'];
            $regimenes->save();
        } else {
            $regimenes = new Detalleregimen();
            $regimenes->id_persona = $persona->id_persona;
            $regimenes->regimenes = $request['regimen'];
            $regimenes->fechas=$request['fechainireg'];
            $regimenes->save();
        }
        return redirect('back/usuarios/'.$id.'/edit');
    }

    public  function formfirmas(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'contrasena_SAT' => 'required|same:confirmar_contrasena_SAT',
            'contrasena_llave_privada' => 'required|same:confirma_contrasena_llave_privada',
            'certificado' => 'File',
            'llave_privada' => 'File',
        ]);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }
        $persona = User::find($id)->persona;
        $fiscales = Datosfiscales::find($persona->id_datos_fiscales);
        $fiscales->pass_sat = $request['contrasena_SAT'];
        $fiscales->cer = $request['certificado'];

        if ($request->hasFile('certificado')) {
            $file = $request->file('certificado');
            $extension = $request->file('certificado')->getClientOriginalExtension();
            $url ='Perfil/'.Auth::user()->email. '.' . $extension;
            Storage::disk('custom')->put($url, File::get($file));
            $persona->foto = Auth::user()->email. '.' . $extension;
        }

        $fiscales->key = $request['llave_privada'];

        if ($request->hasFile('llave_privada')) {
            $file = $request->file('llave_privada');
            $extension = $request->file('llave_privada')->getClientOriginalExtension();
            $url ='Perfil/'.Auth::user()->email. '.' . $extension;
            Storage::disk('custom')->put($url, File::get($file));
            $persona->foto = Auth::user()->email. '.' . $extension;
        }

        $fiscales->pass_privado = $request['contrasena_llave_privada'];
        $fiscales->save();
        return redirect('back/usuarios/'.$id.'/edit');
    }
    public function formubicacion(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'rfc'=>'required',
            'cp'=>'required',
            'colonia' => 'required',
            'vialidad' => 'required',
            'num_ext' => 'required',
        ]);

        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $iddatos = User::find($id)->persona->id_datos_persona;
        $persona=User::find($id)->persona;
        $persona->rfc=$request['rfc'];
        $persona->save();
        $datospersona = datospersona::find($iddatos);
        $datospersona->id_codepostal=$request['colonia'];
        $datospersona->localidad=$request['localidad'];
        $datospersona->vialidad=$request['vialidad'];
        $datospersona->num_int=$request['num_int'];
        $datospersona->num_ext=$request['num_ext'];
        $datospersona->save();

        Session::flash('message', '¡Se guardaron los datos con exito!');

        return redirect('back/usuarios/'.$id.'/edit');
    }

    public function formpersona(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'a_paterno'=> 'required',
            'a_materno'=>'required',
        ]);

        if ($validator->fails()){
            return back()->withErrors($validator->errors())->withInput();
        }

        $persona=User::find($id)->persona;
        $persona->nombre = $request['nombre'] ;
        $persona->paterno = $request['a_paterno'] ;
        $persona->materno = $request['a_materno'] ;
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $extension = $request->file('foto')->getClientOriginalExtension();
            $url ='Perfil/'.Auth::user()->email. '.' . $extension;
            Storage::disk('custom')->put($url, File::get($file));
            $persona->foto = Auth::user()->email. '.' . $extension;
        }
//        $persona-> = $request['foto'] ;
        $persona->save();

        return redirect('back/usuarios/'.$id.'/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function formexport ($id){
        $url = 'back/exportlay/'.$id ;
        $metodo='GET';
        return view('backoffice.usuarios.search',['url'=>$url,'metodo'=>$metodo]);
    }

    public function export(Request $request , $id){
        $buscar=[];
        $buscarrfc=[];
        $buscarrazon=[];
        $buscarentre=[];
        $contador=1;
        $tipo='';
        $datafacturas=[];

        $buscar[] = ['id_persona', '=', $id];

        if($request['tipo']==1)
        {
            $tipo = 'Emitida';
        } else if ($request['tipo']==2) {
            $tipo='Recibida';
        }
        if(!empty($request['tipo'])) {
            if($request['tipo']==3)
            {
                $buscar[] = ['tipo', 'like', '%%'];
            }else{
                $buscar[] = ['tipo', '=', $tipo];
            }
            $contador++;
        }

        if(!empty($request['fecha_inicio']) && !empty($request['fecha_final'])) {
            $buscarentre[] = ['fecha',[$request['fecha_inicio'],$request['fecha_final']]];
            $contador++;
        }
        if(!empty($request['monto_minimo']) && !empty($request['monto_maximo'])) {
            $buscarentre[] = ['total', [$request['monto_minimo'] , $request['monto_maximo']]];
            $contador++;
        }
        if(!empty($request['rfc'])) {
            $buscarrfc[] = ['rfcEmisor', 'like', '%' . $request['rfc'] . '%'];
            $buscarrfc[] = ['rfcReceptor', 'like', '%' . $request['rfc'] . '%'];
            $contador++;
            $contador++;
        }
        if(!empty($request['razon'])) {
            $buscarrazon[] = ['nombreEmisor', 'like', '%' . $request['razon'] . '%'];
            $buscarrazon[] = ['nombreReceptor', 'like', '%' . $request['razon'] . '%'];
            $contador++;
            $contador++;
        }
        switch ($contador) {
            case 0:
                Session::flash('message','No se encontraron resultados');
                Session::flash('tipo', 'error');
                return back()->withInput();
                break;
            case 2:
            case 1:
                if(collect($buscarrazon)->count()){
                    $datafacturas= Factura::Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]])->get();
                }
                else if(collect($buscarrfc)->count()){
                    $datafacturas= Factura::Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]])->get();
                }
                else if(collect($buscarentre)->count() && collect($buscar)->count()){
                    $datafacturas= Factura::where($buscar)->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                }
                else if(collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas= Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $datafacturas= Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }
                else if(collect($buscar)->count()){
                    $datafacturas= Factura::where($buscar)->get();
                }
                break;
            case 4:
            case 3:
                if(collect($buscarrazon)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $datafacturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }

                } else if(collect($buscarrfc)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else {
                        $datafacturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                } else if(collect($buscarrazon)->count() && collect($buscar)->count()){
                    $datafacturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                        $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                    })->get();
                } else if(collect($buscarrfc)->count() && collect($buscar)->count()){
                    $datafacturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                        $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                    })->get();
                }else if(collect($buscarentre)->count() && collect($buscar)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas = Factura::where($buscar)
                            ->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $datafacturas = Factura::where($buscar)
                            ->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }else if(collect($buscarrazon)->count() && collect($buscarrfc)->count()){
                    $datafacturas= Factura::Where(function ($query) use ($buscarrfc) {
                        $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                    })->Where(function ($query) use ($buscarrazon) {
                        $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                    })->get();
                }else if(collect($buscarrazon)->count()&& collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2) {
                        $datafacturas = Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->Where(function ($query) use ($buscarrazon) {
                                $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                            })->get();
                    }else{
                        $datafacturas = Factura::whereBetween($buscarentre[0][0],$buscarentre[0][1])->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->get();
                    }

                }else if(collect($buscarentre)->count()&& collect($buscarrfc)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas= Factura::Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $datafacturas= Factura::Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }

                break;
            case 5:
            case 6:
            case 7:
            case 8:
                if(collect($buscarrazon)->count() && collect($buscarrfc)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas= Factura::where($buscar)->
                        Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $datafacturas= Factura::where($buscar)->
                        Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }

                }else if(collect($buscarrazon)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else{
                        $datafacturas= Factura::where($buscar)->Where(function ($query) use ($buscarrazon) {
                            $query->Where([$buscarrazon[0]])->orWhere([$buscarrazon[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }

                } else if(collect($buscarrfc)->count() && collect($buscar)->count() && collect($buscarentre)->count()){
                    if(collect($buscarentre)->count()==2){
                        $datafacturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])
                            ->whereBetween($buscarentre[1][0],$buscarentre[1][1])->get();
                    }else {
                        $datafacturas = Factura::where($buscar)->Where(function ($query) use ($buscarrfc) {
                            $query->Where([$buscarrfc[0]])->orWhere([$buscarrfc[1]]);
                        })->whereBetween($buscarentre[0][0],$buscarentre[0][1])->get();
                    }
                }
                break;
        }
        if(!collect($datafacturas)->count()){
            Session::flash('message','No se encontraron resultados');
            Session::flash('tipo', 'error');
            return back()->withInput();
        }
        $exportar =[];
        $conceptos = [];
        $factsfinal=[];
        $datos = [];
        foreach ($datafacturas as $facturas) {
            $impuestos=[];
            $retenciones=[];
            $temptabla = [];
            $temptabla[] = $facturas->tipo;
            $datosfactura = $facturas->factura;
            if ($facturas->tipo=='Emitida'){
                $temptabla[] = $datosfactura['receptor']['@attributes']['nombre'];
            }else if ($facturas->tipo=='Recibida'){
                $temptabla[] = $datosfactura['emisor']['@attributes']['nombre'];
            }
            $temptabla[] = $facturas->fecha;
            $temptabla[] = '$ '.number_format($facturas->factura['@attributes']['total'], 2, '.', ',');
            if ($facturas->pagada){
                $temptabla[] = 'Pagada '.$facturas->fechapago;
//                '<a class="btn btn-success editpayfact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Marcar no pagada" data-mensaje="<p>Marcar como no pagada la factura con monto total de '.'$ '.number_format($facturatemp->factura['@attributes']['total'], 2, '.', ',').'</p>" data-url="'.url('nopagada/'.$facturatemp->id_factura).'" type="button">
//                <i class="fa fa-edit">
//                                </i>
//                            </a>'
                $temptabla[] = '<a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturas->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
            }else{
                $temptabla[] = 'Pendiente';
                $temptabla[] = '<a class="btn btn-dark detallefact" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Detalle factura"   type="button">
                                <i class="fa fa-file-text">
                                </i>
                            </a><a class="btn btn-success" data-toggle= "tooltip" data-placement="top" title="" data-original-title="Descarga XML"  href="'.url('api/descargafact/' . $facturas->id_factura).'" type="button">
                        <i class="fa fa-cloud-download">
                        </i>
                        </a>';
            }
            $temptabla[]=$facturas->factura;
            $temptabla[]=$facturas->pagado;
            $temptabla[]=number_format($facturas->total,2);
            $datos[] = $temptabla;

            $factura = [];
            $factura['UUID']= $facturas['uuid'];
            $factura['Emisor']= $facturas['rfcEmisor'];
            $factura['Receptor']= $facturas['rfcReceptor'];
            $factura['Subtotal'] = $facturas['subtotal'];
            $factura['Impuesto'] = $facturas['total'] - $facturas['subtotal'];
            $factura['Total'] = $facturas['total'];
            if($facturas['pagada']==1){
                $factura['Fecha_pago'] = $facturas['fechapago'];
            }else{
                $factura['Fecha_pago']='';
            }
            $factura['Tipo'] = $facturas['tipo'];
            $factura['Fecha'] = $facturas['fecha'];


            if ($facturas->version=='3.2'){
                if (collect($facturas->factura)->has('impuestos')){
                    if (collect($facturas->factura['impuestos'])->has('traslados')) {
                        $tempimps = $facturas->factura['impuestos']['traslados']['traslado'];
                        if (collect($tempimps)->has('@attributes')) {
                            $imps = $tempimps['@attributes'];
                            $tempimp = [];
                            foreach ($imps as $key => $valor) {
                                $tempimp[$key] = $valor;
                            }
                            if (collect($impuestos)->has($tempimp['impuesto'])) {
                                $impuestos[$tempimp['impuesto']] += floatval($tempimp['importe']);
                            } else {
                                $impuestos[$tempimp['impuesto']] = floatval($tempimp['importe']);
                            }

                        } else {
                            foreach ($tempimps as $imp) {
                                $tempimp = [];
                                foreach ($imp as $key => $valor) {
                                    $tempimp[$key] = $valor;
                                }
                                if (collect($impuestos)->has($tempimp['impuesto'])) {
                                    $impuestos[$tempimp['impuesto']] += floatval($tempimp['importe']);
                                } else {
                                    $impuestos[$tempimp['impuesto']] = floatval($tempimp['importe']);
                                }
                            }
                        }
                    }
                    if (collect($facturas->factura['impuestos'])->has('retenciones')) {
                        $temprets = $facturas->factura['impuestos']['retenciones']['retencion'];
                        if (collect($temprets)->has('@attributes')) {
                            $ret = $temprets['@attributes'];
                            $tempret = [];
                            foreach ($ret as $key => $valor) {
                                $tempret[$key] = $valor;
                            }
                            if (collect($retenciones)->has($tempimp['impuesto'])) {
                                $retenciones[$tempret['impuesto']] += floatval($tempret['importe']);
                            } else {
                                $retenciones[$tempret['impuesto']] = floatval($tempret['importe']);
                            }

                        } else {
                            foreach ($temprets as $ret) {
                                $tempret = [];
                                foreach ($ret as $key => $valor) {
                                    $tempret[$key] = $valor;
                                }
                                if (collect($retenciones)->has($tempimp['impuesto'])) {
                                    $retenciones[$tempret['impuesto']] += floatval($tempret['importe']);
                                } else {
                                    $retenciones[$tempret['impuesto']] = floatval($tempret['importe']);
                                }
                            }
                        }
                    }
                }

            }
            if (collect($facturas->factura['conceptos']['concepto'])->has('@attributes')) {
                $facts = $facturas->factura['conceptos']['concepto']['@attributes'];
                $temp = [];
                $temp['UUID']=$facturas['uuid'];
                if ($facturas->version=='3.3') {
                    if (collect($facturas->factura['conceptos']['concepto'])->has('impuestos')) {
                        if (collect($facturas->factura['conceptos']['concepto']['impuestos'])->has('traslados')) {
                            $tempimps = $facturas->factura['conceptos']['concepto']['impuestos']['traslados']['traslado'];
                            if (collect($tempimps)->has('@attributes')) {
                                $imps = $tempimps['@attributes'];
                                $tempimp = [];
                                foreach ($imps as $key => $valor) {
                                    $tempimp[$key] = $valor;
                                }
                                if (collect($impuestos)->has($tempimp['impuesto'])) {
                                    $impuestos[$tempimp['impuesto']] += floatval($tempimp['importe']);
                                } else {
                                    $impuestos[$tempimp['impuesto']] = floatval($tempimp['importe']);
                                }
                            } else {
                                foreach ($tempimps as $imp) {
                                    $tempimp = [];
                                    foreach ($imp as $key => $valor) {
                                        $tempimp[$key] = $valor;
                                    }
                                    if (collect($impuestos)->has($tempimp['impuesto'])) {
                                        $impuestos[$tempimp['impuesto']] += floatval($tempimp['importe']);
                                    } else {
                                        $impuestos[$tempimp['impuesto']] = floatval($tempimp['importe']);
                                    }
                                }
                            }
                        }
                        if (collect($facturas->factura['conceptos']['concepto']['impuestos'])->has('retenciones')) {
                            $temprets = $facturas->factura['conceptos']['concepto']['impuestos']['retenciones']['retencion'];
                            if (collect($temprets)->has('@attributes')) {
                                $ret = $temprets['@attributes'];
                                $tempret = [];
                                foreach ($ret as $key => $valor) {
                                    $tempret[$key] = $valor;
                                }
                                if (collect($retenciones)->has($tempimp['impuesto'])) {
                                    $retenciones[$tempret['impuesto']] += floatval($tempret['importe']);
                                } else {
                                    $retenciones[$tempret['impuesto']] = floatval($tempret['importe']);
                                }

                            } else {
                                foreach ($temprets as $ret) {
                                    $tempret = [];
                                    foreach ($ret as $key => $valor) {
                                        $tempret[$key] = $valor;
                                    }
                                    if (collect($retenciones)->has($tempimp['impuesto'])) {
                                        $retenciones[$tempret['impuesto']] += floatval($tempret['importe']);
                                    } else {
                                        $retenciones[$tempret['impuesto']] = floatval($tempret['importe']);
                                    }
                                }
                            }
                        }
                    }
                }
                foreach ($facts as $llave => $fact) {
                    $temp[$llave] = $fact;
                }
                $conceptos[] = $temp;
            } else {
                $facts = $facturas->factura['conceptos']['concepto'];
                foreach ($facts as $concepto) {
                    $temp = [];
                    $temp['UUID']=$facturas['uuid'];
                    foreach ($concepto['@attributes'] as $llave => $fact) {
                        $temp[$llave] = $fact;

                    }
                    $conceptos[] = $temp;
                }
            }

            if(collect($impuestos)->has('002')||collect($impuestos)->has('IVA')){
                if($facturas->version=='3.2') {
                    $factura['iva'] = $impuestos['IVA'];
                }else{
                    $factura['iva']=$impuestos['002'];
                }
            }else{
                $factura['iva']='';
            }
            if(collect($impuestos)->has('001')||collect($impuestos)->has('ISR')){
                if($facturas->version=='3.2'){
                    $factura['isr']=$impuestos['ISR'];
                }else{
                    $factura['isr']=$impuestos['001'];
                }
            }else{
                $factura['isr']='';
            }
            if(collect($retenciones)->has('002')||collect($retenciones)->has('IVA')){
                if($facturas->version=='3.2') {
                    $factura['retenciones-iva'] = $impuestos['IVA'];
                }else{
                    $factura['retenciones-iva']=$impuestos['002'];
                }
            }else{
                $factura['retenciones-iva']='';
            }
            if(collect($retenciones)->has('001')||collect($retenciones)->has('ISR')){
                if($facturas->version=='3.2'){
                    $factura['retenciones-isr']=$impuestos['ISR'];
                }else{
                    $factura['retenciones-isr']=$impuestos['001'];
                }
            }else{
                $factura['retenciones-isr']='';
            }

            $factsfinal[]=$factura;
        }
        $exportar[]=[$factsfinal,$conceptos];
        $fecha=Carbon::now();
        $persona= personas::find($id);
        $name=uniqid($persona->rfc);
        Excel::create($name, function($excel) use($exportar) {
                $excel->sheet('Facturas', function ($sheet) use ($exportar) {
                    $sheet->fromArray($exportar[0][0]);
                });
                $excel->sheet('Conceptos', function ($sheet) use ($exportar) {
                    $sheet->fromArray($exportar[0][1]);
                });
        })->store('xls',storage_path('app/public/'). '/'.$persona->rfc.'/exports');
        $columnas = array(
            ["head" => "Tipo de factura"],
            ["head" => "Razón social"],
            ["head" => "Fecha"],
            ["head" => "Monto total"],
            ["head" => "Estatus"],
            ["head" => "Acciones"]
        );

        $url=url('back/exportlayout/'.$persona->id_persona.'/'.$name);

        return view('factura.listadoback',['datos' => $datos, 'columnas' => $columnas , 'url'=>$url]);
    }

    public function decargalayout($id,$file){
        if(personas::where('id_persona','=',$id)->exists()){
            $persona= personas::find($id);
        }else{
            return back();
        }
        return response()->download(storage_path('app/public/'). '/'.$persona->rfc.'/exports/'.$file.'.xls');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user('Admin')){
            return redirect('/');
        }
        $usuario=User::find($id);
        $dir=0;
        if ($usuario->hasRole('user')){
            $dir=1;
        }
        $datosperosna=$usuario->persona->datospersona;
        $datosfiscales=$usuario->persona->datosfiscales;
        $usuario->delete();
        $datosfiscales->delete();
        $datosperosna->delete();

        return redirect('back/usuarios/'.$dir);
    }
}
