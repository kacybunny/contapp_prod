<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PostNewNotification extends Notification
{
    use Queueable;


    private $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hola '.$this->data['user'])
                    ->line('Ya está lista la solicitud de '.$this->data['title'].' en tu portal, entra directamente a consultarlo ')
                    ->action($this->data['title'], url($this->data['url']))
                    ->subject('Tu '.$this->data['title'].' está disponible');
    }

    public function toDatabase($notifiable)
    {
        return [
            'id'=>$this->data['id'],
            'user'=>$this->data['user'],
            'title'=>$this->data['title'],
            'data'=>$this->data['data'],
            'url'=>$this->data['url'],
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
//    public function toArray($notifiable)
//    {
//        return [
//            'id'=>$this->data['id'],
//            'title'=>$this->data['title'],
//            'data'=>$this->data['data']
//        ];
//    }
}
