<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plazos extends Model
{
    protected $table='plazos';

    protected $primaryKey='id_plazo';

    protected $fillable=[
        'id_plan', 'titulo', 'mesnaje', 'intervalo', 'prueba', 'porcentaje', 'visible',
    ];
    public function plan()
    {
        return $this->hasOne('App\Models\Planes', 'id_plan', 'id_plan');
    }


}
