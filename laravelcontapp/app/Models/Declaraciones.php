<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Declaraciones extends Model
{
    protected $table = 'declaraciones';

    protected $primaryKey = 'id_declaraciones';

    protected $fillable = [
        'id_persona', 'mes', 'anio', 'monto_mes', 'ingresos_mes','iva_ret_mes','deducciones_mes', 'iva_acre_mes', 'iva_pag_mes', 'isr_mes', 'isr_pag_mes',
    ];

    public function persona()
    {
        return $this->hasOne('App\Models\personas', 'id_persona', 'id_persona');
    }
    public function meses()
    {
        return $this->hasOne('App\Models\Meses', 'number', 'mes');
    }
}
