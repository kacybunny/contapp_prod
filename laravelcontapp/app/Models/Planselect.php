<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planselect extends Model
{

    protected $table='planselect';

    protected $primaryKey='id_selectplan';

    protected $fillable=[
        'id_usuario', 'id_plan'
    ];

    public function plan()
    {
        return $this->hasOne('App\Models\Planes', 'id_plan', 'id_plan');
    }

    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }
}
