<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleDeclaracion extends Model
{
    protected $table = 'detalledeclaracion';

    protected $primaryKey = 'id_detalle_decalarcion';

    protected $fillable = [
        'id_usuario', 'ticket','archivorevision','archivofinal','paso', 'pago', 'entregado',
    ];

    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }

    public function peticion()
    {
        return $this->hasOne('App\Models\Peticion', 'ticket', 'ticket');
    }
}
