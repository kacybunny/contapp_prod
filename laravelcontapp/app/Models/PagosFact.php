<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagosFact extends Model
{
    protected $table = 'pagos_facts';

    protected $primaryKey = 'id_pagos';

    protected $fillable = [
        'id_factura', 'id_usuario', 'pago', 'tipo', 'fecha',
    ];

    public function factura()
    {
        return $this->hasOne('App\Models\Factura', 'id_factura', 'id_factura');
    }

    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }
}
