<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class datospersona extends Model
{
    protected $table = 'datospersona';

    protected $primaryKey = 'id_datos_persona';

    protected $fillable = [
        'telefono', 'id_codepostal', 'localidad', 'vialidad', 'num_int', 'num_ext',
    ];

    public function persona()
    {
        return $this->belongsTo('App\Models\personas', 'id_persona', 'id_persona');
    }

    public function cp(){
        return $this->hasOne('App\Models\Codepostals', 'id_codepostal', 'id_codepostal');
    }

}
