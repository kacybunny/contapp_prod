<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $table = 'facturas';

    protected $primaryKey = 'id_factura';

    protected $fillable = [
        'id_persona', 'tipo', 'version', 'serie', 'folio', 'fecha', 'uuid', 'file', 'pagada', 'fechapago', 'formaPago', 'subtotal','descuento', 'total', 'moneda', 'tipoCambio', 'TipoComprobante', 'metodoPago', 'lugarExpedicion','rfcEmisor', 'nombreEmisor', 'regimenEmisor', 'rfcReceptor', 'nombreReceptor', 'residenciaReceptor', 'impuesto','importeImp', 'tasaImp','factura','conceptos',
    ];

    protected $casts = [
        'conceptos' => 'array',
        'factura' => 'array',
    ];

    public function persona()
    {
        return $this->hasOne('App\Models\personas', 'id_persona', 'id_persona');
    }
    public function pagos()
    {
        return $this->hasMany('App\Models\PagosFact', 'id_factura', 'id_factura');
    }
}
