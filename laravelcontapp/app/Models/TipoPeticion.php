<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPeticion extends Model
{
    protected $table = 'tipopeticion';

    protected $primaryKey = 'id_tipo_peticion';

    protected $fillable = [
        'tipo',
    ];

    public function peticiones()
    {
        return $this->hasMany('App\Models\Peticion', 'id_tipo_peticion', 'id_tipo_peticion');
    }
}
