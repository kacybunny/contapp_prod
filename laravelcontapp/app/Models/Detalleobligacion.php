<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detalleobligacion extends Model
{
    protected $table = 'detalleobligacion';

    protected $primaryKey = 'id_detalleobligacion';

    protected $fillable = [
        'id_persona'
    ];
    protected $casts = [
        'obligaciones' => 'array',
        'fechas' => 'array'
    ];

    public function persona()
    {
        return $this->hasOne('App\Models\personas', 'id_persona', 'id_persona');
    }
}
