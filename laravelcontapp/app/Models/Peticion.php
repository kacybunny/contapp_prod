<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peticion extends Model
{
    protected $table = 'peticion';

    protected $primaryKey = 'id_peticion';

    protected $fillable = [
        'id_peticion', 'id_tipo_peticion','id_usuarioasignado', 'id_usuario', 'ticket', 'mes', 'ano', 'id_detalle',
    ];

    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }

    public function usuarioasignado()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuarioasignado');
    }

    public function tipo()
    {
        return $this->hasOne('App\Models\TipoPeticion', 'id_tipo_peticion', 'id_tipo_peticion');
    }

    public function detalledeclaracion()
    {
        return $this->belongsTo('App\Models\DetalleDeclaracion', 'ticket', 'ticket');
    }

    public function detalleposicion()
    {
        return $this->belongsTo('App\Models\DetallePosicionfinan', 'ticket', 'ticket');
    }
    public function detallecalculo()
    {
        return $this->belongsTo('App\Models\DetalleCalculoimp', 'ticket', 'ticket');
    }
    public function meses()
    {
        return $this->hasOne('App\Models\Meses', 'number', 'mes');
    }

}
