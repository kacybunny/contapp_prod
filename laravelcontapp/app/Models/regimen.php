<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class regimen extends Model
{
    protected $table = 'regimenfiscal';

    protected $primaryKey = 'id';

    protected $fillable = [
        'c_RegimenFiscal','	Descripcion',
    ];
}
