<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detalleactividad extends Model
{
    protected $table = 'detalleactividad';

    protected $primaryKey = 'id_detalleactividad';

    protected $fillable = [
        'id_persona'
    ];
    protected $casts = [
        'actividades' => 'array',
        'fechas' => 'array'
    ];

    public function persona()
    {
        return $this->hasOne('App\Models\personas', 'id_persona', 'id_persona');
    }
}
