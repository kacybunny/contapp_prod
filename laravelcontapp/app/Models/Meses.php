<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meses extends Model
{
    protected $table = 'meses';

    protected $primaryKey = 'id_mes';

    protected $fillable = [
        'number','mes'
    ];

    public function declaraciones()
    {
        return $this->belongsToMany('App\Models\Declaraciones','declaraciones' ,'id_declaraciones', 'id_mes');
    }
}
