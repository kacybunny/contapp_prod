<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleCalculoimp extends Model
{
    protected $table = 'detallecalculoimp';

    protected $primaryKey = 'id_detalle_calculo';

    protected $fillable = [
        'id_usuario', 'ticket', 'archivo', 'entregado',
    ];

    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }

    public function peticion()
    {
        return $this->hasOne('App\Models\Peticion', 'ticket', 'ticket');
    }
}
