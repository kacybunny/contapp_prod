<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webhooks extends Model
{
    protected $table='webhooks';
    protected $primaryKey='id_webhook';
    protected $fillable=[
        'id_usuario','usado','tipo','objeto',
    ];
    protected $casts=[
        'objeto'=>'array'
    ];
    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }
}
