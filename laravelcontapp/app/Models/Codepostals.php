<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Codepostals extends Model
{
    protected $table = 'codepostals';

    protected $primaryKey = 'id_codepostal';

    protected $fillable = [
        'd_codigo', 'd_asenta', 'D_mnpio', 'd_estado',
    ];

    public function datospersona()
    {
        return $this->belongsTo('App\Models\datospersonas', 'id_codepostal', 'id_codepostal');
    }

}
