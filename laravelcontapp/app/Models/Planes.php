<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planes extends Model
{
    protected $table='planes';

    protected $primaryKey='id_plan';

    protected $fillable=[
     'plan', 'titulo', 'mensaje', 'url',
    ];

    public function plazo()
    {
        return $this->hasMany('App\Models\Plazos', 'id_plan', 'id_plan');
    }
    public function precio()
    {
        return $this->hasMany('App\Models\Precios', 'id_plan', 'id_plan');
    }
}
