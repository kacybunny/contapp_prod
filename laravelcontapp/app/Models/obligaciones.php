<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class obligaciones extends Model
{
    protected $table = 'obligaciones';

    protected $primaryKey = 'id';

    protected $fillable = [
        'desc',
    ];

//    public function datosfiscales()
//    {
//        return $this->belongsToMany('App\Models\Datosfiscales', 'datosfiscales','id', 'id_obligacion');
//    }
}
