<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetallePosicionfinan extends Model
{
    protected $table = 'detalleposicion';

    protected $primaryKey = 'id_detalle_posicion';

    protected $fillable = [
        'id_usuario', 'ticket', 'archivo', 'entregado',
    ];

    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }

    public function peticion()
    {
        return $this->hasOne('App\Models\Peticion', 'ticket', 'ticket');
    }
}
