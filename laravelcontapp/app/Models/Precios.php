<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Precios extends Model
{
    protected $table='precios';

    protected $primaryKey='id_precios';

    protected $fillable=[
        'id_plan', 'precio', 'fechaactivo',
    ];

    public function plan()
    {
        return $this->hasOne('App\Models\Planes', 'id_plan', 'id_plan');
    }

}
