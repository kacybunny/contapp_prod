<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detalleregimen extends Model
{
    protected $table = 'detalleregimen';

    protected $primaryKey = 'id_detalleregimen';

    protected $fillable = [
        'id_persona'
    ];
    protected $casts = [
        'regimenes' => 'array',
        'fechas' => 'array'
    ];

    public function persona()
    {
        return $this->hasOne('App\Models\personas', 'id_persona', 'id_persona');
    }
}
