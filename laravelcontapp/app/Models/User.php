<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Auth\Notifications\ResetPassword;
use Dinkbit\ConektaCashier\Billable;
use Dinkbit\ConektaCashier\Contracts\Billable as BillableContract;

class User extends Authenticatable
{
	use Notifiable;
	use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id_usuario';

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    protected $fillable = [
        'name', 'email', 'password','verified','email_token','active', 'conekta_active', 'conekta_id', 'conekta_subscription', 'conekta_plan', 'card_type', 'last_four', 'trial_ends_at', 'subscription_ends_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function persona()
    {
        return $this->belongsTo('App\Models\personas', 'id_usuario', 'id_usuario');
    }

    public function pagosfact()
    {
        return $this->hasMany('App\Models\PagosFact', 'id_usuario', 'id_usuario');
    }

    public function detalledeclaracion()
    {
        return $this->hasMany('App\Models\DetalleDeclaracion', 'id_usuario', 'id_usuario');
    }

    public function detalleposicion()
    {
        return $this->hasMany('App\Models\DetallePosicionfinan', 'id_usuario', 'id_usuario');
    }
    public function detallecalculo()
    {
        return $this->hasMany('App\Models\DetalleCalculoimp', 'id_usuario', 'id_usuario');
    }

    public function peticion()
    {
        return $this->hasMany('App\Models\Peticion', 'id_usuario', 'id_usuario');
    }

    public function webhook()
    {
        return $this->hasMany('App\Models\Webhooks', 'id_usuario', 'id_usuario');
    }

    public function plazo()
    {
        return $this->hasOne('App\Models\Plazos', 'id_plazo', 'conekta_plan');
    }
    public function verified()
    {
        $this->verified = 1;
        $this->email_token = null;
        $this->save();
    }

    public function unlocked()
    {
        $this->verified = 1;
        $this->active = 1;
        $this->email_token = null;
        $this->save();
    }

    public function planselect (){

        return $this->hasOne('App\Models\Planselect', 'id_usuario', 'id_usuario');

    }



    public function sendPasswordResetNotification($token) {
        return $this->notify(new ResetPassword($token,$this->name));
    }
}
