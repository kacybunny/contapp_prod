<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{

    protected $table = 'pagos';

    protected $primaryKey = 'id_pagos';

    protected $fillable = [
        'id_persona', 'id_plazo',
    ];

    public function persona()
    {
        return $this->hasOne('App\Models\personas', 'id_persona', 'id_persona');
    }
    public function plazo()
    {
        return $this->hasOne('App\Models\Plazos', 'id_plazo', 'id_plazo');
    }

}
