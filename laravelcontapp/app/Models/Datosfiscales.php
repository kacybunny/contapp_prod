<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Datosfiscales extends Model
{
    protected $table = 'datosfiscales';

    protected $primaryKey = 'id_datos_fiscales';

    protected $fillable = [
        'ini_oper','iva','pass_sat','key','cer','pass_privado',
    ];

}
