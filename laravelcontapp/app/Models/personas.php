<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class personas extends Model
{
    protected $primaryKey = 'id_persona';

    protected $fillable = [
        'id_datos_persona','id_datos_fiscales', 'id_usuario', 'nombre', 'paterno', 'materno', 'rfc','edad', 'foto', 'inicio_operaciones',
    ];

    public function datospersona()
    {
        return $this->hasOne('App\Models\datospersona', 'id_datos_persona', 'id_datos_persona');
    }

    public function datosfiscales()
    {
        return $this->hasOne('App\Models\Datosfiscales', 'id_datos_fiscales', 'id_datos_fiscales');
    }

    public function usuario()
    {
        return $this->hasOne('App\Models\User', 'id_usuario', 'id_usuario');
    }

    public function facturas()
    {
        return $this->hasMany('App\Models\Factura', 'id_persona', 'id_persona');
    }

    public function declaraciones ()
    {
        return $this->hasMany('App\Models\Declaraciones', 'id_persona', 'id_persona');
    }

    public function tempfact ()
    {
        return $this->hasMany('App\Models\Tempfac', 'id_persona', 'id_persona');
    }

    public function obligaciones ()
    {
        return $this->belongsTo('App\Models\Detalleobligacion', 'id_persona', 'id_persona');
    }
    public function actividades ()
    {
        return $this->belongsTo('App\Models\Detalleactividad', 'id_persona', 'id_persona');
    }
    public function regimenes ()
    {
        return $this->belongsTo('App\Models\Detalleregimen', 'id_persona', 'id_persona');
    }


}
