<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actividades extends Model
{
    protected $table = 'actividades_economicas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'codigo', 'descripcion'
    ];

}
