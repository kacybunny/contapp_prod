<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tempfac extends Model
{
    protected $table = 'tempfact';

    protected $primaryKey = 'id_tempfact';

    protected $casts = [
        'factura' => 'array',
    ];

    protected $fillable = [
        'id_persona','factura','tipo','fecha', 'uuid', 'file'
    ];

    public function persona()
    {
        return $this->hasOne('App\Models\personas', 'id_persona', 'id_persona');
    }
}
