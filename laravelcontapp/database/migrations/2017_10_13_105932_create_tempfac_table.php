<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempfacTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TempFact', function (Blueprint $table) {
            $table->increments('id_tempfact');
            $table->integer('id_persona')->unsigned();
            $table->string('tipo');
            $table->date('fecha');
            $table->string('uuid');
            $table->string('file');
            $table->text('factura');
            $table->foreign('id_persona')->references('id_persona')->on('personas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TempFact');
    }
}
