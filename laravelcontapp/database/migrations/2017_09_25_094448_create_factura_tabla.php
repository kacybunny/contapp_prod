<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturaTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id_factura');
            $table->integer('id_persona')->unsigned();
            $table->string('tipo');
            $table->string('uuid');
            $table->string('file');
            $table->tinyInteger('pagada')->default(0);
            $table->string('version');
            $table->string('serie');
            $table->string('folio');
            $table->date('fecha');
            $table->string('formaPago');
            $table->double('subtotal', 15, 8);
            $table->double('descuento', 15, 8);
            $table->double('total', 15, 8);
            $table->string('moneda');
            $table->double('tipoCambio', 15, 8);
            $table->string('TipoComprobante');
            $table->string('metodoPago');
            $table->string('lugarExpedicion');
            $table->string('rfcEmisor');
            $table->string('nombreEmisor');
            $table->string('regimenEmisor');
            $table->string('rfcReceptor');
            $table->string('nombreReceptor');
            $table->string('residenciaReceptor');
            $table->string('impuesto');
            $table->double('importeImp');
            $table->string('tasaImp');
            $table->text('conceptos');
            $table->text('factura');
            $table->foreign('id_persona')->references('id_persona')->on('personas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
