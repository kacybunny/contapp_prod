<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleobligaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalleobligacion', function (Blueprint $table) {
            $table->increments('id_detalleobligacion');
            $table->integer('id_persona')->unsigned();
            $table->text('obligaciones');
            $table->text('fechas');
            $table->foreign('id_persona')->references('id_persona')->on('personas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalleobligacion');
    }
}
