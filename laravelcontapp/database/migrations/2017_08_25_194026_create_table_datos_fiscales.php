<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDatosFiscales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datosfiscales', function (Blueprint $table) {
            $table->increments('id_datos_fiscales');
            $table->date('ini_oper')->nullable();
            $table->string('iva')->nullable();
            $table->string('pass_sat')->nullable();
            $table->string('key')->nullable();
            $table->string('cer')->nullable();
            $table->string('pass_privado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datosfiscales');
    }
}
