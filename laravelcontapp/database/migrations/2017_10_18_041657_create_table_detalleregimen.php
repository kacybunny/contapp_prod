<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleregimen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalleregimen', function (Blueprint $table) {
            $table->increments('id_detalleregimen');
            $table->integer('id_persona')->unsigned();
            $table->text('regimenes');
            $table->text('fechas');
            $table->foreign('id_persona')->references('id_persona')->on('personas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalleregimen');
    }
}
