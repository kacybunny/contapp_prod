<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleCalculoimp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detallecalculoimp', function (Blueprint $table) {
            $table->increments('id_detalle_calculo');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->string('ticket');
            $table->string('archivo')->nullable();
            $table->tinyInteger('entregado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detallecalculoimp');
    }
}
