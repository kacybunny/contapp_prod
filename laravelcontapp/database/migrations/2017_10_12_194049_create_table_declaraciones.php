<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDeclaraciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declaraciones', function (Blueprint $table) {
            $table->increments('id_declaraciones');
            $table->integer('id_persona')->unsigned();
            $table->string('mes');
            $table->string('anio');
            $table->double('monto_mes', 15, 8);
            $table->double('ingresos_mes', 15, 8);
            $table->double('iva_ret_mes', 15, 8);
            $table->double('deducciones_mes', 15, 8);
            $table->double('iva_acre_mes', 15, 8);
            $table->double('iva_pag_mes', 15, 8);
            $table->double('isr_mes', 15, 8);
            $table->double('isr_pag_mes', 15, 8);
            $table->foreign('id_persona')->references('id_persona')->on('personas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declaraciones');
    }
}
