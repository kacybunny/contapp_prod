<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlanselect extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planselect', function (Blueprint $table) {
            $table->increments('id_selectplan');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_plan')->unsigned();
            $table->tinyInteger('watch')->default(0);
            $table->foreign('id_plan')->references('id_plan')->on('planes')->onDelete('cascade');
            $table->foreign('id_usuario')->references('id_usuario')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
