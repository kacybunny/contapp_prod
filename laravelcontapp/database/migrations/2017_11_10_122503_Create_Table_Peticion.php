<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeticion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peticion', function (Blueprint $table) {
            $table->increments('id_peticion');
            $table->integer('id_tipo_peticion')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_usuarioasignado')->unsigned()->nullable();
            $table->string('ticket');
            $table->integer('mes');
            $table->string('ano');
            $table->integer('id_detalle');
            $table->foreign('id_tipo_peticion')->references('id_tipo_peticion')->on('tipopeticion')->onDelete('cascade');
            $table->foreign('id_usuario')->references('id_usuario')->on('users')->onDelete('cascade');
            $table->foreign('id_usuarioasignado')->references('id_usuario')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peticion');
    }
}
