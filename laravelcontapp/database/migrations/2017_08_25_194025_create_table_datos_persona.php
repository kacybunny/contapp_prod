<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDatosPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datospersona', function (Blueprint $table) {
            $table->increments('id_datos_persona');
            $table->integer('id_codepostal')->nullable()->unsigned();
            $table->string('telefono')->nullable();
            $table->string('localidad')->nullable();
            $table->string('vialidad')->nullable();
            $table->string('num_int')->nullable();
            $table->string('num_ext')->nullable();
//            $table->foreign('id_entidad')->references('id_entidad')->on('entidades')->onDelete('cascade');
//            $table->foreign('id_municipio')->references('id_municipio')->on('municipios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datospersona');
    }
}
