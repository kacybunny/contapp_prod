<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodepostalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('Codepostals', function (Blueprint $table) {
            $table->increments('id_codepostal');
            $table->string('d_codigo');
            $table->string('d_asenta');
            $table->string('D_mnpio');
            $table->string('d_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Codepostals');
    }
}
