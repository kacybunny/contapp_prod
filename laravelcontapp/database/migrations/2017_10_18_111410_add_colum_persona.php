<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->integer('id_datos_fiscales')->unsigned()->after('id_usuario');
            $table->foreign('id_datos_fiscales')->references('id_datos_fiscales')->on('datosfiscales')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->dropForeign(['id_datos_fiscales']);
            $table->dropColumn('id_datos_fiscales');
        });
    }
}
