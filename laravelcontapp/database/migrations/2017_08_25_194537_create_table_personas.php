<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id_persona');
            $table->integer('id_datos_persona')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_datos_fiscales')->unsigned();
            $table->string('nombre')->nullable();
            $table->string('paterno')->nullable();
            $table->string('materno')->nullable();
            $table->string('rfc')->nullable();
            $table->integer('edad')->nullable();
            $table->string('foto')->nullable();
            $table->string('inicio_operaciones')->nullable();
            $table->foreign('id_datos_persona')->references('id_datos_persona')->on('datospersona');
            $table->foreign('id_usuario')->references('id_usuario')->on('users')->onDelete('cascade');
            $table->foreign('id_datos_fiscales')->references('id_datos_fiscales')->on('datosfiscales')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
