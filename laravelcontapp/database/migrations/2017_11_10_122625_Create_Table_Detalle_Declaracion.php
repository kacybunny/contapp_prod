<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleDeclaracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalledeclaracion', function (Blueprint $table) {
            $table->increments('id_detalle_decalarcion');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->string('ticket');
            $table->string('archivorevision')->nullable();
            $table->string('archivofinal')->nullable();
            $table->tinyInteger('paso')->default(4);
            $table->tinyInteger('entregado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalledeclaracion');
    }
}
