<?php

use Illuminate\Database\Seeder;
use App\Models\Plazos;
use App\Models\Planes;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class planes_and_plazos_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */



    public function run()
    {
        $basico=Planes::create([
            'plan' => 'Basico',
            'precio' => 0,
            'titulo' => 'Tú manejas las cosas, nosotros te damos la herramienta',
            'mensaje' => 'Con nuestro sistema, administra mejor tus facturas y se te facilitará el cálculo mensual de tus declaraciones',
            'fechaactivo'=>'',
            'fechainactivo'=>'',
        ]);

        $medio=Planes::create([
            'plan' => 'Medio',
            'precio' => 500,
            'titulo' => 'Hacemos la parte pesada, tú sólo revisas y declaras',
            'mensaje' => 'Conseguimos tus facturas por ti, te decimos que puedes deducir y te damos recomendaciones para que ahorres en impuestos',
            'fechaactivo'=>'',
            'fechainactivo'=>'',
        ]);

        $completo=Planes::create([
            'plan' => 'Completo',
            'precio' => 800,
            'titulo' => '¡Nosotros hacemos todo!',
            'mensaje' => 'Presentamos tus declaraciones, facturamos, te hacemos recomendaciones y te acompañamos todo el mes para hacerte la vida más fácil',
            'fechaactivo'=>'',
            'fechainactivo'=>'',
        ]);


        Plazos::create([
            'id_plan' => $basico->id_plan,
            'meses' =>1,
            'titulo' =>'EN PAGO MENSUAL',
            'mesnaje' =>'cada mes',
            'descuento' =>0,
        ]);
        Plazos::create([
            'id_plan' => $basico->id_plan,
            'meses' =>12,
            'titulo' =>'EN PAGO ANUAL',
            'mesnaje' =>'por todo un año',
            'descuento' =>0,
        ]);

        Plazos::create([
            'id_plan' => $medio->id_plan,
            'meses' =>1,
            'titulo' =>'EN PAGO MENSUAL',
            'mesnaje' =>'cada mes',
            'descuento' =>0,
        ]);
        Plazos::create([
            'id_plan' => $medio->id_plan,
            'meses' =>12,
            'titulo' =>'EN PAGO ANUAL',
            'mesnaje' =>'por todo un año',
            'descuento' =>0.6,
        ]);

        Plazos::create([
            'id_plan' => $completo->id_plan,
            'meses' =>1,
            'titulo' =>'EN PAGO MENSUAL',
            'mesnaje' =>'cada mes',
            'descuento' =>0,
        ]);
        Plazos::create([
            'id_plan' => $completo->id_plan,
            'meses' =>12,
            'titulo' =>'EN PAGO ANUAL',
            'mesnaje' =>'por todo un año',
            'descuento' =>0.625,
        ]);

        $role = Role::create(['name' => 'backuser']);
        $role = Role::create(['name' => 'user']);
    }
}
