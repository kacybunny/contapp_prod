<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@index');
Route::get('register/{plan}', 'Auth\RegisterController@showRegistrationFormplan')->where('plan', '^[0-9]+$');
Route::prefix('register')->group(function () {
    Route::get('verify/{token}','Auth\RegisterController@verify');
    Route::get('active/{token}','Auth\LoginController@activeusr');
    Route::post('complete','Auth\RegisterController@terminateRegister');
    Route::post('exito','Auth\LoginController@changepass');
    Route::get('reenvioverificacion/{dato}','Auth\RegisterController@reenvio');
    Route::get('complete', function()
    {
        return redirect('/');
    });
    Route::get('exito', function()
    {
        return redirect('/');
    });


});
Route::prefix('webhooks')->group(function () {
    Route::post('prueba','WebhooksController@recibewebhoook');
});



Route::middleware(['auth'])->group(function () {

    Route::post('import','Import@importcsv');
    Route::post('pagada/{id}','FacturaController@facturapagada');
    Route::post('declaracionpagada/{id}','PeticionController@declaracionpagada');
    Route::put('nopagada/{id}','FacturaController@facturanopagada');
    Route::resource('factura', 'FacturaController');
    Route::resource('persona', 'PersonasController');
    Route::resource('datospersona', 'DatospersonaController');
    Route::resource('datosfiscales', 'DatosfiscalesController');
    Route::resource('notified', 'NotifiedController');
    Route::resource('declaracion', 'DeclaracionesAntController');
    Route::get('construct', function (){
        return view('construct');
    });
    Route::resource('pagos', 'PagosController');
    Route::resource('peticiones', 'PeticionController');
    Route::get('buscarpeticion/{id}', 'PeticionController@buscarform');
    Route::get('peticionesresult/{id}','PeticionController@buscarpeticion');
    Route::get('subefact/{id}','FacturaController@create2');
    Route::get('facturasave','FacturaController@guardafactura');
    Route::get('facturaclean','FacturaController@limpiarlista');
    Route::get('facturasave/{id}','FacturaController@guardafactura2');
    Route::get('facturaclean/{id}','FacturaController@limpiarlista2');
    Route::get('facturas/{id}','FacturaController@buscarform');
    Route::get('facturasresult/{id}','FacturaController@buscarfactura');
    Route::get('pagosfact','FacturaController@busquedapagos');
    Route::get('pagosfactsearch','FacturaController@pagosfact');
    Route::post('facturas/{id}','FacturaController@store2');

    Route::get('checkout', function()
    {
        return view('pagos.checkout');
    });
    Route::get('persona','PersonasController@personajson');
    Route::prefix('api')->group(function () {
        Route::get('persona','PersonasController@personajson');
        Route::get('persona/{id}','PersonasController@personajson2');
        Route::post('persona','PersonasController@rfconly');
        Route::get('tempfact','FacturaController@facturastemporales');
        Route::get('tempfact/{id}','FacturaController@facturastemporales2');
        Route::get('eliminatempfact/{id}','FacturaController@eliminafactemp');
        Route::post('notificacion','NotifiedController@readednote');
        Route::get('descarga/{ticket}','PeticionController@descarga');
        Route::get('descargadeclaracion/{ticket}','PeticionController@descargadeclaracion');
        Route::get('acepta/{act}/{ticket}','PeticionController@aceptadeclaracion');
        Route::get('miplan','PlanesController@miplan');
        Route::get('descargafact/{id}','FacturaController@descarga');
    });

    Route::get('/views/{view}', function($view)
    {
        $usuario = \App\Models\User::first();
        return view($view,['user'=>$usuario]);
    });

    Route::get('vistomensaje','DatospersonaController@pasosReg');
    Route::get('codepostal/{code}','DatospersonaController@codpostal');

    Route::group(['middleware' => ['role:backuser']], function () {
        Route::prefix('back')->group(function () {
            Route::get('declaraciones/{id}/edit','DeclaracionesAntController@editadeclaracion');
            Route::delete('declaraciones/{id}','DeclaracionesAntController@borradeclaracion');
            Route::put('declaraciones/{id}','DeclaracionesAntController@updatedeclaracion');
            Route::post('declaraciones/{id}','DeclaracionesAntController@agregadeclacionantback');
            Route::get('declaraciones/{id}','DeclaracionesAntController@declaracionesantback');
            Route::get('export/{id}','UsuariosController@formexport');
            Route::get('exportlay/{id}','UsuariosController@export');
            Route::get('exportlayout/{id}/{file}','UsuariosController@decargalayout');
            Route::get('planeslist','PlanesController@indexplan');
            Route::get('plazoslist','PlanesController@indexplazo');
            Route::get('precioslist','PlanesController@indexprecios');
            Route::get('updateplan/{id}','PlanesController@modificaplan');
            Route::get('updateplazo/{id}','PlanesController@modificaplazo');
            Route::put('updateplan/{id}','PlanesController@updateplan');
            Route::put('updateplazo/{id}','PlanesController@updateplazo');
            Route::post('storeprecio','PlanesController@storeprecio');
            Route::get('creaprecio','PlanesController@creaprecio');
            Route::resource('planes', 'PlanesController');
            Route::resource('peticiones', 'PeticionController');
            Route::resource('usuarios', 'UsuariosController');
            Route::get('peticiones/{ticket}/reasigna','PeticionController@reasigna');
            Route::post('peticiones/{ticket}/reasigna','PeticionController@asigna');
            Route::post('usuarios/{id}/mens','UsuariosController@notificar');
            Route::get('usuarios/{id}/mens','UsuariosController@mensaje');
            Route::get('usuarios/{id}/act','UsuariosController@activar');
            Route::get('usuarios/{id}/desact','UsuariosController@desactivar');
            Route::put('persona/{id}','UsuariosController@formpersona');
            Route::put('fiscales/{id}','UsuariosController@formactoblreg');
            Route::put('firmas/{id}','UsuariosController@formfirmas');
            Route::put('datospersona/{id}','UsuariosController@formubicacion');
            Route::get('descarga/{ticket}','PeticionController@descarga');
            Route::get('descargadeclaracion/{ticket}','PeticionController@descargadeclaracion');
            Route::put('cargadeclaracion/{ticket}','PeticionController@updatedeclaracion');
            Route::get('reset/{id}','UsuariosController@linkpassword');
            Route::get('permisouser/{id}','UsuariosController@formpermisos');
            Route::put('permisouser/{id}','UsuariosController@permisos');
            Route::get('suscript/{id}','UsuariosController@cancelsuscription');
            Route::prefix('api')->group(function () {
//                Route::get('planes','PlanesController@planeslist');
                Route::post('asignar/{id}','PeticionController@asignarpeticion');
            });
        });
    });
});

