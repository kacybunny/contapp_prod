var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Gentelella vendors path : vendor/bower_components/gentelella/vendors

elixir(function(mix) {
    
    /********************/
    /* Copy Stylesheets */
    /********************/

    // Bootstrap
    mix.copy('vendor/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css', '../public_html/css/bootstrap.min.css');

    // Font awesome
    mix.copy('vendor/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css', '../public_html/css/font-awesome.min.css');

    // Gentelella
    mix.copy('vendor/bower_components/gentelella/build/css/custom.min.css', '../public_html/css/gentelella.min.css');

    // animate
    mix.copy('vendor/bower_components/gentelella/vendors/animate.css/animate.min.css', '../public_html/css/animate/animate.min.css');

    // iCheck
    mix.copy('vendor/bower_components/gentelella/vendors/iCheck', '../public_html/css/iCheck');

    // nprogress
    mix.copy('vendor/bower_components/gentelella/vendors/nprogress/nprogress.css', '../public_html/css/nprogress/nprogress.css');

    // bootstrap-daterangepicker
    mix.copy('vendor/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css', '../public_html/css/daterangepicker/daterangepicker.css');

    // NProgress
    mix.copy('vendor/bower_components/gentelella/vendors/nprogress/nprogress.css', '../public_html/css/nprogress/nprogress.css');

    // bootstrap-wysiwyg
    mix.copy('vendor/bower_components/gentelella/vendors/google-code-prettify/bin/prettify.min.css', '../public_html/css/google-code-prettify/prettify.min.css');
    // Select2
    mix.copy('vendor/bower_components/gentelella/vendors/select2/dist/css/select2.min.css', '../public_html/css/select2/select2.min.css');
    mix.copy('vendor/bower_components/gentelella/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css', '../public_html/css/select2-bootstrap-theme/select2-bootstrap.min.css');
    mix.copy('vendor/bower_components/gentelella/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.css', '../public_html/css/select2-bootstrap-theme/select2-bootstrap.css');
    mix.copy('vendor/bower_components/gentelella/node_modules/select2-bootstrap-css/select2-bootstrap.min.css', '../public_html/css/select2-bootstrap-css/select2-bootstrap.min.css');

    //bootstrap-select
    mix.copy('vendor/bower_components/gentelella/node_modules/bootstrap-select/dist/css/bootstrap-select.css', '../public_html/css/bootstrap-select/bootstrap-select.css');
    mix.copy('vendor/bower_components/gentelella/node_modules/bootstrap-select/dist/css/bootstrap-select.min.css', '../public_html/css/bootstrap-select/bootstrap-select.min.css');


    // Switchery
    mix.copy('vendor/bower_components/gentelella/vendors/switchery/dist/switchery.min.css', '../public_html/css/switchery/switchery.min.css');
    // starrr
    mix.copy('vendor/bower_components/gentelella/vendors/starrr/dist/starrr.css', '../public_html/css/starrr/starrr.css');

    // Datatables
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css', '../public_html/css/datatables/dataTables.bootstrap.min.css');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css', '../public_html/css/datatables/buttons.bootstrap.min.css');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css', '../public_html/css/datatables/fixedHeader.bootstrap.min.css');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css', '../public_html/css/datatables/responsive.bootstrap.min.css');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css', '../public_html/css/datatables/scroller.bootstrap.min.css');

    // dropzone
    mix.copy('vendor/bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.css', '../public_html/css/dropzone/dropzone.min.css');

    // pnotify
    mix.copy('vendor/bower_components/gentelella/vendors/pnotify/dist/pnotify.css', '../public_html/css/pnotify/pnotify.css');

    mix.copy('vendor/bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.css', '../public_html/css/pnotify/pnotify.buttons.css');

    mix.copy('vendor/bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.css', '../public_html/css/pnotify/pnotify.nonblock.css');

    /****************/
    /* Copy Scripts */
    /****************/

    // dropzone
    mix.copy('vendor/bower_components/gentelella/vendors/dropzone/dist/min/dropzone.min.js', '../public_html/js/dropzone/dropzone.min.js');

    // Bootstrap
    mix.copy('vendor/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js', '../public_html/js/bootstrap.min.js');

    // jQuery
    mix.copy('vendor/bower_components/gentelella/vendors/jquery/dist/jquery.min.js', '../public_html/js/jquery.min.js');

    // Gentelella
    mix.copy('vendor/bower_components/gentelella/build/js/custom.min.js', '../public_html/js/gentelella.min.js');
    mix.copy('vendor/bower_components/gentelella/build/js/custom.js', '../public_html/js/gentelella.js');

    //bootstrap-select
    mix.copy('vendor/bower_components/gentelella/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js', '../public_html/js/bootstrap-select/bootstrap-select.min.js');

    // fastclick
    mix.copy('vendor/bower_components/gentelella/vendors/fastclick/lib/fastclick.js', '../public_html/js/fastclick/fastclick.js');

    // nprogress
    mix.copy('vendor/bower_components/gentelella/vendors/nprogress/nprogress.js', '../public_html/js/nprogress/nprogress.js');

    // bootstrap-progressbar
    mix.copy('vendor/bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js', '../public_html/js/progressbar/bootstrap-progressbar.min.js');

    // jquery-nicescroll
    mix.copy('vendor/bower_components/gentelella/vendors/jquery.nicescroll-master/jquery.nicescroll.min.js', '../public_html/js/nicescroll/jquery.nicescroll.min.js');

    // iCheck
    mix.copy('vendor/bower_components/gentelella/vendors/iCheck/icheck.min.js', '../public_html/js/iCheck/icheck.min.js');

    // cropper
    mix.copy('vendor/bower_components/gentelella/vendors/cropper/dist/cropper.min.js', '../public_html/js/cropper/cropper.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/cropper/main.js', '../public_html/js/cropper/profile.js');

    // moment
    mix.copy('vendor/bower_components/gentelella/vendors/moment/min/moment.min.js', '../public_html/js/moment/moment.min.js');

    // bootstrap-daterangepicker
    mix.copy('vendor/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js', '../public_html/js/daterangepicker/daterangepicker.js');

    // bootstrap-wysiwyg
    mix.copy('vendor/bower_components/gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js', '../public_html/js/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js');
    // bootstrap-wysiwyg
    mix.copy('vendor/bower_components/gentelella/vendors/jquery.hotkeys/jquery.hotkeys.js', '../public_html/js/jquery.hotkeys/jquery.hotkeys.js');
    mix.copy('vendor/bower_components/gentelella/vendors/google-code-prettify/src/prettify.js', '../public_html/js/google-code-prettify/prettify.js');
    // jQuery Tags Input
    mix.copy('vendor/bower_components/gentelella/vendors/jquery.tagsinput/src/jquery.tagsinput.js', '../public_html/js/jquery.tagsinput/jquery.tagsinput.js');
    // Switchery
    mix.copy('vendor/bower_components/gentelella/vendors/switchery/dist/switchery.min.js', '../public_html/js/switchery/switchery.min.js');
    // Select2
    mix.copy('vendor/bower_components/gentelella/vendors/select2/dist/js/select2.full.min.js', '../public_html/js/select2/select2.full.min.js');
    // Parsley
    mix.copy('vendor/bower_components/gentelella/vendors/parsleyjs/dist/parsley.min.js', '../public_html/js/parsleyjs/parsley.min.js');
    // Autosize
    mix.copy('vendor/bower_components/gentelella/vendors/autosize/dist/autosize.min.js', '../public_html/js/autosize/autosize.min.js');
    // jQuery autocomplete
    mix.copy('vendor/bower_components/gentelella/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js', '../public_html/js/devbridge-autocomplete/jquery.autocomplete.min.js');
    // starrr
    mix.copy('vendor/bower_components/gentelella/vendors/starrr/dist/starrr.js', '../public_html/js/starrr/starrr.js');

    // Chart.js
    mix.copy('vendor/bower_components/gentelella/vendors/Chart.js/src', '../public_html/js/Chart');

    // morris.js
    mix.copy('vendor/bower_components/gentelella/vendors/morris.js/morris.min.js', '../public_html/js/morris/morris.min.js');

    // raphael
    mix.copy('vendor/bower_components/gentelella/vendors/raphael/raphael.min.js', '../public_html/js/raphael/raphael.min.js');

    // pace
    mix.copy('vendor/bower_components/gentelella/vendors/pace-master/pace.min.js', '../public_html/js/pace/pace.min.js');

    <!-- Datatables -->
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js', '../public_html/js/datatable/jquery.dataTables.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js', '../public_html/js/datatable/dataTables.bootstrap.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js', '../public_html/js/datatable/dataTables.buttons.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js', '../public_html/js/datatable/buttons.bootstrap.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js', '../public_html/js/datatable/buttons.flash.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js', '../public_html/js/datatable/buttons.html5.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js', '../public_html/js/datatable/buttons.print.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js', '../public_html/js/datatable/dataTables.fixedHeader.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js', '../public_html/js/datatable/dataTables.keyTable.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js', '../public_html/js/datatable/dataTables.responsive.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js', '../public_html/js/datatable/responsive.bootstrap.js');
    mix.copy('vendor/bower_components/gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js', '../public_html/js/datatable/dataTables.scroller.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/jszip/dist/jszip.min.js', '../public_html/js/datatable/jszip.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/pdfmake/build/pdfmake.min.js', '../public_html/js/datatable/pdfmake.min.js');
    mix.copy('vendor/bower_components/gentelella/vendors/pdfmake/build/vfs_fonts.js', '../public_html/js/datatable/vfs_fonts.js');


    mix.copy('vendor/bower_components/gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js', '../public_html/js/jQuery-Smart-Wizard/jquery.smartWizard.js');

    mix.copy('vendor/bower_components/gentelella/vendors/RitsC-PrintArea-2cc7234/demo/jquery.PrintArea.js', '../public_html/js/RitsC-PrintArea-2cc7234/jquery.PrintArea.js');

    mix.copy('vendor/bower_components/gentelella/vendors/jquery-ui-1.12.1/jquery-ui.min.js', '../public_html/js/jquery-ui/jquery-ui.min.js');

    <!-- pnotify -->
    mix.copy('vendor/bower_components/gentelella/vendors/pnotify/dist/pnotify.js', '../public_html/js/pnotify/pnotify.js');
    mix.copy('vendor/bower_components/gentelella/vendors/pnotify/dist/pnotify.buttons.js', '../public_html/js/pnotify/pnotify.buttons.js');
    mix.copy('vendor/bower_components/gentelella/vendors/pnotify/dist/pnotify.nonblock.js', '../public_html/js/pnotify/pnotify.nonblock.js');


    /**************/
    /* Copy Fonts */
    /**************/

    // Bootstrap
    mix.copy('vendor/bower_components/gentelella/vendors/bootstrap/fonts/', '../public_html/fonts');

    // Font awesome
    mix.copy('vendor/bower_components/gentelella/vendors/font-awesome/fonts/', '../public_html/fonts');
});
