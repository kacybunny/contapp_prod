<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'exist' => 'No tenemos registrado a este usuario, <a href="'.url('register').'" class="to_register">¿deseas crear una cuenta?</a>',
    'throttle' => 'Demasiados intentos de inicio de sesión. Inténtalo de nuevo en :seconds segundos.',
    'bruteforce' => 'Demasiados intentos de inicio de sesión. Su cuenta esta inhabilitada. Enviamos por correo las instrucciones para la reactivación consulte su buzón',
    'dimiss' => 'Su cuenta esta deshabilitada comuníquese con el administrador para cualquier aclaración',


];
