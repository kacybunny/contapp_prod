@extends('datosfiscales.datosfiscales')

@section('title_section')
    <h2>Editar datos fiscales<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include('datosfiscales.formfirmas')
@endsection