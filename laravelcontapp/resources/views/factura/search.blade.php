@extends('factura.factura')

@section('title_section')
    <h2>Buscar facturas<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include('factura.busqueda')
@endsection
