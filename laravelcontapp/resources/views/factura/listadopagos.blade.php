@extends('factura.factura')

@section('title_section')
    <h2>Listado pagos facturas<small></small></h2>
@endsection

@section('contentx')
    <div class="row">
        <div class="col-xs-12" >
            <div class="col-xs-5" >
                {!! BootForm::radios('estatus','Tipo', ['Pago'   => ' Pago','Cobro' => ' Cobro','' => ' Ambos'],null,true,['class'=>'flat']) !!}
            </div>
        </div>
    </div>
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-personal" class="table table-striped table-bordered">
    </table>
@endsection
@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        configtable.data={!! json_encode($datos,JSON_UNESCAPED_UNICODE) !!};
        configtable.columns={!! json_encode($columnas,JSON_UNESCAPED_UNICODE) !!};
        funciondialogo=function () {
            fecha = $('#fechapago').val();
            pago = $('#pago').val();
            datos={ 'fecha':fecha , 'pago':pago };
            return datos;
        };

        function accionespage (){
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            $('.detallefact').off( "click").click(function () {
                datatabx=table.row( $(this).parent().parent() ).data();
                $('#nomrecep').html(datatabx[5].receptor['{{'@attributes'}}']['nombre']);
                $('#rfcrecept').html(datatabx[5].receptor['{{'@attributes'}}']['rfc']);
                $('#nomemi').html(datatabx[5].emisor['{{'@attributes'}}']['nombre']);
                $('#rfcemi').html(datatabx[5].emisor['{{'@attributes'}}']['rfc']);
                $('#subtotalfact').html(mascaraMoneda(datatabx[5]['{{'@attributes'}}']['subtotal'],'$'));
                $('#totalfact').html(mascaraMoneda(datatabx[5]['{{'@attributes'}}']['total'],'$'));
                $('#impuestosfact').html(mascaraMoneda((parseFloat(datatabx[5]['{{'@attributes'}}']['total'])-parseFloat(datatabx[5]['{{'@attributes'}}']['subtotal'])),'$'));
                $('#fechafact').html(datatabx[6]);
                $('#tipofact').html(datatabx[7].capitalize());
                var dataconcepto='';
                var conceptos= '';
                var head='';
                var cabeceras=[];
                if (datatabx[5].conceptos.concepto['{{'@attributes'}}'] != undefined){
                    dataconcepto += '<tr>';
                    $.each(datatabx[5].conceptos.concepto['{{'@attributes'}}'],function (llave,valor) {
                        if (cabeceras.indexOf(llave) < 0){
                            cabeceras.push(llave);
                            head+='<th>'+llave.capitalize()+'</th>';
                        }
                        conceptos += '<td>'+valor+'</td>'
                    });
                    dataconcepto += conceptos+' </tr>';
                }else{
                    $.each(datatabx[5].conceptos.concepto,function (key,val) {
                        dataconcepto += '<tr>';
                        conceptos= '';
                        $.each(val['{{'@attributes'}}'],function (llave,valor) {
                            if (cabeceras.indexOf(llave) < 0){
                                cabeceras.push(llave);
                                head+='<th>'+llave.capitalize()+'</th>';
                            }
                            conceptos+='<td>'+valor+'</td>'
                        });
                        dataconcepto+= conceptos+' </tr> ';
                    });
                }
                $('#headconceptos').html(head);
                $('#conceptostabla').html(dataconcepto);
                $('#detalletempfactura').modal();
            });
        }
        init=function () {
            accionespage ()
        };
        tabladinamica('datatable-personal',init);
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var tipo=$("input[name='estatus']:checked").val();
                var estatus =  data[0] ;// use data for the age column
                if (estatus.indexOf(tipo) >= 0 )
                {
                    return true;
                }else if (tipo == undefined || tipo == ''){
                    return true;
                }
                return false;
            }
        );
        setTimeout(function(){  $("input[name='estatus']").each(function () {
            $(this).parent().click(function () {
                table.draw();
            });
            $(this).siblings().click(function () {
                table.draw();
            });
            $(this).parent().parent().click(function () {
                table.draw();
            });
        }); }, 1000);
        table.on('dblclick', 'tr', function () {
            {{--data = table.row( this ).data();--}}
            {{--if(data!=undefined){--}}
{{--//                console.log(data);--}}
                {{--dialogooption('{{url('back/api/asignar')}}/'+data[0],'<p>Deseas procesar la solicitud '+data[0]+'</p>','Asignar solicitud',data,true);--}}
            {{--}--}}
        } ).on('draw.dt', function() {
            accionespage ()
            // console.log('draw');
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
            accionespage ()
        } );

//        $("input[name='estatus']").each(function () {
//            $(this).parent().parent().click(function () {
//                table.draw();
//            })
//        });
    });
</script>
@endpush