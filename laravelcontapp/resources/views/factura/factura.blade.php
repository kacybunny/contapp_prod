@extends('layouts.blank')

@extends('factura.js')

@extends('factura.css')

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Facturas
                        {{--<small>Some examples to get you started</small>--}}
                    </h3>
                </div>

                {{--<div class="title_right">--}}
                    {{--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">--}}
                        {{--<div class="input-group">--}}
                            {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                            {{--<span class="input-group-btn">--}}
                      {{--<button class="btn btn-default" type="button">Go!</button>--}}
                    {{--</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            @yield('title_section')
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @yield('contentx')
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('modals.detalletempfact')
    <!-- /page content -->
@endsection

