@extends('factura.factura')

@section('title_section')
    <h2>Listado Facturas<small></small></h2>
@endsection

@section('contentx')
    <div class="row">
        <div class="col-xs-12" >
            <div class="col-xs-5" >
                {!! BootForm::radios('estatus','Estatus factura', ['Pagada'   => ' Pagada','Pendiente' => ' Pendiente','' => ' Ambos'],'',true,['class'=>'flat']) !!}
            </div>
        </div>
    </div>
    <center>
    <a href="{{$url}}" type="button" class="btn btn-success btn-lg" id="completa">Descargar layout</a>
    </center>
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-personal" class="table table-striped table-bordered">
    </table>
@endsection
@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        configtable.data={!! json_encode($datos,JSON_UNESCAPED_UNICODE) !!};
        configtable.columns={!! json_encode($columnas,JSON_UNESCAPED_UNICODE) !!};
        funciondialogo=function () {
            fecha = $('#fechapago').val();
            pago = $('#pago').val();
            datos={ 'fecha':fecha , 'pago':pago };
            return datos;
        };

        function accionespage (){
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            $('.payfact').click(function () {
                datatabx=table.row( $(this).parent().parent() ).data();
                validadialogo=function () {
                    dato = $('#fechapago').val();
                    dato2 = $('#pago').val();
                    if (dato!='' && dato2!=''){
                        rule='^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$';
                        rule2='^[0-9]+([.][0-9]+)?$';
                        if(dato.match(rule) && dato2.match(rule2)){
                            if(parseFloat(dato2) <= (parseFloat(datatabx[8].replace(',','')) - parseFloat(datatabx[7])) && dato2 != 0 ){
                                return true;
                            }else{
                                $('#pago').val('');
                                return false;
                            }
                        }else{
                            $('#fechapago').val('');
                            $('#pago').val('');
                            return false;
                        }
                    }else{
                        return false;
                    }
                };
                iniciadiag=function () {
                    cant= parseFloat(datatabx[8].replace(',','')) - parseFloat(datatabx[7]);
                    if(cant != 'NaN'){
                        $('#pago').val(cant.toFixed(2));
                    }else{
                        $('#pago').val(datatabx[8].toFixed(2));
                    }
                    restringe('pago','[^0-9.]');
                };
                mensaje=$(this).attr('data-mensaje');
                url=$(this).attr('data-url');
                dialogooption(url,mensaje,'Marcar pagos',[],true,'POST',funciondialogo,validadialogo,iniciadiag);
                configdatepicker.maxDate='0';
                $('.bootdatepick').datepicker(configdatepicker);
            });
            $('.editpayfact').click(function () {
                mensaje=$(this).attr('data-mensaje');
                url=$(this).attr('data-url');
                dialogooption(url,mensaje,'Marcar pagos',[],true,'PUT');
            });
            $('.detallefact').off( "click").click(function () {
                datatabx=table.row( $(this).parent().parent() ).data();
                $('#nomrecep').html(datatabx[6].receptor['{{'@attributes'}}']['nombre']);
                $('#rfcrecept').html(datatabx[6].receptor['{{'@attributes'}}']['rfc']);
                $('#nomemi').html(datatabx[6].emisor['{{'@attributes'}}']['nombre']);
                $('#rfcemi').html(datatabx[6].emisor['{{'@attributes'}}']['rfc']);
                $('#subtotalfact').html(mascaraMoneda(datatabx[6]['{{'@attributes'}}']['subtotal'],'$'));
                $('#totalfact').html(mascaraMoneda(datatabx[6]['{{'@attributes'}}']['total'],'$'));
                $('#impuestosfact').html(mascaraMoneda((parseFloat(datatabx[6]['{{'@attributes'}}']['total'])-parseFloat(datatabx[6]['{{'@attributes'}}']['subtotal'])),'$'));
                $('#fechafact').html(datatabx[2]);
                $('#tipofact').html(datatabx[0].capitalize());
                var dataconcepto='';
                var conceptos= '';
                var head='';
                var cabeceras=[];
                if (datatabx[6].conceptos.concepto['{{'@attributes'}}'] != undefined){
                    dataconcepto += '<tr>';
                    $.each(datatabx[6].conceptos.concepto['{{'@attributes'}}'],function (llave,valor) {
                        if (cabeceras.indexOf(llave) < 0){
                            cabeceras.push(llave);
                            head+='<th>'+llave.capitalize()+'</th>';
                        }
                        conceptos += '<td>'+valor+'</td>'
                    });
                    dataconcepto += conceptos+' </tr>';
                }else{
                    $.each(datatabx[6].conceptos.concepto,function (key,val) {
                        dataconcepto += '<tr>';
                        conceptos= '';
                        $.each(val['{{'@attributes'}}'],function (llave,valor) {
                            if (cabeceras.indexOf(llave) < 0){
                                cabeceras.push(llave);
                                head+='<th>'+llave.capitalize()+'</th>';
                            }
                            conceptos+='<td>'+valor+'</td>'
                        });
                        dataconcepto+= conceptos+' </tr> ';
                    });
                }
                $('#headconceptos').html(head);
                $('#conceptostabla').html(dataconcepto);
                $('#detalletempfactura').modal();
            });
        }
        init=function () {
            accionespage ()
        };
        tabladinamica('datatable-personal',init);
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var tipo=$("input[name='estatus']:checked").val();
                var estatus =  data[4] ;// use data for the age column
                if (estatus.indexOf(tipo) >= 0 )
                {
                    return true;
                }else if (tipo == undefined || tipo == ''){
                    return true;
                }
                return false;
            }
        );
        setTimeout(function(){  $("input[name='estatus']").each(function () {
            $(this).parent().click(function () {
                table.draw();
            });
            $(this).siblings().click(function () {
                table.draw();
            });
            $(this).parent().parent().click(function () {
                table.draw();
            });
        }); }, 1000);
        table.on('dblclick', 'tr', function () {
            {{--data = table.row( this ).data();--}}
            {{--if(data!=undefined){--}}
{{--//                console.log(data);--}}
                {{--dialogooption('{{url('back/api/asignar')}}/'+data[0],'<p>Deseas procesar la solicitud '+data[0]+'</p>','Asignar solicitud',data,true);--}}
            {{--}--}}
        } ).on('draw.dt', function() {
            accionespage ()
            // console.log('draw');
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
            accionespage ()
        } );

        table.draw();
//        $("input[name='estatus']").each(function () {
//            $(this).parent().parent().click(function () {
//                table.draw();
//            })
//        });
    });
</script>
@endpush