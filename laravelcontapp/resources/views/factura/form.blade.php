<style>
    .alert-success {
        color: #fff;
        border-color: rgba(38,185,154,.88);
        background-color: #60c240;
    }
    .dropzone {
        margin-top: 60px;
         min-height: 0px;
        border: 2px solid #eb6a00;
    }
    .dropzone.dz-started .dz-message {
        font-size: 1.58em;
    }
    .dz-default .dz-message {
        font-size: 1.58em;
    }
    .dropzone .dz-message {
        font-size: 1.58em;
    }
</style>
<p class="text-muted font-13 m-b-30">

</p>
<h2>Revisa la información de tus facturas y confírmalas para ser cargadas al sistema</h2>

<table id="datatable-personal" class="table table-striped table-bordered">
    <thead>
    <tr id="headtable">

    </tr>
    </thead>
    {{--<tfoot>--}}
    {{--<tr id="pieTabla">--}}

    {{--</tr>--}}
    </tfoot>
</table>

<div class="col-xs-12" >
    <center>
        <div class="row" >
            <div class="col-xs-12" >
                {{--@if (session('tipo')!= 'success')--}}
                    <div class="col-xs-6"  >
                        <a class="btn btn-success btn-lg"  type="button" id="limpiafact"  {{factemp()?'':'disabled'}} >
                            Limpiar lista
                        </a>
                    </div>
                    <div class="col-xs-6" >
                        <a  {{factemp()?'':'disabled'}} type="button" id="cargafacturas" class="btn btn-success btn-lg">
                            Confirmar carga
                        </a>
                    </div>
                {{--@else--}}
                    {{--<div class="col-xs-12"  >--}}
                        {{--<a class="btn btn-success btn-lg" href="{{url('/')}}">--}}
                            {{--Cerrar--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--@endif--}}
            </div>
        </div>
    </center>
</div>

{!! BootForm::open(['url' => url('/factura'), 'method' => 'post', 'id'=> 'facturadrop', 'class'=>'dropzone dz-clickable dz-started']) !!}


{!! BootForm::close() !!}

@include('factura.templateDropzone')

@include('modals.pedirrfc')

@include('modals.cargafactura')

@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        $(function () {
            var ajax={};
            var datatableaccess;
{{--            configtable.data={!! json_encode($datos) !!};--}}
            configtable.columns={!! json_encode($columnas) !!};
            function accionespage (){
                $('.eliminar').off( "click").click(function () {
                    url=$(this).attr('data-url');
                    ajax.success=function (json) {
                        if (json != undefined){
                            $('#datatable-personal').DataTable().ajax.reload();
                        }
                    };
                    ajax.url=url;
                    peticionAJAX(ajax);
                });
                $('.detallefact').off( "click").click(function () {
                    datatabx=table.row( $(this).parent().parent() ).data();
                    $('#nomrecep').html(datatabx.datosfact.receptor['{{'@attributes'}}']['nombre']);
                    $('#rfcrecept').html(datatabx.datosfact.receptor['{{'@attributes'}}']['rfc']);
                    $('#nomemi').html(datatabx.datosfact.emisor['{{'@attributes'}}']['nombre']);
                    $('#rfcemi').html(datatabx.datosfact.emisor['{{'@attributes'}}']['rfc']);
                    $('#subtotalfact').html(mascaraMoneda(datatabx.datosfact['{{'@attributes'}}']['subtotal'],'$'));
                    $('#totalfact').html(mascaraMoneda(datatabx.datosfact['{{'@attributes'}}']['total'],'$'));
                    $('#impuestosfact').html(mascaraMoneda((parseFloat(datatabx.datosfact['{{'@attributes'}}']['total'])-parseFloat(datatabx.datosfact['{{'@attributes'}}']['subtotal'])),'$'));
                    $('#fechafact').html(datatabx.Fecha);
                    $('#tipofact').html(datatabx['Tipo de factura'].capitalize());
                    var dataconcepto='';
                    var conceptos= '';
                    var head='';
                    var cabeceras=[];
                    if (datatabx.datosfact.conceptos.concepto['{{'@attributes'}}'] != undefined){
                        dataconcepto += '<tr>';
                        $.each(datatabx.datosfact.conceptos.concepto['{{'@attributes'}}'],function (llave,valor) {
                            if (cabeceras.indexOf(llave) < 0){
                                cabeceras.push(llave);
                                head+='<th>'+llave.capitalize()+'</th>';
                            }
                            conceptos += '<td>'+valor+'</td>'
                        });
                        dataconcepto += conceptos+' </tr>';
                    }else{
                        $.each(datatabx.datosfact.conceptos.concepto,function (key,val) {
                            dataconcepto += '<tr>';
                            conceptos= '';
                            $.each(val['{{'@attributes'}}'],function (llave,valor) {
                                if (cabeceras.indexOf(llave) < 0){
                                    cabeceras.push(llave);
                                    head+='<th>'+llave.capitalize()+'</th>';
                                }
                                conceptos+='<td>'+valor+'</td>'
                            });
                            dataconcepto+= conceptos+' </tr> ';
                        });
                    }
                    $('#headconceptos').html(head);
                    $('#conceptostabla').html(dataconcepto);
                    $('#detalletempfactura').modal();
                });
            }
//            console.log();
            init=function () {
                accionespage ()
            };
//            tabladinamica('datatable-personal',init);

            tabladinamicaajax('datatable-personal','headtable','{{url('api/tempfact')}}');
            table.on('click', 'tr', function () {

            } ).on('responsive-display', function () {
                accionespage ()
            } ).on('draw.dt', function () {
                accionespage ()
            } );
            botonhref('cargafacturas','{{url('facturasave')}}');
            botonhref('limpiafact','{{url('facturaclean')}}');
            if ( $("#facturadrop").length ) {
                Dropzone.options.facturadrop = {
                    paramName: "file", // The name that will be used to transfer the file
                    maxFilesize: 2, // MB
                    dictDefaultMessage: "Carga tus facturas <br/>Arrastra tus archivos XML o haz click aquí",
//            addRemoveLinks:true,
//            dictRemoveFile: "Remove",
                    previewTemplate: document.querySelector('#templatedrop').innerHTML,
                    init: function () {
                        var that = this;
                        that.on("drop", function (event) {
                        });
                        that.on('sending', function (file, xhr, formData) {
                        });
                        that.on('complete', function (file) {
                            if (file.status != 'error' ) {
                                $('#datatable-personal').DataTable().ajax.reload();
                                time(this, file);

                            }
                        });
                        that.on("addedfile", function (file) {
                            if($( ".alert" ).length){
                                $( ".alert" ).remove();
                            }
                            $.ajax({
                                url: "{{url('/api/persona')}}",
                                type: "GET",
                                datatype: "application/json",
                                beforeSend: function () {
                                }
                            }).always(function (json, textStatus, errorThrown) {
                                if (json != undefined) {
                                    if (!json.rfc) {
                                        $('#llenarfc').modal();
                                        that.removeFile(file);
                                    }
                                }
                            });

                        });
                        that.on("error", function (data, done) {
                        });
                    },
                    success: function (file, response) {
                        if (response.resp == 'success') { // succeeded
                            $('#cargafacturas').removeAttr("disabled");
                            $('#limpiafact').removeAttr("disabled");
                            return file.previewElement.classList.add("dz-success"); // from source
                        }
                    },
                    error: function (file, message) {
                        var node, _i, _len, _ref, _results;
                        if (file.previewElement) {
                            file.previewElement.classList.add("dz-error");
                            if (message.resp == 'error') { // succeeded
                                if (typeof message !== "String" && message.error) {
                                    message = message.error.file[0].replace('file', file.name);
                                }
                            }else{
                                message = 'Error al cargar intente denuevo';
                            }
                        }
                        _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                        _results = [];
                        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                            node = _ref[_i];
                            _results.push(node.textContent = message);
                        }
                        return _results;
                    }
                };
                function time(that, file) {
                    setInterval(removetimer(that, file), 5000);
                }

                function removetimer(that, file) {
                    that.removeFile(file);
                }
            }
        })
    });
</script>

@endpush