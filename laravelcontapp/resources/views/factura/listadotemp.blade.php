@extends('factura.factura')

@section('title_section')
    <h2>Facturas a subir</h2>
@endsection

@section('contentx')
    <style>
        .alert-success {
            color: #fff;
            border-color: rgba(38,185,154,.88);
            background-color: #60c240;
        }
    </style>
    <p class="text-muted font-13 m-b-30">

    </p>
    <h2>Revisa la informacion de tus facturas y confirmala para ser cargada al sistema</h2>
    @if (session('message'))
        <div class="alert alert-{{ session('tipo') }}">
            {{ session('message') }}
        </div>
    @endif
    <table id="datatable-personal" class="table table-striped table-bordered">

    </table>

    <div class="col-xs-12" >
        <center>
            <div class="row" >
                <div class="col-xs-12" >
                    @if (session('tipo')!= 'success')
                        <div class="col-xs-6"  >
                            <a class="btn btn-success btn-lg" href="{{url('facturaclean')}}">
                                Limpiar lista
                            </a>
                        </div>
                        <div class="col-xs-6" >
                            <a  {{factemp()?'':'disabled'}} type="button" id="cargafacturas" class="btn btn-success btn-lg">
                                Confirmar carga
                            </a>
                        </div>
                    @else
                        <div class="col-xs-12"  >
                            <a class="btn btn-success btn-lg" href="{{url('/')}}">
                               Cerrar
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </center>
    </div>
@endsection
@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        configtable.data={!! json_encode($datos,JSON_UNESCAPED_UNICODE) !!};
        configtable.columns={!! json_encode($columnas,JSON_UNESCAPED_UNICODE) !!};
        console.log();
        init=function () {
//            console.log('init')
        };
        tabladinamica('datatable-personal',init);

        table.on('dblclick', 'tr', function () {
        } );
        botonhref('cargafacturas','{{url('facturasave')}}');
    });
</script>
@endpush