
{{--<div class="col-xs-offset-4 col-xs-4">--}}
    {{--<div class="progress">--}}
        {{--<div class="progress-bar progress-bar-warning" id="myBar" data-transitiongoal="90" aria-valuenow="90" style="width: 90%;">--}}
            {{--<span class="show"> </span>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

{!! BootForm::open(['url' => url($url), 'method' => $metodo, 'id'=>'declaraciones']) !!}
{!! BootForm::hidden('mes', $declaracion->mes ) !!}
{!! BootForm::hidden('anio', $declaracion->anio ) !!}
<div class="col-xs-5" >
    {!! BootForm::text('ingresos', 'Ingresos', $declaracion->monto_mes ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_ingresos', 'IVA de ingresos', $declaracion->ingresos_mes, ['required'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_retenido', 'IVA retenido', $declaracion->iva_ret_mes, ['required'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('deducciones', 'Deducciones', $declaracion->deducciones_mes, ['required'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_acreditable', 'IVA acreditable', $declaracion->iva_acre_mes, ['required'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_pagado', 'IVA pagado', $declaracion->iva_pag_mes, ['required'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('retenciones_ISR', 'Retenciones ISR', $declaracion->isr_mes, ['required'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('ISR_pagado', 'ISR pagado', $declaracion->isr_pag_mes, ['required'] ) !!}
</div>
<div class="col-xs-12" >
{!! BootForm::submit('Guardar',['class'=>'btn btn-success']) !!}
</div>
{!! BootForm::close() !!}

@push('scriptspersonal')

<script type="text/javascript">
    $(document).ready(function() {
        restringe('ingresos','[^0-9.]');
        restringe('iva_ingresos','[^0-9.]');
        restringe('iva_retenido','[^0-9.]');
        restringe('deducciones','[^0-9.]');
        restringe('iva_acreditable','[^0-9.]');
        restringe('iva_pagado','[^0-9.]');
        restringe('retenciones_ISR','[^0-9.]');
        restringe('ISR_pagado','[^0-9.]');
        if ($('#declaraciones').length) {
            ayudaicon('declaraciones');
            $('#ayu_ingresos').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/4.ISR_INGRESOS_COBRADOS.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_ingresos').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/5.IVA_DE_INGRESOS.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_retenido').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/6.IVA_RETENIDO.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_deducciones').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/7.ISR_DEDUCCIONES_REALIZADAS.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_acreditable').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/8.IVA_ACREDITABLE.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_pagado').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/9.IVA_PAGADO.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_retenciones_ISR').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/10.RETENCIONES_ISR.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_ISR_pagado').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/11.ISR_PAGADO_DEL_MES.jpg')}}');
                $('#infomodalform').modal();
            });
        }
    });
</script>

@endpush