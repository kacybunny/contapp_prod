<br>
<center>
    <h2>¿Deseas agregar otra declaracion?</h2>
</center>
<br>
<br>
<center>
    <div class="col-xs-6" >
        {!! BootForm::button('Si',['class'=>'btn btn-success btn-lg','onclick'=>'$(location).attr("href","'.url('/declaracion/create').'")']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::button('no',['class'=>'btn btn-error btn-lg','onclick'=>'$(location).attr("href","'.url('/declaracion').'")']) !!}
    </div>
</center>