@extends('declaraciones.declaraciones')

@section('title_section')
    <h2>Listado declaraciones anteriores<small></small></h2>
@endsection

@section('contentx')
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-personal" class="table table-striped table-bordered">
    </table>
@endsection
@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        configtable.data={!! json_encode($datos) !!};
        configtable.columns={!! json_encode($columnas) !!};

        console.log();
        init=function () {
//            console.log('init')
        };
        tabladinamica('datatable-personal',init);

        table.on('dblclick', 'tr', function () {
            {{--data = table.row( this ).data();--}}
            {{--if(data!=undefined){--}}
{{--//                console.log(data);--}}
                {{--dialogooption('{{url('back/api/asignar')}}/'+data[0],'<p>Deseas procesar la solicitud '+data[0]+'</p>','Asignar solicitud',data,true);--}}
            {{--}--}}
        } );
    });
</script>
@endpush