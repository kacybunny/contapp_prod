@extends('declaraciones.declaraciones')

@section('title_section')
    <h2>Agregar declaración anterior<small></small></h2>
@endsection

@section('contentx')
    <br />
    @if (session('message'))
        <div class="alert alert-error">
            {{ session('message') }}
        </div>
    @endif
    @include('declaraciones.form')
@endsection
