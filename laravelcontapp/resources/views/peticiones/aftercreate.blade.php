@extends('peticiones.peticiones')

@section('title_section')
    <h2><small></small></h2>
@endsection

@section('contentx')
    <br />
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    @include('peticiones.afterform')
@endsection
