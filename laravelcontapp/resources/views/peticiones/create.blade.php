@extends('peticiones.peticiones')

@section('title_section')
    <h2>Agregar declaración<small></small></h2>
@endsection

@section('contentx')
    <br />
    @if (session('message'))
        <div class="alert alert-error">
            {{ session('message') }}
        </div>
    @endif
    @include('peticiones.formsearch')
@endsection
