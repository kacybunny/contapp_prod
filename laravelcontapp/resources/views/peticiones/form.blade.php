{!! BootForm::open(['url' => url($url), 'method' => $metodo, 'id'=>'formpet']) !!}
<div class="col-xs-5" >
{!! BootForm::select('anio', 'Año',$anio,old('anio'), ['required','class'=>'selectpicker','data-live-search'=>'true']) !!}
</div>
<div class="col-xs-5" >
{!! BootForm::select('mes', 'Mes',[],null, ['required','class'=>'selectpicker','data-live-search'=>'true']) !!}
</div>
<br/>

{!! BootForm::close() !!}
<div class="col-xs-12" >
    {!! BootForm::button('Solicitar',['class'=>'btn btn-success','id'=>'solicitud']) !!}
</div>
<p class="text-muted font-13 m-b-30">

</p>
<br/>

<br/>
<table id="datatable-personal" class="table table-striped table-bordered">

</table>

@push('scriptspersonal')

<script type="text/javascript">
    $(document).ready(function() {
        if ( $("#enviarfc").length ) {
            errorTreal("enviarfc");
            $('#btn_rfc').click(function() {
                if(validarfrm('enviarfc')){
                    $.ajax({
                        url: "{{url('/api/persona')}}",
                        type: "POST",
                        data:$('#enviarfc').serializeArray(),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        datatype: "application/json",
                        beforeSend: function () {
                            $('#btn_rfc').attr("disabled",true);
                        }
                    }).always(function (json, textStatus, errorThrown) {
                        if (json != undefined) {
                            if (json.responseJSON != undefined){
                                $.each(json.responseJSON.error, function(i,item){
                                    if (item.campo!= undefined){
                                        agregaerror(item);
                                    }
                                });
                            }else{
                                $('#llenarfc').modal('toggle');
                            }
                        }
                        $('#btn_rfc').removeAttr("disabled");
                    });
                }
            });
        }
        var datameses={!! $meses !!};
        if ($('#formpet').length) {

            $('#solicitud').click(function () {
                $.ajax({
                    url: "{{url('/api/persona')}}",
                    type: "GET",
                    datatype: "application/json",
                    beforeSend: function () {
                    }
                }).always(function (json, textStatus, errorThrown) {
                    if(json != undefined){
                        if(!json.rfc){
                            $('#llenarfc').modal();
                        }else{
                            $('#formpet').submit();
                        }
                    }
                });
            });
            anio=$('#anio').find(":selected").val();
            apendcomboanio ('mes',datameses,anio,selectpicker=true);
            $( "#anio" ).change(function() {
                anio=$(this).find(":selected").val();
                $("#mes").show("slow");
                identificador=$('#anio').val();
                if(identificador==""){
                    $("#mes").hide("slow");
                }
                apendcomboanio ('mes',datameses,anio,selectpicker=true);
            });
            $('#mes').selectpicker('val', '{{old('mes')}}');
//            disabledchange('declaraciones', 'mes', 'data-icon', 'glyphicon-ok',true,'anio');
        }

        configtable.data={!! json_encode($datos) !!};
        configtable.columns={!! json_encode($columnas) !!};
        funciondialogo=function () {
            fecha = $('#fechapago').val();
            datos={'fecha':fecha};
            return datos;
        };
        validadialogo=function () {
            dato=$('#fechapago').val();
            if (dato!=''){
                rule='^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$';
                if(dato.match(rule)){
                    return true;
                }else{
                    $('#fechapago').val('');
                    return false;
                }
            }else{
                return false;
            }
        };
        iniciadiag=function () {
        };
        accionespage = function(){
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            $('.autoriza').off('click').click(function () {
                si =$(this).attr('data-href-yes');
                no =$(this).attr('data-href-no');
                $('#modalno').off('click').click(function () {
                    location.assign(no);
                });
                $('#modalsi').off('click').click(function () {
                    location.assign(si);
                });
                $('#autorizarmodal').modal();
            });
            $('.payfact').off('click').click(function () {
                mensaje=$(this).attr('data-mensaje');
                url=$(this).attr('data-url');
                dialogooption(url,mensaje,'Marcar pagos',[],true,'POST',funciondialogo,validadialogo,iniciadiag);
                $('.bootdatepick').datepicker(configdatepicker);
            });
        };
        init=function () {
            accionespage();
        };
        tabladinamica('datatable-personal',init);

        table.on('dblclick', 'tr', function () {

        } ).on('draw.dt', function() {
            accionespage ()
            // console.log('draw');
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
            accionespage ()
        } );
    });
</script>
@include('modals.pedirrfc')
@include('modals.Autoriza')
@endpush