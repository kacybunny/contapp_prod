@extends('peticiones.peticiones')

@section('title_section')
    <h2>Listado {{$titulo}}<small></small></h2>
@endsection

@section('contentx')
<p class="text-muted font-13 m-b-30">

</p>
<br/>

<br/>
<table id="datatable-personal" class="table table-striped table-bordered">

</table>
@endsection

@push('scriptspersonal')

<script type="text/javascript">
    $(document).ready(function() {

        configtable.data={!! json_encode($datos) !!};
        configtable.columns={!! json_encode($columnas) !!};
        funciondialogo=function () {
            fecha = $('#fechapago').val();
            datos={'fecha':fecha};
            return datos;
        };
        validadialogo=function () {
            dato=$('#fechapago').val();
            if (dato!=''){
                rule='^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$';
                if(dato.match(rule)){
                    return true;
                }else{
                    $('#fechapago').val('');
                    return false;
                }
            }else{
                return false;
            }
        };
        iniciadiag=function () {
        };
        accionespage = function(){
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            $('.autoriza').off('click').click(function () {
                si =$(this).attr('data-href-yes');
                no =$(this).attr('data-href-no');
                $('#modalno').off('click').click(function () {
                    location.assign(no);
                });
                $('#modalsi').off('click').click(function () {
                    location.assign(si);
                });
                $('#autorizarmodal').modal();
            });
            $('.payfact').off('click').click(function () {
                mensaje=$(this).attr('data-mensaje');
                url=$(this).attr('data-url');
                dialogooption(url,mensaje,'Marcar pagos',[],true,'POST',funciondialogo,validadialogo,iniciadiag);
                $('.bootdatepick').datepicker(configdatepicker);
            });
        };
        init=function () {
            accionespage();
        };
        tabladinamica('datatable-personal',init);

        table.on('dblclick', 'tr', function () {
//            data = table.row( this ).data();
////                console.log(data);
//            dialogooption('','<p>hola</p>','prueba',data,false);
        } ).on('draw.dt', function() {
            accionespage ()
            // console.log('draw');
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
            accionespage ()
        } );
    });
</script>

@include('modals.Autoriza')
@endpush