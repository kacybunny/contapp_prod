<h2>Llena uno o más campos de acuerdo al criterio de búsqueda</h2>
@if (session('message'))
    <div class="alert alert-{{ session('tipo') }}">
        {{ session('message') }}
    </div>
@endif
<style>
    @media (max-width: 581px) {
        .col-xs-6 {
            width: 100%;
        }
    }
</style>
{!! BootForm::open(['url' => url($url), 'method' => $metodo, 'id'=> 'busquedafact', 'class'=>'date bootdatepick']) !!}
<div class="row">
    <div class="col-xs-6" >
        <div class="row">
            <div class="col-xs-12">
                <center>
                    <h2>Rango de fecha</h2>
                </center>
            </div>
            <div class="col-xs-12">
                <center>
                    <div class="col-xs-6">
                        {!! BootForm::text('fecha_inicio', 'Inicio',old('fecha_inicio'), ['placeholder' => 'Desde fecha']) !!}
                    </div>
                    <div class="col-xs-6">
                        {!! BootForm::text('fecha_final', 'Final',old('fecha_final'), ['placeholder' => 'Hasta fecha']) !!}
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class="col-xs-6" >
        <div class="row">
            <div class="col-xs-12">
                <center>
                    <h2>Estatus</h2>
                </center>
            </div>
            <div class="col-xs-12" >
                {!! BootForm::radios('tipo', ['html' => ' '], ['1'   => ' Finalizado','2' => ' Pendiente','3' => ' Ambos'],3,true,['class'=>'flat']) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-6" >
        <center>
        <div class="row">
            <div class="col-xs-12">
                <center>
                    <h2>Mes solicitado</h2>
                </center>
            </div>
            <div class="col-xs-12" >
                {!! BootForm::select('mes', 'Mes',$meses,null, ['placeholder'=>'','class'=>'selectpicker','data-live-search'=>'true']) !!}
            </div>
        </div>
        </center>
    </div>
    <br/>
    <br/>
    <div class="col-xs-12">
    {!! BootForm::submit('Buscar',['class' => 'btn btn-success btn-lg']) !!}
    </div>
</div>
{!! BootForm::close() !!}

@include('modals.pedirrfc')

@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        $(function () {
            $('#fecha_inicio').datepicker(configdatepicker);
            $('#fecha_final').datepicker(configdatepicker);
            {{--configtable.data={!! json_encode($datos) !!};--}}
            {{--configtable.columns={!! json_encode($columnas) !!};--}}
            {{--init=function () {--}}

            {{--};--}}
            {{--tabladinamica('datatable-personal',init);--}}

            {{--table.on('dblclick', 'tr', function () {--}}

            {{--} );--}}

        })
    });
</script>

@endpush