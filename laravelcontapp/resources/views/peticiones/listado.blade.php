@extends('peticiones.peticiones')

@section('title_section')
    <h2>Listado {{$titulo}}<small></small></h2>
@endsection

@section('contentx')
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Mes</th>
            <th>Año</th>
            <th>Ingresos</th>
            <th>Iva de ingresos</th>
            <th>Iva retenido</th>
            <th>Deducciones declaradas</th>
            <th>Iva acreditable</th>
            <th>Iva pagado</th>
            <th>Retenciones ISR</th>
            <th>ISR pagado</th>
        </tr>
        </thead>


        <tbody>

        @foreach($declaraciones as $declaracion)
            <tr>
                <td>{{$declaracion->meses->mes}}</td>
                <td>{{$declaracion->anio}}</td>
                <td>{{$declaracion->monto_mes}}</td>
                <td>{{$declaracion->ingresos_mes}}</td>
                <td>{{$declaracion->iva_ret_mes}}</td>
                <td>{{$declaracion->deducciones_mes}}</td>
                <td>{{$declaracion->iva_acre_mes}}</td>
                <td>{{$declaracion->iva_pag_mes}}</td>
                <td>{{$declaracion->isr_mes}}</td>
                <td>{{$declaracion->isr_pag_mes}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
