<!-- Global site tag (gtag.js) - Google AdWords: 845595255 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-845595255"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-845595255');
</script>
