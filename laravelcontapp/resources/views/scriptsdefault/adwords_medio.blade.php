I<!-- Event snippet for Registro en Plan Medio conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-845595255/BDcJCLqZnHwQ94SbkwM',
      'value': 80.0,
      'currency': 'USD',
      'event_callback': callback
  });
  return false;
}
</script>
