<link href='{{asset('images/CONTAPP.ico')}}' rel='shortcut icon' type='image/x-icon'>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Contapp</title>

<!-- Bootstrap -->
<link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=PT+Sans|Ubuntu" rel="stylesheet">

<style>
    .login_content form input[type=submit] {
        float: none;
        margin-left: 0px;
    }
    body {
        font-family: 'Ubuntu', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
    .login_content h1 {
        font: 400 25px Ubuntu,sans-serif;
    }
</style>

@include('scriptsdefault.zendesk')
@include('scriptsdefault.analitics')
@include('scriptsdefault.analitics')
