<!DOCTYPE html>
<html lang="en">
<head>
	@include('auth.head')
	<style>
		h5{
			font-size: 16px;
			text-align: left;
		}
		.login_wrapper{
			max-width: 50%;
		}
		#content form .submit, .login_content form input[type=submit] {
			float: none;
			margin-left: 0;
		}
		.login_content {

			padding: 10px 0 0;

		}
	</style>
</head>

<body class="login">
<div>
	<div class="login_wrapper">
		<div class="animate form login_form">
			<center>
				<img class="img-responsive" src="{{ config('app.logo') }}">
			</center>

			<section class="login_content">
				<h1>¡has cambiado tu contraseña!</h1>

				<div class="seearfix">
					<h2>
						Ya puedes entrar nuevamente a tu portal personal Contapp

					</h2>

					<div>
						{!! BootForm::open(['url' => url('/')]) !!}
						{!! BootForm::submit('Continuar', ['class' => 'btn btn-default submit']) !!}
						{!! BootForm::close() !!}
					</div>

				</div>

				<div class="separator">
					<div class="clearfix"></div>
					<br />

					<div>
						<h1></h1>
						<p>©2017 Desarrollado por Hologram</p>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
</body>
</html>