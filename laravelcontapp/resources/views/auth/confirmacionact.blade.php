<!DOCTYPE html>
<html lang="en">
<head>
	@include('auth.head')
</head>

<body class="login">
<div class="login_wrapper">
	<div class="animate form login_form">
		<center>
			<img class="img-responsive" src="{{ config('app.logo') }}">
		</center>
		<section class="login_content">
			{!! BootForm::open(['url' => url('register/exito'), 'method' => 'post']) !!}

			<h1>Activa tu cuenta {{ $user->name }}</h1>
			<h2>
				Cambia tu contraseña
			</h2>

				<h2>
					Email: {{session('mail')}}
				</h2>


			{!! BootForm::password('password', 'Password', ['placeholder' => 'Password']) !!}

			{!! BootForm::password('password_confirmation', 'Confirmacion Contraseña', ['placeholder' => 'Confirmation']) !!}

			{!! BootForm::submit('Registrar', ['class' => 'btn btn-default']) !!}

			<div class="clearfix"></div>

			<div class="separator">

				<div class="clearfix"></div>
				<br />

				<div>
					<h1></h1>
					<p>©2017 Desarrollado por Hologram</p>
				</div>
			</div>
			{!! BootForm::close() !!}
		</section>
	</div>
</div>

{{--@if ($errors->any())--}}
{{--<div class="alert alert-danger">--}}
{{--<ul>--}}
{{--@foreach ($errors->all() as $error)--}}
{{--<li>{{ $error }}</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--</div>--}}
{{--@endif--}}
</body>
</html>