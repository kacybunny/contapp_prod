<!DOCTYPE html>
<html lang="en">
<head>
	@include('auth.head')
	<style>
		.login_content form input[type=submit] {
			float: none;
			margin-left: 0;
		}
	</style>
</head>

<body class="login">
<div class="login_wrapper">
	<div class="animate form login_form">
		<center>
			<img class="img-responsive" style="width:80%" src="{{ config('app.logo') }}">
		</center>
		<section class="login_content">
			{!! BootForm::open(['url' => url('/register/complete'), 'method' => 'post']) !!}

			<h1>Define tu contraseña</h1>

			<div class="progress">
				<div class="progress-bar progress-bar-warning" id="myBar" data-transitiongoal="20" aria-valuenow="20" style="width: 20%;">
					20%
				</div>
			</div>

			<h2>
				Usuario: {{session('mail')}}
			</h2>

			<center>
			{!! BootForm::password('password', 'Contraseña', ['placeholder' => 'Contraseña']) !!}

			{!! BootForm::password('password_confirmation', 'Confirmación password', ['placeholder' => 'Confirmación']) !!}

			{!! BootForm::submit('Registrar', ['class' => 'btn btn-info btn-lg']) !!}
			</center>
			<div class="clearfix"></div>

			<div class="separator">

				<div class="clearfix"></div>
				<br />

				<div>
					<h1></h1>
					<p>©2017 Desarrollado por Hologram</p>
				</div>
			</div>
			{!! BootForm::close() !!}
		</section>
	</div>
</div>

{{--@if ($errors->any())--}}
{{--<div class="alert alert-danger">--}}
{{--<ul>--}}
{{--@foreach ($errors->all() as $error)--}}
{{--<li>{{ $error }}</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--</div>--}}
{{--@endif--}}
<script>
    (function() {
        var elem = document.getElementById("myBar");
        var width = 20;
        var id = setInterval(frame, 80);
        function frame() {
            if (width >= 30) {
                clearInterval(id);
            } else {
                width++;
                elem.style.width = width + '%';
                elem.innerHTML = width * 1 + '%';
            }
        }

    })();
</script>
</body>
</html>