<!DOCTYPE html>
<html lang="en">
<head>
	@include('auth.head')
	<style>
		.login_content form input[type=text], .login_content form input[type=email], .login_content form input[type=password] {

			 margin: 0;

		}
		.login_wrapper {
			right: 0;
			margin: 0 auto 0;
			max-width: 450px;
			position: relative;
		}
		.login_wrapper input {
			align-self: center;
			max-width: 350px;
		}
		.login_content {
			padding: 0px 0 0;
		}
		.login_content form {
			 margin: 0px 0;
		}
		.login_content h1 {
			margin: 5px 0 20px;
			padding: 0px 0 0;
		}
		.checkbox label, .radio label {
			color: #73879c;
		}
		.has-error .jqstooltip {
			background: #ec0e00 !important;
			width: 30px !important;
			height: 22px !important;
			text-decoration: none
		}
		.has-error .tooltip {
			background: #ec0e00 !important;
			width: 30px !important;
			height: 22px !important;
			text-decoration: none
		}
		#content form .submit, .login_content form input[type=submit] {
			float: none;
			margin-left: 0px;
			margin-top: 10px;
		}
		.form-group {
			margin-bottom: 0px;
		}
		.help-block {
			 margin-top: 0px;
			 margin-bottom: 0px;
		}
		.checkbox, .radio {
			 margin-top: 0px;
			 margin-bottom: 0px;
		}
	</style>
</head>

<body class="login">
<div class="login_wrapper">
	<div class="animate form login_form">
		<center>
			<img class="img-responsive" style="width: 60%" src="{{ config('app.logo') }}">
		</center>
		<section class="login_content">
			@if (session('message'))
				<div class="alert alert-success">
					{{ session('message') }}
				</div>
			@endif
			@if(session('reenvio'))
				<div class="alert alert-success">
					<center>
						<h2>Este correo ya esta registrado,</h2>
						<br/>
						<h2>Sin embargo no has verificado tu cuenta.</h2>
						<br/>
						<h2>¿ Deseas que reenviemos el correo de verificación ?</h2>
						<br/>
						<br/>
						<a href="{{ url('register/reenvioverificacion/'.session('reenvio')) }}" style="background-color: #ff6b00" class="btn btn-info" type="button"> Reenviar correo</a>
					</center>
				</div>
			@endif
			{!! BootForm::open(['url' => url('/register'), 'method' => 'post']) !!}

			<h1>Regístrate</h1>

			<div class="progress">
				<div class="progress-bar progress-bar-info" id="myBar" data-transitiongoal="0" aria-valuenow="0" style="width: 0%;">
				</div>
			</div>

			<h2>
				Para iniciar tu registro, por favor déjanos los siguientes datos
			</h2>
<center>
			{!! BootForm::text('nombre', ['html' => ' '], old('nombre'), ['placeholder' => 'Nombre','data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"" ,'data-original-title'=>"Nombre"]) !!}

			{!! BootForm::text('paterno', ['html' => ' '], old('paterno'), ['placeholder' => 'Apellido paterno','data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"" ,'data-original-title'=>"Apellido paterno"]) !!}

			{!! BootForm::text('materno', ['html' => ' '], old('materno'), ['placeholder' => 'Apellido materno','data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"" ,'data-original-title'=>"Apellido materno"]) !!}

			{!! BootForm::email('email', ['html' => ' '], old('email'), ['placeholder' => 'Email','data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"" ,'data-original-title'=>"Email"]) !!}

			{!! BootForm::text('telefono', ['html' => ' '], old('telefono'), ['placeholder' => 'Teléfono (opcional)','data-toggle'=>"tooltip", 'data-placement'=>"top", 'title'=>"" ,'data-original-title'=>"Teléfono"]) !!}

			{!! BootForm::checkboxes('terminos','Términos y condiciones', ['terminos'=>'He leído y acepto la  <a href="http://contapp.mx/privacy" target="_blank" style="color:#ff6b00; margin: 10px 5px 0 0;" > Política de Privacidad</a>y los  <a href="http://contapp.mx/terms" target="_blank" style="color:#ff6b00; margin: 10px 5px 0 0;">Términos y Condiciones </a> del servicio ContApp</p>'],[],false,[]) !!}

			{!! BootForm::submit('Registrar', ['class' => 'btn btn-info btn-lg']) !!}
</center>
			<div class="clearfix"></div>

			<div class="separator">
				<p class="change_link">¿Ya eres usuario?
					<a href="{{ url('/login') }}" class="to_register"> Iniciar sesión </a>
				</p>

				<div>
					<h1></h1>
					<p>©2017 Desarrollado por Hologram</p>
				</div>
			</div>
			{!! BootForm::close() !!}
		</section>
	</div>
</div>
<script src="{{ asset("js/jquery.min.js") }}"></script>

<script src="{{ asset("js/jquery-ui/jquery-ui.min.js") }}"></script>
<!-- Bootstrap -->
<script src="{{ asset("js/bootstrap.min.js") }}"></script>

<script src="{{ asset("js/jsrender/jsrender.min.js") }}"></script>

<script src="{{ asset("js/moment/moment.min.js") }}"></script>

<script src="{{ asset("js/personal.js?ac=".str_random(2)) }}"></script>
{{--@if ($errors->any())--}}
{{--<div class="alert alert-danger">--}}
{{--<ul>--}}
{{--@foreach ($errors->all() as $error)--}}
{{--<li>{{ $error }}</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--</div>--}}
{{--@endif--}}
<script>
    (function() {
        var elem = document.getElementById("myBar");
        var width = 0;
        var id = setInterval(frame, 80);
        function frame() {
            if (width >= 20) {
                clearInterval(id);
            } else {
                width++;
                elem.style.width = width + '%';
                elem.innerHTML = width * 1 + '%';
            }
        }

    })();

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
        restringe('telefono','[^0-9]',10);
        restringe('nombre','[^a-zA-ZáéíóúÁÉÍÓÚ]');
        restringe('paterno','[^a-zA-ZáéíóúÁÉÍÓÚ]');
        restringe('materno','[^a-zA-ZáéíóúÁÉÍÓÚ]');

    });
</script>
</body>
</html>