<!DOCTYPE html>
<html lang="en">
<head>
	@include('auth.head')
	<style>
		h5{
			font-size: 18px;
			/*text-align: left;*/
		}
		.login_wrapper{
			max-width: 60%;
			margin: 0% auto;
		}
		.list-unstyled{
			width: 65%;
		}
		.timeline .tags {
			top: 5px;
		}
		.login_content form input[type=submit] {
			float: none;
			margin-left: 0;
		}
		.login_content {

			 padding: 0;

		}
		.login_content h1:after, .login_content h1:before {

			width: 14%;
		}

	</style>
	@include('scriptsdefault.adwords_basico')
</head>

<body class="login">
<div>
	<div class="login_wrapper">
		<div class="animate form login_form">
			<center>
				<img class="img-responsive" style="width: 30%" src="{{ config('app.logo') }}">
			</center>

			<section class="login_content">
				<h1>Bienvenido, ¡has creado con éxito tu cuenta!</h1>

				<div class="seearfix">
					<br>
					<h5>
						A partir de ahora, ya puedes utilizar tu portal personal Contapp
					</h5>
					<br>
					<h5>
						Para aprovechar al 100% nuestro servicio, debes terminar tu registro. <br/> Te faltan pocos pasos y los podrás ir completando gradualmente:
					</h5>
					<br>
					<center>
						<ul class="list-unstyled timeline">
							<li>
								<div class="block">
									<div class="tags">
										<ul class="tag">
											<span>1</span>
										</ul>
									</div>
									<div class="block_content">
										<h2 class="title">
											Seleccionar el plan
										</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="block">
									<div class="tags">
										<ul class="tag">
											<span>2</span>
										</ul>
									</div>
									<div class="block_content">
										<h2 class="title">
											Realizar el pago*
										</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="block">
									<div class="tags">
										<ul class="tag">
											<span>3</span>
										</ul>
									</div>
									<div class="block_content">
										<h2 class="title">
											Darnos información necesaria para calcular tus impuestos
										</h2>
									</div>
								</div>
							</li>
						</ul>
					</center>
					<br>
					<h5>
						Ahora estas registrado en el Plan Básico*.   En cualquier momento podrás seleccionar un plan superior para incrementar los beneficios.
					</h5>
					<br>
					<h5>
						*Recuerda que el Plan Básico no tiene costo
					</h5>
					<br>
					<center>
					<div>
						{!! BootForm::open(['url' => url('/'),'method' => 'get']) !!}
						{!! BootForm::submit('Continuar', ['class' => 'btn btn-info btn-lg']) !!}
						{!! BootForm::close() !!}
					</div>
					</center>
				</div>

				<div class="separator">
					<div class="clearfix"></div>
					<br />

					<div>
						<h1></h1>
						<p>©2017 Desarrollado por Hologram</p>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
</body>
</html>
