<!DOCTYPE html>
<html lang="en">
<head>
	@include('auth.head')
</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
			<center>
				<img class="img-responsive" style="width: 70%" src="{{ config('app.logo') }}">
			</center>
            <section class="login_content">
				@if (session('message'))
					<div class="alert alert-success">
						{{ session('message') }}
					</div>
				@endif
				@if(session('reenvio'))
					<div class="alert alert-success">
						<center>
							No has verificado tu cuenta.
							<br/>
							¿ Deseas que reenviemos el correo de verificación ?
							<br/>
							<br/>
							<a href="{{ url('register/reenvioverificacion/'.session('reenvio')) }}" style="background-color: #ff6b00"  class="btn btn-info" type="button"> Reenviar correo</a>
						</center>
					</div>
				@endif
						{!! BootForm::open(['url' => url('/login'), 'method' => 'post']) !!}

						<h1>Bienvenido</h1>

						{!! BootForm::email('email', 'Email', old('email'), ['placeholder' => 'Email', 'afterInput' => '<span>test</span>'] ) !!}

						{!! BootForm::password('password', 'Password', ['placeholder' => 'Password']) !!}

						{!! BootForm::submit('Iniciar sesión', ['class' => 'btn btn-info btn-lg']) !!}

					<div>


							<a class="reset_pass" href="{{  url('/password/reset') }}">¿Olvidaste tu contraseña?</a>
					</div>
				<div class="clearfix"></div>
                    
				<div class="separator">
					<p class="change_link">¿No tienes cuenta?
						<a href="{{ url('/register') }}" class="to_register">Regístrate</a>
					</p>
                        
					<div class="clearfix"></div>
					<br />
                        
					<div>
						<h1></h1>
						<p>©2017 Desarrollado por Hologram</p>
					</div>
				</div>
				{!! BootForm::close() !!}
            </section>
        </div>
    </div>
</div>
<script language="javascript">
    //Codigo que muestra una cuenta atras hasta finalizar el tiempo indicado
    //La Web del Programador
    //http://www.lawebdelprogramador.com
    obj = document.getElementById("email").nextElementSibling;
	if(obj) {
        //variables que determinan el tital de horas, minutos y segundos para la cuenta atras
        if (obj.innerHTML.indexOf("intentos")) {
            var count = parseInt(obj.innerHTML.replace("Demasiados intentos de inicio de sesión. Inténtalo de nuevo en ", ""));
            if (count > 0 && count != 0) {
                var toHour = 0;
                var toMinute = 0;
                var toSecond = 0;
                var time = count;

                var hours = Math.floor(time / 3600);
                var minutes = Math.floor((time % 3600) / 60);
                var seconds = time % 60;

                //Anteponiendo un 0 a los minutos si son menos de 10
                minutes = minutes < 10 ? '0' + minutes : minutes;

                //Anteponiendo un 0 a los segundos si son menos de 10
                seconds = seconds < 10 ? '0' + seconds : seconds;

//        var result = hours + ":" + minutes + ":" + seconds;  // 2:41:30

                //cuenta atras
                toHour = parseInt(hours);
                toMinute = parseInt(minutes);
                toSecond = parseInt(seconds);


                function countDown() {
                    toSecond = toSecond - 1;
                    if (toSecond < 0) {
                        toSecond = 59;
                        toMinute = toMinute - 1;
                    }


                    if (toMinute < 0) {
                        toMinute = 59;
                        toHour = toHour - 1;
                    }


                    obj.innerHTML='Demasiados intentos de inicio de sesión. Inténtalo de nuevo en '+toMinute+' minutos y '+toSecond+' segundos.';
                    if (toHour < 0) {
                        //final
                        obj.innerHTML='Demasiados intentos de inicio de sesión. Inténtalo de nuevo en 0 minutos y 0 segundos.';
                    } else {
                        setTimeout("countDown()", 1000);
                    }
                }
                countDown();
            }
        }
    }

</script>
</body>
</html>