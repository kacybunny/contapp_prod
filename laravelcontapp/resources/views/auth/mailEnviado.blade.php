<!DOCTYPE html>
<html lang="en">
<head>
	@include('auth.head')

	<style>
		.login_wrapper{
			max-width: 50%;
		}
		.login_content{
			margin-top: 6%;
		}

		#content form .submit, .login_content form input[type=submit] {
			 float: none;
			 margin-left: 0px;
		}
	</style>

</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
			<center>
				<img class="img-responsive" style="width:50%" src="{{ config('app.logo') }}">
			</center>
            <section class="login_content">

				<h1>Confirma tu email</h1>

				<h2>Para continuar con tu registro, primero debes abrir el correo que te mandamos y dar click al enlace.
					<br>
					<br>
					Si no lo encuentras, por favor revisa en la bandeja de spam.
					<br>
					<br>
					Para cualquier duda favor de escribirnos a:
					<a href="mailto:{{'soporte@contapp.mx'}}">{{'soporte@contapp.mx'}}</a>
					<br>
					<br>
					¡Bienvenido!
				</h2>
				<br/>
				<div>
					<center>
						{!! BootForm::open(['url' => url('login'),'method' => 'get']) !!}
						{!! BootForm::submit('Continuar', ['class' => 'btn btn-info btn-lg']) !!}
						{!! BootForm::close() !!}
					</center>
				</div>
                    
				<div class="clearfix" ></div>
                    
				<div class="separator">
					<div class="clearfix"></div>
					<br />
                        
					<div>
						<h1></h1>
						<p>©2017 Desarrollado por Hologram</p>
					</div>
				</div>
            </section>
        </div>
    </div>
</div>

<script language="javascript">
    function cerrar() {
        ventana=window.parent.self;
        ventana.opener=window.parent.self;
        ventana.close();
    }
</script>
</body>
</html>