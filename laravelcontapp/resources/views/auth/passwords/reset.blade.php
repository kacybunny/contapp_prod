<!DOCTYPE html>
<html lang="en">
<head>
    @include('auth.head')
    <style>
        .login_content h1:after, .login_content h1:before {

            width: 15% !important;
        }

    </style>

</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <center>
                <img class="img-responsive" src="{{ config('app.logo') }}">
            </center>
            <section class="login_content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                    {!! BootForm::open(['url' => url('/password/reset'), 'method' => 'post']) !!}
                    <h2>Restablecer Password</h2>

                    {!! BootForm::hidden('token', $token) !!}

                    {!! BootForm::email('email', 'Email', old('email'), ['placeholder' => 'Email']) !!}

                    {!! BootForm::password('password', 'Password', ['placeholder' => 'Password']) !!}

                    {!! BootForm::password('password_confirmation', 'Password confirmation', ['placeholder' => 'Confirmation']) !!}

                    {!! BootForm::submit('Restablecer contraseña', ['class' => 'btn btn-default col-md-9']) !!}

                    {!! BootForm::close() !!}

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">¿Ya tienes tu password?
                        <a href="{{ url('/login') }}" class="to_register"> Iniciar sesión </a>
                    </p>

                    <div class="clearfix"></div>
                    <br />

                    <div>
                        <h1></h1>
                        <p>©2017 Desarrollado por Hologram</p>
                    </div>
                </div>

            </section>
        </div>
    </div>
</div>
</body>
</html>