<!DOCTYPE html>
<html lang="en">
<head>
    @include('auth.head')
    @if (session('status'))
        <style>
            .login_content h1:after, .login_content h1:before {

                width: 0% !important;
            }

        </style>
    @else
        <style>
            .login_content h1:after, .login_content h1:before {

                width: 15% !important;
            }

        </style>
    @endif

</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <center>
                <img class="img-responsive" src="{{ config('app.logo') }}">
            </center>
            <section class="login_content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (!session('status'))
                    {!! BootForm::open(['url' => url('/password/email'), 'method' => 'post']) !!}
                    <h1>Restablecer Password</h1>

                    {!! BootForm::email('email', 'Email', old('email'), ['placeholder' => 'Email']) !!}

                    {!! BootForm::submit('Restablecer contraseña', ['class' => 'btn btn-default col-md-9']) !!}
                @endif
                <div class="clearfix"></div>

                <div class="separator">
                    @if (!session('status'))
                        <p class="change_link">¿Ya tienes tu password?
                            <a href="{{ url('/login') }}" class="to_register"> Iniciar sesión </a>
                        </p>
                    @endif
                    <div class="clearfix"></div>
                    <br />

                    <div>
                        <h1></h1>
                        <p>©2017 Desarrollado por Hologram</p>
                    </div>
                </div>

                {!! BootForm::close() !!}
            </section>
        </div>
    </div>
</div>
</body>
</html>