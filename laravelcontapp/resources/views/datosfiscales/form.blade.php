<div class="col-xs-offset-4 col-xs-4">
    <div class="progress">
        <div class="progress-bar progress-bar-warning" id="myBar" data-transitiongoal="70" aria-valuenow="70" style="width: 30%;">
        </div>
    </div>
</div>
{!! BootForm::open(['url' => url('datosfiscales/0'), 'method' => 'put','id'=>'ubicacion'] ) !!}
<div class="row">
    <div class="col-xs-12" >
        <div class="col-xs-5" >
            {!! BootForm::radios('tasaiva','Tasa de IVA de tu actividad', ['1'   => ' Exentos de IVA','2' => ' Gravados(16%,0%)','3' => ' Mixtos (exentos y gravados)'],((int)$fiscales->iva),false,['class'=>'flat']) !!}

</div>
    </div>
</div>
<h2>Régimen, actividades y obligaciones </h2>

<div class="row" id="agregamasreg" >
    <br/>
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">
            <label for="obligaciones" class="control-label">
                Regimen
            </label>
        </div>
        <div class="form-group col-xs-5">
            <label for="fechainiobl" class="control-label">
                Fecha de inicio
            </label>
        </div>
        <div class="form-group col-xs-2">
            <label for="obligaciones" class="control-label">
            </label>
        </div>

    </div>
    <div class="col-xs-12" id="regimengroup">
    </div>
</div>
<div class="col-xs-12" >
    <button id="adicionalreg" name="adicionalreg" type="button" class="btn btn-success"> <i class="fa fa-plus-square" ></i> Agregar regimen</button>
</div>
<br/>
<br/>
<div class="row" id="agregamasact" >
    <br/>
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">
            <label for="actividades" class="control-label">
                Actividades
            </label>
        </div>
        <div class="form-group col-xs-5">
            <label for="fechainiobl" class="control-label">
                Fecha de inicio
            </label>
        </div>
        <div class="form-group col-xs-2">
            <label for="obligaciones" class="control-label">
            </label>
        </div>

    </div>
    <div class="col-xs-12" id="actividadesgroup">
    </div>
</div>
<div class="col-xs-12" >
    <button id="adicionalact" name="adicionalact" type="button" class="btn btn-success"> <i class="fa fa-plus-square" ></i> Agregar actividad</button>
</div>
<br/>
<br/>
<div class="row" id="agregamasobl" >
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">
            <label for="obligaciones" class="control-label">
                Obligaciones
            </label>
        </div>
        <div class="form-group col-xs-5">
            <label for="fechainiobl" class="control-label">
                Fecha de inicio
            </label>
        </div>
        <div class="form-group col-xs-2">
            <label for="obligaciones" class="control-label">
            </label>
        </div>

    </div>
    <div class="col-xs-12" id="obligaciongroup">

    </div>
</div>
<div class="col-xs-12" >
    <button id="adicionalobl" name="adicionalobl" type="button" class="btn btn-success"> <i class="fa fa-plus-square" ></i> Agregar obligación</button>
</div>
<br/><br/><br/>

{!! BootForm::close() !!}
<div class="col-xs-12" >
    <center>
        <div class="row" >
            <div class="col-xs-12" >
                <div class="col-xs-6" >
                    {!! BootForm::button('Cancelar',['onclick'=>'location.assign("'.url('/').'")','class'=>'btn btn-gray btn-lg']) !!}
                </div>
                <div class="col-xs-6"  >
                    {!! BootForm::submit('Continuar',['class'=>'btn btn-success btn-lg','id'=>'guardar']) !!}
                </div>
            </div>
        </div>
    </center>
</div>
@push('scriptspersonal')
    <script type="text/javascript">

        $(document).ready(function() {

            alldata=[];

            alldata.push( obligaciones={
                'datos':{!! $obligaciones !!},
                'tmpl':'tmpl_obligacionescombo',
                'agrega':'adicionalobl',
                'grupo':'obligaciongroup',
                'selectpick': 'selectpickobl',
                'datepick':'datepickobl',
                'selectid':'obligacion',
                'picker':'fechainiobl[]',
                'datosuser':{!! old('obligaciones') ? json_encode(old('obligaciones')) : json_encode($obligacionespersona->obligaciones) !!},
                'fechas':{!! old('fechainiobl') ? json_encode(old('fechainiobl')) : json_encode($obligacionespersona->fechas) !!}
            });
            alldata.push(  actividades={
                'datos':{!! $actividades !!},
                'tmpl':'tmpl_atividadescombo',
                'agrega':'adicionalact',
                'grupo':'actividadesgroup',
                'selectpick': 'selectpickact',
                'datepick':'datepickact',
                'selectid':'actividad',
                'picker':'fechainiact[]',
                'datosuser':{!! old('actividades') ? json_encode(old('actividades')) : json_encode($actividadespersona->actividades) !!},
                'fechas': {!! old('fechainiact') ? json_encode(old('fechainiact')) : json_encode($actividadespersona->fechas) !!}
            });
            alldata.push(  regimen={
                'datos':{!! $regimen !!},
                'tmpl':'tmpl_regimencombo',
                'agrega':'adicionalreg',
                'grupo':'regimengroup',
                'selectpick': 'selectpickreg',
                'datepick':'datepickreg',
                'selectid':'regimen',
                'picker':'fechainireg[]',
                'datosuser':{!! old('regimen') ? json_encode(old('regimen')) : json_encode($regimenpersona->regimenes) !!},
                'fechas':{!! old('fechainireg') ? json_encode(old('fechainireg')) : json_encode($regimenpersona->fechas) !!}
            });

            formcampodinamico(alldata,'guardar','select','ubicacion');

        });
    </script>
    @include('plantillasrender.datosfiscales')
@endpush