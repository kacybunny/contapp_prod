<div class="col-xs-offset-4 col-xs-4">
    <div class="progress">
        <div class="progress-bar progress-bar-warning" id="myBar" data-transitiongoal="80" aria-valuenow="80" style="width: 30%;">
        </div>
    </div>
</div>
{!! BootForm::open(['url' => url('datosfiscales/1'), 'files'=>true, 'method' => 'put','id'=>'ubicacion'] ) !!}
<div class="row">
    <div class="col-xs-12" >
        <div class="col-xs-12" >
            <h2> FIEL / e.firma</h2>
            <div class="col-xs-6" >
                {!! BootForm::file('certificado','Certificado (.cer)') !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::file('llave_privada','Llave privada (.key)') !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::password('contrasena_llave_privada', 'Contraseña llave privada', ['placeholder' => 'Contraseña llave privada','required']) !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::password('confirma_contrasena_llave_privada', 'Confirma contraseña llave privada', ['placeholder' => 'Confirma contraseña llave privada','required']) !!}

            </div>
        </div>
        <div class="col-xs-12" >
            <h2>Contraseña SAT </h2>
            <div class="col-xs-6" >
                {!! BootForm::password('contrasena_SAT', 'Contraseña del SAT', ['placeholder' => 'Contraseña del sat','required']) !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::password('confirmar_contrasena_SAT', 'Confirma contraseña del SAT', ['placeholder' => 'Confirma contraseña del SAT','required']) !!}
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12" >
    <center>
        <div class="row" >
            <div class="col-xs-12" >
                <div class="col-xs-6" >
                    {!! BootForm::button('Cancelar',['onclick'=>'location.assign("'.url('/').'")','class'=>'btn btn-gray btn-lg']) !!}
                </div>
                <div class="col-xs-6"  >
                    {!! BootForm::submit('Continuar',['class'=>'btn btn-success btn-lg','id'=>'guardar']) !!}
                </div>
            </div>
        </div>
    </center>
</div>


{!! BootForm::close() !!}

@push('scriptspersonal')
    <script type="text/javascript">
        $(document).ready(function() {
            $(function(){
                progresbar('myBar',80,90,80);
            }());
        });
    </script>
@endpush