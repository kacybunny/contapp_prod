@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush
@push('libraries')

<script src="{{ asset("js/chart.js/Chart.min.js") }}"></script>
{{--<script src="{{ asset("js/Chart.js-master/chart.js") }}"></script>--}}

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
@role('user')
        <!-- top tiles -->
        <div class="row tile_count">
            <div class="col-md-3 col-sm-5 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-group"></i> Cuentas por cobrar</span>
                <div class="count">{{dashboard(0)}}</div>
                {{--<span class="count_bottom"><i class="green">4% </i> From last Week</span>--}}
            </div>
            <div class="col-md-3 col-sm-5 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-group"></i> Cuentas por pagar</span>
                <div class="count">{{dashboard(1)}}</div>
                {{--<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>--}}
            </div>
            <div class="col-md-3 col-sm-5 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-group"></i> Iva por pagar</span>
                <div class="count ">{{dashboard(2)}}</div>
                {{--<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>--}}
            </div>
            {{--<div class="col-md-3 col-sm-5 col-xs-6 tile_stats_count">--}}
                {{--<span class="count_top"><i class="fa fa-group"></i> Iva pagado</span>--}}
                {{--<div class="count"></div>--}}
                {{--<span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>--}}
            {{--</div>--}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Facturas que te deben <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="faccobr"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Facturas que debes <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="facdev"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Rentabilidad<small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="pierentable"></canvas>
                    </div>
                </div>
            </div>
            <!-- line graph morris -->
            {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Line Graph <small>Sessions</small></h2>--}}
                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content2">--}}
                        {{--<div id="graph_line" style="width:100%; height:300px;"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- /line graph -->--}}
            {{--<!-- pie graph echarts -->--}}
            {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Pie Graph</h2>--}}
                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}

                        {{--<div id="echart_pie" style="height:350px;"></div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- line graph echarts -->--}}
            {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Line Graph</h2>--}}
                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}

                        {{--<div id="echart_line" style="height:350px;"></div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <!-- /top tiles -->
@endrole

    </div>
    <!-- /page content -->
@endsection

@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        if ($('#facdev').length) {
            faccobr
            var facdev = document.getElementById("facdev");
            var chartfacdev = new Chart(facdev, {
                type: 'bar',
                data: {
                    labels: ["1-30", "31-60", "61-∞"],
                    datasets: [{
                        label: '$',
                        data: [ {{dashboard(7)}} , {{dashboard(8)}} , {{dashboard(9)}} ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
//                    legend: {
//                        display: true,
//                        labels: {
//                            fontColor: 'rgb(255, 99, 132)'
//                        }
//                    }
//                    title: {
//                        display: true,
//                        text: 'Facturas que te deben'
//                    }
                }
            });
        }
        if ($('#faccobr').length) {
            var faccobr = document.getElementById("faccobr");
            var chartfaccobr = new Chart(faccobr, {
                type: 'bar',
                data: {
                    labels: ["1-30", "31-60", "61-∞"],
                    datasets: [{
                        label: '$',
                        data: [ {{dashboard(4)}} , {{dashboard(5)}} , {{dashboard(6)}} ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: true
                }
            });
        }
        if ($('#pierentable').length ){

            var piecampo = document.getElementById("pierentable");
            var data = {
                datasets: [{
                    data: [0, 0],
                    backgroundColor: [
                        "#455C73",
                        "#9B59B6"
                    ],
                    label: 'Rentabilidad' // for legend
                }],
                labels: [
                    "Ingreso",
                    "Gasto"
                ]
            };

            var pierentable = new Chart(piecampo, {
                data: data,
                type: 'pie',
                otpions: {
                    legend: true
                }
            });

        }
    });
</script>
@endpush
