<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <center>
            <a href="{{ url('/') }}" style="padding-left: 0px;" class="site_title">
                <img style="max-height: 75%; display: inline;" class="img-responsive" src="{{asset('image/sidebartopmediaquery.png')}}">
                <img style="max-height: 70%; display: inline;" class="img-responsive textlogo" src="{{asset('image/textlogo.png')}}">
            </a>
            </center>
            {{--<a href="{{ url('/') }}" class="site_title"><i class="fa fa-cloud"></i> <span>ContApp</span></a>--}}
        </div>
        
        <div class="clearfix"></div>
        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{ Auth::user()->persona->foto ? asset('image/Perfil/'.Auth::user()->persona->foto.'?'.str_random(2)) : Gravatar::src(Auth::user()->email)}}" alt="Avatar of {{ Auth::user()->name }}" class="img-circle profile_img img-ajuste">
            </div>
            <div class="profile_info">
                <span>Bienvenido,</span>
                <h2>{{ Auth::user()->name }}</h2>
                @role('user')
                <h2>Plan: {{ plan ()}}</h2>
                @endrole
            </div>
        </div>
        <!-- /menu profile quick info -->

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            @role('user')
            <div class="menu_section">

                {{--<h3>Group 1</h3>--}}
                <ul class="nav side-menu">
                    <li>
                        <a href="{{ url('/') }}">
                            <i class="fa fa-laptop"></i>
                            Home
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    <li><a><i class="fa fa-tasks"></i> Facturas <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @if(empty( \Illuminate\Support\Facades\Auth::user()->conekta_plan ))
                            <li>
                                <a href="{{ url('/factura/create') }}">
                                    <i class="fa fa-cloud-upload"></i>
                                    Subir facturas
                                    {{--<span class="label label-success pull-right">Flag</span>--}}
                                </a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ url('/facturas/1') }}">
                                    <i class="fa fa-search"></i>
                                    Consultar
                                    {{--<span class="label label-success pull-right">Flag</span>--}}
                                </a>
                            </li>
                            {{--<li><a href="{{ url('/factura/2') }}"><i class="fa fa-external-link-square"></i>Emitidas</a></li>--}}
                        </ul>
                    </li>

                    <li><a><i class="fa fa-money"></i> Pagos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/factura/1') }}"><i class="fa fa-external-link"></i>Cobrados</a></li>
                            <li><a href="{{ url('/factura/2') }}"><i class="fa fa-external-link-square"></i>Recibidos</a></li>
                            <li><a href="{{ url('/pagosfact') }}"><i class="fa fa-search"></i>Consultar</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-calculator"></i> Cálculo de impuestos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/peticiones/Cálculo/edit') }}"><i class="fa fa-external-link"></i>Solicitar</a></li>
                            <li><a href="{{ url('/buscarpeticion/Cálculo') }}"><i class="fa fa-search"></i>Consultar</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-line-chart"></i> Posición financiera <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/peticiones/Posición/edit') }}"><i class="fa fa-external-link"></i>Solicitar</a></li>
                            <li><a href="{{ url('/buscarpeticion/Posición') }}"><i class="fa fa-search"></i>Consultar</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-files-o"></i> Declaraciones <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/declaracion/create') }}"><i class="fa fa-pencil"></i>Registrar declaración anterior</a></li>
                            <li><a href="{{ url('/declaracion') }}"><i class="fa fa-list"></i>Declaraciones anteriores</a></li>
                            <li><a href="{{ url('/peticiones/Declaración/edit') }}"><i class="fa fa-external-link"></i>Solicitar declaracion</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('/construct') }}">
                            <i class="fa fa-bank"></i>
                            Artículos de tu interés
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                </ul>
            </div>
            @endrole
            @role('backuser')
            <div class="menu_section">

                {{--<h3>Group 1</h3>--}}
                <ul class="nav side-menu">
                    <li>
                        <a href="{{ url('/') }}">
                            <i class="fa fa-laptop"></i>
                            Dashboard @if(peticiones(0) != 0)<span class="badge badge-success">{{peticiones(0)}}</span>@endif
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    @role('Admin')
                    <li>
                        <a href="{{ url('back/peticiones') }}">
                            <i class="fa fa-random"></i>
                            Reasignar peticiones
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    @endrole
                    <li><a><i class="fa fa-group"></i> Control de usuarios <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/back/usuarios/1') }}"><i class="fa fa-asterisk"></i>Usuarios</a></li>
                            @role('Admin')
                            <li><a href="{{ url('/back/usuarios/0') }}"><i class="fa fa-star"></i>Usuarios backoffice</a></li>
                            <li><a href="{{ url('/back/usuarios/create') }}"><i class="fa fa-child"></i>Crear usuario backoffice</a></li>
                            @endrole
                        </ul>
                    </li>

                    <li>
                        <a href="{{ url('notified/create') }}">
                            <i class="fa fa-comments"></i>
                            Notificaciones
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    @role('Admin')
                    <li><a><i class="fa fa-certificate"></i> Control de planes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('/back/planeslist') }}"><i class="fa fa-cube"></i>Listado de planes</a></li>
                            <li><a href="{{ url('/back/plazoslist') }}"><i class="fa fa-cubes"></i>Listado de plazos</a></li>
                            <li><a href="{{ url('/back/precioslist') }}"><i class="fa fa-money"></i>Listado de precios</a></li>
                            <li><a href="{{ url('/back/creaprecio') }}"><i class="fa fa-plus-square"></i>Agregar precio</a></li>
                        </ul>
                    </li>
                    @endrole
                    <li><a><i class="fa fa-calculator"></i> Cálculo impuestos @if(peticiones(3) != 0) <span class="badge badge-success">{{peticiones(3)}}</span> @endif <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('back/peticiones/Cálculo/edit') }}"><i class="fa fa-external-link"></i>Peticiones Activas</a></li>
                            <li><a href="{{ url('back/peticiones/Cálculo') }}"><i class="fa fa-external-link-square"></i>Peticiones finalizadas</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-line-chart"></i> Posición financiera @if(peticiones(2) != 0) <span class="badge badge-success">{{peticiones(2)}}</span> @endif <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('back/peticiones/Posición/edit') }}"><i class="fa fa-external-link"></i>Peticiones Activas</a></li>
                            <li><a href="{{ url('back/peticiones/Posición') }}"><i class="fa fa-external-link-square"></i>Peticiones finalizadas</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-files-o"></i> Declaraciones @if(peticiones(1) != 0) <span class="badge badge-success">{{peticiones(1)}}</span> @endif <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ url('back/peticiones/Declaración/edit') }}"><i class="fa fa-external-link"></i>Peticiones Activas</a></li>
                            <li><a href="{{ url('back/peticiones/Declaración') }}"><i class="fa fa-external-link-square"></i>Peticiones finalizadas</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            @endrole
            {{--<div class="menu_section">--}}
                {{--<h3>Group 2</h3>--}}
                {{--<ul class="nav side-menu">--}}
                    {{--<li>--}}
                        {{--<a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>--}}
                        {{--<ul class="nav child_menu">--}}
                            {{--<li>--}}
                                {{--<a href="#">Level One</a>--}}
                                {{--<li>--}}
                                    {{--<a>Level One<span class="fa fa-chevron-down"></span></a>--}}
                                    {{--<ul class="nav child_menu">--}}
                                        {{--<li class="sub_menu">--}}
                                            {{--<a href="#">Level Two</a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#">Level Two</a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#">Level Two</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">Level One</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        
        </div>
        <!-- /sidebar menu -->
        
        <!-- /menu footer buttons -->
        {{--<div class="sidebar-footer hidden-small">--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="Settings">--}}
                {{--<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>--}}
            {{--</a>--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="FullScreen">--}}
                {{--<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>--}}
            {{--</a>--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="Lock">--}}
                {{--<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>--}}
            {{--</a>--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}">--}}
                {{--<span class="glyphicon glyphicon-off" aria-hidden="true"></span>--}}
            {{--</a>--}}
        {{--</div>--}}
        <!-- /menu footer buttons -->
    </div>
</div>