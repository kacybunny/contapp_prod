<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ Auth::user()->persona->foto ? asset('image/Perfil/'.Auth::user()->persona->foto.'?'.str_random(2)) : Gravatar::src(Auth::user()->email)}}" alt="Avatar of {{ Auth::user()->name }}">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{url('/persona/0/edit')}}"> Perfil</a></li>
                        @role('user')
                        <li class="dropdown-submenu">
                            <a class="test" tabindex="-1" href="#">
                                <span>Registro</span>
                                <span class=" fa fa-angle-down pull-right"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('/datospersona/0/edit') }}" >Datos Ubicación</a></li>
                                <li><a href="{{ url('/datosfiscales/0/edit') }}" >Régimen y actividades fiscales</a></li>
                                <li><a href="{{ url('/datosfiscales/1/edit') }}" >Contraseña SAT / FIEL</a></li>
                                <li><a href="{{ url('/declaracion/create') }}" >Declaraciones anteriores</a></li>
                                {{--<li class="dropdown-submenu">--}}
                                    {{--<a class="test" href="#">Another dropdown <span class="caret"></span></a>--}}
                                    {{--<ul class="dropdown-menu">--}}
                                        {{--<li><a href="#">3rd level dropdown</a></li>--}}
                                        {{--<li><a href="#">3rd level dropdown</a></li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            </ul>
                        </li>
                        <li><a href="{{ url('/pagos/create') }}">Cambio de plan</a></li>
                        <li><a href="{{ url('/pagos') }}">Historial transacciones</a></li>
                        @endrole
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Cerrar sesión</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-green">{{ notificacionnum() }}</span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        @foreach ( notificaciones() as $not)
                        <li {!!   $not->read_at?"class='notified read'":"class='notified unread'" !!} id="{{$not->id}}">
                            <a href="{{$not->data['url']}}">
                                <span class="image"><img src="{{ config('app.logo')}}" alt="Profile Image" /></span>
                                <span>
                                     <span class="time">{{ $not->created_at->format('d/m/Y h:i') }}</span>
                                    {{--</br>--}}
                          <span>{{textlargenotifi($not->data['title'],1)}}</span>
                        </span>
                                <span class="message">
                                    {{textlargenotifi($not->data['data'],2)}}
                        </span>
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <div class="text-center">
                                <a href="{{ url('/notified') }}">
                                    <strong>Mostrar todos los mensajes</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->