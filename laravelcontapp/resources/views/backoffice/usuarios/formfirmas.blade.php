
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">
    <div class="col-xs-12" >
        <div class="col-xs-12" >
            <h2> FIEL / e.firma</h2>
            <div class="col-xs-6" >
                {!! BootForm::file('certificado','Certificado (.cer)') !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::file('llave_privada','Llave privada (.key)') !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::password('contrasena_llave_privada', 'Contraseña llave privada', ['placeholder' => 'Contraseña llave privada','required']) !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::password('confirma_contrasena_llave_privada', 'Confirma contraseña llave privada', ['placeholder' => 'Confirma contraseña llave privada','required']) !!}

            </div>
        </div>
        <div class="col-xs-12" >
            <h2>Contraseña SAT </h2>
            <div class="col-xs-6" >
                {!! BootForm::password('contrasena_SAT', 'Contraseña del SAT', ['placeholder' => 'Contraseña del sat','required']) !!}
            </div>
            <div class="col-xs-6" >
                {!! BootForm::password('confirmar_contrasena_SAT', 'Confirma contraseña del SAT', ['placeholder' => 'Confirma contraseña del SAT','required']) !!}
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12" >
    <center>
        <div class="row" >
            <div class="col-xs-12" >
                <div class="col-xs-6"  >
                    {!! BootForm::submit('Guardar',['class'=>'btn btn-success btn-lg']) !!}
                </div>
            </div>
        </div>
    </center>
</div>


{!! BootForm::close() !!}

@push('scriptspersonal')
    <script type="text/javascript">
        $(document).ready(function() {
            $(function(){

            }());
        });
    </script>
@endpush