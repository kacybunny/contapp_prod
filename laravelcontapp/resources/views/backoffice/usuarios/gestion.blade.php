@extends('backoffice.usuarios.usuariospanels')

@section('panels')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.usuarios.formplan')
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.usuarios.formpersona',['url'=>'back/persona/'.$id])
            </div>
        </div>
    </div>

    @if($usuario->hasRole('user'))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.usuarios.formdatospersona',['url'=>'back/datospersona/'.$id])
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.usuarios.formfiscales',['url'=>'back/fiscales/'.$id])
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.usuarios.formfirmas',['url'=>'back/firmas/'.$id])
            </div>
        </div>
    </div>
    @endif
@endsection