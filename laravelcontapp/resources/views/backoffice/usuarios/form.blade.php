
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">
    <div class="col-xs-6">
    {!! BootForm::text('nombre', 'Nombre', old('nombre'), ['placeholder' => 'Nombre']) !!}
    </div>
    <div class="col-xs-6">
    {!! BootForm::text('paterno', 'Apellido paterno', old('paterno'), ['placeholder' => 'Apellido paterno']) !!}
    </div>
    <div class="col-xs-6">
    {!! BootForm::text('materno', 'Apellido Materno', old('materno'), ['placeholder' => 'Apellido materno']) !!}
    </div>
    <div class="col-xs-6">
    {!! BootForm::email('email', 'Email', old('email'), ['placeholder' => 'Email']) !!}
    </div>
    <div class="col-xs-6">
    {!! BootForm::text('telefono', 'Telefono', old('telefono'), ['placeholder' => 'Teléfono (opcional)']) !!}
    </div>
    <div class="col-xs-6">
    {!! BootForm::select('tipo_usuario', 'Tipo de usuario', [1=>'Administrador',2=>'Operador'],[],['required']) !!}
    </div>
    <div class="col-xs-12">
    {!! BootForm::submit('Registrar', ['class' => 'btn btn-info btn-lg']) !!}
    </div>
</div>

{!! BootForm::close() !!}


@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        restringe('telefono','[^0-9]',10);
        restringe('nombre','[^a-zA-ZáéíóúÁÉÍÓÚ]');
        restringe('paterno','[^a-zA-ZáéíóúÁÉÍÓÚ]');
        restringe('materno','[^a-zA-ZáéíóúÁÉÍÓÚ]');
    });
</script>
@endpush