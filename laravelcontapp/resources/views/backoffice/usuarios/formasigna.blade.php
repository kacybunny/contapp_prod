
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">

    <div class="col-xs-12" >
        {!! BootForm::select('tipo_usuario', 'Tipo de usuario', [1=>'Administrador',2=>'Operador'],[$user->hasRole('Admin')?1:2],['required']) !!}
    </div>
    <div class="col-xs-12" >
        <center>
            <div class="row" >
                <div class="col-xs-12" >
                    <div class="col-xs-6"  >
                        {!! BootForm::submit('Enviar',['class'=>'btn btn-success btn-lg']) !!}
                    </div>
                </div>
            </div>
        </center>
    </div>
</div>

{!! BootForm::close() !!}

@push('scriptspersonal')
    <script type="text/javascript">

        $(document).ready(function() {

        });
    </script>
@endpush