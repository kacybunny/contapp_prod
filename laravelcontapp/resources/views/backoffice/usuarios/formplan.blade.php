<div class="row">
    @if (session('reset'))
        <div class="alert alert-success">
            {{ session('reset') }}
        </div>
    @endif
    {{--<div class="col-xs-6" >--}}
    {{--{!! BootForm::text('edad', 'Edad',$persona->edad, ['required']) !!}--}}
    {{--</div>--}}
    <div class="col-xs-12" >
        <div class="col-xs-2" >
            {!! BootForm::button('Enviar link cambio de contraseña',['onclick'=>'location.assign("'.url('back/reset/'.$id).'")','class'=>'btn btn-success btn-lg']) !!}
        </div>
    </div>
        @if($usuario->hasRole('user'))
    <div class="col-xs-12" >
        <div class="col-xs-2" >
            {!! BootForm::button('Cancelar suscripcion',['onclick'=>'location.assign("'.url('back/suscript/'.$id).'")','class'=>'btn btn-danger btn-lg']) !!}
        </div>
    </div>
            @endif
</div>


{!! BootForm::close() !!}

@push('scriptspersonal')
    <script type="text/javascript">

    </script>
@endpush