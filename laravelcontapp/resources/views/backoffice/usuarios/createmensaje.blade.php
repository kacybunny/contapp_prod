@extends('backoffice.usuarios.usuarios')

@section('title_section')
    <h2>Envia notificacion a {{$user->name}}<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include('backoffice.usuarios.formdnotifica')
@endsection