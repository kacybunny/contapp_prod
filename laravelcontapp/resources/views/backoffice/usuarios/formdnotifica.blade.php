
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">

    <div class="col-xs-12" >
    {!! BootForm::text('titulo', 'Titulo',null, ['required','placeholder' => 'Titulo']) !!}
    </div>
    <div class="col-xs-12" >
        {!! BootForm::textarea('mensaje', 'Mensaje', null, ['required', 'placeholder' => 'Mensaje','rows'=>'3']) !!}
    </div>

    <div class="col-xs-12" >
        <center>
            <div class="row" >
                <div class="col-xs-12" >
                    <div class="col-xs-6"  >
                        {!! BootForm::submit('Enviar',['class'=>'btn btn-success btn-lg']) !!}
                    </div>
                </div>
            </div>
        </center>
    </div>
</div>

{!! BootForm::close() !!}


@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        restringe('titulo','',100);
        restringe('mensaje','',200);
    });
</script>
@endpush