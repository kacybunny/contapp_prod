@extends('backoffice.usuarios.usuarios')

@section('title_section')
    <h2>Listado usuarios<small></small></h2>
@endsection

@section('contentx')
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-personal" class="table table-striped table-bordered">
        {{--<thead>--}}
        {{--<tr id="headTabla">--}}
        {{--</tr>--}}
        {{--</thead>--}}
        {{--<tfoot>--}}
        {{--<tr id="pieTabla">--}}
        {{--</tr>--}}
        {{--</tfoot>--}}
    </table>
    <div id="dialog_eliminar" title="Eliminar usuario" style="display:none">
        <p id="textodiag"></p>
    </div>
    <form method="POST" id="form">

        {!! csrf_field() !!}

        <input type="hidden" name="_method" value="DELETE">
    </form>
@endsection

@push('scriptspersonal')
<script>
    $(document).ready(function() {
        (function(){
            function getOptionEliminar(url) {
                $("#dialog_eliminar").dialog({
                    buttons: [
                        {
                            text: "Si",
                            click: function () {

                                $('#form').attr('action', url);
                                $('#form').submit();
                                $(this).dialog("close");

                            }
                        }, {
                            text: "No",
                            click: function () {
                                $(this).dialog("close");

                            }


                        }
                    ]
                });
            }
            function accionespage (){
                $('[data-toggle="tooltip"]').tooltip({
                    container: 'body'
                });
                $('.eliminar').off( "click").click(function () {
                    datatabx=table.row( $(this).parent().parent() ).data();
                    url=$(this).attr('data-url');
                    $('#textodiag').html('¿Estas seguro que deseas Eliminar al usuario: '+datatabx[0]+'?')
                    getOptionEliminar(url);
                });
            }
            {{--configtable.ajax.url="{{url('back/api/planes')}}";--}}
                configtable.data={!! json_encode($datos) !!};
//                configtable.data=[];
            configtable.columns={!! json_encode($columnas) !!};

            console.log();
            init=function () {
                accionespage ()
            };
            tabladinamica('datatable-personal',init);

            table.on('dblclick', 'tr', function () {
//                data = table.row( this ).data();
////                console.log(data);
//                dialogooption('','<p>hola</p>','prueba',data,false);
            } ).on('draw.dt', function() {
                accionespage ()
                // console.log('draw');
            }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
                // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
                accionespage ()
            } );
        }());
    });

</script>
@endpush


{{--tabs = json.columns;--}}
{{--$.each(tabs, function(i,item){--}}
{{--$("#pieTabla").append("<th>"+tabs[i].data+"</th>");--}}
{{--$("#headTabla").append("<th>"+tabs[i].data+"</th>");--}}
{{--});--}}
{{--$('#empresas tfoot th').each( function () {--}}
{{--var title = $(this).text();--}}
{{--$(this).html( '<input type="text" placeholder="Search '+title+'" />' );--}}
{{--});--}}
{{--table = $('#empresas').on( 'init.dt', function () {--}}
{{--}).DataTable(json);--}}






