
@if (session('send'))
    <div class="alert alert-success">
        {{ session('send') }}
    </div>
@endif
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">

    <div class="col-xs-12" >
        <div class="row">
            <div class="col-xs-12" >
                {!! BootForm::radios('tipo', ['html' => ' '], ['1'   => ' Todos','2' => ' Seleccionar usuarios'],1,true,['class'=>'flat']) !!}
            </div>
        </div>
    </div>

    <div class="col-xs-12" id="userselect" style="display: none">
        {!! BootForm::select('usuario[]', 'Usuario',$users,[], ['placeholder' => '','id'=>'usuario','multiple','class'=>'selectpicker','data-live-search'=>'true']) !!}
    </div>
    <div class="col-xs-12" >
        {!! BootForm::text('titulo', 'Titulo',null, ['required','placeholder' => 'Titulo']) !!}
    </div>
    <div class="col-xs-12" >
        {!! BootForm::textarea('mensaje', 'Mensaje', null, ['required', 'placeholder' => 'Mensaje','rows'=>'3']) !!}
    </div>
    <div class="col-xs-12" >
        <center>
            <div class="row" >
                <div class="col-xs-12" >
                    <div class="col-xs-6"  >
                        {!! BootForm::submit('Enviar',['class'=>'btn btn-success btn-lg']) !!}
                    </div>
                </div>
            </div>
        </center>
    </div>
</div>

{!! BootForm::close() !!}


@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        restringe('titulo','',100);
        restringe('mensaje','',200);
        function check ( ) {
            var tipo = $("input[name='tipo']:checked").val();
            if(tipo == 1){
                $('#userselect').hide('slow');
            }else{
                $('#userselect').show('slow');
            }
        }
        setTimeout(function(){  $("input[name='tipo']").each(function () {
            $(this).parent().click(function () {
                check();
            });
            $(this).siblings().click(function () {
                check();
            });
            $(this).parent().parent().click(function () {
                check();
            });
        }); }, 1000);
    });
</script>
@endpush