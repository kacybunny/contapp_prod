
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">

    <div class="col-xs-6" >
        {!! BootForm::textarea('titulo', 'Titulo', $plazo->titulo , ['placeholder' => '','required','rows'=>'3']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::textarea('mensaje', 'Mensaje',$plazo->mesnaje, ['placeholder' => '','required','rows'=>'3']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('prueba', 'Prueba', $plazo->prueba, [] ) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('porcentaje', 'Porcentaje', $plazo->porcentaje, [] ) !!}
    </div>
    <div class="row">
        <div class="col-xs-12" >
            <div class="col-xs-5" >
                {!! BootForm::radios('visible','Visible', [1   => ' SI',0 => 'No'],$plazo->visible,true,['class'=>'flat']) !!}

            </div>
        </div>
    </div>

    <div class="col-xs-12" >
        {!! BootForm::submit('Guardar',['class'=>'btn btn-success btn-lg','id'=>'guardar']) !!}
    </div>
</div>


{!! BootForm::close() !!}


@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
@endpush