@extends('backoffice.planes.planes')

@section('title_section')
    <h2>{{$titulo}}<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include($form)
@endsection