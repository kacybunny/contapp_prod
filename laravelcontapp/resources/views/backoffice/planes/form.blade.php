<div class="col-xs-offset-4 col-xs-4">
    <div class="progress">
        <div class="progress-bar progress-bar-warning" id="myBar" data-transitiongoal="70" aria-valuenow="70" style="width: 30%;">
        </div>
    </div>
</div>
{!! BootForm::open(['url' => url('datosfiscales/0'), 'method' => 'put','id'=>'ubicacion'] ) !!}
<div class="row">
    <div class="col-xs-12" >

        <div class="col-xs-6" >
            {!! BootForm::text('plan', 'Plan', null, ['disabled'] ) !!}
        </div>
        <div class="col-xs-6" >
            {!! BootForm::select('titulo', 'Titulo',[],null, ['required','class'=>'selectpicker','data-live-search'=>'true']) !!}
        </div>
        <div class="col-xs-6" >
            {!! BootForm::text('precio', 'Precio', $datospersona->localidad, [] ) !!}
        </div>
        <div class="col-xs-6" >
            {!! BootForm::text('mensaje', 'Mensaje', $datospersona->vialidad, [] ) !!}
        </div>
        <div class="col-xs-6" >
            {!! BootForm::text('', 'Numero interior', $datospersona->num_int, [] ) !!}
        </div>
        <div class="col-xs-6" >
            {!! BootForm::text('num_ext', 'Numero exterior', $datospersona->num_ext, [] ) !!}
        </div>

        <div class="input-group date" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
</div>
<h2>Regimen, actividades y obligaciones </h2>
<div class="row" id="agregamasobl" >
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">
            <label for="obligaciones" class="control-label">
                Obligaciones
            </label>
        </div>
        <div class="form-group col-xs-5">
            <label for="fechainiobl" class="control-label">
                Fecha de inicio
            </label>
        </div>
        <div class="form-group col-xs-2">
            <label for="obligaciones" class="control-label">
            </label>
        </div>

    </div>
    <div class="col-xs-12" id="obligaciongroup">

    </div>
</div>
<div class="col-xs-12" >
    <button id="adicionalobl" name="adicionalobl" type="button" class="btn btn-success"> <i class="fa fa-plus-square" ></i> Agregar obligacion</button>
</div>
<br/>
<br/>
<div class="row" id="agregamasact" >
    <br/>
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">
            <label for="actividades" class="control-label">
                Actividades
            </label>
        </div>
        <div class="form-group col-xs-5">
            <label for="fechainiobl" class="control-label">
                Fecha de inicio
            </label>
        </div>
        <div class="form-group col-xs-2">
            <label for="obligaciones" class="control-label">
            </label>
        </div>

    </div>
    <div class="col-xs-12" id="actividadesgroup">
    </div>
</div>
<div class="col-xs-12" >
    <button id="adicionalact" name="adicionalact" type="button" class="btn btn-success"> <i class="fa fa-plus-square" ></i> Agregar actividad</button>
</div>
<br/>
<br/>
<div class="row" id="agregamasreg" >
    <br/>
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">
            <label for="obligaciones" class="control-label">
                Regimen
            </label>
        </div>
        <div class="form-group col-xs-5">
            <label for="fechainiobl" class="control-label">
                Fecha de inicio
            </label>
        </div>
        <div class="form-group col-xs-2">
            <label for="obligaciones" class="control-label">
            </label>
        </div>

    </div>
    <div class="col-xs-12" id="regimengroup">
    </div>
</div>
<div class="col-xs-12" >
    <button id="adicionalreg" name="adicionalreg" type="button" class="btn btn-success"> <i class="fa fa-plus-square" ></i> Agregar regimen</button>
</div>
<br/><br/><br/>




{!! BootForm::close() !!}
<div class="col-xs-12" >
    <center>
        <div class="row" >
            <div class="col-xs-12" >
                <div class="col-xs-6" >
                    {!! BootForm::submit('Guardar',['class'=>'btn btn-success btn-lg','id'=>'guardar']) !!}
                </div>
                <div class="col-xs-6"  >
                    {!! BootForm::button('Continuar',['onclick'=>'location.assign("'.url('datosfiscales/1/edit').'")','class'=>'btn btn-info btn-lg']) !!}
                </div>
            </div>
        </div>
    </center>
</div>
@push('scriptspersonal')
    <script type="text/javascript">

        $(document).ready(function() {
            $(function(){
                progresbar('myBar',70,80,90);
                alldata=[];

                alldata.push( obligaciones={
                    'datos':{!! $obligaciones !!},
                    'tmpl':'tmpl_obligacionescombo',
                    'agrega':'adicionalobl',
                    'grupo':'obligaciongroup',
                    'datepick':'selectpickobl',
                    'selectid':'obligacion',
                    'picker':'fechainiobl[]',
                    'datosuser':{!! old('obligaciones') ? json_encode(old('obligaciones')) : json_encode($obligacionespersona->obligaciones) !!},
                    'fechas':{!! old('fechainiobl') ? json_encode(old('fechainiobl')) : json_encode($obligacionespersona->fechas) !!}
                });
                alldata.push(  actividades={
                    'datos':{!! $actividades !!},
                    'tmpl':'tmpl_atividadescombo',
                    'agrega':'adicionalact',
                    'grupo':'actividadesgroup',
                    'datepick':'selectpickact',
                    'selectid':'actividad',
                    'picker':'fechainiact[]',
                    'datosuser':{!! old('actividades') ? json_encode(old('actividades')) : json_encode($actividadespersona->actividades) !!},
                    'fechas': {!! old('fechainiact') ? json_encode(old('fechainiact')) : json_encode($actividadespersona->fechas) !!}
                });
                alldata.push(  regimen={
                    'datos':{!! $regimen !!},
                    'tmpl':'tmpl_regimencombo',
                    'agrega':'adicionalreg',
                    'grupo':'regimengroup',
                    'datepick':'selectpickreg',
                    'selectid':'regimen',
                    'picker':'fechainireg[]',
                    'datosuser':{!! old('regimen') ? json_encode(old('regimen')) : json_encode($regimenpersona->regimenes) !!},
                    'fechas':{!! old('fechainireg') ? json_encode(old('fechainireg')) : json_encode($regimenpersona->fechas) !!}
                });

                formcampodinamico(alldata,'guardar','select','ubicacion');

                function formcampodinamico(obj , boton , tipo , form){

                    var arrayid=[];

                    $.each(obj,function (key , value) {
                        if(value.datosuser != undefined && typeof value.datosuser === 'object' && value.datosuser.length > 0){
                            $.each(value.datosuser ,function (llave , valor) {
                                html = renderdefault(value.tmpl,value);
                                $('#'+value.grupo).append(html);
                                layouts=$(tipo+"[id="+value.selectid+"]");
                                $(layouts[llave]).selectpicker('val',valor);
                                layoutsfecha=$("input[name='"+value.picker+"']");
                                $(layoutsfecha[(layoutsfecha.length-1)]).val(value.fechas[llave]);
                            });
                        } else{
                            html = renderdefault(value.tmpl,value);
                            $('#'+value.grupo).append(html);
                            layouts=$(tipo+"[id="+value.selectid+"]");
                            $(layouts[0]).selectpicker('val','');
                        }
                        arrayid.push(value.selectid);
                        agrega(value.agrega, value.grupo, html, value.datepick, 'has-feedback-left',tipo,value.selectid);
                    });
                    if (typeof boton === 'object' ){
                        $.each( boton ,function (asoc,data) {
                            $('#'+data).click(function () {
                                if(validar(arrayid,tipo)){
                                    $('#'+form).submit();
                                }
                            });
                        });
                    }
                    else if (typeof boton === 'string' ){
                        $('#'+boton).click(function () {
                            if(validar(arrayid,tipo)){
                                $('#'+form).submit();
                            }
                        });
                    }

                    elimina('eliminar','selectpick');
                }

                // Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla

                // Evento que selecciona la fila y la elimina

                function renderdefault(tmpl,datos){
                    html = $("#"+tmpl).render(datos);
                    return html;
                }

                function validar(valores,tipo){
                    var resp = true;
                    if(typeof valores==='object'){
                        var datos = mapear(valores,tipo);
                        Object.keys(datos).forEach(function (key) {
                            if(!validaarray(datos[key],key)){
                                resp=false;
                            }
                        });

                    }else{
                        resp=false;
                    }
                    return resp;
                }
                function mapear(claves,tipo='input'){
                    var temp=[];
                    if (typeof claves==='object') {
                        $.each( claves, function( key, value ) {
                            temp[value]= $(tipo+"[id='"+value+"']").map(function(){
                                return $(this).val();
                            }).get();
                        });
                    } else if(typeof claves==='string') {
                        temp[claves] = $(tipo+"[id='"+claves+"']").map(function () {
                            return $(this).val();
                        }).get();
                    } else {
                        temp=false;
                    }
                    return temp;
                }
                function notificar(campos,type='',clase='',nonb=false){
                    if (typeof campos==='object') {
                        campos.type = switchtype(type);
                        campos.addclass=clase;
                        campos.styling= 'bootstrap3';
                        campos.nonblock= {
                            nonblock: nonb
                        };
                            new PNotify(campos);
                    }
                }
                function switchtype(type){
                    if (type != undefined ){
                        switch (type){
                            case 1:
                                return 'success';
                                break;
                            case 2:
                                return 'info';
                                break;
                            case 3:
                                return 'error';
                                break;
                            default:
                                return '';
                        }
                    }else{
                        return '';
                    }

                }
                function validaarray(dato,nombre){
                    var resp = false;
                    detalle={};
                    if (dato.length > 0){
                        resp = true;
                        if (!dato.repetidos()){
                            detalle.title='Revisar '+nombre+'es';
                            detalle.text=nombre+' repetido';
                            notificar(detalle);
                            resp = false;
                        }
                    }else {
                        detalle.title = 'Revisar ' + nombre;
                        if (nombre != 'regimen') {
                        detalle.text = 'Debe elegir almenos una ' + nombre;
                        }else{
                            detalle.text = 'Debe elegir almenos un ' + nombre;
                        }
                        notificar(detalle);
                        resp = false;

                    }
                    return resp;
                }
            });
        });
    </script>
    @include('plantillasrender.datosfiscales')
@endpush