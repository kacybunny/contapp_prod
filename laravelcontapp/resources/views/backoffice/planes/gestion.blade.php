@extends('backoffice.planes.planespanels')

@section('panels')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.planes.formplan')
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.planes.formplazos')
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('backoffice.planes.formprecios')
            </div>
        </div>
    </div>
@endsection