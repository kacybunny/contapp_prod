@extends('backoffice.planes.planes')

@section('title_section')
    <h2>Listado<small></small></h2>
@endsection

@section('contentx')
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-personal" class="table table-striped table-bordered">
        {{--<thead>--}}
        {{--<tr id="headTabla">--}}
        {{--</tr>--}}
        {{--</thead>--}}
        {{--<tfoot>--}}
        {{--<tr id="pieTabla">--}}
        {{--</tr>--}}
        {{--</tfoot>--}}
    </table>

@endsection

@push('scriptspersonal')
<script>
    $(document).ready(function() {
        (function(){
            {{--configtable.ajax.url="{{url('back/api/planes')}}";--}}
                configtable.data={!! json_encode($datos) !!};
//                configtable.data=[];
            configtable.columns={!! json_encode($columnas) !!};
            function accionespage (){
                $('[data-toggle="tooltip"]').tooltip({
                    container: 'body'
                });
            }
            init=function () {
                accionespage ()
            };
            tabladinamica('datatable-personal',init);

            table.on('dblclick', 'tr', function () {
//                data = table.row( this ).data();
////                console.log(data);
//                dialogooption('','<p>hola</p>','prueba',data,false);
            } ).on('draw.dt', function() {
                accionespage ()
                // console.log('draw');
            }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
                // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
                accionespage ()
            } );
        }());
    });

</script>
@endpush


{{--tabs = json.columns;--}}
{{--$.each(tabs, function(i,item){--}}
{{--$("#pieTabla").append("<th>"+tabs[i].data+"</th>");--}}
{{--$("#headTabla").append("<th>"+tabs[i].data+"</th>");--}}
{{--});--}}
{{--$('#empresas tfoot th').each( function () {--}}
{{--var title = $(this).text();--}}
{{--$(this).html( '<input type="text" placeholder="Search '+title+'" />' );--}}
{{--});--}}
{{--table = $('#empresas').on( 'init.dt', function () {--}}
{{--}).DataTable(json);--}}






