
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">

    <div class="col-xs-6" >
        {!! BootForm::select('plan', 'Plan',$planes,null, ['placeholder' => '','required','class'=>'selectpicker','data-live-search'=>'true']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('precio', 'Precio',null, ['placeholder' => '','required']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('fecha_inicio', 'Inicio',old('fecha_inicio'), ['placeholder' => 'Desde fecha']) !!}
    </div>

    <div class="col-xs-12" >
        {!! BootForm::submit('Guardar',['class'=>'btn btn-success btn-lg','id'=>'guardar']) !!}
    </div>
</div>


{!! BootForm::close() !!}


@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        $('#fecha_inicio').datepicker(configdatepicker);
        $('#fecha_fin').datepicker(configdatepicker);
    });
</script>
@endpush