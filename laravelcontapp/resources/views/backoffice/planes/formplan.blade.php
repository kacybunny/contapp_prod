
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">

    <div class="col-xs-6" >
        {!! BootForm::textarea('titulo', 'Titulo',$plan->titulo, ['placeholder' => '','required','rows'=>'3']) !!}
    </div>
    {{--<div class="col-xs-6" >--}}
        {{--{!! BootForm::textarea('subtitulo', 'Subtitulo',null, ['placeholder' => '','required','rows'=>'3']) !!}    --}}
    {{--</div>--}}
    <div class="col-xs-6" >
        {!! BootForm::textarea('mensaje', 'Mensaje',$plan->mensaje, ['placeholder' => '','required','rows'=>'3']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('url', 'Url', $plan->url, [] ) !!}
    </div>

    <div class="col-xs-12" >
        {!! BootForm::submit('Guardar',['class'=>'btn btn-success btn-lg','id'=>'guardar']) !!}
    </div>
</div>


{!! BootForm::close() !!}


@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
@endpush