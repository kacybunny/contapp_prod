@extends('backoffice.declaraciones.declaraciones')

@section('title_section')
    <h2>Agregar declaración anterior<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include('backoffice.declaraciones.form')
@endsection