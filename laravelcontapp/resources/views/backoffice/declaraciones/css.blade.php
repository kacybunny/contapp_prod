@push('stylesheets')

    		{{--<link href="{{ asset("css/animate/animate.min.css") }}" rel="stylesheet">--}}
<link href="{{ asset("css/animate/animate.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/daterangepicker/daterangepicker.css") }}" rel="stylesheet">
<link href="{{ asset("css/iCheck/skins/flat/green.css") }}" rel="stylesheet">
<link href="{{ asset("css/nprogress/nprogress.css") }}" rel="stylesheet">
<link href="{{ asset("css/google-code-prettify/prettify.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/bootstrap-select/bootstrap-select.min.css") }}" rel="stylesheet">

{{--<link href="{{ asset("css/select2/select2.min.css") }}" rel="stylesheet">--}}
{{--<link href="{{ asset("css/select2-bootstrap-theme/select2-bootstrap.css") }}" rel="stylesheet">--}}
{{--<link href="{{ asset("css/select2-bootstrap-css/select2-bootstrap.min.css") }}" rel="stylesheet">--}}
<link href="{{ asset("css/switchery/switchery.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/starrr/starrr.css") }}" rel="stylesheet">
<link href="{{ asset("css/datatables/dataTables.bootstrap.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/datatables/buttons.bootstrap.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/datatables/fixedHeader.bootstrap.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/datatables/responsive.bootstrap.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/datatables/scroller.bootstrap.min.css") }}" rel="stylesheet">

<link href="{{ asset("css/dropzone/dropzone.min.css") }}" rel="stylesheet">

<link href="{{ asset("css/pnotify/pnotify.css") }}" rel="stylesheet">
<link href="{{ asset("css/pnotify/pnotify.buttons.css") }}" rel="stylesheet">
<link href="{{ asset("css/pnotify/pnotify.nonblock.css") }}" rel="stylesheet">

@endpush


@push('customstylesheets')
			
        	{{--<link href="{{ asset("css/custom.css") }}" rel="stylesheet"> --}}
<style>
    .form-group .select2-container {
        position: relative;
        z-index: 2;
        float: left;
        width: 100%;
        margin-bottom: 0;
        display: table;
        table-layout: fixed;
    }
</style>

@endpush