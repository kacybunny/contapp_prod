{!! BootForm::open(['url' => url($url), 'method' => $metodo, 'id'=>'declaraciones']) !!}
<div class="col-xs-5" >
    {!! BootForm::select('anio', 'Año',$anio,old('anio'), ['required','data-ayuda'=>'off','class'=>'selectpicker','data-live-search'=>'true']) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::select('mes', 'Mes',[],null, ['required','data-ayuda'=>'off','class'=>'selectpicker','data-live-search'=>'true']) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('ingresos', 'Ingresos', null, ['disabled'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_ingresos', 'IVA de ingresos', null, ['disabled'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_retenido', 'IVA retenido', null, ['disabled'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('deducciones', 'Deducciones', null, ['disabled'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_acreditable', 'IVA acreditable', null, ['disabled'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('iva_pagado', 'IVA pagado', null, ['disabled'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('retenciones_ISR', 'Retenciones ISR', null, ['disabled'] ) !!}
</div>
<div class="col-xs-5" >
    {!! BootForm::text('ISR_pagado', 'ISR pagado', null, ['disabled'] ) !!}
</div>
<div class="col-xs-12" >
    {!! BootForm::submit('Agregar',['class'=>'btn btn-success']) !!}
</div>
{!! BootForm::close() !!}


<p class="text-muted font-13 m-b-30">

</p>
<table id="datatable-personal" class="table table-striped table-bordered">
</table>
<div id="dialog_eliminar" title="Eliminar Declaración" style="display:none">
    <p id="textodiag"></p>
</div>
<form method="POST" id="form">

    {!! csrf_field() !!}

    <input type="hidden" name="_method" value="DELETE">
</form>
@include('modals.infoimage')
@push('scriptspersonal')

<script type="text/javascript">
    $(document).ready(function() {
        restringe('ingresos','[^0-9.]');
        restringe('iva_ingresos','[^0-9.]');
        restringe('iva_retenido','[^0-9.]');
        restringe('deducciones','[^0-9.]');
        restringe('iva_acreditable','[^0-9.]');
        restringe('iva_pagado','[^0-9.]');
        restringe('retenciones_ISR','[^0-9.]');
        restringe('ISR_pagado','[^0-9.]');
        var datameses={!! $meses !!};
        if ($('#declaraciones').length) {
            ayudaicon('declaraciones');
            $('#ayu_ingresos').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/4.ISR_INGRESOS_COBRADOS.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_ingresos').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/5.IVA_DE_INGRESOS.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_retenido').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/6.IVA_RETENIDO.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_deducciones').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/7.ISR_DEDUCCIONES_REALIZADAS.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_acreditable').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/8.IVA_ACREDITABLE.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_iva_pagado').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/9.IVA_PAGADO.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_retenciones_ISR').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/10.RETENCIONES_ISR.jpg')}}');
                $('#infomodalform').modal();
            });
            $('#ayu_ISR_pagado').click(function (e) {
                $('#modalinfo').attr('src','{{asset('image/info_declaracion/11.ISR_PAGADO_DEL_MES.jpg')}}');
                $('#infomodalform').modal();
            });

            anio=$('#anio').find(":selected").val();
            apendcomboanio ('mes',datameses,anio,selectpicker=true);
            $( "#anio" ).change(function() {
                anio=$(this).find(":selected").val();
                $("#mes").show("slow");
                identificador=$('#anio').val();
                if(identificador==""){
                    $("#mes").hide("slow");
                }
                apendcomboanio ('mes',datameses,anio,selectpicker=true);
            });
            $('#mes').selectpicker('val', '{{old('mes')}}');
            disabledchange('declaraciones', ['mes','anio'], 'data-icon', 'glyphicon-ok',datameses);
        }

        configtable.data={!! json_encode($datos) !!};
        configtable.columns={!! json_encode($columnas) !!};
        function getOptionEliminar(url) {
            $("#dialog_eliminar").dialog({
                buttons: [
                    {
                        text: "Si",
                        click: function () {

                            $('#form').attr('action', url);
                            $('#form').submit();
                            $(this).dialog("close");

                        }
                    }, {
                        text: "No",
                        click: function () {
                            $(this).dialog("close");

                        }


                    }
                ]
            });
        }
        function accionespage (){
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            $('.eliminar').off( "click").click(function () {
                datatabx=table.row( $(this).parent().parent() ).data();
                url=$(this).attr('data-url');
                $('#textodiag').html('¿Estas seguro que deseas Eliminar la declaración del año: '+datatabx[1]+' del mes de '+datatabx[0]+' ?')
                getOptionEliminar(url);
            });
        }
        init=function () {
            accionespage ()
        };
        tabladinamica('datatable-personal',init);

        table.on('dblclick', 'tr', function () {
//                data = table.row( this ).data();
////                console.log(data);
//                dialogooption('','<p>hola</p>','prueba',data,false);
        } ).on('draw.dt', function() {
            accionespage ()
            // console.log('draw');
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
            accionespage ()
        } );

    });
</script>

@endpush