@extends('backoffice.peticion.peticion')

@section('title_section')
    <h2>{{$titulo}}<small></small></h2>
@endsection

@section('contentx')
    @if (session('file'))
        <div class="alert alert-error">
            {{ session('file') }}
        </div>
    @endif
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-personal" class="table table-striped table-bordered">

    </table>
@endsection

@push('scriptspersonal')
@include('modals.subepdf')
<script type="text/javascript">
    $(document).ready(function() {
        configtable.data={!! json_encode($datos) !!};
        configtable.columns={!! json_encode($columnas) !!};

        function accionespage() {
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            $('.filepdf').off( "click").click(function () {
                mensaje=$(this).attr('data-mensaje');
                url=$(this).attr('data-url');
                $('#subepdf').attr('action',url);
                $('#mensajearchivo').html(mensaje);
                $('#subirarchivo').modal();
            });
        }
        
        console.log();
        init=function () {
//            console.log('init')
            accionespage();
        };
        tabladinamica('datatable-personal',init);

        table.on('dblclick', 'tr', function () {
            {{--data = table.row( this ).data();--}}
            {{--if(data!=undefined){--}}
{{--//                console.log(data);--}}
                {{--dialogooption('{{url('back/api/asignar')}}/'+data[0],'<p>Deseas procesar la solicitud '+data[0]+'</p>','Asignar solicitud',data,true);--}}
            {{--}--}}
        } ).on('draw.dt', function() {
            accionespage();
            // console.log('draw');
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            // console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
            accionespage();
        } );
    });
</script>
@endpush