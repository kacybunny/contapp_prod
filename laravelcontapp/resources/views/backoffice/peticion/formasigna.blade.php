
{!! BootForm::open(['url' => url($url), 'method' => $metodo] ) !!}
<div class="row">

    <div class="col-xs-12" >
        {!! BootForm::select('usuario', 'Usuario',$users,[], ['placeholder' => '','required','class'=>'selectpicker','data-live-search'=>'true']) !!}
    </div>
    <div class="col-xs-12" >
        <center>
            <div class="row" >
                <div class="col-xs-12" >
                    <div class="col-xs-6"  >
                        {!! BootForm::submit('Enviar',['class'=>'btn btn-success btn-lg']) !!}
                    </div>
                </div>
            </div>
        </center>
    </div>
</div>

{!! BootForm::close() !!}

@push('scriptspersonal')
    <script type="text/javascript">

        $(document).ready(function() {

        });
    </script>
@endpush