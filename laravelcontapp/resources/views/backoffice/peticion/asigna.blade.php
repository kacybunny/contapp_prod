@extends('backoffice.peticion.peticion')

@section('title_section')
    <h2>{{$text}} ticket {{$peticion->ticket}}<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include('backoffice.peticion.formasigna')
@endsection