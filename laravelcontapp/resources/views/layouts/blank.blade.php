<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='{{asset('images/CONTAPP.ico')}}' rel='shortcut icon' type='image/x-icon'>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>Contapp</title>
        <link href="{{ asset("css/jquery-ui/jquery-ui.min.css") }}" rel="stylesheet">

        <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">

        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">

        <link href="{{ asset("css/bootstrap-datepicker/bootstrap-datepicker3.min.css") }}" rel="stylesheet">

        @stack('stylesheets')

        <!-- Custom Theme Style -->
        <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

        @stack('customstylesheets')

        <link href="{{ asset("css/personal.css?ac=".str_random(2)) }}" rel="stylesheet">

        @role('backuser')
        <style>
            body.nav-md ul.nav.child_menu li:before {
                background: #ff6b00;
            }

            body.nav-md ul.nav.child_menu li:after {
                border-left: 1px solid #ff6b00;
            }

            .left_col {
                background: #33b6a4;
            }

            .nav-sm ul.nav.child_menu {
                background: #33b6a4;
            }

            body {
                color: #ff6b00;
                background: #33b6a4;

            }

            .nav_title {

                /*background: #33b6a5 */
                color: #ff6b00;
                text-shadow: rgba(0, 0, 0, 0.25) 0 -1px 0;
                background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #5b6479), color-stop(100%, #4c5566)), #686e78;
                background: -webkit-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: -moz-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: -o-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: linear-gradient(#ff6b00, #eb6a00), #e06800;
                -webkit-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                -moz-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
            }

            .active a span.fa {
                color: #33b6a4;
            }

            .nav.side-menu> li.active > a {
                color: #33b6a4;
                text-shadow: rgba(0, 0, 0, 0.25) 0 -1px 0;
                background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #5b6479), color-stop(100%, #4c5566)), #686e78;
                background: -webkit-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: -moz-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: -o-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: linear-gradient(#ff6b00, #eb6a00), #e06800;
                -webkit-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                -moz-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
            }

            .btn-seller {
                color: #ff6b00;
                text-shadow: rgba(0, 0, 0, 0.25) 0 -1px 0;
                background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #5b6479), color-stop(100%, #4c5566)), #686e78;
                background: -webkit-linear-gradient(#33b6a4, #339583), #33806e;
                background: -moz-linear-gradient(#33b6a4, #339583), #33806e;
                background: -o-linear-gradient(#33b6a4, #339583), #33806e;
                background: linear-gradient(#33b6a4, #339583), #33806e;
                -webkit-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                -moz-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
            }

            .nav.side-menu>li.active, .nav.side-menu>li.current-page {
                border-right: 5px solid #ff6b00;
            }
            .nav li li.current-page a, .nav.child_menu li li a.active, .nav.child_menu li li a:hover{
                text-shadow: rgba(0, 0, 0, 0.25) 0 -1px 0;
                background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #5b6479), color-stop(100%, #4c5566)), #686e78;
                background: -webkit-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: -moz-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: -o-linear-gradient(#ff6b00, #eb6a00), #e06800;
                background: linear-gradient(#ff6b00, #eb6a00), #e06800;
                -webkit-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                -moz-box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
                box-shadow: rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0;
            }

            .nav.child_menu>li>a, .nav.side-menu>li>a {
                color: #ffffff;
            }
        </style>
        @endrole

        @role('user')
        @include('scriptsdefault.zendesk')
        @include('scriptsdefault.analitics')
        @include('scriptsdefault.adwords')
        @endrole
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')

                @yield('main_container')

                @include('includes/footer')

                @include('includes/plantillasdialogos')

            </div>
        </div>
        @role('user')
            @include('modals.pasosRegistro')
            @include('modals.upgrade')
        @endrole
        @include('modals.loading')
        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>

        <script src="{{ asset("js/bootstrap.min.js") }}"></script>

        <script src="{{ asset("js/jquery-ui/jquery-ui.min.js") }}"></script>

        <!-- Bootstrap -->
        <script src="{{ asset("js/jsrender/jsrender.min.js") }}"></script>

        <script src="{{ asset("js/moment/moment.min.js") }}"></script>

        <script src="{{ asset("js/bootstrap-datepicker/bootstrap-datepicker.min.js") }}"></script>

        <script src="{{ asset("js/jquery.payment/lib/jquery.payment.js") }}"></script>

        @stack('libraries')

        @stack('scriptsgentelella')

        <!-- Custom Theme Scripts -->
        <script src="{{ asset("js/gentelella.js") }}"></script>

        <script src="{{ asset("js/personal.js?ac=".str_random(2)) }}"></script>

        <script>
        $( document ).ready(function() {
            $('.notified').click(function(){
                id=$(this).attr('id');
                $.ajax({
                    url: "{{url('/api/notificacion')}}",
                    type: "POST",
                    data:{'nota':id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    datatype: "application/json",
                    beforeSend: function () {
                    }
                }).always(function (json, textStatus, errorThrown) {
                    if (json != undefined) {
                        if (json.responseJSON != undefined){

                        }else{
                            $(location).attr('href',json.url);
                        }
                    }
                });
            });
        });
        </script>
        @stack('scriptspersonal')


    </body>
</html>
