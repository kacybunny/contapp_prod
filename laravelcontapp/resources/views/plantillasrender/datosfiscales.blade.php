

<script id="tmpl_obligacionescombo" type="text/x-jsrender">
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">

            <div>
                <select required="" class="form-control selectpickobl" data-live-search="true"  id="obligacion" name="obligaciones[]">
                    {%for datos%}
    <option value="{%:id%}" > {%:desc%}</option>
{%/for%}


                </select>
            </div>
        </div>
        <div class="form-group col-xs-5">
                <div class="input-group date datepickobl">
                    <input type="text" name="fechainiobl[]" class="form-control">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th">
                        </i>
                    </span>
                </div>
        </div>
        <div class="form-group col-xs-2">

            <div>
                <button class="btn btn-danger eliminar" type="button"><i class="fa fa-minus-circle" ></i></button>
            </div>
        </div>
    </div>
</script>

<script id="tmpl_atividadescombo" type="text/x-jsrender">
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">

            <div>
                <select required="" class="form-control selectpickact" data-live-search="true"  id="actividad" name="actividades[]">
                                     {%for datos%}
    <option value="{%:id%}" > {%:Descripcion%}</option>
{%/for%}
                </select>
            </div>
        </div>
        <div class="form-group col-xs-5">


                <div class="input-group date datepickact">
                    <input type="text" name="fechainiact[]" class="form-control">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th">
                        </i>
                    </span>
                </div>

        </div>
        <div class="form-group col-xs-2">

            <div>
                <button class="btn btn-danger eliminar" type="button"><i class="fa fa-minus-circle" ></i></button>
            </div>
        </div>
    </div>
</script>

<script id="tmpl_regimencombo" type="text/x-jsrender">
    <div class="row col-xs-12">
        <div class="form-group col-xs-5">

            <div>
                <select required="" class="form-control selectpickreg" data-live-search="true"  id="regimen" name="regimen[]">
                                        {%for datos%}
    <option value="{%:id%}" > {%:Descripcion%}</option>
{%/for%}
                </select>
            </div>
        </div>
        <div class="form-group col-xs-5">

                <div class="input-group date datepickreg">
                    <input type="text" name="fechainireg[]" class="form-control">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th">
                        </i>
                    </span>
                </div>
        </div>
        <div class="form-group col-xs-2">

            <div>
                <button class="btn btn-danger eliminar" type="button"><i class="fa fa-minus-circle" ></i></button>
            </div>
        </div>
    </div>
</script>

