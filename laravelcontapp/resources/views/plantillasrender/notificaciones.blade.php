
<!-- price element -->
<script id="tmpl_notify" type="text/x-jsrender">
<li {%if read_at %} class="messagenot read" {%else%} class="messagenot unread" {%/if %} id="{%: id %}" >
    <a href="{%: data.url %}">
        <span class="image">
            <img style="width:10%;" src="{{ config('app.logo')}}" alt="img">
        </span>
        <span>
            <span class="time list">
                {%dateformat: created_at %}
            </span>
            <span>
                {%: data.title %}
            </span>
        </span>
        <span class="message">
            {%: data.data %}
        </span>
    </a>
</li>
</script>
