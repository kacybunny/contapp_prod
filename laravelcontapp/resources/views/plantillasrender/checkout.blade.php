
                    <!-- price element -->
<script id="tmpl_plazoPago" type="text/x-jsrender">
<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="pricing" id="plazo_{%: num %}">
        <div class="title">
             <br/>
             <h2>{%: titulo %}</h2>
             <br/>
        </div>
        <div class="x_content">
            <div class="">
                <div class="pricing_features">
                    <center>
                     <br/>
                    <h1 style="font-size: 25px; display: inline;" >{%MonedaMX: precio %} </h1><small style="font-size: 15px;">/ MES</small>
                        <h4>
                        <br/>
                            {%: mensaje %}
                        </h4>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
</script>

<script id="tmpl_planactual" type="text/x-jsrender">

<center>
    <h4>Actualmente ya tienes contratado el plan {%: plan %}</h4>
    <br/>
    <h4>Sin embargo, todavía faltan algunos datos fiscales en tu registro. <br/></h4>
    <br/>
    <button type="button" class="btn btn-gray btn-lg" data-dismiss="modal">Elegir otro plan</button>
    <a href="{{url('datospersona/0/edit')}}" type="button" class="btn btn-success btn-lg" id="completa">Completar datos fiscales</a>
</center>

</script>

<script id="tmpl_resumen" type="text/x-jsrender">
<br/>
<br/>
<table class="table">
    <tbody>
    <tr>
        <th style="width:50%">Modalidad:</th>
        <td>{%: modalidad %}</td>
    </tr>
    <tr>
        <th style="width:50%">Plan:</th>
        <td>{%: plan %}</td>
    </tr>
    <tr>
        <th style="width:50%">Subtotal:</th>
        <td>{%MonedaMX: subtotal %}</td>
    </tr>
    <tr>
        <th>IVA {%: iva %}</th>
        <td>{%MonedaMX: precioiva %}</td>
    </tr>
    <tr>
        <th>Total:</th>
        <td>{%MonedaMX: total %}</td>
    </tr>
    </tbody>
</table>

</script>

<script id="tmpl_planes" type="text/x-jsrender">
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="pricing">
        <div class="title">
            <h2></h2>
            <h2>{%: plan %}</h2>
            {%if !precio %}
            <h1 style="color:#fcd272" >Gratis</h1>
            <h2 style="text-transform: lowercase; font-size: 18px;">de por vida</h2>
            {%else %}
            <h1  style="color:#fcd272"  >{%MonedaMX: precio %}</h1>
            <h2 style=" font-size: 18px;">MXN Mensuales</h2>
            {%/if%}
        </div>
        <div class="x_content">
            <div class="">
                <div class="pricing_features">
                    <ul class="list-unstyled text-center">
                        <li><strong>{%: titulo %}</strong></li>
                        <br/>
                        <br/>
                        <li>{%: mensaje %}</li>
                        <br/>
                    </ul>
                    <p>
                        <a href="{%: url %}" target="_blank">Detalle del plan</a>
                    </p>
                </div>
            </div>
            <div class="pricing_footer">

                <a href="javascript:void(0);" class="btn btn-success btn-block btn-seller" id="{%: plan %}" role="button">Elegir </a>

            </div>
        </div>
    </div>
</div>
</script>

{{--var html = $("#tmpl_Showplanesconta1").render(aPlanesPrecios);--}}
{{--$("#planesprecios").html(html);--}}
