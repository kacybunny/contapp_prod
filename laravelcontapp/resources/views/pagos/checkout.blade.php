@extends('pagos.pagos')

@section('title_section')
    <h2 id="titulosub">Selecciona el que mejor te convenga<small></small></h2>
@endsection

@section('contentx')
    <br/>
<style>
    #cambiaplan {
        border-radius: 3px;
        margin-top: 0px;
        background-color: #eb6a00;
    }
    #planselect.h1, .h2, .h3, h1, h2, h3 {
        margin-top: 0;
        margin-bottom: 0;
    }
    #cambiaplan:hover {
        border-radius: 3px;
        background-color: #34b6a6;
    }
    .pricing_features {
        background: #ecf0f1;
         padding: 10px 7.5px;
         min-height: 100%;
        font-size: 13.5px;
    }
    .pricing .title {
        background: #1ABB9C;
        height: 100%;
        color: #fff;
        padding: 5px 0 0;
        text-align: center;
    }
    .pricing .title h1 {
        font-size: 30px;
        margin: 5px;
    }
    .form-group {
        margin-bottom: 0;
        margin-top: -5px;
    }
    .lead {
        margin-bottom: 5px;
    }
    .x_content {

         margin-top: 0px;
    }
    .row {
        margin-top: -2%;
    }
    .pricing .title h2 {
        text-transform: none;
    }
</style>
    <div id="step-1">
        <section class="content invoice">
            <!-- Table row -->
            <div class="row">
                <div class="col-xs-offset-4 col-xs-4">
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" id="myBar" data-transitiongoal="30" aria-valuenow="30" style="width: 30%;">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 table" id="planesprecios" >

                    {{--<!-- price element -->--}}
                    {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                    {{--<div class="pricing">--}}
                    {{--<div class="title">--}}
                    {{--<h2></h2>--}}
                    {{--<h1>Medio</h1>--}}
                    {{--<h2>$ 300.00</h2>--}}

                    {{--<span>Al mes</span>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}
                    {{--<div class="">--}}
                    {{--<div class="pricing_features">--}}
                    {{--<ul class="list-unstyled text-center">--}}
                    {{--<li><strong>Hacemos la parte pesada, tú sólo revisas y declaras</strong></li>--}}
                    {{--<br/>--}}
                    {{--<br/>--}}
                    {{--<li>Conseguimos tus facturas por ti, te decimos que puedes deducir y te damos recomendaciones para que ahorres en impuestos</li>--}}
                    {{--<br/>--}}
                    {{--</ul>--}}
                    {{--<p>--}}
                    {{--<a href="javascript:void(0);">Detalle del plan</a>--}}
                    {{--</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="pricing_footer">--}}

                    {{--<a href="javascript:void(0);" class="btn btn-success btn-block btn-seller " id="medio" role="button">Download <span> now!</span></a>--}}

                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- price element -->--}}

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
    </div>
    <div id="step-2" style="display: none">
        <section class="content invoice">
            <!-- Table row -->
            <div class="row">
                <div class="col-xs-offset-4 col-xs-4">
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" id="plazos" data-transitiongoal="30" aria-valuenow="30" style="width: 30%;">
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-xs-6">
                            <h1 id="planselect" ></h1>
                    </div>
                    <div class="col-xs-6">
                        <button class="btn btn-info" id="cambiaplan"><i class="fa fa-mail-reply"></i> Cambiar Plan</button>
                    </div>

                </div>
                <br/>
                <div class="col-xs-12 table" id="plazosR">

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
                    <p class="lead">Forma de pago</p>
                    <img src="{{asset('image/cards/visa.png')}}" style="display: none" id="visaimg" alt="Visa">
                    <img src="{{asset('image/cards/mastercard.png')}}" style="display: none" id="masterimg" alt="Mastercard">
                    <img src="{{asset('image/cards/american-express.png')}}" style="display: none" id="americanimg" alt="American Express">
                    {{--<img src="{{asset('image/cards/paypal.png')}}" alt="Paypal">--}}
                    {!! BootForm::vertical(['url' => url('/pagos'), 'method' => 'post','id'=>'card-form']) !!}
                    <div class="row" >

                        <div class="col-xs-12" >
                            {!! BootForm::text('titular', ['html' => ' '],null, ['placeholder' => 'Nombre del titular','required','data-conekta'=>'card[name]']) !!}
                        </div>
                        <div class="col-xs-12" >
                            {!! BootForm::text('tarjeta', ['html' => ' '],null, ['placeholder' => 'Numero de tarjeta','required','data-conekta'=>'card[number]']) !!}
                        </div>
                        <div class="col-xs-5" >
                            {!! BootForm::text('mesano', ['html' => ' '],null, ['placeholder' => 'Mes/Año','required','data-conekta'=>'card[expcard]']) !!}
                        </div>
                        <div class="col-xs-5" >
                            {!! BootForm::text('ccv', ['html' => ' '],null, ['placeholder' => 'CCV','required','data-conekta'=>'card[cvc]']) !!}
                        </div>
                    </div>
                    <div class="form-group " style="    margin-bottom: 0; margin-top: 5px;">
                        {{--<label for="terminos"  class="control-label">--}}
                            {{--Guardar tus datos--}}
                        {{--</label>--}}
                        <div>
                            <div class="checkbox">
                                <label style="    padding-left: 0; ">
                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 0; margin-bottom: 0px;">
                                        <input name="datopago" id="datopago" type="checkbox" value="datopago" style="position: inherit; margin-left: 0;">
                                        Deseas que guardemos tus datos de pago
                                    </p>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group " style="    margin-bottom: 0; margin-top: 5px;">
                        <label for="terminos"  class="control-label">
                            Términos y condiciones
                        </label>
                        <div>
                            <div class="checkbox">
                                <label style="    padding-left: 0; ">
                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 0; margin-bottom: 0px;">
                                        <input name="terminos" id="terminos" type="checkbox" value="terminos" style="position: inherit; margin-left: 0;">
                                        He leído, y acepto totalmente, los términos y condiciones así como la política de cancelación del servicio ContApp que estoy contratando
                                        <br/>
                                        Al realizar mi pago estoy aceptando que se realice el cargo automático a mi tarjeta con la periodicidad seleccionada
                                    </p>
                                </label>
                            </div>
                        </div>
                    </div>

                    {!! BootForm::close() !!}
                </div>
                <!-- /.col -->
                <div class="col-xs-6" id="printcot">
                    <p class="lead">Resumen compra</p>
                    <div class="table-responsive" id="resumenComp">

                    </div>


                    <center>
                        <button class="btn btn-success btn-lg " id="pagar"><i class="fa fa-credit-card"></i> Realizar pago</button>
                    </center>
                    <div id="notificacionguia">

                    </div>
                </div>

                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    {{--<button class="btn btn-default" onclick="$('#printcot').printArea()"><i class="fa fa-print"></i> Imprimirr</button>--}}
                    <button class="btn btn-info pull-right" id="regresa"><i class="fa fa-mail-reply"></i> Regresar</button>
                    {{--<button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>--}}
                </div>
            </div>
        </section>
    </div>
    <div id="step-3" style="display: none">
        <section class="content invoice">
            <div class="row">

                <div class="col-xs-12" id="printcot">
                    <p class="lead">Talón de suscripción </p>
                    <div class="table-responsive" id="talonsuscrip">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width:50%">Suscripción:</th>
                                <td id="suscripcion_tal"></td>
                            </tr>
                            <tr>
                                <th style="width:50%">Plan:</th>
                                <td id="plan_tal"></td>
                            </tr>
                            <tr>
                                <th style="width:50%">Modalidad:</th>
                                <td id="mod_tal"></td>
                            </tr>
                            <tr>
                                <th style="width:50%">Subtotal:</th>
                                <td id="sub_tal"></td>
                            </tr>
                            <tr>
                                <th>IVA 16% </th>
                                <td id="iva_tal"></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td id="tot_tal"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <p class="lead">Este es un comprobante de tu suscripición solamente, en unos momentos recibirás la confirmación de tu pago
                    </p>
                    <div id="notificacionguia">

                    </div>
                </div>
                <button class="btn btn-default" onclick="$('#talonsuscrip').printArea()"><i class="fa fa-print"></i> Imprimir</button>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- this row will not appear when printing -->
            <div class="row no-print">

            </div>
        </section>
    </div>

@endsection

@push('scriptspersonal')

@include('modals.mensaje_planes')

<script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        (function(){
            var compra={};
            var objetoajax= {!!  json_encode($planes)!!};
            @if (session('pago'))
                var pago ='{{ session('pago') }}';
            @else
                var pago =undefined;
            @endif
            $('#mesano').payment('formatCardExpiry');
            $('#ccv').payment('formatCardCVC');
            $('#tarjeta').payment('formatCardNumber');
            errorTreal('card-form');
            $('#pagar').click(function () {
                pagar();
            });
            var html = $("#tmpl_planes").render(objetoajax);
            $("#planesprecios").html(html);
            progresbar('myBar',30,40,80);
            $.each(objetoajax, function(i,item){
                $('#'+objetoajax[i].plan).click(function(){
                    plan(objetoajax[i]);
                    $('#titulosub').text('Realizar pago');
                    progresbar('plazos',40,50,150);

                });
                if (pago!=undefined){
                    if (pago == objetoajax[i].plan){
                        plan(objetoajax[i]);
                        $('#titulosub').text('Realizar pago');
                        progresbar('plazos',40,50,150);
                    }
                }
            });
            $("#regresa").click(function () {
                regresarpaso ();
            });
            $("#cambiaplan").click(function () {
                regresarpaso ();
            });
            function regresarpaso (){
                $("#step-2").hide('slow');
                $("#step-1").show('slow');
                $('#titulosub').text('Selecciona el que mejor te convenga');
                progresbar('myBar',30,40,150);
            }
            function plan(elemento){
                ajax={};
                url='{{ url('api/miplan') }}';
                ajax.success=function (json) {
                    if (json != undefined){
                        if (json.plan==elemento.plan){
                            var html = $("#tmpl_planactual").render(elemento);
                            $('#planesalerta').html(html);
                            $('#planesmensaje').modal();
                        }else {

                            if(json.plan=='Básico' && elemento.plan!='Básico' ){
                                muestraplazos(elemento);
                            }else if (elemento.plan!='Básico' && json.plan!='Completo' ){
                                $('#planesalerta').html('<h2 style="font-size: 17px;" >Una vez confirmado el cambio, se sustituirá tu plan MEDIO por el plan COMPLETO.<br/> Tu nueva fecha de cobro sería el día '+moment().format('DD')+' de cada mes. <br/><br/> Aunque te pediremos tus datos de pago a continuación, <br/> el primer cobro te lo haremos hasta dentro de 30 días! <br/> De todas maneras, ya podrás disfrutar de todos los beneficios desde ahora. <br/><br/><br/> ¿Deseas continuar con tu Upgrade? </h2><br/><button type="button" class="btn btn-gray btn-lg regresaplan" >Cancelar</button><button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Aceptar</button>');
                                $('.regresaplan').click(function () {
                                    regresarpaso ();
                                    $('#planesmensaje').modal('hide');
                                });
                                $('#planesmensaje').modal();
                                muestraplazos(elemento);
                            }else{
                                $('#planesalerta').html('<h2> Para bajar a un plan menor, debes contactarnos enviando un correo a {{'soporte@contapp.mx'}}</h2>');
                                $('#planesmensaje').modal();
                            }
                        }
                    }
                };
                ajax.url=url;
                peticionAJAX(ajax);
            }
            function muestraplazos(plazo){
                compra.plan=plazo.plan;
                compra.modalidad=plazo.plazo[0].modalidad;
                compra.idplan=plazo.plazo[0].num;
                compra.subtotal=plazo.plazo[0].subtotal;
                compra.iva=plazo.plazo[0].iva;
                compra.precioiva=plazo.plazo[0].precioiva;
                compra.total=plazo.plazo[0].total;
                $('#planselect').text('Plan seleccionado: '+compra.plan);
                var html = $("#tmpl_plazoPago").render(plazo.plazo);
                $("#plazosR").html('<h3>Selecciona tu modalidad de pago</h3>'+html);
                var html = $("#tmpl_resumen").render(compra);
                $("#resumenComp").html(html);
                $("#step-1").hide('slow');
                $("#step-2").show('slow');
                restringirform('card-form');
                cargaplazo(plazo.plazo);
            }

            function cargaplazo(plazo) {
                $.each(plazo, function(i,item){
                    $('#plazo_'+plazo[i].num).click(function(){
                        compra.idplan=plazo[i].num;
                        compra.modalidad=plazo[i].modalidad;
                        compra.subtotal=plazo[i].subtotal;
                        compra.iva=plazo[i].iva;
                        compra.precioiva=plazo[i].precioiva;
                        compra.total=plazo[i].total;
                        var html = $("#tmpl_resumen").render(compra);
                        $("#resumenComp").html(html);
                    });
                });
            }
            function pagar () {
                Conekta.setPublicKey('key_XgjuSsJmTU8xLeHXDMsEQMw');
                Conekta.setLanguage("es");
                var conektaSuccessResponseHandler = function(token) {
                    console.log('envia:'+token.id);
                    var $form = $("#card-form");
                    //Inserta el token_id en la forma para que se envíe al servidor
                    $form.append($('<input type="hidden" id="planid" name="planid"> ').val(compra.idplan));
                    $form.append($('<input type="hidden" id="conektaTokenId" name="conektaTokenId"> ').val(token.id));
                    ajaxpago={};
                    ajaxpago.url=$('#card-form').attr('action');
                    ajaxpago.type=$('#card-form').attr('method');
                    ajaxpago.data=$('#card-form').serialize();
                    ajaxpago.success = function (json) {
                        if (json.resp != undefined ){
                            $('#stepwait').modal('hide');
                            $('#titulosub').text('Paquetes y Servicios');
                            $('#plan_tal').text(compra.plan);
                            $('#mod_tal').text(compra.modalidad);
                            $('#sub_tal').text(mascaraMoneda(compra.subtotal, '$'));
                            $('#tot_tal').text(mascaraMoneda(compra.total, '$'));
                            $('#iva_tal').text(mascaraMoneda(compra.precioiva, '$'));
                            $('#suscripcion_tal').text(json.suscripcion);
                            $("#step-2").hide('slow');
                            $("#step-3").show('slow');
                        }else{
                            $('#stepwait').modal('hide');
                        }
                    };
                    ajaxpago.error = function (json) {
                        $('#stepwait').modal('hide');
                    };
                    peticionAJAX(ajaxpago);
//                    Hace submit
//                    $form.get(0).submit();
                };
                var conektaErrorResponseHandler = function(response) {
                    $('#stepwait').modal('hide');
//                    $('.ui-pnotify-move').attr('style',"display: none; width: 300px; top: 160px; cursor: auto;");
                    $form.find("button").prop("disabled", false);

                };

                var $form = $('#card-form');
                // Previene hacer submit más de una vez
                $form.find("button").prop("disabled", true);

                if(validarfrm('card-form')){
                    if ($('#terminos').is(':checked')){
                        terminoserror={
                            'resp':true,
                            'campo':'terminos',
                            'mensaje': ''
                        };
                        agregaerrorchecktext(terminoserror);
                        $('#stepwait').modal();
                        tarjeta=$('#tarjeta').val();
                        nombre=$('#titular').val();
                        exp=$('#mesano').val();
                        expcard=$.payment.cardExpiryVal(exp);
                        cvc=$('#ccv').val();
                        var tokenParams = {
                            "card": {
                                "number": tarjeta,
                                "name": nombre,
                                "exp_month": expcard.month.toString(),
                                "exp_year": expcard.year.toString(),
                                "cvc": cvc
                            }
                        };
                        Conekta.Token.create(tokenParams, conektaSuccessResponseHandler, conektaErrorResponseHandler);

                    }else{
                        terminoserror={
                            'resp':false,
                            'campo':'terminos',
                            'mensaje': 'Debes aceptar terminos y condiciones'
                        };
                        agregaerrorchecktext(terminoserror);
                        return false;
                    }
                }else{

                    return false;
                }

            }
        }());
    });
</script>
@include('plantillasrender.checkout')
@endpush

