@extends('pagos.pagos')

@section('title_section')
    <h2>Listado<small>Pagos</small></h2>
@endsection

@section('contentx')
    <p class="text-muted font-13 m-b-30">

    </p>
    <table id="datatable-personal" class="table table-striped table-bordered">
    </table>
@endsection
@push('scriptspersonal')
<script type="text/javascript">
    $(document).ready(function() {
        configtable.data={!! json_encode($datos,JSON_UNESCAPED_UNICODE) !!};
        configtable.columns={!! json_encode($columnas,JSON_UNESCAPED_UNICODE) !!};
        function accionespage (){
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
            $('.detalle').off( "click").click(function () {
                datatabx=table.row( $(this).parent().parent() ).data();
                console.log(datatabx);
            });
        }
        init=function () {
            accionespage ()
        };
        tabladinamica('datatable-personal',init);

        table.on('dblclick', 'tr', function () {
        } ).on('draw.dt', function() {
            accionespage ()
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            accionespage ()
        } );
    });
</script>
@endpush