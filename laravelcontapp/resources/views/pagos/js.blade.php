
@push('libraries')

{{--<script src="{{ asset("js/fastclick/fastclick.js") }}"></script>--}}
<script src="{{ asset("js/fastclick/fastclick.js") }}"></script>

<script src="{{ asset("js/nprogress/nprogress.js") }}"></script>

<script src="{{ asset("js/progressbar/bootstrap-progressbar.min.js") }}"></script>

<script src="{{ asset("js/iCheck/icheck.min.js") }}"></script>

<script src="{{ asset("js/moment/moment.min.js") }}"></script>

<script src="{{ asset("js/daterangepicker/daterangepicker.js") }}"></script>

<script src="{{ asset("js/jquery.hotkeys/jquery.hotkeys.js") }}"></script>

<script src="{{ asset("js/google-code-prettify/prettify.js") }}"></script>

<script src="{{ asset("js/jquery.tagsinput/jquery.tagsinput.js") }}"></script>

<script src="{{ asset("js/switchery/switchery.min.js") }}"></script>

<script src="{{ asset("js/dropzone/dropzone.min.js") }}"></script>

<script src="{{ asset("js/jQuery-Smart-Wizard/jquery.smartWizard.js") }}"></script>

{{--<script src="{{ asset("js/select2/select2.full.min.js") }}"></script>--}}
<script src="{{ asset("js/bootstrap-select/bootstrap-select.min.js?ac=1") }}"></script>

<script src="{{ asset("js/autosize/autosize.min.js") }}"></script>

<script src="{{ asset("js/devbridge-autocomplete/jquery.autocomplete.min.js") }}"></script>

<script src="{{ asset("js/starrr/starrr.js") }}"></script>

<script src="{{ asset("js/datatable/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset("js/datatable/dataTables.bootstrap.min.js") }}"></script>
<script src="{{ asset("js/datatable/dataTables.buttons.min.js") }}"></script>
<script src="{{ asset("js/datatable/buttons.bootstrap.min.js") }}"></script>
<script src="{{ asset("js/datatable/buttons.flash.min.js") }}"></script>
<script src="{{ asset("js/datatable/buttons.html5.min.js") }}"></script>
<script src="{{ asset("js/datatable/buttons.print.min.js") }}"></script>
<script src="{{ asset("js/datatable/dataTables.fixedHeader.min.js") }}"></script>
<script src="{{ asset("js/datatable/dataTables.keyTable.min.js") }}"></script>
<script src="{{ asset("js/datatable/dataTables.responsive.min.js") }}"></script>
<script src="{{ asset("js/datatable/responsive.bootstrap.js") }}"></script>
<script src="{{ asset("js/datatable/dataTables.scroller.min.js") }}"></script>
<script src="{{ asset("js/datatable/jszip.min.js") }}"></script>
<script src="{{ asset("js/datatable/pdfmake.min.js") }}"></script>
<script src="{{ asset("js/datatable/vfs_fonts.js") }}"></script>
<script src="{{ asset("js/RitsC-PrintArea-2cc7234/jquery.PrintArea.js") }}"></script>

<script src="{{ asset("js/pnotify/pnotify.js") }}"></script>
<script src="{{ asset("js/pnotify/pnotify.buttons.js") }}"></script>
<script src="{{ asset("js/pnotify/pnotify.nonblock.js") }}"></script>



@endpush

@push('scriptsgentelella')
<script type="text/javascript">

</script>

@endpush

@push('scriptspersonal')

<script type="text/javascript">

</script>
@endpush