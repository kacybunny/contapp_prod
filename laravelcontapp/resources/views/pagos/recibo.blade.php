@extends('pagos.pagos')

@section('title_section')
    <h2 id="titulosub">Paquetes y Servicios<small></small></h2>
@endsection

@section('contentx')
    <br/>
    <div id="step-3">
        <section class="content invoice">
            <div class="row">

                <div class="col-xs-12" id="printcot">
                    <p class="lead">Talón de suscripción </p>
                    <div class="table-responsive" id="resumenComp">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width:50%">Suscripción:</th>
                                <td>{{$user->conekta_subscription}}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">Plan:</th>
                                <td>{{$user->plazo->plan->plan}}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">Modalidad:</th>
                                <td>{{$user->plazo->titulo}}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">Subtotal:</th>
                                <td>{{number_format((($user->plazo->porcentaje==0? 1 : $user->plazo->porcentaje)* $precio * 12 )/1.16 , 2, '.', '')}}</td>
                            </tr>
                            <tr>
                                <th>IVA 16% </th>
                                <td>{{number_format((($user->plazo->porcentaje==0? 1 : $user->plazo->porcentaje)* $precio * 12 )-((($user->plazo->porcentaje==0? 1 : $user->plazo->porcentaje)* $precio * 12 )/1.16) , 2, '.', '')}}</td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td>{{number_format(($user->plazo->porcentaje==0? 1 : $user->plazo->porcentaje) * $precio * 12, 2, '.', '')}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <p class="lead">Este es un comprobante de tu suscripición solamente, en unos momentos recibirás la confirmación de tu pago
                    </p>
                    <div id="notificacionguia">

                    </div>
                </div>
                <button class="btn btn-default" onclick="$('#resumenComp').printArea()"><i class="fa fa-print"></i> Imprimir</button>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- this row will not appear when printing -->
            <div class="row no-print">

            </div>
        </section>
    </div>

@endsection

@push('scriptspersonal')
<script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        (function(){

        }());
    });


</script>
@endpush

