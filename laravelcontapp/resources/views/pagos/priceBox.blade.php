@extends('pagos.pagos')

@section('title_section')
    <h2>Relizar pago<small></small></h2>
@endsection

@section('contentx')
    <br />
    <section class="content invoice">
        <!-- title row -->
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 invoice-header">--}}
                {{--<h1>--}}
                    {{--<i class="fa fa-globe"></i> Invoice.--}}
                    {{--<small class="pull-right">Date: 16/08/2016</small>--}}
                {{--</h1>--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
        {{--</div>--}}
        <!-- info row -->
        {{--<div class="row invoice-info">--}}
            {{--<div class="col-sm-4 invoice-col">--}}
                {{--From--}}
                {{--<address>--}}
                    {{--<strong>Iron Admin, Inc.</strong>--}}
                    {{--<br>795 Freedom Ave, Suite 600--}}
                    {{--<br>New York, CA 94107--}}
                    {{--<br>Phone: 1 (804) 123-9876--}}
                    {{--<br>Email: ironadmin.com--}}
                {{--</address>--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
            {{--<div class="col-sm-4 invoice-col">--}}
                {{--To--}}
                {{--<address>--}}
                    {{--<strong>John Doe</strong>--}}
                    {{--<br>795 Freedom Ave, Suite 600--}}
                    {{--<br>New York, CA 94107--}}
                    {{--<br>Phone: 1 (804) 123-9876--}}
                    {{--<br>Email: jon@ironadmin.com--}}
                {{--</address>--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
            {{--<div class="col-sm-4 invoice-col">--}}
                {{--<b>Invoice #007612</b>--}}
                {{--<br>--}}
                {{--<br>--}}
                {{--<b>Order ID:</b> 4F3S8J--}}
                {{--<br>--}}
                {{--<b>Payment Due:</b> 2/22/2014--}}
                {{--<br>--}}
                {{--<b>Account:</b> 968-34567--}}
            {{--</div>--}}
            {{--<!-- /.col -->--}}
        {{--</div>--}}
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table">
                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pricing">
                        <div class="title">
                            <h2>EN PAGO ANUAL</h2>
                            <h1>$500 / MES</h1>
                        </div>
                        <div class="x_content">
                            <div class="">
                                <div class="pricing_features">
                                    <ul class="list-unstyled text-left">
                                        <li><i class="fa fa-times text-danger"></i> 2 years access <strong> to all storage locations</strong></li>
                                        <li><i class="fa fa-times text-danger"></i> <strong>Unlimited</strong> storage</li>
                                        <li><i class="fa fa-check text-success"></i> Limited <strong> download quota</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Cash on Delivery</strong></li>
                                        <li><i class="fa fa-check text-success"></i> All time <strong> updates</strong></li>
                                        <li><i class="fa fa-times text-danger"></i> <strong>Unlimited</strong> access to all files</li>
                                        <li><i class="fa fa-times text-danger"></i> <strong>Allowed</strong> to be exclusing per sale</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing_footer">
                                <a href="javascript:void(0);" class="btn btn-success btn-block" role="button">Download <span> now!</span></a>
                                <p>
                                    <a href="javascript:void(0);">Sign up</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- price element -->

                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pricing ui-ribbon-container">
                        <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                                30% Off
                            </div>
                        </div>
                        <div class="title">
                            <h2>Tally Box Design</h2>
                            <h1>$25</h1>
                            <span>Monthly</span>
                        </div>
                        <div class="x_content">
                            <div class="">
                                <div class="pricing_features">
                                    <ul class="list-unstyled text-left">
                                        <li><i class="fa fa-check text-success"></i> 2 years access <strong> to all storage locations</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Unlimited</strong> storage</li>
                                        <li><i class="fa fa-check text-success"></i> Limited <strong> download quota</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Cash on Delivery</strong></li>
                                        <li><i class="fa fa-check text-success"></i> All time <strong> updates</strong></li>
                                        <li><i class="fa fa-times text-danger"></i> <strong>Unlimited</strong> access to all files</li>
                                        <li><i class="fa fa-times text-danger"></i> <strong>Allowed</strong> to be exclusing per sale</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing_footer">
                                <a href="javascript:void(0);" class="btn btn-primary btn-block" role="button">Download <span> now!</span></a>
                                <p>
                                    <a href="javascript:void(0);">Sign up</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- price element -->

                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pricing">
                        <div class="title">
                            <h2>Tally Box Design</h2>
                            <h1>$25</h1>
                            <span>Monthly</span>
                        </div>
                        <div class="x_content">
                            <div class="">
                                <div class="pricing_features">
                                    <ul class="list-unstyled text-left">
                                        <li><i class="fa fa-check text-success"></i> 2 years access <strong> to all storage locations</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Unlimited</strong> storage</li>
                                        <li><i class="fa fa-check text-success"></i> Limited <strong> download quota</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Cash on Delivery</strong></li>
                                        <li><i class="fa fa-check text-success"></i> All time <strong> updates</strong></li>
                                        <li><i class="fa fa-times text-danger"></i> <strong>Unlimited</strong> access to all files</li>
                                        <li><i class="fa fa-times text-danger"></i> <strong>Allowed</strong> to be exclusing per sale</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing_footer">
                                <a href="javascript:void(0);" class="btn btn-success btn-block" role="button">Download <span> now!</span></a>
                                <p>
                                    <a href="javascript:void(0);">Sign up</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- price element -->

                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="pricing">
                        <div class="title">
                            <h2>Tally Box Design</h2>
                            <h1>$25</h1>
                            <span>Monthly</span>
                        </div>
                        <div class="x_content">
                            <div class="">
                                <div class="pricing_features">
                                    <ul class="list-unstyled text-left">
                                        <li><i class="fa fa-check text-success"></i> 2 years access <strong> to all storage locations</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Unlimited</strong> storage</li>
                                        <li><i class="fa fa-check text-success"></i> Limited <strong> download quota</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Cash on Delivery</strong></li>
                                        <li><i class="fa fa-check text-success"></i> All time <strong> updates</strong></li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Unlimited</strong> access to all files</li>
                                        <li><i class="fa fa-check text-success"></i> <strong>Allowed</strong> to be exclusing per sale</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing_footer">
                                <a href="javascript:void(0);" class="btn btn-success btn-block" role="button">Download <span> now!</span></a>
                                <p>
                                    <a href="javascript:void(0);">Sign up</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- price element -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
@endsection