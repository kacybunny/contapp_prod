<div class="col-xs-offset-4 col-xs-4">
    <div class="progress">
        <div class="progress-bar progress-bar-warning" id="myBar" data-transitiongoal="60" aria-valuenow="60" style="width: 30%;">
        </div>
    </div>
</div>
{!! BootForm::open(['url' => url('datospersona/0'), 'method' => 'put','id'=>'ubicacion'] ) !!}
<div class="row">

    <div class="col-xs-12" >
    {!! BootForm::text('rfc', 'RFC', $persona->rfc , ['placeholder' => 'RFC']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('cp', 'Código postal', $cp, [] ) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('entidad', 'Entidad', null, ['disabled']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('municipio', 'Municipio', null, ['disabled'] ) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::select('colonia', 'Colonia',[],null, ['required','class'=>'selectpicker','data-live-search'=>'true']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('localidad', 'Localidad', $datospersona->localidad, [] ) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('vialidad', 'Vialidad', $datospersona->vialidad, [] ) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('num_int', 'Número interior', $datospersona->num_int, [] ) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('num_ext', 'Número exterior', $datospersona->num_ext, [] ) !!}
    </div>
    <div class="col-xs-12" >
        <center>
            <div class="row" >
                <div class="col-xs-12" >
                    <div class="col-xs-6" >
                        {!! BootForm::button('Cancelar',['onclick'=>'location.assign("'.url('/').'")','class'=>'btn btn-gray btn-lg']) !!}
                    </div>
                    <div class="col-xs-6"  >
                        {!! BootForm::submit('Continuar',['class'=>'btn btn-success btn-lg','id'=>'guardar']) !!}
                    </div>
                </div>
            </div>
        </center>
    </div>


</div>


{!! BootForm::close() !!}

@push('scriptspersonal')
    <script type="text/javascript">
        $(document).ready(function() {
            progresbar('myBar',50,70,80);
            if ($('#ubicacion').length) {
                cp=$('#cp').val();
                ubicacioncp (cp,'{{$idcp}}');
                code={};
                code.resp=true;
                code.campo='cp';
                agregaerror(code);
                $( "#cp" ).change(function() {
                    codigopostal(this);
                });
                $('#cp').keyup(function() {
                    if($(this).val().length >= 5) {
                        codigopostal(this);
                    }
                });
            }

            function limpiacombo(){
                $('#entidad').val('');
                $('#municipio').val('');
                apendcombo ('colonia',[],selectpicker=true);
            }

            function codigopostal(campo){
                item={};
                if($(campo).val().length < 5){
                    item.resp=false;
                    item.campo='cp';
                    item.mensaje='No es un codigo postal valido';
                    agregaerror(item);
                    limpiacombo();
                }else if ($(campo).val().length == 5 ){
                    cp=$(campo).val();
                    item.resp=true;
                    item.campo='cp';
                    agregaerror(item);
                    ubicacioncp (cp);
                }else if($(campo).val().length > 5 ){
                    item.resp=false;
                    item.campo='cp';
                    item.mensaje='No es un codigo postal valido';
                    agregaerror(item);
                    limpiacombo();
                }
            }

            function ubicacioncp (cp,selected=''){
                $.ajax({
                    url: "{{url('codepostal')}}/"+cp,
                    type: "GET",
                    datatype: "application/json",
                    beforeSend: function () {
                    }
                }).always(function (json, textStatus, errorThrown) {
                    if(json != undefined){
                        if(json.colonia){

                            if(Object.keys(json.colonia).length>1){
                                $('#entidad').val(json.colonia.d_estado);
                                $('#municipio').val(json.colonia.D_mnpio);
                                apendcombo ('colonia',json.colonia.d_asenta,selectpicker=true);
                                if(selected != ''){
                                    $('#colonia').selectpicker('val',selected);
                                }

                            }else{
                                item.resp=false;
                                item.campo='cp';
                                item.mensaje='No es un codigo postal valido';
                                agregaerror(item);
                                limpiacombo();
                            }
                        }
                    }
                });
            }


        });
    </script>
@endpush