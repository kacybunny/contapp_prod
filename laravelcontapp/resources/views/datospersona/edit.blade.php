@extends('datospersona.datospersona')

@section('title_section')
    <h2>Editar datos de ubicación<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include('datospersona.form')
@endsection