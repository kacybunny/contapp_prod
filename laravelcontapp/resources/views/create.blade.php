@extends('declaraciones.declaraciones')

@section('title_section')
    <h2>Agregar declaración<small></small></h2>
@endsection

@section('contentx')
    <br />
    @if (session('message'))
        <div class="alert alert-error">
            {{ session('message') }}
        </div>
    @endif
    {!! BootForm::open(['url' => url('import'), 'method' => 'post', 'id'=> 'facturadrop', 'class'=>'dropzone dz-clickable dz-started']) !!}


    {!! BootForm::close() !!}
@endsection
