@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush
@push('libraries')

<script src="{{ asset("js/raphael/raphael.min.js") }}"></script>
<script src="{{ asset("js/echarts/echarts.min.js") }}"></script>
<script src="{{ asset("js/morris/morris.min.js") }}"></script>

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Sección en construcción</h2>
                    {{--<ul class="nav navbar-right panel_toolbox">--}}
                    {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                    {{--</li>--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                    {{--<ul class="dropdown-menu" role="menu">--}}
                    {{--<li><a href="#">Settings 1</a>--}}
                    {{--</li>--}}
                    {{--<li><a href="#">Settings 2</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <img class="img-responsive" src="{{asset('image/construccion.jpg')}}">

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

