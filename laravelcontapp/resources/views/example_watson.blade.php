{!! BootForm::horizontal(['model' => $casillas, 'store' => 'CasillasController@store', 'update' => 'CasillasController@update']) !!}

{!! BootForm::text('Default Input', 'Default Input',null, ['placeholder' => 'Default Input','required']) !!}

{!! BootForm::text('Disabled Input', 'Disabled Input',null, ['placeholder' => 'Disabled Input','required','disabled'=>'disabled']) !!}

{!! BootForm::text('Read Only Input', 'Read Only Input',null, ['placeholder' => 'Read Only Input','required','readonly'=>'readonly']) !!}

{!! BootForm::textarea('Date Of Birth *', 'Date Of Birth *',null, ['placeholder' => 'rows="3"','required','rows'=>'3']) !!}

{!! BootForm::password('Password', 'Password',['placeholder' => 'Password','required']) !!}

{!! BootForm::password('Password', 'Password',['placeholder' => 'Password','required']) !!}

{!! BootForm::select('Select Multiple', 'Select Multiple', ['Choose option','Option one','Option two','Option three','Option four','Option five','Option six'],['Option two'],['class'=>'select2_multiple','multiple'=>'multiple']) !!}

{!! BootForm::text('tags_1', 'Input Tags','social, adverts, sales', ['required','class'=>'tags']) !!}

{!! BootForm::checkboxes('normal[]', ['html' => 'Checkboxes and radios<br><small class="text-navy">Normal Bootstrap elements</small>'], ['Option one. select more than one options','Option two. select more than one options'],null,false) !!}

{!! BootForm::radios('normal',['html' => '<br>'], ['option1' => 'Option one. only select one option','option2' => 'Option two. only select one option'],'option1', false) !!}

{!! BootForm::checkboxes('flat[]', ['html' => 'Checkboxes and radios<br><small class="text-navy">Normal Bootstrap elements</small>'], ['checked'=>' Checked','unchecked'=>' Unchecked'],['checked'],false,['class'=>'flat']) !!}

{!! BootForm::checkboxes('flat[]', ['html' => '<br>'], ['disabled'=>' Disabled','disabled & checked'=>' Disabled & checked'],['disabled & checked'],false,['class'=>'flat','disabled'=>'disabled']) !!}

{!! BootForm::radios('iCheck',['html' => '<br>'], ['checked'   => ' Checked','unchecked' => ' Unchecked'],'checked',false,['class'=>'flat']) !!}

{!! BootForm::radios('iCheck3',['html' => '<br>'], ['disabled'   => ' Disabled','disabled & Checked' => ' Disabled & Checked'],'disabled & Checked',false,['class'=>'flat','disabled'=>'disabled']) !!}

{!! BootForm::checkboxes('switch[]','Switch', ['checked'=>' Checked','unchecked'=>' Unchecked'],['checked'],false,['class'=>'js-switch']) !!}

{!! BootForm::checkboxes('flat[]', ['html' => '<br>'], ['disabled'=>' Disabled','disabled & checked'=>' Disabled & checked'],['disabled & checked'],false,['class'=>'js-switch','disabled'=>'disabled']) !!}

{!! BootForm::button('Cancel') !!}

{!! BootForm::button('Reset') !!}

{!! BootForm::submit('Submit',['class'=>'btn btn-success']) !!}

{{--{!! BootForm::text('tel', 'Phone', null, ['suffix' => BootForm::addonButton('Call', ['class' => 'btn-success'])] ) !!}--}}

{{--{!! BootForm::text('tel', 'Phone', null, ['prefix' => BootForm::addonButton('Call', ['class' => 'btn-success'])] ) !!}--}}

{{--{!! BootForm::text('direccion', 'Direccion',$casillas->direccion, ['suffix' => BootForm::addonText('1-'), 'prefix' => BootForm::addonIcon('phone'),'placeholder' => 'Direccion','required','class'=>'mono']) !!}--}}

{{--{!! BootForm::select('tipo', 'Tipo', $tipos,['especial']) !!}--}}

{{--{!! BootForm::select('tipo', 'Tipo', $tipos,['S'],['class'=>'select2_multiple','multiple'=>'multiple']) !!}--}}

{!! BootForm::close() !!}