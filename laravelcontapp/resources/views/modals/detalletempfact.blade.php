
<div class="modal fade bs-example-modal-lg" id="detalletempfactura" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                {{--<h4 class="modal-title" id="myModalLabel">Hola {{ Auth::user()->name }}</h4>--}}
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">
                        <h2>Receptor</h2>
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th scope="row">Nombre</th>
                                <td id="nomrecep">Mark</td>
                            </tr>
                            <tr>
                                <th scope="row">Rfc</th>
                                <td id="rfcrecept"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <h2>Emisor</h2>
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th scope="row">Nombre</th>
                                <td id="nomemi">Mark</td>
                            </tr>
                            <tr>
                                <th scope="row">Rfc</th>
                                <td id="rfcemi"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <h2></h2>
                        <table class="table table-striped">
                            <h2>Detalles</h2>
                            <tbody>
                                <tr>
                                    <th scope="row">Fecha</th>
                                    <td id="fechafact">Mark</td>
                                </tr>
                                <tr>
                                    <th scope="row">Tipo</th>
                                    <td id="tipofact"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <h2>Totales</h2>
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th scope="row">Subtotal</th>
                                <td id="subtotalfact">Mark</td>
                            </tr>
                            <tr>
                                <th scope="row">Impuestos</th>
                                <td id="impuestosfact"></td>
                            </tr>
                            <tr>
                                <th scope="row">Total</th>
                                <td id="totalfact"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12">
                        <center>
                        <h2>Conceptos</h2>
                        <table class="table table-striped">
                            <thead>
                                <tr id="headconceptos">

                                </tr>
                            </thead>
                            <tbody id="conceptostabla">

                            </tbody>
                        </table>
                        </center>
                    </div>
                </div>


                <br/>
                </center>
                    <center>
                        {{--<a href="{{url('/')}}" type="button" class="btn btn-success btn-lg">No</a>--}}
                        <button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Cerrar</button>
                    </center>
            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>

@push('scriptspersonal')
<script>
    $(document).ready(function() {
        @if(Session::has('tipo'))
                $('#cargacompleta').modal();
        @endif
    });
</script>
@endpush
