
<div class="modal fade bs-example-modal-lg" id="pasosreg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Bienvenido {{ Auth::user()->name }}</h4>
            </div>
            <div class="modal-body">
                <center>
                <h4>Todavía te faltan terminar algunos pasos de tu registro.</h4>
                </center>
                <br/>
                <style>
                    .timeline .tags {
                        top: 5px;
                    }
                    .list-unstyled{
                        width: 65%;
                    }
                </style>
                <center>
                    {{--<h3><span class="label label-default">Seleccionar Plan</span></h3>--}}
                    {{--<span class="glyphicon glyphicon-arrow-down" style="font-size: 2em" aria-hidden="true"></span>--}}
                    {{--<h3><span class="label label-primary">Realizar tu pago</span></h3>--}}
                    {{--<span class="glyphicon glyphicon-arrow-down" style="font-size: 2em" aria-hidden="true"></span>--}}
                    {{--<h3><span class="label label-primary">Información fiscal</span></h3>--}}
                    <ul class="list-unstyled timeline">
                        <li>
                            <div class="block">
                                <div class="tags">
                                    <ul class="tag {{pasosreg(0)}}">
                                        <span>1</span>
                                    </ul>
                                </div>
                                <div class="block_content">
                                    <h2 class="title">
                                        Seleccionar el plan
                                    </h2>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="block">
                                <div class="tags">
                                    <ul class="tag {{pasosreg(0)}}">
                                        <span>2</span>
                                    </ul>
                                </div>
                                <div class="block_content">
                                    <h2 class="title">
                                        Realizar el pago*
                                    </h2>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="block">
                                <div class="tags">
                                    <ul class="tag {{pasosreg(2)}}">
                                        <span>3</span>
                                    </ul>
                                </div>
                                <div class="block_content">
                                    <h2 class="title">
                                        Darnos información necesaria para calcular tus impuestos
                                    </h2>
                                </div>
                            </div>
                        </li>
                    </ul>
                </center>
                <br/>
                <center>
                <h4>Cuando completes todos los pasos, podrás aprovechar al 100% los beneficios de tu plan contratado</h4>
                <br/>
                </center>
                <center>
                    <button type="button" class="btn btn-gray btn-lg" data-dismiss="modal">Llenar más tarde</button>
                    <button type="button" class="btn btn-success btn-lg" id="completa">Completar datos</button>
                </center>
            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>

@push('scriptspersonal')
<script>
    $(document).ready(function() {
        @if(pasosreg(0)!='' || pasosreg(2)!='')
        @if(!Session::has('pasosreg'))
                $('#pasosreg').modal();
        @endif

        $('#pasosreg').on('hidden.bs.modal', function (e) {
            pasosReg();
        });
        $('#completa').click(function () {
            pagosmodal();
        });

        function pasosReg() {
            $.ajax({
                url: '{{url('vistomensaje')}}',
                type: 'get',
                success: function (response) {
                    $('#pasosreg').modal('hide');
                }
            });
        }
        function pagosmodal() {
            $.ajax({
                url: '{{url('vistomensaje')}}',
                type: 'get',
                success: function (response) {
                    $('#pasosreg').modal('hide');
                    location.assign("{{url(pasosreg(1))}}");
                }
            });
        }
        @endif
    });
</script>
@endpush
