
<div class="modal fade bs-example-modal-lg" id="upgrade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Hola {{ Auth::user()->name }}</h4>
            </div>
            <div class="modal-body">
                <center>
                <h4>Debes tener el paquete completo para Acceder a esta sección.</h4>
                    <br/>
                <center>
                <h4>Revisa los paquetes que tenemos para ti</h4>
                <br/>
                </center>
                <center>
                    <button type="button" class="btn btn-success btn-lg" data-dismiss="modal">En otro momento</button>
                    <a href="{{url('pagos/create')}}" type="button" class="btn btn-success btn-lg">Paquetes y servicios</a>
                </center>
            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>

@push('scriptspersonal')
<script>
    $(document).ready(function() {
        @if(Session::has('upgrade') && empty( \Illuminate\Support\Facades\Auth::user()->conekta_plan ))
                $('#upgrade').modal();
        @endif
    });
</script>
@endpush
