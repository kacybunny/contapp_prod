
<div class="modal fade bs-example-modal-lg" id="cargacompleta" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Hola {{ Auth::user()->name }}</h4>
            </div>
            <div class="modal-body">
                <center>
                    @if (session('message'))
                        <div class="alert alert-{{ session('tipo') }}">
                            {{ session('message') }}
                        </div>
                    @endif

                    <br/>
                <center>
                    <h4>Desesas cargar mas facturas</h4>
                <br/>
                </center>
                    <center>
                        <a href="{{url('/')}}" type="button" class="btn btn-success btn-lg">No</a>
                        <button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Si</button>
                    </center>
            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>

@push('scriptspersonal')
<script>
    $(document).ready(function() {
        @if(Session::has('tipo'))
                $('#cargacompleta').modal();
        @endif
    });
</script>
@endpush
