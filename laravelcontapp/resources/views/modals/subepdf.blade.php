
<div class="modal fade bs-example-modal-lg" id="subirarchivo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Hola {{ Auth::user()->name }}</h4>
            </div>
            <div class="modal-body">
                <center id="mensajearchivo">
                {{--<h4 ></h4>--}}
                </center>
                <center>
                    {!! BootForm::open(['url' => '#', 'id'=>'subepdf' , 'method' => $metodo,'files'=>true]) !!}
                    {!! BootForm::file('archivo', 'Archivo',['required']) !!}
                    {!! BootForm::submit('Subir Archivo',['class'=>'btn btn-info', 'id' => 'btn_pdf']) !!}
                    {!! BootForm::close() !!}

                <h4></h4>
                <br/>
                </center>
            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>
