
<div class="modal fade bs-example-modal-lg" id="autorizarmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Hola {{ Auth::user()->name }}</h4>
            </div>
            <div class="modal-body">
                <center id="mensajearchivo">
                <h4 > Autorizar Declaración Previa</h4>
                    <p>¿ Nos autorizas a presentar tu declaración con estos datos ?</p>
                </center>
                <center>
                    <br/>
                    <div class="col-xs-12" >
                        <div class="col-xs-6" >
                            {!! BootForm::button('No',['id'=>'modalno','class'=>'btn btn-gray btn-large']) !!}
                        </div>
                        <div class="col-xs-6"  >
                            {!! BootForm::button('Sí autorizo',['id'=>'modalsi','class'=>'btn btn-success btn-large']) !!}
                        </div>
                    </div>
                <h4></h4>
                <br/>
                </center>
            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>
