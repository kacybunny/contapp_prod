
<div class="modal fade bs-example-modal-lg" id="llenarfc" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Hola {{ Auth::user()->name }}</h4>
            </div>
            <div class="modal-body">
                <center>
                <h4>Para utilizar esta funcionalidad se necesita tu RFC</h4>
                <br/>
                <h4>Cuando completes este paso podras utilizar esta función</h4>
                <br/>
                    {!! BootForm::open(['url' => url('/persona'), 'id'=>'enviarfc' , 'method' => 'post']) !!}
                    {!! BootForm::text('rfc', 'RFC',null, ['placeholder' => 'RFC','required','pattern'=>'^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$','ref'=>'RFC','title'=>'Introduce un RFC valido',''=>'RFC']) !!}
                    {!! BootForm::close() !!}
                <h4></h4>
                <br/>
                </center>
                <center>
                    <button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="btn_rfc" class="btn btn-success btn-lg">Agregar</button>
                </center>
            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>
