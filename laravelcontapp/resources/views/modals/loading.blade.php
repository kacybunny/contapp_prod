<style>
    /*.modal-content {*/
        /*position: relative;*/
        /*background-color: rgba(255, 0, 0, 0.5);*/
        /*-webkit-background-clip: padding-box;*/
        /*background-clip: padding-box;*/
        /*border: 1px solid #999;*/
        /*border: 1px solid rgba(0,0,0,.2);*/
        /*border-radius: 6px;*/
        /*outline: 0;*/
        /*-webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);*/
        /*box-shadow: 0 3px 9px rgba(0,0,0,.5);*/
    /*}*/
    .modalcenter {
        text-align: center;
        padding: 0!important;
    }

    .modalcenter:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modalcenter-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }
    .loader {
        border-top: 16px solid #ff6b00;
        border-right: 16px solid #1abb9c;
        border-bottom: 16px solid #ff6b00;
        border-left: 16px solid #1abb9c;
    }
    .imageloadercenter{
        background-image: url('{{ asset('image/logoloader.png') }}');
        background-attachment: fixed;
        background-repeat:no-repeat;
        background-position: center center;
        background-size: 75px 65px;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<div class="modal modalcenter fade bs-example-modal-lg" id="stepwait" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false">
    <div class="modal-dialog modalcenter-dialog modal-lg">
        <div class="modal-content">

            {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>--}}
                {{--</button>--}}
                {{--<h4 class="modal-title" id="myModalLabel">Hola {{ Auth::user()->name }}</h4>--}}
            {{--</div>--}}
            <div class="modal-body">
                <center>
                    <h2 style="margin-top: 0px; margin-bottom: 0px;"> Espera un momento</h2>
                    <div style="" class="imageloadercenter" >
                        <div class="loader">

                        </div>
                    </div>
                </center>
            </div>
            {{--<div class="modal-footer">--}}

            {{--</div>--}}

        </div>
    </div>
</div>
