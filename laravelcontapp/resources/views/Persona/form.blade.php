{!! BootForm::open(['url' => url('persona/0'), 'files'=>true, 'method' => 'put','id'=>'ubicacion'] ) !!}
<div class="row">
    <div class="col-xs-6" >
        {!! BootForm::text('nombre', 'Nombre', $persona->nombre, ['required'] ) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('a_paterno', 'Apellido paterno', $persona->paterno, ['required']) !!}
    </div>
    <div class="col-xs-6" >
        {!! BootForm::text('a_materno', 'Apellido Materno', $persona->materno, ['required'] ) !!}
    </div>
    {{--<div class="col-xs-6" >--}}
        {{--{!! BootForm::text('edad', 'Edad',$persona->edad, ['required']) !!}--}}
    {{--</div>--}}
    <div class="col-xs-6" >
        {!! BootForm::file('foto', 'Foto') !!}
    </div>
    <div class="col-xs-12" >
        <div class="col-xs-2" >
            {!! BootForm::submit('Guardar',['class'=>'btn btn-success']) !!}
        </div>
        @role('user')
        <div class="col-xs-2"  >
            {!! BootForm::button('Continuar',['onclick'=>'location.assign("'.url('datospersona/0/edit').'")','class'=>'btn btn-info']) !!}
        </div>
        @endrole
    </div>
</div>


{!! BootForm::close() !!}

@push('scriptspersonal')
    <script type="text/javascript">

    </script>
@endpush