@extends('Persona.persona')

@section('title_section')
    <h2>Edita tu perfil<small></small></h2>
@endsection

@section('contentx')
    <br />
    @include('Persona.form')
@endsection